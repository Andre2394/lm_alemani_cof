package it.polimi.cof;

import it.polimi.cof.model.*;
import it.polimi.cof.model.exceptions.LogicException;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.model.market.MarketSession;
import it.polimi.cof.network.NetworkException;
import it.polimi.cof.network.NetworkType;
import it.polimi.cof.network.client.AbstractClient;
import it.polimi.cof.network.client.ClientConnectionException;
import it.polimi.cof.network.client.IClient;
import it.polimi.cof.network.client.rmi.RMIClient;
import it.polimi.cof.network.client.socket.SocketClient;
import it.polimi.cof.network.exceptions.CreateRoomException;
import it.polimi.cof.network.exceptions.InvalidConfigurationException;
import it.polimi.cof.network.exceptions.JoinRoomException;
import it.polimi.cof.network.exceptions.LoginException;
import it.polimi.cof.network.protocol.ErrorCodes;
import it.polimi.cof.ui.AbstractUi;
import it.polimi.cof.ui.UiController;
import it.polimi.cof.ui.UiType;
import it.polimi.cof.ui.cli.CommandLineUserInterface;
import it.polimi.cof.ui.gui.GraphicalUserInterface;
import it.polimi.cof.util.Debug;

import java.util.List;

/**
 * This class represent the base game class. It works like an internal bus for communication between the selected
 * UserInterface and the Client. {@link UiController UiController} and {@link IClient IClient} are used as callbacks.
 */
/*package-local*/ class MainGame implements UiController, IClient {

    /**
     * Abstract class that represent the selected ui.
     */
    private AbstractUi mUi;

    /**
     * Abstract class that represent the selected client.
     */
    private AbstractClient mClient;

    /**
     * Current player's nickname.
     */
    private String mNickname;

    /**
     * Model that represent the server session of the game.
     */
    private BaseGame mGameModel;

    /**
     * Model that represent the server session of the market.
     */
    private MarketSession mMarketSession;

    /**
     * The creation of this class is delegated to {@link Launcher Launcher}.
     * @param uiType NotNull type of the selected UserInterface.
     * @throws IllegalArgumentException if uiType is not valid.
     */
    /*package-local*/ MainGame(UiType uiType) {
        switch (uiType) {
            case CLI:
                mUi = new CommandLineUserInterface(this);
                break;
            case GUI:
                mUi = new GraphicalUserInterface(this);
                break;
            default:
                throw new IllegalArgumentException("UiType not recognized");
        }
    }

    /**
     * Open a client connection to the server. This method tell the ui to ask the user for network params and than will
     * try to establish a connection with it calling {@link #setNetworkSettings(NetworkType, String, int)}.
     */
    /*package-local*/ void start() {
        mUi.showNetworkSettingMenu();
    }

    /**
     * This method is triggered by {@link UiController UiController} from NetworkSettingMenu.
     * @param networkType desired network type.
     * @param address address of the server.
     * @param port valid port of the server.
     * @throws ClientConnectionException if client cannot establish a connection to the server.
     */
    @Override
    public void setNetworkSettings(NetworkType networkType, String address, int port) throws ClientConnectionException {
        switch (networkType) {
            case SOCKET:
                mClient = new SocketClient(this, address, port);
                break;
            case RMI:
                mClient = new RMIClient(this, address, port);
                break;
            default:
                throw new IllegalArgumentException("Network type not recognized");
        }
        mClient.connect();
        mClient.initializeConnection();
        Debug.debug("Client successfully connected to server");
        mUi.showLoginMenu();
    }

    /**
     * This method is triggered by {@link AbstractUi#showLoginMenu()}.
     * @param nickname to use for login session.
     */
    @Override
    public void loginPlayer(String nickname) {
        try {
            Debug.debug("Try to login user with nickname: " + nickname);
            mClient.loginPlayer(nickname);
            mNickname = nickname;
            joinFirstAvailableRoom();
        } catch (LoginException e) {
            Debug.debug("Nickname is already in use on server", e);
            mUi.showLoginErrorMessage();
        } catch (NetworkException e) {
            Debug.error("Cannot send login request", e);
        }
    }

    /**
     * This method is called by {@link #loginPlayer(String)} after a successfully login.
     * It send a request to server to join the player to the first available room. It will manage the case when
     * no available room is found on server, so it will prompt the user to create a new room.
     */
    private void joinFirstAvailableRoom() {
        try {
            mClient.joinFirstAvailableRoom();
            mUi.showJoinRoomSuccess();
        } catch (JoinRoomException e) {
            Debug.debug("No room found where join the player", e);
            mUi.showJoinRoomFailed();
        } catch (NetworkException e) {
            Debug.error("Cannot send join room request", e);
        }
    }

    /**
     * This method is triggered by {@link AbstractUi#showJoinRoomFailed()}.
     * @param maxPlayers is the limit to set internally the room.
     */
    @Override
    public void createRoom(int maxPlayers) {
        try {
            Debug.debug("Sending a request to create a new room with max players: " + maxPlayers);
            Configuration configurations = mClient.createNewRoom(maxPlayers);
            mUi.showCreatingRoomSuccess();
            mUi.showGameConfigurator(configurations);
        } catch (CreateRoomException e) {
            Debug.debug("Another room has been created in the meanwhile on the server and the current player has been added", e);
            mUi.showCreatingRoomFailed();
        } catch (NetworkException e) {
            Debug.error("Cannot send create room request", e);
        }
    }

    /**
     * Retrieve the nickname of the current player.
     * @return the nickname of the current player.
     */
    @Override
    public String getNickname() {
        return mNickname;
    }

    /**
     * Check if the provided nickname belongs to the current player.
     * @param nickname to check.
     * @return true if matches.
     */
    @Override
    public boolean isCurrentPlayer(String nickname) {
        return mNickname != null && mNickname.equals(nickname);
    }

    /**
     * Check if the provided update state belongs to the current player.
     * @param updateState to check.
     * @return true if belongs to him.
     */
    @Override
    public boolean isCurrentPlayer(UpdateState updateState) {
        return isCurrentPlayer(updateState.getNickname());
    }

    /**
     * This method is triggered by {@link AbstractUi#showGameConfigurator(Configuration)}.
     * @param configuration bundle that contains all admin choices.
     */
    @Override
    public void onGameConfigured(Configuration configuration) {
        Debug.verbose("Sending game configuration");
        try {
            mClient.applyGameConfiguration(configuration);
            mUi.showGameConfigurationDone();
        } catch (InvalidConfigurationException e) {
            Debug.debug("Invalid configuration file", e);
            mUi.showInvalidGameConfiguration(e.getMessage());
        } catch (NetworkException e) {
            Debug.error("Cannot send game configuration request", e);
        }
    }

    /**
     * Notify game is started and dispatch initial game state.
     * @param baseGame snapshot of the initial state of the game on server.
     */
    @Override
    public void onGameStarted(BaseGame baseGame) {
        Debug.debug("Game is started!");
        mGameModel = baseGame;
        mUi.notifyGameStarted();
    }

    /**
     * MVC pattern: this method will return to {@link AbstractUi} the current state of the model.
     * @return the updated model that represent the game session on the client.
     */
    @Override
    public BaseGame getGameModel() {
        return mGameModel;
    }

    /**
     * Retrieve the current market session stored into internal bus.
     * @return the internal market session if the game is in a market session, null otherwise.
     */
    @Override
    public MarketSession getMarketSession() {
        return mMarketSession;
    }

    /**
     * Ui callback used to notify the bus to draw a politic card.
     */
    @Override
    public void drawPoliticCard() {
        try {
            mClient.drawPoliticCard();
        } catch (NetworkException e) {
            Debug.error("Cannot send draw politic card request", e);
        }
    }

    /**
     * Notify player turn is started.
     * @param nickname of the player that should start his turn.
     */
    @Override
    public void onTurnStarted(String nickname, int remainingTime) {
        mUi.showGameTurnStarted(nickname, remainingTime);
    }

    /**
     * Notify internal bus that a new market session is started.
     */
    @Override
    public void onMarketSessionStarted() {
        mMarketSession = new MarketSession();
        mUi.showMarketSessionStarted();
    }

    /**
     * Notify internal bus that a new market sell turn is started.
     * @param nickname of the player that has started the turn.
     * @param remainingTime that the player has to make the actions.
     */
    @Override
    public void onMarketSellTurnStarted(String nickname, int remainingTime) {
        mUi.showMarketSellTurnStarted(nickname, remainingTime);
    }

    /**
     * Notify internal bus that a new market buy turn is started.
     * @param nickname of the player that has started the turn.
     * @param remainingTime that the player has to make the actions.
     */
    @Override
    public void onMarketBuyTurnStarted(String nickname, int remainingTime) {
        mUi.showMarketBuyTurnStarted(nickname, remainingTime);
    }

    /**
     * Notify internal bus that a the market session is finished.
     */
    @Override
    public void onMarketSessionFinished() {
        mUi.showMarketSessionFinished();
        mMarketSession = null;
    }

    /**
     * Notify internal bus that a new market item has been added to the market.
     * @param item added.
     */
    @Override
    public void onMarketItemAdded(Item item) {
        mMarketSession.applyUpdate(item);
        mUi.showMarketItemAddedOnSale(item);
    }

    /**
     * Notify internal bus that a market item has been sold.
     * @param marketId that has been sold.
     * @param buyer nickname of the player that has bought the item.
     */
    @Override
    public void onMarketItemBought(String marketId, String buyer) {
        try {
            mMarketSession.buyItem(mGameModel.getPlayer(buyer), marketId, mGameModel);
            mUi.showMarketItemBought(mMarketSession.getItem(marketId));
        } catch (LogicException e) {
            Debug.error("Something went wrong while updating local market model", e);
        }
    }

    /**
     * Notify ui that remaining turn's time is changed.
     * @param remainingTime updated remaining time in seconds. If 0 the turn is ended.
     */
    @Override
    public void onTurnUpdateCountdown(int remainingTime) {
        mUi.updateTurnRemainingTime(remainingTime);
    }

    /**
     * Apply silently an upcoming update state to the local game state.
     * @param updateState to apply.
     */
    private void applyModelUpdate(UpdateState updateState) {
        try {
            mGameModel.applyUpdate(updateState);
        } catch (LogicException e) {
            Debug.error("Something went wrong when updating local game model", e);
        }
    }

    /**
     * Look into update state and check if player has to earn a special reward.
     * @param updateState to look into.
     */
    private void checkForSpecialRewards(UpdateState updateState) {
        if (!updateState.isPlayer(mNickname)) {
            // skip check
            return;
        }
        NobilityReward nobilityReward = updateState.getNobilityReward();
        if (nobilityReward != null) {
            if (nobilityReward.getFirstSpecialBonusCount() > 0) {
                // check if the player has at least one emporium in a valid city
                List<City> cities = mGameModel.getCitiesWherePlayerHasBuilt(mNickname);
                int availableRewards = getAvailableCityFirstSpecialReward(cities);
                if (availableRewards > 0) {
                    mUi.showFirstSpecialRewards(Math.min(availableRewards, nobilityReward.getFirstSpecialBonusCount()));
                } else {
                    Debug.verbose("Player has received a first special reward from nobility track but he has no available city where he can retrieve a reward");
                }
            } else if (nobilityReward.getSecondSpecialBonusCount() > 0) {
                // no limit here, more than one permit tile can be drawn
                mUi.showSecondSpecialRewards(nobilityReward.getSecondSpecialBonusCount());
            } else if (nobilityReward.getThirdSpecialBonusCount() > 0) {
                // check if the player has at least one permit tile
                Player player = mGameModel.getPlayer(mNickname);
                int totalPermitTileCount = player.getBusinessPermitTiles().size() + player.getUsedBusinessPermitTiles().size();
                if (totalPermitTileCount > 0) {
                    mUi.showThirdSpecialRewards(Math.min(totalPermitTileCount, nobilityReward.getThirdSpecialBonusCount()));
                } else {
                    Debug.verbose("Player has receiver a third special reward from nobility track but he has no business permit tile from which take a reward");
                }
            }
        }
    }

    /**
     * Check how many cities are available for a first special rewards.
     * @param cities list of cities.
     * @return the number of available cities.
     */
    private int getAvailableCityFirstSpecialReward(List<City> cities) {
        int availableRewards = 0;
        for (City city : cities) {
            Reward reward = city.getReward();
            if (reward != null && reward.getNobilityStepCount() == 0) {
                availableRewards += 1;
            }
        }
        return availableRewards;
    }

    /**
     * Notify internal bus that a new politic card has been drawn.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onDrawnPoliticCard(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyPoliticCardDrawn(updateState);
    }

    /**
     * Ui callback used to notify the bus to answer the server for the player action list.
     */
    @Override
    public void getActionList() {
        try {
            mClient.getActionList();
        } catch (NetworkException e) {
            Debug.error("Cannot send action list request", e);
        }
    }

    /**
     * Notify internal bus that the action list is ready.
     * @param actionList from server to show to the user.
     */
    @Override
    public void onActionList(ActionList actionList) {
        mUi.showActionList(actionList);
    }

    /**
     * Ui callback used to end the player turn.
     */
    @Override
    public void endTurn() {
        try {
            mClient.endTurn();
        } catch (NetworkException e) {
            Debug.error("Cannot send end turn request", e);
        }
    }

    /**
     * Ui callback used to elect a councillor.
     * @param councilor to elect.
     * @param region where make the election.
     */
    @Override
    public void actionElectCouncillor(Councilor councilor, String region) {
        try {
            mClient.electCouncillor(councilor, region);
        } catch (NetworkException e) {
            Debug.error("Cannot send elect councillor request", e);
        }
    }

    /**
     * Ui callback used to acquire a business permit tile.
     * @param politicCards to use to satisfy the balcony.
     * @param region where satisfy the balcony.
     * @param permitTileIndex of the permit tile to take.
     */
    @Override
    public void actionAcquireBusinessPermitTile(List<PoliticCard> politicCards, String region, int permitTileIndex) {
        try {
            mClient.acquireBusinessPermitTile(politicCards, region, permitTileIndex);
        } catch (NetworkException e) {
            Debug.error("Cannot send acquire bpt request", e);
        }
    }

    /**
     * Ui callback used to build an emporium with a business permit tile.
     * @param businessPermitTile to use to build the emporium.
     * @param city where the emporium should be built.
     */
    @Override
    public void actionBuildEmporiumWithBusinessPermitTile(BusinessPermitTile businessPermitTile, String city) {
        try {
            mClient.buildEmporiumWithBusinessPermitTile(businessPermitTile, city);
        } catch (NetworkException e) {
            Debug.error("Cannot send build emporium with bpt request", e);
        }
    }

    /**
     * Ui callback used to build an emporium with the help of the king.
     * @param politicCards to use to satisfy the king's balcony.
     * @param cities where the king should move.
     */
    @Override
    public void actionBuildEmporiumWithKingHelp(List<PoliticCard> politicCards, List<String> cities) {
        try {
            mClient.buildEmporiumWithKingHelp(politicCards, cities);
        } catch (NetworkException e) {
            Debug.error("Cannot send build emporium with king help request", e);
        }
    }

    /**
     * Ui callback used to engage an assistant.
     */
    @Override
    public void actionEngageAssistant() {
        try {
            mClient.engageAssistant();
        } catch (NetworkException e) {
            Debug.error("Cannot send engage assistant request", e);
        }
    }

    /**
     * Ui callback used to change the visible permit tiles of the given region.
     * @param region where make the substitution.
     */
    @Override
    public void actionChangeBusinessPermitTile(String region) {
        try {
            mClient.changeBusinessPermitTiles(region);
        } catch (NetworkException e) {
            Debug.error("Cannot send change business permit tile request", e);
        }
    }

    /**
     * Ui callback used to send an assistant to elect a councillor.
     * @param councilor to elect.
     * @param region where the councillor should be elected.
     */
    @Override
    public void actionSendAssistantElectCouncillor(Councilor councilor, String region) {
        try {
            mClient.sendAssistantElectCouncillor(councilor, region);
        } catch (NetworkException e) {
            Debug.error("Cannot send elect councillor request", e);
        }
    }

    /**
     * Ui callback used to answer for an additional main action.
     */
    @Override
    public void actionPerformAdditionalMainAction() {
        try {
            mClient.performAdditionalMainAction();
        } catch (NetworkException e) {
            Debug.error("Cannot send perform additional main action request", e);
        }
    }

    /**
     * Ui callback used to sell a politic card on the market.
     * @param politicCard to sell.
     * @param price of the politic card.
     */
    @Override
    public void sellPoliticCard(PoliticCard politicCard, int price) {
        try {
            mClient.sellPoliticCard(politicCard, price);
        } catch (NetworkException e) {
            Debug.error("Cannot send sell politic card request", e);
        }
    }

    /**
     * Ui callback used to sell a business permit tile on the market.
     * @param businessPermitTile to sell.
     * @param price of the business permit tile.
     */
    @Override
    public void sellBusinessPermitTile(BusinessPermitTile businessPermitTile, int price) {
        try {
            mClient.sellBusinessPermitTile(businessPermitTile, price);
        } catch (NetworkException e) {
            Debug.error("Cannot send sell permit tile request", e);
        }
    }

    /**
     * Ui callback used to sell an assistant on the market.
     * @param price of the assistant.
     */
    @Override
    public void sellAssistant(int price) {
        try {
            mClient.sellAssistant(price);
        } catch (NetworkException e) {
            Debug.error("Cannot send sell assistant request", e);
        }
    }

    /**
     * Ui callback used to buy an item on the market.
     * @param marketId id of the item to buy.
     */
    @Override
    public void buyItem(String marketId) {
        try {
            mClient.buyItem(marketId);
        } catch (NetworkException e) {
            Debug.error("Cannot send buy market item request", e);
        }
    }

    /**
     * Ui callback used to earn a first special rewards.
     * @param cities where the player want to take the reward.
     */
    @Override
    public void earnFirstSpecialRewards(List<String> cities) {
        try {
            mClient.earnFirstSpecialRewards(cities);
        } catch (NetworkException e) {
            Debug.error("Cannot send earn first special rewards request", e);
        }
    }

    /**
     * Ui callback used to earn a second special rewards.
     * @param regions where the player want to take a permit tile.
     * @param indices where the player want to take a permit tile.
     */
    @Override
    public void earnSecondSpecialRewards(List<String> regions, List<Integer> indices) {
        try {
            mClient.earnSecondSpecialRewards(regions, indices);
        } catch (NetworkException e) {
            Debug.error("Cannot send earn second special rewards request", e);
        }
    }

    /**
     * Ui callback used to earn a third special rewards.
     * @param businessPermitTiles list of business permit tile to use.
     */
    @Override
    public void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles) {
        try {
            mClient.earnThirdSpecialRewards(businessPermitTiles);
        } catch (NetworkException e) {
            Debug.error("Cannot send earn third special rewards request", e);
        }
    }

    /**
     * Ui callback to send a chat message.
     * @param receiver nickname of the receiver if it is a private message, null otherwise.
     * @param message to send.
     */
    @Override
    public void sendChatMessage(String receiver, String message) {
        try {
            mClient.sendChatMessage(receiver, message);
        } catch (NetworkException e) {
            Debug.error("Cannot send chat message request", e);
        }
    }

    /**
     * Notify internal bus that a councillor has been elected.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onActionElectCouncillor(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyCouncilorAdded(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that a business permit tile has been acquired.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onActionAcquireBusinessPermitTile(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyBusinessPermitTileAcquired(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that an emporium has been built with a permit tile.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onActionBuildEmporiumWithBusinessPermitTile(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyEmporiumBuiltWithBusinessPermitTile(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that an emporium has been built with the help of the king.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onActionBuildEmporiumWithKingHelp(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyEmporiumBuiltWithKingHelp(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that an assistant has been engaged.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onActionEngageAssistant(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyAssistantEngaged(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that the visible permit tiles of a region are changed.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onActionChangeBusinessPermitTiles(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyBusinessPermitTilesChanged(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that an assistant has been sent to elect a councillor.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onActionSendAssistantToElectCouncillor(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyAssistantSentToElectCouncillor(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that a player has bought an additional main action.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onActionPerformAdditionalMainAction(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyNewAdditionalMainActionAvailable(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that server has respond with an error code.
     * @param errorCode that identify the error. See {@link ErrorCodes} for details.
     */
    @Override
    public void onActionNotValid(int errorCode) {
        mUi.showActionNotValid(errorCode);
    }

    /**
     * Notify internal bus that a first special reward has been earned.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onFirstSpecialRewardsEarned(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyFirstSpecialRewardsEarned(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that a second special reward has been earned.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onSecondSpecialRewardsEarned(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifySecondSpecialRewardsEarned(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that a third special reward has been earned.
     * @param updateState to apply to the client game state.
     */
    @Override
    public void onThirdSpecialRewardsEarned(UpdateState updateState) {
        applyModelUpdate(updateState);
        mUi.notifyThirdSpecialRewardsEarned(updateState);
        checkForSpecialRewards(updateState);
    }

    /**
     * Notify internal bus that a new chat message is arrived.
     * @param privateMessage true if message is private, false if public.
     * @param author of the message.
     * @param message body of the message.
     */
    @Override
    public void onChatMessage(boolean privateMessage, String author, String message) {
        mUi.showChatMessage(privateMessage, author, message);
    }

    /**
     * Notify internal bus that a player has disconnected.
     * @param nickname of the player that has disconnected.
     */
    @Override
    public void onPlayerDisconnected(String nickname) {
        mGameModel.getPlayer(nickname).setOnline(false);
        mUi.showPlayerDisconnected(nickname);
    }

    /**
     * Notify internal bus that the last game turn is started.
     * @param nickname of the player that has started the last game turn.
     */
    @Override
    public void onLastTurnStarted(String nickname) {
        mUi.showLastTurnStarted(nickname);
    }

    /**
     * Notify internal bus that the game is over.
     * @param updateStates list of final update states to apply.
     * @param ranking list of players sorted from the winner to the last loser.
     */
    @Override
    public void onGameEnded(UpdateState[] updateStates, List<String> ranking) {
        for (UpdateState updateState : updateStates) {
            applyModelUpdate(updateState);
        }
        mUi.notifyGameEnded(updateStates, ranking);
    }
}
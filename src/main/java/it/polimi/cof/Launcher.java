package it.polimi.cof;

import it.polimi.cof.ui.UiType;
import it.polimi.cof.ui.cli.DisplayPrinter;
import it.polimi.cof.util.Debug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class is useful to launch the game on client.
 * It will let the user choice if start the game in CLI mode or GUI mode.
 */
public class Launcher {

    /**
     * Private constructor. This class has not been designed to be instantiated.
     */
    private Launcher() {
        // hide constructor
    }

    /**
     * Static launcher for starting the game session.
     * @param args provided by starting class from command line shell.
     */
    public static void main(String[] args) {
        Debug.initialize(Debug.LEVEL_ERROR);
        MainGame mainGame = new MainGame(getFavouriteUi());
        mainGame.start();
    }

    /**
     * Ask the user if he prefer to start in CLI mode or GUI mode. This will check even if user not respect available
     * answers. In this case it will ask this question until user respond correctly.
     * @return the selected UserInterface type.
     */
    private static UiType getFavouriteUi() {
        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
        DisplayPrinter printer = new DisplayPrinter();
        printer.print("Which user interface do you prefer ?");
        while (true) {
            printer.print("1 -> Command Line Interface (CLI)");
            printer.print("2 -> Graphic User Interface (GUI)");
            try {
                int choice = Integer.parseInt(keyboard.readLine());
                switch (choice) {
                    case 1:
                        return UiType.CLI;
                    case 2:
                        return UiType.GUI;
                    default:
                        throw new NumberFormatException("Not a valid choice");
                }
            } catch (IOException | NumberFormatException e) {
                Debug.debug(e);
            }
        }
    }
}
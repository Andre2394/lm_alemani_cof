package it.polimi.cof.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class is an implementation of a three list. Every item is a node and every node can be linked with a list of
 * nodes of the same list. It works like a three.
 * @param <K> is the key to associate to every node.
 * @param <V> is the value of every single node.
 */
public class LinkedList<K extends Serializable, V extends Serializable> implements Serializable {

    /**
     * Internal representation of the three, is an HashMap and not a list to speed up the research of nodes.
     */
    private HashMap<K, Node<V>> mHashMap;

    /**
     * Base constructor.
     */
    public LinkedList() {
        mHashMap = new HashMap<>();
    }

    /**
     * Add a node to the three list.
     * @param key to associate to the node. (If already there the value will be overwritten).
     * @param value to add.
     */
    public void addNode(K key, V value) {
        mHashMap.put(key, new Node<>(value));
    }

    /**
     * Add a list of nodes from a map.
     * @param map to unpack.
     */
    public void addNodes(Map<K, V> map) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            mHashMap.put(entry.getKey(), new Node<>(entry.getValue()));
        }
    }

    /**
     * Create a link from one node to another one.
     * The link will be created in both directions.
     * @param key1 key associated to node 1.
     * @param key2 key associated to node 2.
     */
    public void linkNode(K key1, K key2) {
        Node<V> node1 = mHashMap.get(key1);
        Node<V> node2 = mHashMap.get(key2);
        node1.linkTo(key2, node2);
        node2.linkTo(key1, node1);
    }

    /**
     * Check if two nodes are linked together.
     * @param key1 key associated to node 1.
     * @param key2 key associated to node 2.
     * @return true if linked, false otherwise.
     */
    public boolean areNodesLinked(K key1, K key2) {
        Node<V> node1 = mHashMap.get(key1);
        return node1 != null && node1.isLinkedTo(key2);
    }

    /**
     * Get the value of the node associated with the given key.
     * @param key to search.
     * @return the value associated.
     */
    public V getValue(K key) {
        Node<V> node = mHashMap.get(key);
        return node != null ? node.getValue() : null;
    }

    /**
     * Merge all the nodes linked to the node associated with the provided key.
     * @param keyToRemove key that represent the node to remove.
     */
    public void mergeNodes(K keyToRemove) {
        // get node to remove
        Node<V> node = mHashMap.get(keyToRemove);
        Map<K, Node<V>> linkedNodes = node.getLinkedNodes();
        // now iterate all nodes
        for (Map.Entry<K, Node<V>> entry : mHashMap.entrySet()) {
            if (entry.getKey().equals(keyToRemove)) {
                // skip node to remove
                continue;
            }
            Node<V> entryNode = entry.getValue();
            if (entryNode.isLinkedTo(keyToRemove)) {
                // we have found a node that is directly linked to the node to remove
                for (Map.Entry<K, Node<V>> link : linkedNodes.entrySet()) {
                    linkMergedNodes(entry.getKey(), entryNode, link);
                }
                entryNode.getLinkedNodes().remove(keyToRemove);
            }
        }
        // finally remove the node
        mHashMap.remove(keyToRemove);
    }

    /**
     * Create a link from one node to another node if not himself.
     * @param key of the item to link.
     * @param node of the item to link.
     * @param link entry from which create the link.
     */
    private void linkMergedNodes(K key, Node<V> node, Map.Entry<K, Node<V>> link) {
        if (!link.getKey().equals(key)) {
            node.linkTo(link.getKey(), link.getValue());
        }
    }

    /**
     * Get a list of all keys of the nodes directly linked to the node associated to the given key.
     * @param key to look for.
     * @return a list of all keys linked.
     */
    public List<K> getLinkedKeys(K key) {
        List<K> keys = new ArrayList<>();
        Node<V> node = mHashMap.get(key);
        if (node != null) {
            Map<K, Node<V>> linkedNodes = node.getLinkedNodes();
            keys.addAll(linkedNodes.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList()));
        }
        return keys;
    }

    /**
     * Get a list of all values of the nodes directly linked to the node associated to the given key.
     * @param key to look for.
     * @return a list of all values linked.
     */
    public List<V> getLinkedNodes(K key) {
        List<V> values = new ArrayList<>();
        Node<V> node = mHashMap.get(key);
        if (node != null) {
            Map<K, Node<V>> linkedNodes = node.getLinkedNodes();
            for (Map.Entry<K, Node<V>> entry : linkedNodes.entrySet()) {
                Node<V> value = entry.getValue();
                if (value != null) {
                    values.add(value.getValue());
                }
            }
        }
        return values;
    }

    /**
     * Get all the values of the list.
     * @return a list of all values.
     */
    public List<V> getValues() {
        return mHashMap.values().stream().map(Node::getValue).collect(Collectors.toList());
    }

    /**
     * Internal representation of a node.
     * @param <T> if the value to represent.
     */
    private class Node<T extends Serializable> implements Serializable {

        /**
         * Internal caching of the value.
         */
        private final T mValue;

        /**
         * Internal map that represent the links with other nodes.
         */
        private final HashMap<K, Node<T>> mLinkedNodes;

        /**
         * Private constructor of a node.
         * @param value to represent.
         */
        private Node(T value) {
            mValue = value;
            mLinkedNodes = new HashMap<>();
        }

        /**
         * Get the represented value.
         * @return the represented value.
         */
        private T getValue() {
            return mValue;
        }

        /**
         * Check if the node if linked to a node associated with the given key.
         * @param key to look for.
         * @return true if linked, false otherwise.
         */
        private boolean isLinkedTo(K key) {
            return mLinkedNodes.containsKey(key);
        }

        /**
         * Create a link to the given node.
         * @param key associated to the given node.
         * @param node to link.
         */
        private void linkTo(K key, Node<T> node) {
            mLinkedNodes.put(key, node);
        }

        /**
         * Get the map of all linked nodes.
         * @return the map of all linked nodes.
         */
        private Map<K, Node<T>> getLinkedNodes() {
            return mLinkedNodes;
        }
    }
}
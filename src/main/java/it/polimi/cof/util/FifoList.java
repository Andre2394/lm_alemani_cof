package it.polimi.cof.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * This class create a list of objects (of the type provided T) and manage it with a sort of FIFO policy.
 * When creating the list you should provide a valid MAX_NUMBER of objects that the list will manage.
 * When you try to add an object and the list is full, the oldest object will be removed from the list.
 * This ensure that the list will always contains 0 <= x <= MAX_NUMBER of items.
 */
public class FifoList<T> implements Serializable, Iterable<T> {

    /**
     *  Final attribute that represent the MAX_NUMBER of items that the list will contains.
     *  It must be a number > 0.
     */
    private final int mMaxSize;

    /**
     *  Internal ArrayList used to manage the objects queue.
     */
    private ArrayList<T> mList;

    /**
     * Create a new instance of the FifoList.
     * @param maxSize must be a number > 0.
     * @throws IllegalArgumentException if maxSize is not valid.
     */
    public FifoList(int maxSize) {
        if (maxSize <= 0) {
            throw new IllegalArgumentException("Max size must be >= 0.");
        }
        mMaxSize = maxSize;
        mList = new ArrayList<>();
    }

    /**
     * Add a new object to the list.
     * @param element to add to the list.
     * @return the oldest object in list that is going to be removed if MAX_NUMBER of elements is reached,
     * NULL otherwise.
     */
    public T add(T element) {
        T firstElement = null;
        if (mList.size() >= mMaxSize) {
            firstElement = mList.get(0);
            mList.remove(0);
        }
        mList.add(element);
        return firstElement;
    }

    /**
     * Check if list is full. If full, the next item added will remove the first item on the top.
     * @return true if full, false if not.
     */
    public boolean isFull() {
        return mList.size() == mMaxSize;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     * @return an Iterator.
     */
    @Override
    public Iterator<T> iterator() {
        return mList.iterator();
    }
}
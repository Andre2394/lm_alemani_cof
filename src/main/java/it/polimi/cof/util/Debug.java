package it.polimi.cof.util;

import it.polimi.cof.ui.cli.DisplayPrinter;
import it.polimi.cof.ui.cli.PrintColor;

/**
 * This singleton class is useful for debugging. It should be initialized before usage with {@link #initialize(int)} providing
 * a valid debug level. Every message to print with a minor level than the level selected at initialization are printed.
 */
public class Debug {

    /**
     * No message will be printed.
     */
    public static final int LEVEL_DISABLED = 0;

    /**
     * Only critical errors are printed.
     */
    public static final int LEVEL_CRITICAL = 1;

    /**
     * Only critical and standard error are printed.
     */
    public static final int LEVEL_ERROR = 2;

    /**
     * Only verbose messages and errors are printed.
     */
    public static final int LEVEL_VERBOSE = 3;

    /**
     * Everything is printed.
     */
    public static final int LEVEL_DEBUG = 4;

    /**
     * Singleton instance.
     */
    private static Debug mInstance;

    /**
     * Current debug level.
     */
    private final int mLevel;

    /**
     * Display driver used to print messages into console's window.
     */
    private final DisplayPrinter mPrinter;

    /**
     * Private constructor for debug initialization.
     * @param level selected debug level.
     */
    private Debug(int level) {
        mLevel = level;
        mPrinter = new DisplayPrinter();
    }

    /**
     * Initialization of the debugger.
     * @param level selected debug level.
     * @throws IllegalArgumentException if provided level is not valid.
     */
    public static void initialize(int level) {
        if (level >= LEVEL_DISABLED && level <= LEVEL_DEBUG) {
            mInstance = new Debug(level);
        } else {
            throw new IllegalArgumentException(String.format("Debug level %d unknown", level));
        }
    }

    /**
     * Get the debugger instance. If it has not been initialized yet, it will be auto-initialized with LEVEL_CRITICAL.
     * @return a not null debugger instance.
     */
    public static Debug getDebugger() {
        if (mInstance == null) {
            mInstance = new Debug(LEVEL_CRITICAL);
        }
        return mInstance;
    }

    /**
     * Print a critical message. It will be printed as red string.
     * @param message to print.
     */
    public static void critical(String message) {
        printLevel(LEVEL_CRITICAL, message, PrintColor.RED);
    }

    /**
     * Print a critical message. It will be printed as red string.
     * @param throwable to print.
     */
    public static void critical(Throwable throwable) {
        printLevel(LEVEL_CRITICAL, throwable, PrintColor.RED);
    }

    /**
     * Print a critical message. It will be printed as red string.
     * @param message to print.
     * @param throwable to print.
     */
    public static void critical(String message, Throwable throwable) {
        printLevel(LEVEL_CRITICAL, message, throwable, PrintColor.RED);
    }

    /**
     * Print an error message. It will be printed as yellow string.
     * @param message to print.
     */
    public static void error(String message) {
        printLevel(LEVEL_ERROR, message, PrintColor.YELLOW);
    }

    /**
     * Print an error message. It will be printed as yellow string.
     * @param throwable to print.
     */
    public static void error(Throwable throwable) {
        printLevel(LEVEL_ERROR, throwable, PrintColor.YELLOW);
    }

    /**
     * Print an error message. It will be printed as yellow string.
     * @param message to print.
     * @param throwable to print.
     */
    public static void error(String message, Throwable throwable) {
        printLevel(LEVEL_ERROR, message, throwable, PrintColor.YELLOW);
    }

    /**
     * Print a verbose message. It will be printed as cyan string.
     * @param message to print.
     */
    public static void verbose(String message) {
        printLevel(LEVEL_VERBOSE, message, PrintColor.CYAN);
    }

    /**
     * Print a verbose message. It will be printed as cyan string.
     * @param message to print.
     * @param args Arguments referenced by the format specifiers in the format string.
     */
    public static void verbose(String message, Object... args) {
        printLevel(LEVEL_VERBOSE, String.format(message, args), PrintColor.CYAN);
    }

    /**
     * Print a verbose message. It will be printed as cyan string.
     * @param throwable to print.
     */
    public static void verbose(Throwable throwable) {
        printLevel(LEVEL_VERBOSE, throwable, PrintColor.CYAN);
    }

    /**
     * Print a verbose message. It will be printed as cyan string.
     * @param message to print.
     * @param throwable to print.
     */
    public static void verbose(String message, Throwable throwable) {
        printLevel(LEVEL_VERBOSE, message, throwable, PrintColor.CYAN);
    }

    /**
     * Print a debug message. It will be printed as green string.
     * @param message to print.
     */
    public static void debug(String message) {
        printLevel(LEVEL_DEBUG, message, PrintColor.GREEN);
    }

    /**
     * Print a debug message. It will be printed as green string.
     * @param message to print.
     * @param args Arguments referenced by the format specifiers in the format string.
     */
    public static void debug(String message, Object... args) {
        printLevel(LEVEL_DEBUG, String.format(message, args), PrintColor.GREEN);
    }

    /**
     * Print a debug message. It will be printed as green string.
     * @param throwable to print.
     */
    public static void debug(Throwable throwable) {
        printLevel(LEVEL_DEBUG, throwable, PrintColor.GREEN);
    }

    /**
     * Print a debug message. It will be printed as green string.
     * @param message to print.
     * @param throwable to print.
     */
    public static void debug(String message, Throwable throwable) {
        printLevel(LEVEL_DEBUG, message, throwable, PrintColor.GREEN);
    }

    /**
     * Print a message with provided level.
     * @param level of the message.
     * @param message body to print.
     * @param throwable to print.
     * @param color of the console message.
     */
    private static void printLevel(int level, String message, Throwable throwable, PrintColor color) {
        printLevel(level, message + " - Message: " + throwable.getMessage(), color);
    }

    /**
     * Print a message with provided level.
     * @param level of the message.
     * @param throwable to print.
     * @param color of the console message.
     */
    private static void printLevel(int level, Throwable throwable, PrintColor color) {
        printLevel(level, throwable.getMessage(), color);
    }

    /**
     * Print a message with provided level.
     * @param level of the message.
     * @param message body to print.
     * @param color of the console message.
     */
    private static void printLevel(int level, String message, PrintColor color) {
        Debug debugger = getDebugger();
        if (debugger.mLevel >= level) {
            debugger.mPrinter.print(message, color);
        }
    }
}
package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.Player;
import it.polimi.cof.ui.gui.GuiUtils;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Panel that handle the final ranking
 */
public class FinalRankingPanel extends JFXPanel {

    @SuppressWarnings("unchecked")
    private FinalRankingPanel(List<Player> ranking) {
        Parent root = GuiUtils.loadFxml(this, "dialog_final_ranking");
        Label winner = (Label) root.lookup("#winnerPlayer");
        TableView<Player> tableView = (TableView<Player>) root.lookup("#playerTable");
        TableColumn<Player, String> nicknameColumn = new TableColumn<>("Nickname");
        TableColumn<Player, Integer> victoryPointColumn = new TableColumn<>("Victory points");
        TableColumn<Player, Integer> assistantColumn = new TableColumn<>("Assistants");
        TableColumn<Player, Integer> politicCardColumn = new TableColumn<>("Politic cards");
        nicknameColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<String> simpleObjectProperty = new SimpleObjectProperty<>();
            String nickname = param.getValue().getNickname();
            simpleObjectProperty.setValue(nickname + (param.getValue().isOnline() ? "" : " [OFFLINE]"));
            return simpleObjectProperty;
        });
        victoryPointColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getVictoryPoints());
            return simpleObjectProperty;
        });
        assistantColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getAssistants());
            return simpleObjectProperty;
        });
        politicCardColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getPoliticCards().size());
            return simpleObjectProperty;
        });
        Platform.runLater(() -> {
            tableView.getColumns().addAll(
                    nicknameColumn,
                    victoryPointColumn,
                    assistantColumn,
                    politicCardColumn
            );
            winner.setText(String.format("The winner is %s", ranking.get(0).getNickname()));
            tableView.setItems(FXCollections.observableArrayList(ranking));
        });
    }

    /**
     * Handle dialog creation of final ranking
     * @param parent root
     * @param ranking of players
     * @return
     */
    public static JDialog createDialog(Component parent, List<Player> ranking) {
        FinalRankingPanel panel = new FinalRankingPanel(ranking);
        return GuiUtils.createDialog(parent, "Final ranking", 600, 500, panel);
    }
}
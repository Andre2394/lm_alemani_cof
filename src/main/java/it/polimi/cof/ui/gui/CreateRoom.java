package it.polimi.cof.ui.gui;

import it.polimi.cof.util.Debug;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;

import javax.swing.*;

/**
 * This class is about the server request to client about room creation.
 * It's purpose is to catch the maximum players in room int variable.
 * Created by Guido on 13/06/2016.
 */
/*package-local*/ class CreateRoom extends JFXPanel {

    private static final int MIN_PLAYERS = 2;

    private final Callback mCallback;

    private Button mConfirm;
    private TextField mMaxPlayers;
    private Label mLabelError;


    /**
     * Prepare Create Room scene and events.
     *
     * @param callback callback to the GraphicalUserInterface class
     */
    /*package-local*/ CreateRoom(Callback callback) {
        mCallback = callback;
        Parent root = GuiUtils.loadFxml(this, "sCR");

        GridPane pane = (GridPane) root.lookup("#pane");
        mConfirm = (Button) root.lookup("#confirmButton");
        mMaxPlayers = (TextField) root.lookup("#maxPlayerField");
        mLabelError = (Label) root.lookup("#labelError");

        mMaxPlayers.textProperty().addListener((observableValue, s, s2) -> toggleButton());
        mConfirm.setOnAction(event -> tryCreateRoom());
        mConfirm.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                tryCreateRoom();
            }
        });
        mMaxPlayers.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                tryCreateRoom();
            }
        });

        Image pergamena = new Image(getClass().getResource("/images/pergamena.png").toExternalForm());
        Image sfondo = new Image(getClass().getResource("/images/sfondo1.jpg").toExternalForm(),500,500,false,true);
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSizeUnder = new BackgroundSize(pane.getWidth(), pane.getHeight(), false, false, true, true);
        BackgroundSize backgroundSize = new BackgroundSize(pane.getWidth(), pane.getHeight(), false, false, true, false);
        // new BackgroundImage(image, repeatX, repeatY, position2, size)
        BackgroundImage backgroundImage = new BackgroundImage(pergamena, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        BackgroundImage backgroundImageUnder = new BackgroundImage(sfondo, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, backgroundSizeUnder);
        // new Background(images...)
        Background background = new Background(backgroundImageUnder, backgroundImage);
        pane.setBackground(background);
    }

    /**
     * It prevents invalid output disabling the button
     */
    private void toggleButton() {
        try {
            mConfirm.setDisable((mMaxPlayers.getText().isEmpty()) || (Integer.parseInt(mMaxPlayers.getText()) < MIN_PLAYERS));
        } catch (NumberFormatException e) {
            mLabelError.setVisible(true);
        }
    }


    /**
     * Using the callback send to the GraphicalUserInterface class the information that the user has given.
     */
    private void tryCreateRoom() {
        try {
            int players = Integer.parseInt(mMaxPlayers.getText());
            Audio.play("click");
            mCallback.tryCreateRoom(players);
        } catch (NumberFormatException e) {
            Debug.verbose("Not a number, there are characters");
            int res = JOptionPane.showOptionDialog(null, "You cannot insert character instead of numbers", "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
            if (res == 0) {
                Audio.play("click");
            }
        }
    }

    /**
     * Callback to main class
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * @param maxPlayers maximum players in the room choice by the user
         */
        void tryCreateRoom(int maxPlayers);
    }
}

package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.PoliticCard;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;

import javax.swing.*;
import java.awt.*;

/**
 * This class represent the panel displayed when drawing a politic card.
 */
public class PoliticCardDrawnPanel extends JFXPanel {

    /**
     * Base constructor.
     * @param politicCard to display.
     */
    private PoliticCardDrawnPanel(PoliticCard politicCard) {
        Parent root = GuiUtils.loadFxml(this, "dialog_politic_card_drawn");
        Canvas canvas = (Canvas) root.lookup("#canvasPoliticCard");
        Platform.runLater(() -> {
            double canvasWidth = canvas.getWidth();
            double canvasHeight = canvas.getHeight();
            double width = 80;
            double height = 140;
            double x = canvasWidth / 2 - width / 2;
            double y = canvasHeight / 2 - height / 2;
            Painter.drawPoliticCard(canvas.getGraphicsContext2D(), politicCard, x, y, width, height);
        });
    }

    /**
     * Create the dialog.
     * @param parent component.
     * @param politicCard to display.
     * @return the created dialog.
     */
    public static JDialog createDialog(Component parent, PoliticCard politicCard) {
        PoliticCardDrawnPanel panel = new PoliticCardDrawnPanel(politicCard);
        return GuiUtils.createDialog(parent, "Politic card drawn", 350, 300, panel);
    }
}
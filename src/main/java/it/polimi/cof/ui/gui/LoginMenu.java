package it.polimi.cof.ui.gui;

import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;


/**
 * This class after loaded its FXML file, ask to the client the nickname preferred by the client and save it.
 * Created by Guido on 07/06/2016.
 */
/*package-local*/ class LoginMenu extends JFXPanel {

    private final Callback mCallback;
    private Button mJoin;
    private TextField mUser;
    private Label mLabelError;

    /**
     * Set the scene for the login menu
     *
     * @param callback to the main class
     */
    /*package-local*/ LoginMenu(Callback callback) {
        mCallback = callback;
        Parent root = GuiUtils.loadFxml(this, "sLM");

        GridPane pane = (GridPane) root.lookup("#pane");
        mJoin = (Button) root.lookup("#joinButton");
        mUser = (TextField) root.lookup("#userField");
        mLabelError = (Label) root.lookup("#labelError");

        mJoin.setOnAction(event -> doJoin());
        mJoin.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                doJoin();
            }
        });
        mUser.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                doJoin();
            }
        });
        mUser.textProperty().addListener((observableValue, s, s2) -> toggleButton(s2));

        Image pergamena = new Image(getClass().getResource("/images/pergamena.png").toExternalForm());
        Image sfondo = new Image(getClass().getResource("/images/sfondo1.jpg").toExternalForm(),500,500,false,true);
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSizeUnder = new BackgroundSize(pane.getWidth(), pane.getHeight(), false, false, true, true);
        BackgroundSize backgroundSize = new BackgroundSize(pane.getWidth(), pane.getHeight(), false, false, true, false);
        // new BackgroundImage(image, repeatX, repeatY, position2, size)
        BackgroundImage backgroundImage = new BackgroundImage(pergamena, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        BackgroundImage backgroundImageUnder = new BackgroundImage(sfondo, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, backgroundSizeUnder);
        // new Background(images...)
        Background background = new Background(backgroundImageUnder, backgroundImage);
        pane.setBackground(background);
    }

    private void toggleButton(String text) {
        boolean disable = false;
        if (text.isEmpty()) {
            disable = true;
        }
        for (char c : text.toCharArray()) {
            if (Character.isWhitespace(c)) {
                disable = true;
            }
        }
        mJoin.setDisable(disable);
    }

    private void doJoin() {
        String username = mUser.getText();
        Audio.play("click");
        mCallback.tryLogin(username);
    }

    /*package-local*/ void showLoginError() {
        mLabelError.setVisible(true);
    }

    /**
     * Callback to the main class
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * @param nickname nickname to use to login
         */
        void tryLogin(String nickname);
    }
}

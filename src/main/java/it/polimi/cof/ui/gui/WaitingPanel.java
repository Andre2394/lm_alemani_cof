package it.polimi.cof.ui.gui;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
 * Panel that is showed up by the CardLayout when someone is waiting for Godot...
 * Waiting screen for other players during game creation
 */
/*package-local*/ class WaitingPanel extends JFXPanel {

    /*package-local*/ WaitingPanel() {
        Platform.runLater(() -> {
            Parent root = GuiUtils.loadFxml(this, "waiting_pane");
            AnchorPane anchorPane = (AnchorPane) root.lookup("#backgroundPane");
            ImageView imageView = new ImageView(TextureManager.get("waiting", "jpg"));
            imageView.fitWidthProperty().bind(anchorPane.widthProperty());
            imageView.fitHeightProperty().bind(anchorPane.heightProperty());
            anchorPane.getChildren().add(imageView);
        });
    }

}
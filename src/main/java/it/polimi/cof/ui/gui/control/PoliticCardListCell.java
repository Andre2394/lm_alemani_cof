package it.polimi.cof.ui.gui.control;

import it.polimi.cof.model.PoliticCard;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

/**
 * This class is used by {@link ListView} to represent the {@link PoliticCard} item in the list.
 */
public class PoliticCardListCell extends ListCell<PoliticCard> {

    /**
     * @param item politic cards of the list
     * @param empty true if an empty list
     */
    @Override
    protected void updateItem(PoliticCard item, boolean empty) {
        super.updateItem(item, empty);
        Platform.runLater(() -> {
            if (item == null || empty) {
                setGraphic(null);
            } else {
                Canvas canvas = new Canvas(80, 120);
                Painter.drawPoliticCard(canvas, item);
                setGraphic(canvas);
            }
        });
    }
}
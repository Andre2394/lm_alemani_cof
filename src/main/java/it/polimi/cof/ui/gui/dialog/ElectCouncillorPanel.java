package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.Councilor;
import it.polimi.cof.ui.gui.GuiUtils;

import javax.swing.*;
import java.awt.*;

/**
 * Class that handle councilor electing panel
 */
public class ElectCouncillorPanel extends AbstractCouncillorElectionPanel {

    private final Callback mCallback;

    /**
     * Prepare events and information for the scene.
     * @param baseGame game state
     * @param callback to the main class
     * */
    private ElectCouncillorPanel(BaseGame baseGame, Callback callback) {
        super(baseGame, "dialog_elect_councillor");
        mCallback = callback;
    }

    /**
     * Method used to create dialog using code specified in GuiUtils class
     * @param parent root
     * @param baseGame game state
     * @param callback to the server class
     * @return
     */
    public static JDialog createDialog(Component parent, BaseGame baseGame, Callback callback) {
        ElectCouncillorPanel panel = new ElectCouncillorPanel(baseGame, callback);
        return GuiUtils.createDialog(parent, "Main action 1", 500, 600, panel);
    }

    /**
     * Handle event mouse selected
     * @param councilor
     * @param region
     */
    @Override
    protected void onCouncillorSelected(Councilor councilor, String region) {
        mCallback.onCouncillorElected(councilor, region);
    }

    /**
     * Cancel button
     */
    @Override
    protected void onCancel() {
        mCallback.onCancel();
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * Handle event, call main class to response
         * @param councilor going to be elected
         * @param region in which it happens
         */
        void onCouncillorElected(Councilor councilor, String region);

        /**
         * Disable current window
         */
        void onCancel();
    }
}
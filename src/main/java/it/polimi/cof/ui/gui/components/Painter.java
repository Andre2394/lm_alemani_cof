package it.polimi.cof.ui.gui.components;

import it.polimi.cof.model.*;
import it.polimi.cof.ui.gui.TextureManager;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class is an utility class used to paint game components over canvas.
 */
public class Painter {

    /**
     * Defines.
     */
    private static final String TEXTURE_VICTORY_POINT = "victory_point";

    /**
     * Private constructor. This class has not been designed to be instantiated.
     */
    private Painter() {
        // hide
    }

    /**
     * Paint politic card over relative canvas
     * @param canvas painted over
     * @param politicCard painted
     */
    public static void drawPoliticCard(Canvas canvas, PoliticCard politicCard) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        Image texture = TextureManager.get(String.format("politic_card_%s", politicCard.getColor()));
        context.drawImage(texture, 0, 0, canvas.getWidth(), canvas.getHeight());
    }

    /**
     * Draws politic card
     * @param context
     * @param politicCard
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public static void drawPoliticCard(GraphicsContext context, PoliticCard politicCard, double x, double y, double width, double height) {
        if (politicCard != null) {
            Image background = TextureManager.get(String.format("politic_card_%s", politicCard.getColor()));
            context.drawImage(background, x, y, width, height);
        }
    }

    /**
     * Paint permit tile over relative canvas
     * @param canvas painted over
     * @param businessPermitTile painted
     */
    public static void drawPermitTile(Canvas canvas, BusinessPermitTile businessPermitTile) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        drawPermitTile(context, businessPermitTile, 0, 0, canvas.getWidth(), canvas.getHeight());
    }

    /**
     * Draws permit tile
     * @param context
     * @param businessPermitTile
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public static void drawPermitTile(GraphicsContext context, BusinessPermitTile businessPermitTile, double x, double y, double width, double height) {
        if (businessPermitTile != null) {
            context.setTextAlign(TextAlignment.CENTER);
            context.setTextBaseline(VPos.CENTER);
            // draw background
            Image background = TextureManager.get("permit_tile");
            context.drawImage(background, x, y, width, height);
            // draw permit tile characters
            Font font = new Font(getPermitTileTextSize(height, businessPermitTile.getCities().length));
            String text = getPermitTileCities(businessPermitTile);
            double titleX = x + width / 2;
            double titleY = y + height * 0.1915d;
            context.setFont(font);
            context.setFill(Color.BLACK);
            context.fillText(text, titleX, titleY);
            // draw reward
            double rewardX = x + width * 0.11d;
            double rewardY = y + height * 0.343d;
            double rewardWidth = width * 0.774d;
            double rewardHeight = height * 0.52d;
            // get list of textures
            List<TextureGroup> textures = getRewardTextures(businessPermitTile);
            int rows = textures.size() <= 3 ? 1 : 2;
            for (int i = 0; i < rows; i++) {
                int columns = getColumnsCount(i, textures.size());
                for (int j = 0; j < columns; j++) {
                    int index = columns * i + j;
                    double textureX = rewardX + j * rewardWidth / columns;
                    double textureY = rewardY + i * rewardHeight / rows;
                    double textureWidth = rewardWidth / columns;
                    double textureHeight = rewardHeight / rows;
                    drawTextureGroup(context, textures.get(index), textureX, textureY, textureWidth, textureHeight);
                }
            }
        }
    }

    private static double getPermitTileTextSize(double height, int cities) {
        return height * 0.2558d * (1.05 - cities * 0.05d);
    }

    private static String getPermitTileCities(BusinessPermitTile businessPermitTile) {
        StringBuilder stringBuilder = new StringBuilder();
        char[] cities = businessPermitTile.getCities();
        for (int i = 0; i < cities.length; i++) {
            if (i > 0) {
                stringBuilder.append("/");
            }
            stringBuilder.append(Character.toUpperCase(cities[i]));
        }
        return stringBuilder.toString();
    }

    private static void drawTextureGroup(GraphicsContext context, TextureGroup textureGroup, double x, double y, double width, double height) {
        // texture have sizes 123 x 147, so measure and calculate best canvas sizes to not break the proportions
        double textureRatio = 123d / 147d;
        double canvasRatio = width / height;
        double textureX;
        double textureY;
        double textureWidth;
        double textureHeight;
        if (canvasRatio >= textureRatio) {
            // canvas width > texture width
            textureWidth = height / 147d * 123d;
            textureHeight = height;
            textureX = width / 2 - textureWidth / 2;
            textureY = 0d;
        } else {
            // canvas height > texture height
            textureWidth = width;
            textureHeight = width / 123d * 147d;
            textureX = 0d;
            textureY = height / 2 - textureHeight / 2;
        }
        // draw the texture (remember the initial x and y)
        context.drawImage(textureGroup.getTexture(), x + textureX, y + textureY, textureWidth, textureHeight);
        // draw indicator if necessary
        if (textureGroup.getCount() > 1) {
            context.setTextAlign(TextAlignment.CENTER);
            context.setTextBaseline(VPos.CENTER);
            Font font = new Font(textureHeight / 3);
            String text = String.format("%d", textureGroup.getCount());
            double textX = x + width / 2;
            double textY = y + height / 2;
            context.setFont(font);
            context.setFill(textureGroup.getColor());
            context.fillText(text, textX, textY);
        }
    }

    private static List<TextureGroup> getRewardTextures(Reward reward) {
        List<TextureGroup> textures = new ArrayList<>();
        if (reward != null) {
            addTexture(textures, "assistant", Color.WHITE, reward.getAssistantCount());
            addTexture(textures, TEXTURE_VICTORY_POINT, Color.WHITE, reward.getVictoryPointCount());
            addTexture(textures, "nobility_reward", Color.WHITE, reward.getNobilityStepCount());
            addTexture(textures, "coin", Color.BLACK, reward.getCoinsCount());
            addTexture(textures, "politic_card", Color.WHITE, reward.getPoliticCardCount());
            addTexture(textures, "main_action", Color.WHITE, reward.getMainActionCount());
            if (reward instanceof NobilityReward) {
                addTexture(textures, "first_special_reward", Color.WHITE, ((NobilityReward) reward).getFirstSpecialBonusCount());
                addTexture(textures, "second_special_reward", Color.WHITE, ((NobilityReward) reward).getSecondSpecialBonusCount());
                addTexture(textures, "third_special_reward", Color.WHITE, ((NobilityReward) reward).getThirdSpecialBonusCount());
            }
        }
        return textures;
    }

    private static void addTexture(List<TextureGroup> textures, String texture, Color color, int count) {
        if (count > 0) {
            textures.add(new TextureGroup(TextureManager.get(texture), count, color));
        }
    }

    private static int getColumnsCount(int row, int rewardSize) {
        if (rewardSize <= 3) {
            return rewardSize;
        } else if (rewardSize == 4) {
            return 2;
        } else if (rewardSize == 5) {
            return row == 0 ? 3 : 2;
        }
        return 3;
    }

    /**
     * Paint councillor over relative canvas
     * @param canvas painted over
     * @param councilor painted
     */
    public static void drawCouncillor(Canvas canvas, Councilor councilor) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        Image texture = TextureManager.get(String.format("councillor_%s", councilor.getColor()));
        context.drawImage(texture, 0, 0, canvas.getWidth(), canvas.getHeight());
    }

    /**
     * Paint balcony over relative canvas
     * @param canvas painted over
     * @param colors painted
     */
    public static void drawBalcony(Canvas canvas, String[] colors) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        double width = canvas.getWidth();
        double height = canvas.getHeight();
        double councillorWidth = width / colors.length;
        // draw councillors
        for (int i = 1; i <= colors.length; i++) {
            Image texture = TextureManager.get(String.format("councillor_%s", colors[colors.length - i]));
            context.drawImage(texture, (i - 1) * councillorWidth, 0, councillorWidth, height);
        }
        Image texture = TextureManager.get("balcony_front", "jpg");
        context.drawImage(texture, 0, height * 0.35, width, height * 0.65);
    }

    /**
     * Draws balcony
     * @param context
     * @param colors
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public static void drawTopBalcony(GraphicsContext context, String[] colors, double x, double y, double width, double height) {
        Image balconyTexture = TextureManager.get("balcony");
        context.drawImage(balconyTexture, x, y, width, height);
        double startX = x + width * 0.19758d;
        double councillorWidth = width * 0.17742d;
        double councillorHeight = height * 0.5688d;
        for (int i = 1; i <= colors.length; i++) {
            String color = colors[colors.length - i];
            if (color != null) {
                Image texture = TextureManager.get(String.format("councillor_top_%s", colors[colors.length - i]));
                context.drawImage(texture, startX + (i - 1) * councillorWidth, y, councillorWidth, councillorHeight);
            }
        }
    }

    /**
     * Painted city over relative canvas
     * @param canvas painted over
     * @param city painted
     */
    public static void drawCity(Canvas canvas, City city) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        drawCity(context, city, 0, 0, canvas.getWidth(), canvas.getHeight());
    }

    /**
     * Draws city
     * @param context
     * @param city
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public static void drawCity(GraphicsContext context, City city, double x, double y, double width, double height) {
        Image image = TextureManager.get(city.getType());
        Image name = TextureManager.get(String.format("%s_%s", city.getName().toLowerCase(), city.getType()));
        context.drawImage(image, x, y, width, height);
        context.drawImage(name, x, y, width, height);
    }

    /**
     * Painted reward line over relative canvas
     * @param canvas painted over
     * @param reward painted
     */
    public static void drawRewardLine(Canvas canvas, Reward reward) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        drawRewardLine(context, reward, 0, 0, canvas.getWidth(), canvas.getHeight());
    }

    /**
     * Draw reward line
     * @param context
     * @param reward
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public static void drawRewardLine(GraphicsContext context, Reward reward, double x, double y, double width, double height) {
        List<TextureGroup> textures = getRewardTextures(reward);
        int size = textures.size();
        double textureWidth = width / 6;
        for (int i = 0; i < size; i++) {
            drawTextureGroup(context, textures.get(i), x + i * textureWidth, y, textureWidth, height);
        }
    }

    /**
     * Draws the nobility track
     * @param context
     * @param reward
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public static void drawNobilityRewardColumn(GraphicsContext context, NobilityReward reward, double x, double y, double width, double height) {
        List<TextureGroup> textures = getRewardTextures(reward);
        int rows = textures.size();
        double rewardHeight = height / rows;
        for (int i = 0; i < rows; i++) {
            double rewardY = y + i * rewardHeight;
            drawTextureGroup(context, textures.get(i), x, rewardY, width, rewardHeight);
        }
    }

    /**
     * Painted assistant over relative canvas
     * @param canvas painted over
     */
    public static void drawAssistant(Canvas canvas) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        Image texture = TextureManager.get("assistant");
        context.drawImage(texture, 0, 0, canvas.getWidth(), canvas.getHeight());
    }

    /**
     * Painted bonus card over relative canvas
     * @param canvas painted over
     * @param item painted
     */
    public static void drawBonusCard(Canvas canvas, Map.Entry<String, Reward> item) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        Image texture = TextureManager.get(String.format("reward_%s", item.getKey()));
        context.drawImage(texture, 0, 0, canvas.getWidth(), canvas.getHeight());
        TextureGroup textureGroup = new TextureGroup(TextureManager.get(TEXTURE_VICTORY_POINT), item.getValue().getVictoryPointCount(), Color.WHITE);
        drawTextureGroup(context, textureGroup, canvas.getWidth() / 16 * 9, canvas.getHeight() / 8, canvas.getWidth() / 16 * 6, canvas.getHeight() / 8 * 6);
    }

    /**
     * Painted king reward card over relative canvas
     * @param canvas painted over
     * @param reward painted
     */
    public static void drawKingRewardCard(Canvas canvas, Reward reward) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        Image texture = TextureManager.get("reward_king_card");
        context.drawImage(texture, 0, 0, canvas.getWidth(), canvas.getHeight());
        TextureGroup textureGroup = new TextureGroup(TextureManager.get(TEXTURE_VICTORY_POINT), reward.getVictoryPointCount(), Color.WHITE);
        drawTextureGroup(context, textureGroup, canvas.getWidth() / 8, canvas.getHeight() / 8, canvas.getWidth() / 8 * 6, canvas.getHeight() / 8 * 6);
    }

    private static class TextureGroup {

        private final Image mImage;
        private final Integer mCount;
        private final Color mColor;

        private TextureGroup(Image image, Integer count, Color color) {
            mImage = image;
            mCount = count;
            mColor = color;
        }

        private Image getTexture() {
            return mImage;
        }

        private int getCount() {
            return mCount;
        }

        private Color getColor() {
            return mColor;
        }
    }
}
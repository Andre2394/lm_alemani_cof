package it.polimi.cof.ui.gui;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * This class handle audio request from other classes of the GUI.
 * Optimized for shorts audio.
 * Created by Guido on 02/07/2016.
 */


class Audio {
    private static ExecutorService mExecutor = Executors.newFixedThreadPool(50);
    private static boolean mSound = true;

    private Audio() {
        //hide constructors
    }

    /**
     * Play a given audio file name
     *
     * @param filename Name of the audio file.
     */
    static AudioPlayer play(String filename) {
        if (!mSound) {
            return null;
        }
        AudioPlayer player = new AudioPlayer(filename);
        mExecutor.execute(player);
        return player;
    }

    /**
     * Play a given audio file name
     * @param filename of audio clip
     * @param loop true if it repeats itself
     * @return AudioPlayer that is working on clip
     */
    static AudioPlayer play(String filename, boolean loop) {
        if (!mSound) {
            return null;
        }
        AudioPlayer player = new AudioPlayer(filename, loop);
        mExecutor.execute(player);
        return player;
    }

    /**
     * Play a given audio file name
     * @param filename of audio clip
     * @param loop true if it repeats itself
     * @param sound true if sound is activated in GamePanel, else false
     * @return
     */
    static AudioPlayer play(String filename, boolean loop, boolean sound) {
        if (!mSound && sound) {
            mSound = sound;
        }
        AudioPlayer player = new AudioPlayer(filename, loop, sound);
        mExecutor.execute(player);
        return player;
    }

    /**
     * Used to off sound in GamePanel
     * @param sound false if sound off
     */
    static void setSound(boolean sound) {
        mSound = sound;
    }
}
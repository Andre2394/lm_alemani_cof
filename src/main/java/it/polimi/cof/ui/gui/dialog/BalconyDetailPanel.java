package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;

import javax.swing.*;
import java.awt.*;

/**
* Panel that provide balcony information
 */
public class BalconyDetailPanel extends JFXPanel {

    private BalconyDetailPanel(String region, String[] colors) {
        Parent root = GuiUtils.loadFxml(this, "dialog_balcony_detail");
        Label label = (Label) root.lookup("#regionBalcony");
        Canvas canvas = (Canvas) root.lookup("#canvasBalcony");
        Platform.runLater(() -> {
            label.setText(String.format("%s region's balcony", region));
            Painter.drawBalcony(canvas, colors);
        });
    }

    /**
     * Handle dialog creation for Balcony detail
     * @param parent component
     * @param region of balcony
     * @param colors on that balcony
     * @return
     */
    public static JDialog createDialog(Component parent, String region, String[] colors) {
        BalconyDetailPanel panel = new BalconyDetailPanel(region, colors);
        return GuiUtils.createDialog(parent, "Balcony details", 440, 320, panel);
    }
}
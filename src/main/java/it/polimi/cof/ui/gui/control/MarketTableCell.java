package it.polimi.cof.ui.gui.control;

import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.PoliticCard;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableView;

/**
 * This class is used by {@link TableView} to represent the market {@link Item} in the table.
 */
public class MarketTableCell extends TableCell<Item, Item> {

    /**
     * @param item of the market in their relative list
     * @param empty true if an empty list
     */
    @Override
    protected void updateItem(Item item, boolean empty) {
        super.updateItem(item, empty);
        Platform.runLater(() -> {
            if (item == null || empty) {
                setGraphic(null);
            } else {
                setGraphic(drawItem(item));
            }
        });
    }

    private Canvas drawItem(Item item) {
        if (item.getPoliticCard() != null) {
            return drawPoliticCard(item.getPoliticCard());
        } else if (item.getBusinessPermitTile() != null) {
            return drawPermitTile(item.getBusinessPermitTile());
        } else if (item.getAssistant() == 1) {
            return drawAssistant();
        }
        return null;
    }

    private Canvas drawPoliticCard(PoliticCard politicCard) {
        Canvas canvas = new Canvas(80, 120);
        Painter.drawPoliticCard(canvas, politicCard);
        return canvas;
    }

    private Canvas drawPermitTile(BusinessPermitTile businessPermitTile) {
        Canvas canvas = new Canvas(108, 120);
        Painter.drawPermitTile(canvas, businessPermitTile);
        return canvas;
    }

    private Canvas drawAssistant() {
        Canvas canvas = new Canvas(100, 120);
        Painter.drawAssistant(canvas);
        return canvas;
    }
}
package it.polimi.cof.ui.gui.control;

import it.polimi.cof.model.Reward;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

import java.util.Map;

/**
 * This class is used by {@link ListView} to represent the bonus reward item in the list.
 */
public class BonusListCell extends ListCell<Map.Entry<String, Reward>> {

    /**
     * @param item bonus in bonus list cell
     * @param empty true if there's no bonus
     */
    @Override
    protected void updateItem(Map.Entry<String, Reward> item, boolean empty) {
        super.updateItem(item, empty);
        Platform.runLater(() -> {
            if (item == null || empty) {
                setGraphic(null);
            } else {
                Canvas canvas = new Canvas(120, 68);
                Painter.drawBonusCard(canvas, item);
                setGraphic(canvas);
            }
        });
    }
}
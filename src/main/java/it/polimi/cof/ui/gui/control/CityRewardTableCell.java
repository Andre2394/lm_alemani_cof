package it.polimi.cof.ui.gui.control;

import it.polimi.cof.model.City;
import it.polimi.cof.model.Reward;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableView;

/**
 * This class is used by {@link TableView} to represent the city {@link Reward} in the table.
 */
public class CityRewardTableCell extends TableCell<City, Reward> {

    /**
     * @param item reward to display.
     * @param empty true if an empty list.
     */
    @Override
    protected void updateItem(Reward item, boolean empty) {
        super.updateItem(item, empty);
        Platform.runLater(() -> {
            if (item == null || empty) {
                setGraphic(null);
            } else {
                Canvas canvas = new Canvas(300, 50);
                Painter.drawRewardLine(canvas, item);
                setGraphic(canvas);
            }
        });
    }
}
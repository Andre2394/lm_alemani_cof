package it.polimi.cof.ui.gui.control;

import it.polimi.cof.model.City;
import javafx.application.Platform;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

/**
 * This class is used by {@link ListView} to represent the {@link City} item in the list.
 */
public class CityListCell extends ListCell<City> {

    /**
     * @param item city linked
     * @param empty true if empty city list
     */
    @Override
    protected void updateItem(City item, boolean empty) {
        super.updateItem(item, empty);
        Platform.runLater(() -> {
            if (item == null || empty) {
                setText(null);
            } else {
                setText(item.getName());
            }
        });
    }
}

package it.polimi.cof.ui.gui;

import it.polimi.cof.model.*;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.model.market.MarketSession;
import it.polimi.cof.network.NetworkType;
import it.polimi.cof.network.client.ClientConnectionException;
import it.polimi.cof.network.protocol.ErrorCodes;
import it.polimi.cof.ui.AbstractUi;
import it.polimi.cof.ui.UiController;
import javafx.application.Platform;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * This class is built on top of {@link AbstractUi}. It will manage the user interface into a command line window.
 * The MVC pattern interface can be accessed via {@link #getController()} method.
 * Created by Guido on 31/05/2016.
 */
public class GraphicalUserInterface extends AbstractUi implements NetworkSettingMenu.Callback, LoginMenu.Callback, GameConfigurator.Callback, CreateRoom.Callback, GamePanel.Callback {

    /**
     * Defines.
     */
    private static final String CLICK = "click";
    private static final String INFORMATION_MESSAGE = "Information Message";

    /**
     * Panel identifiers.
     */
    private static final String SPLASH_SCREEN_PANEL = "splashScreenPanel";
    private static final String NETWORK_PANEL = "networkPanel";
    private static final String LOGIN_PANEL = "loginPanel";
    private static final String CONFIGURATOR_PANEL = "configPanel";
    private static final String GAME_PANEL = "gamePanel";
    private static final String WAITING_PANEL = "waitingPanel";
    private static final String CREATE_ROOM_PANEL = "createRoomPanel";

    /**
     * Constants.
     */
    private static final int ALERT_SOUND_SECONDS = 5;

    /**
     * Base window used to display the game.
     */
    private JFrame mMainFrame;

    /**
     * Base panel that contains the card layout.
     */
    private JPanel mMainPanel;

    /**
     * Card layout used to switch between panels.
     */
    private CardLayout mCardLayout;

    /**
     * Login panel used to perform the login.
     */
    private LoginMenu mLoginMenu;

    /**
     * Game configuration panel to show only if player is the admin.
     */
    private GameConfigurator mGameConfigurator;

    /**
     * Game panel used to display the game session.
     */
    private GamePanel mGamePanel;

    /**
     * Abstract constructor. You must provide a valid UiController interface while extending this class.
     *
     * @param controller interface used as callback and MVC.
     */
    public GraphicalUserInterface(UiController controller) {
        super(controller);
        mMainFrame = new JFrame("Council of Four");
        mMainFrame.setSize(600, 400);
        mMainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mCardLayout = new CardLayout();
        mMainPanel = new JPanel(mCardLayout);
        mMainFrame.add(mMainPanel);

        showSplashScreen();
        new Thread(this::initializePanels).start();
    }

    /**
     * Show splash screen.
     */
    private void showSplashScreen() {
        SplashScreenPanel splashScreenPanel = new SplashScreenPanel();
        mMainPanel.add(splashScreenPanel, SPLASH_SCREEN_PANEL);
        mCardLayout.show(mMainPanel, SPLASH_SCREEN_PANEL);
        mMainFrame.setVisible(true);
    }

    /**
     * Initialize all game panels. This should be done in background while showing the splash screen.
     */
    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void initializePanels() {
        // initialize texture manager and load all textures
        TextureManager.initialize();
        // initialization
        NetworkSettingMenu networkSettingMenu = new NetworkSettingMenu(this);
        mLoginMenu = new LoginMenu(this);
        mGameConfigurator = new GameConfigurator(this);
        CreateRoom createRoom = new CreateRoom(this);
        mGamePanel = new GamePanel(this);
        WaitingPanel waitingPanel = new WaitingPanel();
        // add to layout manager
        mMainPanel.add(networkSettingMenu, NETWORK_PANEL);
        mMainPanel.add(mLoginMenu, LOGIN_PANEL);
        mMainPanel.add(createRoom, CREATE_ROOM_PANEL);
        mMainPanel.add(mGameConfigurator, CONFIGURATOR_PANEL);
        mMainPanel.add(mGamePanel, GAME_PANEL);
        mMainPanel.add(waitingPanel, WAITING_PANEL);
        mCardLayout.show(mMainPanel, NETWORK_PANEL);
    }

    /**
     * If this method get called, the CardLayout show the NETWORK_PANEL
     */
    @Override
    public void showNetworkSettingMenu() {
        mCardLayout.show(mMainPanel, NETWORK_PANEL);
    }

    /**
     * If this method get called, the CardLayout show the LOGIN_PANEL
     */
    @Override
    public void showLoginMenu() {
        mCardLayout.show(mMainPanel, LOGIN_PANEL);
    }

    /**
     * If this method get called, it is called the method in the LoginMenu class that handle it
     */
    @Override
    public void showLoginErrorMessage() {
        mLoginMenu.showLoginError();
        Audio.play("boom");
    }

    /**
     * If this method get called, it inform the user that he has been added to an existing room
     */
    @Override
    public void showJoinRoomSuccess() {
        Audio.play("boing");
        int res = JOptionPane.showOptionDialog(mMainFrame, "You have been successfully added to an existing room", INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
        if (res == 0) {
            Audio.play(CLICK);
        }
        showWaitingScreenSafely();
    }

    /**
     * If this method get called, the user can't be added to an existing room. So it shows the CREATE_ROOM_PANEL to create his own room.
     */
    @Override
    public void showJoinRoomFailed() {
        mCardLayout.show(mMainPanel, CREATE_ROOM_PANEL);
    }

    /**
     * If this method get called, the user has created his own room. But before play he has to set the configurator so this OptionDialog informs him.
     */
    @Override
    public void showCreatingRoomSuccess() {
        int res = JOptionPane.showOptionDialog(mMainFrame, "You have created successfully the room. \n You can apply configurator", INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
        if (res == 0) {
            Audio.play(CLICK);
        }
    }

    /**
     * If this method get called, the creation room has failed because a faster user has created a room before him and he has been added in.
     */
    @Override
    public void showCreatingRoomFailed() {
        Audio.play("cucu");
        int res = JOptionPane.showOptionDialog(mMainFrame, "You have been added to a new existing room, you're not the admin. \nJoin has been successfully", INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
        if (res == 0) {
            Audio.play(CLICK);
        }
        showWaitingScreenSafely();
    }

    /**
     * Show waiting screen preventing the overlap of the game panel.
     */
    private void showWaitingScreenSafely() {
        if (!mGamePanel.isVisible()) {
            mCardLayout.show(mMainPanel, WAITING_PANEL);
        }
    }

    /**
     * If this method get called, the CardLayout shows the CONFIGURATION_PANEL
     *
     * @param configuration this is the default configuration loaded
     */
    @Override
    public void showGameConfigurator(Configuration configuration) {
        mCardLayout.show(mMainPanel, CONFIGURATOR_PANEL);
        mGameConfigurator.showConfiguration(configuration);
        mMainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);

    }

    /**
     * If this method get called, the CONFIGURATION_PANEL has been confirmed and the CardLayout show the WAITING_PANEL until the players arrived.
     */
    @Override
    public void showGameConfigurationDone() {
        showWaitingScreenSafely();
    }

    /**
     * If this method get called, the user is informed that the configuration chosen is fault.
     *
     * @param errorMessage report why the user has catch this fault.
     */
    @Override
    public void showInvalidGameConfiguration(String errorMessage) {
        Audio.play("cucu");
        int res = JOptionPane.showOptionDialog(mMainFrame, "The provided configuration is not valid!\nError: " + errorMessage, "Invalid configuration", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
        if (res == 0) {
            Audio.play(CLICK);
        }
    }

    /**
     * If this method get called, there are enough players to play and the game start!
     */
    @Override
    public void notifyGameStarted() {
        Audio.play("laser");
        mCardLayout.show(mMainPanel, GAME_PANEL);
        Platform.runLater(() -> {
            mGamePanel.onGameStarted(getController().getGameModel());
            mMainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        });
    }

    /**
     * If this method get called, it is started a new turn
     *
     * @param nickname      user who is current playing turn
     * @param remainingTime seconds who left to the user's turn
     */
    @Override
    public void showGameTurnStarted(String nickname, int remainingTime) {
        Platform.runLater(() -> mGamePanel.onGameTurnStarted(nickname, remainingTime));
        Audio.play("tink");
    }

    /**
     * Advise game of market session start
     */
    @Override
    public void showMarketSessionStarted() {
        Platform.runLater(() -> mGamePanel.onMarketSessionStarted());
    }

    /**
     * If this method get called, it shows in the GamePanel who is selling's turn now and how much time left to him.
     *
     * @param nickname      user who has the "token" during the selling session of the market
     * @param remainingTime time who left to the user referred
     */
    @Override
    public void showMarketSellTurnStarted(String nickname, int remainingTime) {
        Platform.runLater(() -> mGamePanel.onMarketTurnSellStarted(nickname, remainingTime));
        Audio.play("tink");
    }

    /**
     * If this method get called, it shows in the GamePanel who is buying's turn now and how much time left to him.
     *
     * @param nickname      user who has the "token" during the buying session of the market
     * @param remainingTime time who left to the user referred
     */
    @Override
    public void showMarketBuyTurnStarted(String nickname, int remainingTime) {
        Platform.runLater(() -> mGamePanel.onMarketTurnBuyStarted(nickname, remainingTime));
        Audio.play("tink");
    }

    /**
     * Notify ui that a new market item has been added into market session.
     *
     * @param item that has been added.
     */
    @Override
    public void showMarketItemAddedOnSale(Item item) {
        Platform.runLater(() -> mGamePanel.onMarketItemAddedOnSale(item));
    }

    /**
     * Notify ui that a market item has been bought by someone.
     *
     * @param item that has been bought.
     */
    @Override
    public void showMarketItemBought(Item item) {
        Platform.runLater(() -> mGamePanel.onMarketItemBought(item));
    }

    /**
     * Notify ui that market session is finished, a new game session is starting.
     */
    @Override
    public void showMarketSessionFinished() {
        Platform.runLater(() -> mGamePanel.onMarketSessionFinished());
    }

    /**
     * This method update the time left to the user
     *
     * @param remainingTime updated time value in seconds.
     */
    @Override
    public void updateTurnRemainingTime(int remainingTime) {
        Platform.runLater(() -> mGamePanel.setPlayerRemainingTime(remainingTime));
        if (remainingTime <= ALERT_SOUND_SECONDS) {
            Audio.play("ctcp");
        }
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with the card drawn
     */
    @Override
    public void notifyPoliticCardDrawn(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onPoliticCardDraw(updateState));
    }

    /**
     * Update the client actionList item with an updated copy from server.
     * This method is not currently used in the implementation.
     *
     * @param actionList fresh copy from server.
     */
    @Override
    public void showActionList(ActionList actionList) {
        Platform.runLater(() -> mGamePanel.onActionList(actionList));
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with the councilor added
     */
    @Override
    public void notifyCouncilorAdded(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onCouncillorElected(updateState));
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with the business permit tile acquired
     */
    @Override
    public void notifyBusinessPermitTileAcquired(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onBusinessPermitTileAcquired(updateState));
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with emporium built with business permit tile
     */
    @Override
    public void notifyEmporiumBuiltWithBusinessPermitTile(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onEmporiumBuiltWithPermitTile(updateState));
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with emporium built with king help
     */
    @Override
    public void notifyEmporiumBuiltWithKingHelp(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onEmporiumBuiltWithKingHelp(updateState));
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with assistant engaged
     */
    @Override
    public void notifyAssistantEngaged(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onAssistantEngaged(updateState));
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with business permit tiles changed
     */
    @Override
    public void notifyBusinessPermitTilesChanged(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onBusinessPermitTilesChanged(updateState));
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with assistant sent to elect councillor
     */
    @Override
    public void notifyAssistantSentToElectCouncillor(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onAssistantSentToElectCouncillor(updateState));
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with new additional main action available
     */
    @Override
    public void notifyNewAdditionalMainActionAvailable(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onAdditionalMainActionAcquired(updateState));
    }

    /**
     * This method clarify why are you wrong in a play-try
     *
     * @param errorCode integer that clarify what is your error.
     */
    @Override
    public void showActionNotValid(int errorCode) {
        showError(errorCode);
    }

    /**
     * Notify ui to open the dialog where the user can retrieve his first special rewards.
     *
     * @param count of the rewards that the player can take.
     */
    @Override
    public void showFirstSpecialRewards(int count) {
        mGamePanel.showFirstSpecialRewards(count);
    }

    /**
     * Notify ui to open the dialog where the user can retrieve his second special rewards.
     *
     * @param count of the rewards that the player can take.
     */
    @Override
    public void showSecondSpecialRewards(int count) {
        mGamePanel.showSecondSpecialRewards(count);
    }

    /**
     * Notify ui to open the dialog where the user can retrieve his third special rewards.
     *
     * @param count of the rewards that the player can take.
     */
    @Override
    public void showThirdSpecialRewards(int count) {
        mGamePanel.showThirdSpecialRewards(count);
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with first special rewards earned
     */
    @Override
    public void notifyFirstSpecialRewardsEarned(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onFirstSpecialRewardsEarned(updateState));
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with second special rewards earned
     */
    @Override
    public void notifySecondSpecialRewardsEarned(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onSecondSpecialRewardsEarned(updateState));
    }

    /**
     * Handle the update of game state because of the event
     *
     * @param updateState with third special rewards earned
     */
    @Override
    public void notifyThirdSpecialRewardsEarned(UpdateState updateState) {
        Platform.runLater(() -> mGamePanel.onThirdSpecialRewardsEarned(updateState));
    }

    /**
     * Method who show new chat messages in relative TextField
     *
     * @param privateMessage if there's something new or not
     * @param author         who wrote the message
     * @param message        field of the message
     */
    @Override
    public void showChatMessage(boolean privateMessage, String author, String message) {
        Platform.runLater(() -> mGamePanel.showChatMessage(privateMessage, author, message));
        Audio.play("query");
    }

    /**
     * In the GamePanel it shows who is the disconnected Player
     *
     * @param nickname audioPlayer who disconnected
     */
    @Override
    public void showPlayerDisconnected(String nickname) {
        Platform.runLater(() -> mGamePanel.onPlayerDisconnected(nickname));
        Audio.play("Twang");
    }

    /**
     * Notify ui that the last game turn is started.
     *
     * @param nickname of the player that has built all his emporiums.
     */
    @Override
    public void showLastTurnStarted(String nickname) {
        Platform.runLater(() -> mGamePanel.onLastTurnStarted(nickname));
    }

    /**
     * Handle the update of game state because of the event and give a ranking list
     *
     * @param updateStates final updateState
     * @param ranking      list of the game
     */
    @Override
    public void notifyGameEnded(UpdateState[] updateStates, List<String> ranking) {
        Platform.runLater(() -> mGamePanel.onGameEnded(updateStates, ranking));
    }

    /**
     * @param networkType type of network RMI or Socket connection
     * @param address     address of the server
     * @param port        port of the server
     * @throws ClientConnectionException
     */
    @Override
    public void setNetworkSettings(NetworkType networkType, String address, int port) throws ClientConnectionException {
        getController().setNetworkSettings(networkType, address, port);
    }

    /**
     * @param nickname nickname to use to login
     */
    @Override
    public void tryLogin(String nickname) {
        getController().loginPlayer(nickname);
    }

    /**
     * @param configuration configuration files generated by the users choice
     */
    @Override
    public void onGameConfigured(Configuration configuration) {
        getController().onGameConfigured(configuration);
    }

    /**
     * @param numberPlayers maximum players in the room choice by the user
     */
    @Override
    public void tryCreateRoom(int numberPlayers) {
        getController().createRoom(numberPlayers);
    }

    /**
     * @return the nickname of the Player
     */
    @Override
    public String getNickname() {
        return getController().getNickname();
    }

    /**
     * @param receiver if message start with @nickname, the message will be sent only to him
     * @param message  field of the message sent
     */
    @Override
    public void sendChatMessage(String receiver, String message) {
        getController().sendChatMessage(receiver, message);
        Audio.play(CLICK);
    }

    /**
     * Used to draw a politic card
     */
    @Override
    public void drawPoliticCard() {
        getController().drawPoliticCard();
    }

    /**
     * Used to end a turn
     */
    @Override
    public void endTurn() {
        getController().endTurn();
    }

    /**
     * Used to load GamePanel with configurations
     *
     * @return the game model generated
     */
    @Override
    public BaseGame getModel() {
        return getController().getGameModel();
    }

    /**
     * Retrieve the current market session from internal bus.
     *
     * @return the current market session.
     */
    @Override
    public MarketSession getMarketSession() {
        return getController().getMarketSession();
    }

    /**
     * Used to elect a councillor.
     *
     * @param councilor going to be elect
     * @param region    in which councillor must be elect
     */
    @Override
    public void electCouncillor(Councilor councilor, String region) {
        getController().actionElectCouncillor(councilor, region);
    }

    /**
     * Used to acquire business permit tile.
     *
     * @param politicCards    used to satisfy the region's balcony.
     * @param region          where the permit tile is placed.
     * @param permitTileIndex index of the permit tile to acquire.
     */
    @Override
    public void acquireBusinessPermitTile(List<PoliticCard> politicCards, String region, int permitTileIndex) {
        getController().actionAcquireBusinessPermitTile(politicCards, region, permitTileIndex);
    }

    /**
     * Callback method used to send to the server the intent to build an emporium with a permit tile.
     *
     * @param businessPermitTile to use to make the action.
     * @param city               where the emporium should be built.
     */
    @Override
    public void buildEmporiumWithBusinessPermitTile(BusinessPermitTile businessPermitTile, String city) {
        getController().actionBuildEmporiumWithBusinessPermitTile(businessPermitTile, city);
    }

    /**
     * Callback method used to send to the server the intent to build an emporium with the help of the king.
     *
     * @param politicCards to use to satisfy the king's balcony.
     * @param cities       sorted list of the city where the king should move.
     */
    @Override
    public void buildEmporiumWithKingHelp(List<PoliticCard> politicCards, List<String> cities) {
        getController().actionBuildEmporiumWithKingHelp(politicCards, cities);
    }

    /**
     * Callback method used to send to the server the intent to engage an assistant.
     */
    @Override
    public void engageAssistant() {
        getController().actionEngageAssistant();
    }

    /**
     * Callback method used to send to the server the intent to change the visible permit tiles.
     *
     * @param region where the permit tiles should be changed.
     */
    @Override
    public void changeBusinessPermitTile(String region) {
        getController().actionChangeBusinessPermitTile(region);
    }

    /**
     * Callback method used to send to the server the intent to send an assistant to elect a councillor.
     *
     * @param councilor to elect.
     * @param region    where the councillor should be elected.
     */
    @Override
    public void sendAssistantElectCouncillor(Councilor councilor, String region) {
        getController().actionSendAssistantElectCouncillor(councilor, region);
    }

    /**
     * Callback method used to buy an additional main action.
     */
    @Override
    public void performAdditionalMainAction() {
        getController().actionPerformAdditionalMainAction();
    }

    /**
     * Callback method used to earn the first special rewards.
     *
     * @param cities where the player want to retrieve the bonus.
     */
    @Override
    public void earnFirstSpecialRewards(List<String> cities) {
        getController().earnFirstSpecialRewards(cities);
    }

    /**
     * Callback method used to earn the second special rewards.
     *
     * @param regions where the player want to take the permit tile.
     * @param indices list of index of the permit tiles to take.
     */
    @Override
    public void earnSecondSpecialRewards(List<String> regions, List<Integer> indices) {
        getController().earnSecondSpecialRewards(regions, indices);
    }

    /**
     * Callback method used to earn the third special rewards.
     *
     * @param businessPermitTiles list of permit tiles to use.
     */
    @Override
    public void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles) {
        getController().earnThirdSpecialRewards(businessPermitTiles);
    }

    /**
     * Callback method used to sell a politic card on the market.
     *
     * @param politicCard to sell.
     * @param price       to set.
     */
    @Override
    public void sellPoliticCard(PoliticCard politicCard, int price) {
        getController().sellPoliticCard(politicCard, price);
    }

    /**
     * Callback method used to sell a business permit tile on the market.
     *
     * @param businessPermitTile to sell.
     * @param price              to set.
     */
    @Override
    public void sellBusinessPermitTile(BusinessPermitTile businessPermitTile, int price) {
        getController().sellBusinessPermitTile(businessPermitTile, price);
    }

    /**
     * Callback method used to sell an assistant on the market.
     *
     * @param price to set.
     */
    @Override
    public void sellAssistant(int price) {
        getController().sellAssistant(price);
    }

    /**
     * Callback method used to buy an item from the market.
     *
     * @param marketId of the item to buy.
     */
    @Override
    public void buyItem(String marketId) {
        getController().buyItem(marketId);
    }

    /**
     * Callback method used to show an incoming error from server or from ui-actions.
     *
     * @param code specific error code. {@link ErrorCodes};
     */
    @Override
    public void showError(int code) {
        Audio.play("boom");
        int res = JOptionPane.showOptionDialog(null, getErrorMessage(code), "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
        if (res == 0) {
            Audio.play(CLICK);
        }
    }
}
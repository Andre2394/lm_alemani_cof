package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.*;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.components.Painter;
import it.polimi.cof.ui.gui.control.PermitTileListCell;
import it.polimi.cof.ui.gui.control.PoliticCardListCell;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
* Class that handle acquire of a permit tile
 **/
public class AcquirePermitTilePanel extends JFXPanel {

    private final BaseGame mBaseGame;
    private final Callback mCallback;
    private final Player mPlayer;

    private Canvas mBalconyCanvas;
    private ListView<BusinessPermitTile> mBusinessPermitTileList;
    private ChoiceBox<String> mRegionBoard;
    private ListView<PoliticCard> mPoliticCardList;

    private BusinessPermitTile mSelectedPermitTile;

    /** Settings of fxml and information
     * @param player that ask to acquire
     * @param baseGame game state
     * @param callback to graphical user interface
     */
    @SuppressWarnings("unchecked")
    private AcquirePermitTilePanel(String player, BaseGame baseGame, Callback callback) {
        mBaseGame = baseGame;
        mCallback = callback;
        mPlayer = mBaseGame.getPlayer(player);
        Parent root = GuiUtils.loadFxml(this, "dialog_acquire_permit_tile");
        mBalconyCanvas = (Canvas) root.lookup("#canvasBalcony");
        mBusinessPermitTileList = (ListView<BusinessPermitTile>) root.lookup("#permitTileList");
        mRegionBoard = (ChoiceBox<String>) root.lookup("#regionChoiceBox");
        mPoliticCardList = (ListView<PoliticCard>) root.lookup("#politicCardList");
        Button applyButton = (Button) root.lookup("#applyButton");
        Button cancelButton = (Button) root.lookup("#cancelButton");
        mBusinessPermitTileList.setCellFactory(param -> new PermitTileListCell());
        mBusinessPermitTileList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mSelectedPermitTile = newValue;
            updateApplyButtonVisibility(applyButton);
        });
        applyButton.setOnAction(event -> acquirePermitTile());
        cancelButton.setOnAction(event -> mCallback.onCancel());
        mPoliticCardList.setCellFactory(param -> new PoliticCardListCell());
        mPoliticCardList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        mPoliticCardList.getSelectionModel().getSelectedItems().addListener((ListChangeListener<PoliticCard>) c ->
                Platform.runLater(() -> updateApplyButtonVisibility(applyButton))
        );
        mRegionBoard.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
                Platform.runLater(() -> {
                    drawBalcony(newValue);
                    mBusinessPermitTileList.setItems(FXCollections.observableList(mBaseGame.getVisiblePermitTiles(newValue)));
                }));
        Platform.runLater(() -> {
            mPoliticCardList.setItems(FXCollections.observableList(mPlayer.getPoliticCards()));
            mRegionBoard.setItems(FXCollections.observableList(Arrays.asList(RegionBoard.LEFT_REGION, RegionBoard.CENTER_REGION, RegionBoard.RIGHT_REGION)));
            mRegionBoard.getSelectionModel().selectFirst();
        });
    }

    /**
     * Handle dialog creation about permit tile
     * @param parent of the panel
     * @param player involved in 2nd action
     * @param baseGame game state
     * @param callback to graphical user interface class
     * @return
     */
    public static JDialog createDialog(Component parent, String player, BaseGame baseGame, Callback callback) {
        AcquirePermitTilePanel panel = new AcquirePermitTilePanel(player, baseGame, callback);
        return GuiUtils.createDialog(parent, "Main action 2", 750, 600, panel);
    }

    private void drawBalcony(String region) {
        Painter.drawBalcony(mBalconyCanvas, mBaseGame.getBalconyColors(region));
    }

    private void updateApplyButtonVisibility(Button applyButton) {
        int politicCardCount = mPoliticCardList.getSelectionModel().getSelectedIndices().size();
        applyButton.setDisable(politicCardCount <= 0 || politicCardCount > 4 || mSelectedPermitTile == null);
    }

    private void acquirePermitTile() {
        List<PoliticCard> politicCards = new ArrayList<>(mPoliticCardList.getSelectionModel().getSelectedItems());
        int index = mBaseGame.getVisiblePermitTileIndex(mRegionBoard.getValue(), mSelectedPermitTile);
        mCallback.onBusinessPermitTileAcquired(politicCards, mRegionBoard.getValue(), index);
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * * Handle event, call main class to response
         * @param politicCards spent of same color of the councillor in that council
         * @param region of the council
         * @param permitTileIndex 1 if it's first card of region, 2 if it's the twice
         */
        void onBusinessPermitTileAcquired(List<PoliticCard> politicCards, String region, int permitTileIndex);

        /**
         * Disable current window
         */
        void onCancel();
    }
}
package it.polimi.cof.ui.gui.control;

import it.polimi.cof.model.Reward;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

/**
 * This class is used by {@link ListView} to represent the {@link Reward} item in the list.
 */
public class KingRewardListCell extends ListCell<Reward> {

    /**
     * @param item kingreward in their representative list
     * @param empty true if an empty list
     */
    @Override
    protected void updateItem(Reward item, boolean empty) {
        super.updateItem(item, empty);
        Platform.runLater(() -> {
            if (item == null || empty) {
                setGraphic(null);
            } else {
                Canvas canvas = new Canvas(120, 68);
                Painter.drawKingRewardCard(canvas, item);
                setGraphic(canvas);
            }
        });
    }
}
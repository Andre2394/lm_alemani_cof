package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.City;
import it.polimi.cof.model.Player;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.control.PermitTileListCell;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Panel that handle building an emporium using business permit tile
 */
public class BuildEmporiumPermitTilePanel extends JFXPanel {

    private final BaseGame mBaseGame;
    private final Callback mCallback;
    private final Player mPlayer;

    private ListView<BusinessPermitTile> mBusinessPermitTiles;
    private ListView<String> mCities;

    private BusinessPermitTile mBusinessPermitTile;
    private String mSelectedCity;

    @SuppressWarnings("unchecked")
    private BuildEmporiumPermitTilePanel(String player, BaseGame baseGame, Callback callback) {
        mPlayer = baseGame.getPlayer(player);
        mBaseGame = baseGame;
        mCallback = callback;
        Parent root = GuiUtils.loadFxml(this, "dialog_build_emporium_permit_tile");
        mBusinessPermitTiles = (ListView<BusinessPermitTile>) root.lookup("#businessPermitTileListView");
        mCities = (ListView<String>) root.lookup("#cityListView");
        Button applyButton = (Button) root.lookup("#applyButton");
        Button cancelButton = (Button) root.lookup("#cancelButton");
        mBusinessPermitTiles.setCellFactory(param -> new PermitTileListCell());
        mBusinessPermitTiles.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mBusinessPermitTile = newValue;
            Platform.runLater(() -> {
                loadCities();
                checkApplyButtonVisibility(applyButton);
            });
        });
        mCities.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mSelectedCity = newValue;
            Platform.runLater(() -> checkApplyButtonVisibility(applyButton));
        });
        applyButton.setOnAction(event -> buildEmporium());
        cancelButton.setOnAction(event -> mCallback.onCancel());
        Platform.runLater(() -> mBusinessPermitTiles.setItems(FXCollections.observableList(mPlayer.getBusinessPermitTiles())));
    }

    /**
     * Handle dialog creation for Emporium building
     * @param parent component
     * @param player involved
     * @param baseGame game state
     * @param callback to GraphicalUserInterface
     * @return
     */
    public static JDialog createDialog(Component parent, String player, BaseGame baseGame, Callback callback) {
        BuildEmporiumPermitTilePanel panel = new BuildEmporiumPermitTilePanel(player, baseGame, callback);
        return GuiUtils.createDialog(parent, "Main action 3", 800, 600, panel);
    }

    private void buildEmporium() {
        mCallback.onEmporiumBuiltWithPermitTile(mBusinessPermitTile, mSelectedCity);
    }

    private void loadCities() {
        if (mBusinessPermitTile != null) {
            List<City> cities = mBaseGame.getCitiesWithInitial(mBusinessPermitTile.getCities());
            cities.removeAll(mBaseGame.getCitiesWherePlayerHasBuilt(mPlayer.getNickname()));
            List<String> cityList = cities.stream().map(City::getName).collect(Collectors.toList());
            mCities.setItems(FXCollections.observableList(cityList));
            mCities.getSelectionModel().selectFirst();
        }
    }

    private void checkApplyButtonVisibility(Button applyButton) {
        applyButton.setDisable(mBusinessPermitTile == null || mSelectedCity == null);
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * Emporium built using business permit tile in a city who starts with letters on business permit tile
         * @param businessPermitTile chosen for the building of Emporium
         * @param city in which emporium has been built
         */
        void onEmporiumBuiltWithPermitTile(BusinessPermitTile businessPermitTile, String city);

        /**
         * Disable current window
         */
        void onCancel();
    }
}
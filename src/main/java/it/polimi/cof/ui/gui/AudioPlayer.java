package it.polimi.cof.ui.gui;

import it.polimi.cof.util.Debug;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;

/**
 * This class occurs about create a Clip audio given a filename audio or stop a Clip audio of a specify AudioPlayer instance.
 * Created by Guido on 04/07/2016.
 */
class AudioPlayer implements Runnable, LineListener {

    private boolean playCompleted;
    private String mFilename;
    private Clip audioClip;
    private boolean mLoop = false;
    private boolean mSound = true;

    /**
     * Play a given audio file.
     *
     * @param filename Path of the audio file.
     */
    AudioPlayer(String filename) {
        mFilename = filename;
    }

    /**
     * This constructor is used if the sound must be in loop until the player stop it with relative method.
     *
     * @param filename Path of the audio file
     * @param loop     true if loop
     */
    AudioPlayer(String filename, boolean loop) {
        mFilename = filename;
        mLoop = loop;
    }

    /**
     * Constructor used in GamePanel if sound turn on
     * @param filename of audio clip during the game
     * @param loop of audioclip during the game
     * @param sound activate all sounds on game panel.
     */
    AudioPlayer(String filename, boolean loop, boolean sound) {
        mFilename = filename;
        mLoop = loop;
        mSound = sound;
    }

    /**
     * This method occurs of closing an audio clip of this object.
     */
    void stopSound() {
        mSound = false;
        audioClip.close();
    }


    /**
     * Body of the thread that play the audio clip without interrupting main thread.
     */
    @Override
    public void run() {
        if (!mSound) {
            return;
        }
        try {
            URL url = getClass().getResource("/audio/" + mFilename + ".wav");
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(url);
            AudioFormat format = audioStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            audioClip = (Clip) AudioSystem.getLine(info);
            audioClip.addLineListener(this);
            audioClip.open(audioStream);
            FloatControl gainControl = (FloatControl) audioClip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(-10.0f); // Reduce volume by 10 decibels.
            audioClip.start();
            if (mLoop) {
                audioClip.loop(Clip.LOOP_CONTINUOUSLY);
            }
            while (!playCompleted) {
                // wait for the playback completes
                checkAudioComplete();
            }
        } catch (UnsupportedAudioFileException ex) {
            Debug.error("The specified audio file is not supported. " + mFilename, ex);
        } catch (LineUnavailableException ex) {
            Debug.error("Audio line for playing back is unavailable.", ex);
        } catch (IOException ex) {
            Debug.error("Error playing the audio file.", ex);
        } catch (NullPointerException npe) {
            Debug.error("Error in file audio name, cannot find file: " + mFilename, npe);
        } finally {
            audioClip.close();
        }
    }

    private void checkAudioComplete() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Listens to the START and STOP events of the audio line.
     */
    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();
        if (type == LineEvent.Type.START) {
            Debug.verbose("Playback started.");
            Debug.debug("Playback started.");
        } else if (type == LineEvent.Type.STOP) {
            playCompleted = true;
            Debug.debug("Playback completed.");
        }
    }
}
package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.City;
import it.polimi.cof.model.Player;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.components.Painter;
import it.polimi.cof.ui.gui.control.PlayerListCell;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Panel that handle city detail
 */
public class CityDetailPanel extends JFXPanel {

    @SuppressWarnings("unchecked")
    private CityDetailPanel(City city, BaseGame baseGame) {
        Parent root = GuiUtils.loadFxml(this, "dialog_city_detail");
        Canvas image = (Canvas) root.lookup("#cityCanvas");
        Label name = (Label) root.lookup("#cityName");
        Label type = (Label) root.lookup("#cityType");
        Canvas reward = (Canvas) root.lookup("#rewardCanvas");
        ListView<Player> emporiums = (ListView<Player>) root.lookup("#cityEmporiumList");
        emporiums.setCellFactory(param -> new PlayerListCell());
        Platform.runLater(() -> {
            Painter.drawCity(image, city);
            name.setText(city.getName());
            type.setText(String.format("Type: %s city", city.getType()));
            Painter.drawRewardLine(reward, city.getReward());
            emporiums.setItems(getPlayers(baseGame, getEmporiumList(city)));
        });
    }

    /**
     * Handle dialog creation of city detail
     * @param parent root
     * @param city involved
     * @param baseGame base state of the game
     * @return
     */
    public static JDialog createDialog(Component parent, City city, BaseGame baseGame) {
        CityDetailPanel panel = new CityDetailPanel(city, baseGame);
        return GuiUtils.createDialog(parent, String.format("%s details", city.getName()), 550, 400, panel);
    }

    private ObservableList<Player> getPlayers(BaseGame baseGame, List<String> emporiums) {
        return FXCollections.observableArrayList(emporiums.stream().map(baseGame::getPlayer).collect(Collectors.toList()));
    }

    private ObservableList<String> getEmporiumList(City city) {
        ObservableList<String> emporiums = FXCollections.observableArrayList();
        emporiums.addAll(city.getEmporiums());
        return emporiums;
    }
}
package it.polimi.cof.ui.gui.components;

import it.polimi.cof.model.KingBoard;
import it.polimi.cof.model.RegionBoard;
import it.polimi.cof.ui.gui.RegionBoardCallback;
import it.polimi.cof.ui.gui.TextureManager;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

/**
 * This class represent the panel of the game during configuration and game time.
 * Created by andre on 27/06/2016.
 */
public class GameBoardPane extends Region {

    /**
     * True if the map is in game mode, false if in configurator mode.
     */
    private final boolean mGameMode;
    /**
     * Callback used to send notification of mouse events on the map.
     */
    private final RegionBoardCallback mCallback;
    /**
     * Left region pane.
     */
    private RegionPane mLeftRegionPane;
    /**
     * Center region pane.
     */
    private RegionPane mCenterRegionPane;
    /**
     * Right region pane.
     */
    private RegionPane mRightRegionPane;
    /**
     * King region pane.
     */
    private KingPane mKingRegionPane;
    /**
     * Background image.
     */
    private ImageView mBackgroundImage;

    /**
     * Settings of the GUI
     * @param gameMode true if is in game mode, false in in configurator mode
     * @param callback to graphical user interface
     */
    public GameBoardPane(boolean gameMode, RegionBoardCallback callback) {
        mGameMode = gameMode;
        mCallback = callback;
        GridPane gridPane = new GridPane();
        registerParentColumnRowRules(gridPane);
        gridPane.add(createMap(), 0, 0);
        gridPane.add(createKingPane(), 0, 1);
        getChildren().add(gridPane);
    }

    private void registerParentColumnRowRules(GridPane gridPane) {
        ColumnConstraints column1 = new ColumnConstraints();
        RowConstraints row1 = new RowConstraints();
        RowConstraints row2 = new RowConstraints();
        column1.setPercentWidth(100);
        row1.setPercentHeight(mGameMode ? 80 : 100);
        row2.setPercentHeight(mGameMode ? 20 : 0);
        gridPane.getColumnConstraints().add(column1);
        gridPane.getRowConstraints().addAll(row1, row2);
    }

    private Node createMap() {
        StackPane stackPane = new StackPane();
        initializeBackgroundImage(stackPane);
        initializeRegionBoards(stackPane);
        return stackPane;
    }

    /**
     * Create the background imageView and bind his width and height property to the parent in order to properly resize
     * itself whenever the parent changes his size.
     *
     * @param stackPane where the imageView should be added.
     */
    private void initializeBackgroundImage(StackPane stackPane) {
        mBackgroundImage = new ImageView(TextureManager.get("board", "jpg"));
        stackPane.getChildren().add(mBackgroundImage);
    }

    /**
     * Create a GridPane with three column of the same width. Inside every column is placed a RegionPane that will render
     * the map in that region. It will display the cities of that region, the link between cities, the balcony and the
     * business permit tiles.
     *
     * @param stackPane where the gridPane should be added.
     */
    private void initializeRegionBoards(StackPane stackPane) {
        GridPane gridPane = new GridPane();
        registerColumnRowRules(gridPane);
        initializeRegionPanes(gridPane);
        stackPane.getChildren().add(gridPane);
    }

    /**
     * Register to the gridView the rules of every columns and the single row.
     *
     * @param gridPane where the rules should be applied.
     */
    private void registerColumnRowRules(GridPane gridPane) {
        ColumnConstraints column1 = new ColumnConstraints();
        ColumnConstraints column2 = new ColumnConstraints();
        ColumnConstraints column3 = new ColumnConstraints();
        RowConstraints row1 = new RowConstraints();
        column1.setPercentWidth(33.33d);
        column2.setPercentWidth(33.33d);
        column3.setPercentWidth(33.33d);
        row1.setPercentHeight(100);
        gridPane.getColumnConstraints().addAll(column1, column2, column3);
        gridPane.getRowConstraints().add(row1);
    }

    private Node createKingPane() {
        mKingRegionPane = new KingPane(mCallback);
        return mKingRegionPane;
    }

    /**
     * Initialize all region panes and add them to the gridView.
     *
     * @param gridPane where the region panes should be added.
     */
    private void initializeRegionPanes(GridPane gridPane) {
        mLeftRegionPane = new RegionPane(mCallback, RegionPane.Position.LEFT, !mGameMode);
        mCenterRegionPane = new RegionPane(mCallback, RegionPane.Position.CENTER, !mGameMode);
        mRightRegionPane = new RegionPane(mCallback, RegionPane.Position.RIGHT, !mGameMode);
        gridPane.add(mLeftRegionPane, 0, 0);
        gridPane.add(mCenterRegionPane, 1, 0);
        gridPane.add(mRightRegionPane, 2, 0);
        widthProperty().addListener((observable, oldValue, newValue) -> {
            mLeftRegionPane.widthProperty().setValue(newValue.doubleValue() / 3);
            mCenterRegionPane.widthProperty().setValue(newValue.doubleValue() / 3);
            mRightRegionPane.widthProperty().setValue(newValue.doubleValue() / 3);
            mBackgroundImage.fitWidthProperty().setValue(newValue);
            if (mKingRegionPane != null) {
                mKingRegionPane.setWidth(newValue.doubleValue());
            }
        });
        heightProperty().addListener((observable, oldValue, newValue) -> {
            double value = mGameMode ? 0.8d : 1d;
            mLeftRegionPane.heightProperty().setValue(newValue.doubleValue() * value);
            mCenterRegionPane.heightProperty().setValue(newValue.doubleValue() * value);
            mRightRegionPane.heightProperty().setValue(newValue.doubleValue() * value);
            mBackgroundImage.fitHeightProperty().setValue(newValue.doubleValue() * value);
            if (mKingRegionPane != null) {
                mKingRegionPane.setHeight(newValue.doubleValue() * 0.2d);
            }
        });
    }

    /**
     * Public setter method for left region board
     * @param regionBoard going to be set
     */
    public void setLeftRegionBoard(RegionBoard regionBoard) {
        mLeftRegionPane.setRegionBoard(regionBoard);
    }

    /**
     * Public setter method for center region board
     * @param regionBoard going to be set
     */
    public void setCenterRegionBoard(RegionBoard regionBoard) {
        mCenterRegionPane.setRegionBoard(regionBoard);
    }

    /**
     * Public setter method for right region board
     * @param regionBoard going to be set
     */
    public void setRightRegionBoard(RegionBoard regionBoard) {
        mRightRegionPane.setRegionBoard(regionBoard);
    }

    /**
     * Public setter method for king region board
     * @param kingRegionBoard going to be set
     */
    public void setKingRegionBoard(KingBoard kingRegionBoard) {
        mKingRegionPane.setKingBoard(kingRegionBoard);
    }

    /**
     * Used to 'clear' all boards
     */
    public void invalidate() {
        mLeftRegionPane.invalidate();
        mCenterRegionPane.invalidate();
        mRightRegionPane.invalidate();
        mKingRegionPane.invalidate();
    }
}
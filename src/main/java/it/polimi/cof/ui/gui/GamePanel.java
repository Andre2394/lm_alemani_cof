package it.polimi.cof.ui.gui;

import it.polimi.cof.model.*;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.model.market.MarketSession;
import it.polimi.cof.network.protocol.ErrorCodes;
import it.polimi.cof.ui.gui.components.GameBoardPane;
import it.polimi.cof.ui.gui.control.*;
import it.polimi.cof.ui.gui.dialog.*;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;

import javax.swing.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class is about Game window and handle all relative components. ( game map, chat, game components, etc )
 * It is the next after configuration panel for Admin root and the first game windows for all others clients.
 * Created by Guido on 13/06/2016.
 */
/*package-local*/ class GamePanel extends JFXPanel implements RegionBoardCallback, ElectCouncillorPanel.Callback, AcquirePermitTilePanel.Callback, BuildEmporiumPermitTilePanel.Callback, BuildEmporiumKingHelpPanel.Callback, SendAssistantElectCouncillorPanel.Callback, ChangePermitTilesPanel.Callback, FastActionReminderPanel.Callback, MarketSellItemPanel.Callback, PlayerListCell.PlayerCallback, FirstSpecialBonusPanel.Callback, SecondSpecialBonusPanel.Callback, ThirdSpecialBonusPanel.Callback {

    private static final String TABLE_CELL_STYLE = "-fx-alignment: CENTER;";

    private static final String TURN_GAME = "game";
    private static final String TURN_MARKET_SELL = "market sell";
    private static final String TURN_MARKET_BUY = "market buy";
    private static final int PAGE_OF_TUTORIAL = 7;
    private static final String CLICK = "click";

    private final Callback mCallback;
    private AudioPlayer audioPlayer;
    private boolean sound = true;

    // Tabs
    private TabPane mTabPane;
    private VBox mGameBox;
    private VBox mMarketBox;

    //Tab Tutorial
    private ImageView[] slides;
    private int mPageNumber;
    // Top toolbar
    private Label mPlayerTurnLabel;

    private TextArea mChatTextArea;
    private TextField mChatTextField;

    // Right box
    private Circle mPlayerColor;
    private Label mPlayerNickname;
    private Label mPlayerAssistantLabel;
    private Label mPlayerEmporiumLabel;
    private Label mPlayerCoinLabel;
    private Label mPlayerVictoryPointLabel;
    private Label mPlayerNobilityPositionLabel;
    private ListView<PoliticCard> mPoliticCardList;
    private ListView<BusinessPermitTile> mPermitTileList;
    private ListView<BusinessPermitTile> mUsedPermitTileList;
    private ListView<Map.Entry<String, Reward>> mBonusList;
    private ListView<Reward> mRewardKingCardList;
    private ListView<Player> mPlayerList;
    private Button mSoundButton;
    private TableView<Item> mMarketTable;
    private Button mActionSellPoliticCard;
    private Button mActionSellPermitTile;
    private Button mActionSellAssistant;
    private Button mActionBuyItem;

    private ActionList mActionList;
    private String mCurrentMarketPlayer;

    private GameBoardPane mGameBoard;

    private JDialog mOpenDialog;

    /**
     * Prepare events and information for the scene.
     *
     * @param callback to the main class.
     */
    @SuppressWarnings("unchecked")
    /*package-local*/ GamePanel(Callback callback) {
        mCallback = callback;
        Parent root = GuiUtils.loadFxml(this, "sG");
        createGameBoard(root);
        createMarketTable(root);
        mTabPane = (TabPane) root.lookup("#mainTabPane");
        mSoundButton = (Button) root.lookup("#soundButton");
        mPlayerTurnLabel = (Label) root.lookup("#playerTurnLabel");
        mChatTextArea = (TextArea) root.lookup("#chatTextArea");
        mChatTextField = (TextField) root.lookup("#chatTextField");
        Button chatSendMessage = (Button) root.lookup("#chatSendMessage");
        mPlayerColor = (Circle) root.lookup("#playerColor");
        mPlayerNickname = (Label) root.lookup("#playerNickname");
        mPlayerAssistantLabel = (Label) root.lookup("#assistantLabel");
        mPlayerEmporiumLabel = (Label) root.lookup("#emporiumLabel");
        mPlayerCoinLabel = (Label) root.lookup("#coinLabel");
        mPlayerVictoryPointLabel = (Label) root.lookup("#victoryPointsLabel");
        mPlayerNobilityPositionLabel = (Label) root.lookup("#nobilityTrackPositionLabel");
        mGameBox = (VBox) root.lookup("#gameBox");
        mMarketBox = (VBox) root.lookup("#marketBox");
        Button actionDrawPoliticCard = (Button) root.lookup("#actionDrawPoliticCard");
        Button actionEndTurn = (Button) root.lookup("#actionEndTurn");
        Button actionElectCouncillor = (Button) root.lookup("#actionElectCouncillor");
        Button actionAcquirePermitTile = (Button) root.lookup("#actionAcquirePermitTile");
        Button actionBuildEmporiumPermitTile = (Button) root.lookup("#actionBuildEmporiumPermitTile");
        Button actionBuildEmporiumKingHelp = (Button) root.lookup("#actionBuildEmporiumKingHelp");
        Button actionEngageAssistant = (Button) root.lookup("#actionEngageAssistant");
        Button actionChangeBusinessPermitTile = (Button) root.lookup("#actionChangeBusinessPermitTile");
        Button actionSendAssistantElectCouncillor = (Button) root.lookup("#actionSendAssistantElectCouncillor");
        Button actionPerformAdditionalMainAction = (Button) root.lookup("#actionPerformAdditionalMainAction");
        mActionSellPoliticCard = (Button) root.lookup("#sellPoliticCardButton");
        mActionSellPermitTile = (Button) root.lookup("#sellPermitTileButton");
        mActionSellAssistant = (Button) root.lookup("#sellAssistantButton");
        mActionBuyItem = (Button) root.lookup("#buyItemButton");
        mPoliticCardList = (ListView<PoliticCard>) root.lookup("#politicCardListView");
        mPermitTileList = (ListView<BusinessPermitTile>) root.lookup("#permitTileListView");
        mUsedPermitTileList = (ListView<BusinessPermitTile>) root.lookup("#usedPermitTileListView");
        mBonusList = (ListView<Map.Entry<String, Reward>>) root.lookup("#bonusListView");
        mRewardKingCardList = (ListView<Reward>) root.lookup("#rewardKingCardListView");
        mPlayerList = (ListView<Player>) root.lookup("#playerListView");
        Button page1 = (Button) root.lookup("#p1");
        audioPlayer = Audio.play("sottofondo", true);

        mChatTextField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                Audio.play(CLICK);
                sendChatMessage();
            }
        });
        chatSendMessage.setOnAction(event -> {
            Audio.play(CLICK);
            sendChatMessage();
        });
        actionDrawPoliticCard.setOnAction(event -> {
            Audio.play(CLICK);
            drawPoliticCard();
        });
        actionEndTurn.setOnAction(event -> {
            Audio.play(CLICK);
            endTurn();
        });
        actionElectCouncillor.setOnAction(event -> {
            Audio.play(CLICK);
            electCouncillor();
        });
        actionAcquirePermitTile.setOnAction(event -> {
            Audio.play(CLICK);
            acquirePermitTile();
        });
        actionBuildEmporiumPermitTile.setOnAction(event -> {
            Audio.play(CLICK);
            buildEmporiumPermitTile();
        });
        actionBuildEmporiumKingHelp.setOnAction(event -> {
            Audio.play(CLICK);
            buildEmporiumKingHelp();
        });
        actionEngageAssistant.setOnAction(event -> {
            Audio.play(CLICK);
            engageAssistant();
        });
        actionChangeBusinessPermitTile.setOnAction(event -> {
            Audio.play(CLICK);
            changeBusinessPermitTile();
        });
        actionSendAssistantElectCouncillor.setOnAction(event -> {
            Audio.play(CLICK);
            sendAssistantElectCouncillor();
        });
        actionPerformAdditionalMainAction.setOnAction(event -> {
            Audio.play(CLICK);
            performAdditionalMainAction();
        });
        mActionSellPoliticCard.setOnAction(event -> {
            Audio.play(CLICK);
            sellItem(MarketSellItemPanel.MarketType.POLITIC_CARD);
        });
        mActionSellPermitTile.setOnAction(event -> {
            Audio.play(CLICK);
            sellItem(MarketSellItemPanel.MarketType.PERMIT_TILE);
        });
        mActionSellAssistant.setOnAction(event -> {
            Audio.play(CLICK);
            sellItem(MarketSellItemPanel.MarketType.ASSISTANT);
        });
        mActionBuyItem.setOnAction(event -> {
            Audio.play(CLICK);
            buyItem();
        });
        mSoundButton.setOnAction(event -> handleSound());
        page1.setOnAction(event -> openStubTutorial());

        mPoliticCardList.setCellFactory(param -> new PoliticCardListCell());
        mPermitTileList.setCellFactory(param -> new PermitTileListCell());
        mUsedPermitTileList.setCellFactory(param -> new PermitTileListCell());
        mBonusList.setCellFactory(param -> new BonusListCell());
        mRewardKingCardList.setCellFactory(param -> new KingRewardListCell());
        mPlayerList.setCellFactory(param -> new PlayerListCell(this));

        Platform.runLater(() -> {
            setVisible(mGameBox, true);
            setVisible(mMarketBox, false);
        });
    }

    private void setVisible(Node node, boolean visible) {
        node.setVisible(visible);
        node.setManaged(visible);
    }

    /**
     * Create game board via code because gui editor will not work anymore and in this way the constructor can pass the
     * boolean flag to indicate in which context the board is and the related callback.
     *
     * @param root panel that is created as root of the window.
     */
    private void createGameBoard(Parent root) {
        mGameBoard = new GameBoardPane(true, this);
        Platform.runLater(() -> {
            AnchorPane gameBoardContainer = (AnchorPane) root.lookup("#gameBoardContainer");
            gameBoardContainer.getChildren().add(mGameBoard);
            AnchorPane.setLeftAnchor(mGameBoard, 0d);
            AnchorPane.setTopAnchor(mGameBoard, 0d);
            AnchorPane.setRightAnchor(mGameBoard, 0d);
            AnchorPane.setBottomAnchor(mGameBoard, 0d);
        });
    }
    /**
     * Prepare events and information for market table.
     */
    @SuppressWarnings("unchecked")
    private void createMarketTable(Parent root) {
        mMarketTable = (TableView<Item>) root.lookup("#marketTable");
        TableColumn<Item, Item> objectColumn = new TableColumn<>("Item");
        TableColumn<Item, String> sellerColumn = new TableColumn<>("Seller");
        TableColumn<Item, String> priceColumn = new TableColumn<>("Price");
        TableColumn<Item, String> buyerColumn = new TableColumn<>("Buyer");
        objectColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Item> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue());
            return simpleObjectProperty;
        });
        sellerColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<String> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getSeller());
            return simpleObjectProperty;
        });
        priceColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<String> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(String.format("%d coins", param.getValue().getPrice()));
            return simpleObjectProperty;
        });
        buyerColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<String> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getBuyer());
            return simpleObjectProperty;
        });
        objectColumn.setCellFactory(param -> new MarketTableCell());
        objectColumn.setStyle(TABLE_CELL_STYLE);
        sellerColumn.setStyle(TABLE_CELL_STYLE);
        priceColumn.setStyle(TABLE_CELL_STYLE);
        buyerColumn.setStyle(TABLE_CELL_STYLE);
        Platform.runLater(() -> mMarketTable.getColumns().addAll(
                objectColumn,
                sellerColumn,
                priceColumn,
                buyerColumn
        ));
    }

    /**
     * Prepare loading of tutorial pages
     */
    private void loadImage(ImageView imageView) {
        Platform.runLater(() -> {
            Image image1 = new Image(getClass().getResource("/images/1.jpg").toExternalForm(),1100,800,false,true);
            Image image2 = new Image(getClass().getResource("/images/2.jpg").toExternalForm(),1100,800,false,true);
            Image image3 = new Image(getClass().getResource("/images/3.jpg").toExternalForm(),1100,800,false,true);
            Image image4 = new Image(getClass().getResource("/images/4.jpg").toExternalForm(),1100,800,false,true);
            Image image5 = new Image(getClass().getResource("/images/5.jpg").toExternalForm(),1100,800,false,true);
            Image image6 = new Image(getClass().getResource("/images/6.jpg").toExternalForm(),1100,800,false,true);
            Image image7 = new Image(getClass().getResource("/images/7.jpg").toExternalForm(),1100,800,false,true);
            slides = new ImageView[PAGE_OF_TUTORIAL];
            slides[0] = new ImageView(image1);
            slides[1] = new ImageView(image2);
            slides[2] = new ImageView(image3);
            slides[3] = new ImageView(image4);
            slides[4] = new ImageView(image5);
            slides[5] = new ImageView(image6);
            slides[6] = new ImageView(image7);
            imageView.setImage(slides[0].getImage());
        });
    }

    /**
     * Open a new frame that contains Tutorial pages!
     */
    private void openStubTutorial() {
        mPageNumber = 1;
        JFrame stubFrame = new JFrame("Council Of Four - Tutorial - Page: " + mPageNumber);
        JFXPanel panel = new JFXPanel();
        AnchorPane anchorPane = new AnchorPane();
        ImageView imageView = new ImageView();
        imageView.fitWidthProperty().bind(anchorPane.widthProperty());
        imageView.fitHeightProperty().bind(anchorPane.heightProperty());
        anchorPane.getChildren().add(imageView);
        Button next = new Button("Next");
        Button prev = new Button("Prev");
        next.setCursor(Cursor.HAND);
        prev.setCursor(Cursor.CLOSED_HAND);
        prev.setDisable(true);
        AnchorPane.setLeftAnchor(prev, 0d);
        AnchorPane.setTopAnchor(prev, 0d);
        AnchorPane.setRightAnchor(next, 0d);
        AnchorPane.setTopAnchor(next, 0d);
        anchorPane.getChildren().add(next);
        anchorPane.getChildren().add(prev);
        next.setOnAction(event -> nextPage(imageView, next, prev));
        prev.setOnAction(event -> prevPage(imageView, next, prev));
        Platform.runLater(() -> {
            stubFrame.setSize(1100, 800);
            stubFrame.setVisible(true);
            Scene scene = new Scene(anchorPane);
            panel.setScene(scene); // method of JFXPanel
            stubFrame.add(panel);
            loadImage(imageView);
        });
    }

    /**
     * Handle event to set next page in Tutorial Frame
     * @param imageView reference to physical image shown
     * @param next reference to next button
     * @param prev reference to previous button
     */
    private void nextPage(ImageView imageView, Button next, Button prev) {
        if (mPageNumber == 1) {
            prev.setDisable(false);
        }
        mPageNumber = mPageNumber + 1;
        if (mPageNumber == PAGE_OF_TUTORIAL) {
            next.setDisable(true);
        }
        imageView.setImage(slides[mPageNumber-1].getImage());
    }

    /**
     * Handle event to set previous page in Tutorial Frame
     * @param imageView reference to physical image shown
     * @param next reference to next button
     * @param prev reference to previous button
     */
    private void prevPage(ImageView imageView, Button next, Button prev) {
        if (mPageNumber == PAGE_OF_TUTORIAL) {
            next.setDisable(false);
        }
        mPageNumber = mPageNumber - 1;
        if (mPageNumber == 1) {
            prev.setDisable(true);
        }
        imageView.setImage(slides[mPageNumber-1].getImage());

    }

    /**
     * Notify panel that a game is started, so delegate an Fx thread to load everything in the Ui.
     *
     * @param baseGame game state.
     */
    /*package-local*/ void onGameStarted(BaseGame baseGame) {
        Player player = baseGame.getPlayer(mCallback.getNickname());
        mPoliticCardList.setItems(FXCollections.observableList(player.getPoliticCards()));
        mPermitTileList.setItems(FXCollections.observableList(player.getBusinessPermitTiles()));
        mUsedPermitTileList.setItems(FXCollections.observableList(player.getUsedBusinessPermitTiles()));
        mBonusList.setItems(FXCollections.observableArrayList(player.getBonusCards().entrySet()));
        mRewardKingCardList.setItems(FXCollections.observableArrayList(player.getKingRewards()));
        updatePlayer(player);
        mPlayerList.setItems(FXCollections.observableList(baseGame.getPlayers()));
        mGameBoard.setLeftRegionBoard(baseGame.getLeftRegionBoard());
        mGameBoard.setCenterRegionBoard(baseGame.getCenterRegionBoard());
        mGameBoard.setRightRegionBoard(baseGame.getRightRegionBoard());
        mGameBoard.setKingRegionBoard(baseGame.getKingRegionBoard());
    }

    /**
     * Update Player info into the right panel. Call this method only under a FX dedicated thread.
     *
     * @param player updated instance that should be displayed.
     */
    private void updatePlayer(Player player) {
        mPlayerColor.setFill(GuiUtils.getColor(player.getColor()));
        mPlayerNickname.setText(player.getNickname());
        mPlayerAssistantLabel.setText(String.valueOf(player.getAssistants()));
        mPlayerEmporiumLabel.setText(String.valueOf(player.getEmporiums()));
        mPlayerCoinLabel.setText(String.valueOf(player.getCoins()));
        mPlayerVictoryPointLabel.setText(String.valueOf(player.getVictoryPoints()));
        mPlayerNobilityPositionLabel.setText(String.valueOf(player.getNobilityTrackPosition()));
    }

    /** Turn beginning
     * @param nickname of player's turn
     * @param seconds available
     */
    /*package-local*/ void onGameTurnStarted(String nickname, int seconds) {
        onPlayerTurn(nickname, TURN_GAME, seconds);
    }

    /**
     * Settings when a Market session is started
     */
    /*package-local*/ void onMarketSessionStarted() {
        mActionList = null;
        mTabPane.getSelectionModel().select(1);
        mMarketTable.setItems(FXCollections.observableArrayList());
        setVisible(mGameBox, false);
        setVisible(mMarketBox, true);
        appendChatText("[ROOM] The market session is started");
    }

    /**
     * Settings when a Sell Turn of the Market start
     * @param nickname turn
     * @param seconds available
     */
    /*package-local*/ void onMarketTurnSellStarted(String nickname, int seconds) {
        onPlayerTurn(nickname, TURN_MARKET_SELL, seconds);
        setVisible(mActionSellPoliticCard, true);
        setVisible(mActionSellPermitTile, true);
        setVisible(mActionSellAssistant, true);
        setVisible(mActionBuyItem, false);
        mCurrentMarketPlayer = nickname;
    }

    /**
     * Settings when a Buy turn of the Market start
     * @param nickname turn
     * @param seconds available
     */
    /*package-local*/ void onMarketTurnBuyStarted(String nickname, int seconds) {
        onPlayerTurn(nickname, TURN_MARKET_BUY, seconds);
        setVisible(mActionSellPoliticCard, false);
        setVisible(mActionSellPermitTile, false);
        setVisible(mActionSellAssistant, false);
        setVisible(mActionBuyItem, true);
        mCurrentMarketPlayer = nickname;
    }

    /**
     * Log item on sale
     * @param item put on sale
     */
    /*package-local*/ void onMarketItemAddedOnSale(Item item) {
        mMarketTable.getItems().add(item);
        if (item.getPoliticCard() != null) {
            appendChatText(String.format("[MARKET] %s has putted a politic card on sale for %d coins", item.getSeller(), item.getPrice()));
        } else if (item.getBusinessPermitTile() != null) {
            appendChatText(String.format("[MARKET] %s has putted a permit tile on sale for %d coins", item.getSeller(), item.getPrice()));
        } else if (item.getAssistant() == 1) {
            appendChatText(String.format("[MARKET] %s has putted an assistant on sale for %d coins", item.getSeller(), item.getPrice()));
        }
    }

    /**
     * Handle item bought event
     * @param item bought
     */
    /*package-local*/ void onMarketItemBought(Item item) {
        ObservableList<Item> items = mMarketTable.getItems();
        Iterator<Item> iterator = items.iterator();
        while (iterator.hasNext()) {
            Item i = iterator.next();
            if (i.getMarketId().equals(item.getMarketId())) {
                iterator.remove();
                break;
            }
        }
        items.add(item);
        String nickname = mCallback.getNickname();
        if (item.getSeller().equals(nickname) || item.getBuyer().equals(nickname)) {
            updateUiMarketChanged();
        }
        if (item.getPoliticCard() != null) {
            appendChatText(String.format("[MARKET] %s has bought a politic card from %s for %d coins", item.getBuyer(), item.getSeller(), item.getPrice()));
        } else if (item.getBusinessPermitTile() != null) {
            appendChatText(String.format("[MARKET] %s has bought a permit tile from %s for %d coins", item.getBuyer(), item.getSeller(), item.getPrice()));
        } else if (item.getAssistant() == 1) {
            appendChatText(String.format("[MARKET] %s has bought an assistant from %s for %d coins", item.getBuyer(), item.getSeller(), item.getPrice()));
        }
    }

    /**
     * Give updates of market after buy-sell event
     */
    private void updateUiMarketChanged() {
        Player player = mCallback.getModel().getPlayer(mCallback.getNickname());
        updatePlayer(player);
        notifyPlayerPoliticCardChanged();
        notifyPlayerPermitTileChanged();
    }

    /**
     * Handle closing market session, reset item on sale and market logic.
     */
    /*package-local*/ void onMarketSessionFinished() {
        mTabPane.getSelectionModel().select(0);
        mMarketTable.setItems(null);
        mCurrentMarketPlayer = null;
        setVisible(mGameBox, true);
        setVisible(mMarketBox, false);
        appendChatText("[ROOM] The market session is finished, starting a new game turn");
    }

    /**
     * Notify ui that a new Player turn is started. It will check if the turn started is of the current Player and it
     * will write a line into chat text area and into a label under action buttons.
     *
     * @param nickname of the Player that is starting the turn.
     * @param type     of the turn that is starting.
     * @param seconds  that the Player has to complete the turn.
     */
    private void onPlayerTurn(String nickname, String type, int seconds) {
        if (mCallback.getNickname().equals(nickname)) {
            if (TURN_GAME.equals(type)) {
                mActionList = new ActionList();
            }
            appendChatText(String.format("[ROOM] Your %s turn is started", type));
            setPlayerRemainingTime(seconds);
        } else {
            mActionList = null;
            mPlayerTurnLabel.setText(String.format("It's %s %s turn", nickname, type));
            appendChatText(String.format("[ROOM] %s's %s turn is started", nickname, type));
        }
    }

    /**
     * Update the label under action buttons when the countdown change.
     *
     * @param seconds remaining to the end of the turn.
     */
    /*package-local*/ void setPlayerRemainingTime(int seconds) {
        mPlayerTurnLabel.setText(String.format("You have %ds to complete your turn", seconds));
    }

    /*package-local*/ void onPoliticCardDraw(UpdateState updateState) {
        if (isCurrentPlayer(updateState)) {
            JDialog dialog = PoliticCardDrawnPanel.createDialog(this, updateState.getAddedPoliticCards().get(0));
            dialog.setVisible(true);
            notifyPlayerPoliticCardChanged();
            mActionList.setPoliticCardDrawn();
        } else {
            appendChatText(String.format("[ROOM] Player %s has drawn a politic card", updateState.getNickname()));
        }
    }

    /**
     * Update the current action list with a fresh copy of server ones.
     *
     * @param actionList from server.
     */
    /*package-local*/ void onActionList(ActionList actionList) {
        mActionList = actionList;
    }

    private boolean isCurrentPlayer(UpdateState updateState) {
        return mCallback.getNickname().equals(updateState.getNickname());
    }

    /**
     * Notify panel that a new chat message should be printed.
     *
     * @param privateMessage true if message is private, false if public.
     * @param author         nickname of the Player that has sent the message.
     * @param message        body of the message to show.
     */
    /*package-local*/ void showChatMessage(boolean privateMessage, String author, String message) {
        appendChatText(String.format("[CHAT][%s%s] %s", author, privateMessage ? " -> You" : "", message));
    }

    /**
     * Notify game panel that a Player has disconnected. The panel will print a line into the chatArea to notify
     * the Player and it will update the Player list on the right setting the disconnected Player as "OFFLINE".
     *
     * @param nickname of the Player that has disconnected.
     */
    /*package-local*/ void onPlayerDisconnected(String nickname) {
        appendChatText(String.format("[ROOM] Player %s has disconnected", nickname));
        notifyPlayerListChanged();
    }

    /*package-local*/ void onLastTurnStarted(String nickname) {
        appendChatText(String.format("[ROOM] Player %s has built all his emporiums. The last game turn is starting", nickname));
    }

    /*package-local*/ void onGameEnded(UpdateState[] updateStates, List<String> ranking) {
        appendChatText("[ROOM] Game is finished, players have earned the following victory points");
        for (UpdateState updateState : updateStates) {
            if (updateState.getAddedVictoryPoints() > 0) {
                String nickname = updateState.isPlayer(mCallback.getNickname()) ? "You have" : (updateState.getNickname() + " has");
                appendChatText(String.format("[ROOM] %s earned %d victory points", nickname, updateState.getAddedVictoryPoints()));
            }
        }
        List<Player> players = ranking.stream().map(mCallback.getModel()::getPlayer).collect(Collectors.toList());
        FinalRankingPanel.createDialog(this, players).setVisible(true);
    }

    /**
     * Simple trick to force update the Player list.
     */
    private void notifyPlayerListChanged() {
        ObservableList<Player> players = mPlayerList.getItems();
        mPlayerList.setItems(null);
        mPlayerList.setItems(players);
    }

    /**
     * Update Politic card
     */
    private void notifyPlayerPoliticCardChanged() {
        ObservableList<PoliticCard> politicCards = mPoliticCardList.getItems();
        mPoliticCardList.setItems(null);
        mPoliticCardList.setItems(politicCards);
    }

    /**
     * Update Permit tile
     */
    private void notifyPlayerPermitTileChanged() {
        ObservableList<BusinessPermitTile> permitTiles = mPermitTileList.getItems();
        mPermitTileList.setItems(null);
        mPermitTileList.setItems(permitTiles);
    }

    /**
     * Update Permit tile used
     */
    private void notifyPlayerUsedPermitTileChanged() {
        ObservableList<BusinessPermitTile> permitTiles = mUsedPermitTileList.getItems();
        mUsedPermitTileList.setItems(null);
        mUsedPermitTileList.setItems(permitTiles);
    }

    /**
     * Update the bonus list.
     * @param player instance with updated info.
     */
    private void notifyPlayerBonusChanged(Player player) {
        mBonusList.setItems(FXCollections.observableArrayList(player.getBonusCards().entrySet()));
    }

    /**
     * Update the king rewards list.
     * @param player instance with updated info.
     */
    private void notifyPlayerKingRewardChanged(Player player) {
        mRewardKingCardList.setItems(FXCollections.observableArrayList(player.getKingRewards()));
    }

    /**
     * It send a message via chat. There are two types of message:
     * if there is as a first letter @, the message is sent to a single user
     * otherwise it is a public message
     */
    private void sendChatMessage() {
        String message = mChatTextField.getText();
        if (!message.trim().isEmpty()) {
            String receiver = null;
            if (message.startsWith("@")) {
                String[] words = message.split(" ");
                if (words.length == 1) {
                    return;
                }
                receiver = words[0].substring(1);
                message = concatenate(words, 1);
            }
            mCallback.sendChatMessage(receiver, message);
            appendChatText(String.format("[CHAT][You%s] %s", receiver != null ? " -> " + receiver : "", message));
            mChatTextField.clear();
        }
    }

    /**
     * Handle a request to draw a politic card to the server and catch all possible errors.
     */
    private void drawPoliticCard() {
        if (mActionList == null) {
            mCallback.showError(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } else if (mActionList.isPoliticCardDrawn()) {
            mCallback.showError(ErrorCodes.ERROR_POLITIC_CARD_ALREADY_DRAWN);
        } else {
            mCallback.drawPoliticCard();
        }
    }

    /**
     * Handle a request to end a turn to the server and catch all possible errors
     */
    private void endTurn() {
        if (mActionList != null) {
            if (mActionList.isPoliticCardDrawn()) {
                if (mActionList.getMainActionCount() > 0) {
                    mCallback.showError(ErrorCodes.ERROR_MAIN_ACTION_NOT_DONE);
                } else {
                    mCallback.endTurn();
                }
            } else {
                mCallback.showError(ErrorCodes.ERROR_POLITIC_CARD_NOT_YET_DRAWN);
            }
        } else {
            mCallback.endTurn();
        }
    }

    /**
     * Handle a request to elect a Councillor to the server and catch all possible errors
     */
    private void electCouncillor() {
        if (!checkMainActionErrors()) {
            mOpenDialog = ElectCouncillorPanel.createDialog(this, mCallback.getModel(), this);
            mOpenDialog.setVisible(true);
        }
    }

    /**
     * Handle a request to acquire a Permit tile to the server and catch all possible errors
     */
    private void acquirePermitTile() {
        if (!checkMainActionErrors()) {
            mOpenDialog = AcquirePermitTilePanel.createDialog(this, mCallback.getNickname(), mCallback.getModel(), this);
            mOpenDialog.setVisible(true);
        }
    }

    /**
     * Handle a request to build an emporium (using permit tile) to the server and catch all possible errors
     */
    private void buildEmporiumPermitTile() {
        if (!checkMainActionErrors()) {
            mOpenDialog = BuildEmporiumPermitTilePanel.createDialog(this, mCallback.getNickname(), mCallback.getModel(), this);
            mOpenDialog.setVisible(true);
        }
    }

    /**
     * Handle a request to build an emporium (using king help) to the server and catch all possible errors
     */
    private void buildEmporiumKingHelp() {
        if (!checkMainActionErrors()) {
            mOpenDialog = BuildEmporiumKingHelpPanel.createDialog(this, mCallback.getNickname(), mCallback.getModel(), this);
            mOpenDialog.setVisible(true);
        }
    }

    /**
     * Handle a request to engage an Assistant to the server and catch all possible errors
     */
    private void engageAssistant() {
        if (!checkFastActionErrors()) {
            mOpenDialog = FastActionReminderPanel.createDialog(this, 1, this);
            mOpenDialog.setVisible(true);
        }
    }

    /**
     * Handle a request to change a business permit tile to the server and catch all possible errors
     */
    private void changeBusinessPermitTile() {
        if (!checkFastActionErrors()) {
            mOpenDialog = ChangePermitTilesPanel.createDialog(this, mCallback.getModel(), this);
            mOpenDialog.setVisible(true);
        }
    }

    /**
     * Handle a request to give an assistant and take a councilor
     */
    private void sendAssistantElectCouncillor() {
        if (!checkFastActionErrors()) {
            mOpenDialog = SendAssistantElectCouncillorPanel.createDialog(this, mCallback.getModel(), this);
            mOpenDialog.setVisible(true);
        }
    }

    /**
     * Handle a request to have an additional main action, losing 3 assistants
     */
    private void performAdditionalMainAction() {
        if (!checkFastActionErrors()) {
            mOpenDialog = FastActionReminderPanel.createDialog(this, 4, this);
            mOpenDialog.setVisible(true);
        }
    }

    /**
     * Handle sell item during market sell, organised by type of card
     * @param marketType type of card ( assistants, permit tiles, politic card )
     */
    private void sellItem(MarketSellItemPanel.MarketType marketType) {
        if (checkMarketTurn()) {
            Player player = mCallback.getModel().getPlayer(mCallback.getNickname());
            mOpenDialog = MarketSellItemPanel.createDialog(this, marketType, mCallback.getMarketSession(), player, this);
            mOpenDialog.setVisible(true);
        }
    }

    /**
     * Handle a buy request
     */
    private void buyItem() {
        if (checkMarketTurn()) {
            Item item = mMarketTable.getSelectionModel().getSelectedItem();
            if (item != null) {
                mCallback.buyItem(item.getMarketId());
            }
        }
    }

    /**
     * @return true if it is player turn and he is able to do his action
     */
    private boolean checkMarketTurn() {
        if (mCallback.getMarketSession() == null || !mCallback.getNickname().equals(mCurrentMarketPlayer)) {
            mCallback.showError(ErrorCodes.ERROR_NOT_PLAYER_TURN);
            return false;
        }
        return true;
    }

    /**
     * @return check if action valid
     */
    private boolean checkMainActionErrors() {
        if (checkActionErrors()) {
            return true;
        } else if (mActionList.getMainActionCount() == 0) {
            mCallback.showError(ErrorCodes.ERROR_MAIN_ACTION_NOT_AVAILABLE);
            return true;
        }
        return false;
    }

    /**
     * @return check if fast action invalid checking 'token' variables
     */
    private boolean checkFastActionErrors() {
        if (checkActionErrors()) {
            return true;
        } else if (mActionList.getFastActionCount() == 0) {
            mCallback.showError(ErrorCodes.ERROR_FAST_ACTION_NOT_AVAILABLE);
            return true;
        }
        return false;
    }

    /**
     * @return check if error occurs during action
     */
    private boolean checkActionErrors() {
        if (mActionList == null) {
            mCallback.showError(ErrorCodes.ERROR_NOT_PLAYER_TURN);
            return true;
        } else if (!mActionList.isPoliticCardDrawn()) {
            mCallback.showError(ErrorCodes.ERROR_POLITIC_CARD_NOT_YET_DRAWN);
            return true;
        }
        return false;
    }

    /**
     * Used to built chat message
     * @param words to concatenate in StringBuilder
     * @param skip prefix length of letter deleted in message
     * @return the concatenated message.
     */
    private String concatenate(String[] words, int skip) {
        StringBuilder builder = new StringBuilder();
        for (int i = skip; i < words.length; i++) {
            builder.append(words[i]);
            if (i + 1 != words.length) {
                builder.append(" ");
            }
        }
        return builder.toString();
    }

    /**
     * If chat text field is empty, append the message, else make a new line before.
     * @param text append to Chat Text Field area
     */
    private void appendChatText(String text) {
        if (!mChatTextArea.getText().isEmpty()) {
            mChatTextArea.appendText(String.format("%n"));
        }
        mChatTextArea.appendText(text);
    }


    /**
     * This method handle the button sound.
     * It change the button as the situation require.
     * Call audio method as the situation require.
     */
    private void handleSound() {
        if (!sound) {
            sound = true;
            audioPlayer = Audio.play("sottofondo", true, true);
            mSoundButton.setText("Sound OFF");
        } else {
            sound = false;
            audioPlayer.stopSound();
            Audio.setSound(sound);
            mSoundButton.setText("Sound ON");
        }
    }

    /**
     * This event make a call to open a dialog of city information
     * @param city clicked
     */
    @Override
    public void onCityClicked(City city) {
        JDialog dialog = CityDetailPanel.createDialog(this, city, mCallback.getModel());
        dialog.setVisible(true);
    }

    /**
     *  Open dialog with information on Business Permit Tile clicked
     * @param businessPermitTile clicked
     */
    @Override
    public void onPermitTileClick(BusinessPermitTile businessPermitTile) {
        JDialog dialog = PermitTileDetailPanel.createDialog(this, businessPermitTile);
        dialog.setVisible(true);
    }

    /**
     * Open dialog with information balcony clicked
     * @param region of the balcony
     * @param colors on that balcony
     */
    @Override
    public void onBalconyClick(String region, String[] colors) {
        JDialog dialog = BalconyDetailPanel.createDialog(this, region, colors);
        dialog.setVisible(true);
    }

    /**
     * Handle event, call main class to response
     * @param councilor going to be elected
     * @param region in which it happens
     */
    @Override
    public void onCouncillorElected(Councilor councilor, String region) {
        mCallback.electCouncillor(councilor, region);
        onCancel();
    }

    /**
     * * Handle event, call main class to response
     * @param politicCards spent of same color of the councillor in that council
     * @param region of the council
     * @param permitTileIndex 1 if it's first card of region, 2 if it's the twice
     */
    @Override
    public void onBusinessPermitTileAcquired(List<PoliticCard> politicCards, String region, int permitTileIndex) {
        mCallback.acquireBusinessPermitTile(politicCards, region, permitTileIndex);
        onCancel();
    }

    /**
     * Emporium built using business permit tile in a city who starts with letters on business permit tile
     * @param businessPermitTile chosen for the building of Emporium
     * @param city in which emporium has been built
     */
    @Override
    public void onEmporiumBuiltWithPermitTile(BusinessPermitTile businessPermitTile, String city) {
        mCallback.buildEmporiumWithBusinessPermitTile(businessPermitTile, city);
        onCancel();
    }

    /**
     * Emporium built satisfying king advise. King moves in any city where we want to build an emporium
     * @param politicCards discarded
     * @param cities crossed
     */
    @Override
    public void onEmporiumBuiltWithKingHelp(List<PoliticCard> politicCards, List<String> cities) {
        mCallback.buildEmporiumWithKingHelp(politicCards, cities);
        onCancel();
    }

    /**
     * Players take an available councillor and place him in a balcony.
     * @param councilor going to be elect
     * @param region in which is elelected
     */
    @Override
    public void onAssistantSentToElectCouncillor(Councilor councilor, String region) {
        mCallback.sendAssistantElectCouncillor(councilor, region);
        onCancel();
    }

    /**
     * Giving an assistants, a player is able to change two business permit tile and put them at the bottom and discard two new cards from cards list
     * @param region where the permit tiles are changed.
     */
    @Override
    public void onBusinessPermitTilesChanged(String region) {
        mCallback.changeBusinessPermitTile(region);
        onCancel();
    }

    /**
     * Apply fast action
     * @param action type of fast action
     */
    @Override
    public void onApply(int action) {
        switch (action) {
            case 1:
                mCallback.engageAssistant();
                break;
            case 4:
                mCallback.performAdditionalMainAction();
                break;
            default:
                throw new IllegalArgumentException("Unknown fast action");
        }
        onCancel();
    }

    /**
     * Put in market sell type a politic card
     * @param politicCard on sale
     * @param price target for that card
     */
    @Override
    public void sellPoliticCard(PoliticCard politicCard, int price) {
        mCallback.sellPoliticCard(politicCard, price);
        onCancel();
    }

    /**
     * Put in market sell type a business permit tile
     * @param businessPermitTile on sale
     * @param price target for that card
     */
    @Override
    public void sellBusinessPermitTile(BusinessPermitTile businessPermitTile, int price) {
        mCallback.sellBusinessPermitTile(businessPermitTile, price);
        onCancel();
    }

    /**
     * Assistant on sale
     * @param  price of an assistant
     */
    @Override
    public void sellAssistant(int price) {
        mCallback.sellAssistant(price);
        onCancel();
    }

    /**
     * Disable current window
     */
    @Override
    public void onCancel() {
        if (mOpenDialog != null) {
            mOpenDialog.setVisible(false);
            mOpenDialog = null;
        }
    }

    /**
     * Handle councillor elected event.
     * Two point of view: current player, other players
     * @param updateState with this action
     */
    /*package-local*/ void onCouncillorElected(UpdateState updateState) {
        mGameBoard.invalidate();
        UpdateState.ChangedCouncilor changedCouncilor = updateState.getChangedCouncillors().get(0);
        if (isCurrentPlayer(updateState)) {
            updatePlayer(mCallback.getModel().getPlayer(updateState.getNickname()));
            appendChatText(String.format("[ROOM] You have elected a %s councillor into %s region's balcony and you have gained %d coins",
                    changedCouncilor.getCouncillor().getColor(),
                    changedCouncilor.getRegion(),
                    updateState.getAddedCoins())
            );
            mActionList.decrementMainActionCounter();
        } else {
            appendChatText(String.format("[ROOM] Player %s has elected a %s councillor into %s region's balcony and he gained %d coins",
                    updateState.getNickname(),
                    changedCouncilor.getCouncillor().getColor(),
                    changedCouncilor.getRegion(),
                    updateState.getAddedCoins())
            );
        }
    }

    /**
     * Handle business permit tile acquired.
     * @param updateState with this event
     */
    /*package-local*/ void onBusinessPermitTileAcquired(UpdateState updateState) {
        mGameBoard.invalidate();
        UpdateState.ChangedBusinessPermitTile changedBusinessPermitTile = updateState.getChangedBusinessPermitTiles().get(0);
        if (isCurrentPlayer(updateState)) {
            updatePlayer(mCallback.getModel().getPlayer(updateState.getNickname()));
            notifyPlayerPoliticCardChanged();
            notifyPlayerPermitTileChanged();
            appendChatText(String.format("[ROOM] You have satisfied the %s region's balcony, you have payed %d coins and " +
                            "you have acquired the %s business permit tile of the region. You have gained also all the rewards on the " +
                            "acquired permit tile.",
                    changedBusinessPermitTile.getRegion(),
                    updateState.getRemovedCoins(),
                    changedBusinessPermitTile.getIndex() == 1 ? "first" : "second")
            );
            lookForAdditionalMainActions(updateState);
            mActionList.decrementMainActionCounter();
        } else {
            appendChatText(String.format("[ROOM] Player %s has satisfied the %s region's balcony, he payed %d coins and " +
                    "he acquired the %s business permit tile of the region. He has gained also all the rewards on the " +
                    "acquired permit tile.",
                    updateState.getNickname(),
                    changedBusinessPermitTile.getRegion(),
                    updateState.getRemovedCoins(),
                    changedBusinessPermitTile.getIndex() == 1 ? "first" : "second")
            );
        }
    }

    /**
     * Handle Emporium Built event using permit tile.
     * @param updateState with this event information
     */
    /*package-local*/ void onEmporiumBuiltWithPermitTile(UpdateState updateState) {
        mGameBoard.invalidate();
        if (isCurrentPlayer(updateState)) {
            updateAllPlayerInfo(mCallback.getModel().getPlayer(updateState.getNickname()));
            appendChatText(String.format("[ROOM] You have built an emporium on %s city using a business permit tile",
                    updateState.getAddedEmporiums().get(0))
            );
            lookForAdditionalMainActions(updateState);
            mActionList.decrementMainActionCounter();
        } else {
            appendChatText(String.format("[ROOM] Player %s has built an emporium on %s city using a business permit tiles",
                    updateState.getNickname(),
                    updateState.getAddedEmporiums().get(0))
            );
        }
    }

    /**
     * Handle emporium built event king help
     * @param updateState with this information
     */
    /*package-local*/ void onEmporiumBuiltWithKingHelp(UpdateState updateState) {
        mGameBoard.invalidate();
        if (isCurrentPlayer(updateState)) {
            updateAllPlayerInfo(mCallback.getModel().getPlayer(updateState.getNickname()));
            appendChatText(String.format("[ROOM] You have satisfied the king region's balcony, you have payed %d coins and you have moved the king to %s city where you have built an emporium",
                    updateState.getRemovedCoins(),
                    updateState.getNewKingCity())
            );
            lookForAdditionalMainActions(updateState);
            mActionList.decrementMainActionCounter();
        } else {
            appendChatText(String.format("[ROOM] Player %s has satisfied the king region's balcony, he payed %d coins " +
                    "and he moved the king to %s city where he built an emporium",
                    updateState.getNickname(),
                    updateState.getRemovedCoins(),
                    updateState.getNewKingCity())
            );
        }
    }

    private void updateAllPlayerInfo(Player player) {
        updatePlayer(player);
        notifyPlayerPoliticCardChanged();
        notifyPlayerPermitTileChanged();
        notifyPlayerUsedPermitTileChanged();
        notifyPlayerBonusChanged(player);
        notifyPlayerKingRewardChanged(player);
    }

    /**
     * Update updateState on assistant engaged
     * @param updateState with this information
     */
    /*package-local*/ void onAssistantEngaged(UpdateState updateState) {
        mGameBoard.invalidate();
        if (isCurrentPlayer(updateState)) {
            updatePlayer(mCallback.getModel().getPlayer(updateState.getNickname()));
            appendChatText(String.format("[ROOM] You have payed %d coins and You have engaged %d assistant",
                    updateState.getRemovedCoins(),
                    updateState.getAddedAssistants()));
            lookForAdditionalMainActions(updateState);
            mActionList.decrementFastActionCounter();
        } else {
            appendChatText(String.format("[ROOM] Player %s has payed %d coins and he has engaged %d assistant",
                    updateState.getNickname(),
                    updateState.getRemovedCoins(),
                    updateState.getAddedAssistants()));
        }
    }

    /**
     * Update updateState when business permit tile changed
     * @param updateState with this information
     */
    /*package-local*/ void onBusinessPermitTilesChanged(UpdateState updateState) {
        mGameBoard.invalidate();
        String region = updateState.getChangedBusinessPermitTiles().get(0).getRegion();
        if (isCurrentPlayer(updateState)) {
            updatePlayer(mCallback.getModel().getPlayer(updateState.getNickname()));
            appendChatText(String.format("[ROOM] You have released %d assistant and you have changed %s region's visible business permit tiles",
                    updateState.getRemovedAssistants(),
                    region));
            lookForAdditionalMainActions(updateState);
            mActionList.decrementFastActionCounter();
        } else {
            appendChatText(String.format("[ROOM] Player %s has released %d assistant and has changed %s region's visible " +
                    "business permit tiles",
                    updateState.getNickname(),
                    updateState.getRemovedAssistants(),
                    region));
        }
    }

    /**
     * Update updateState when an assistant used to elect a councillor
     * @param updateState with this information
     */
    /*package-local*/ void onAssistantSentToElectCouncillor(UpdateState updateState) {
        mGameBoard.invalidate();
        UpdateState.ChangedCouncilor changedCouncilor = updateState.getChangedCouncillors().get(0);
        if (isCurrentPlayer(updateState)) {
            updatePlayer(mCallback.getModel().getPlayer(updateState.getNickname()));
            appendChatText(String.format("You have sent an assistant to elect a %s councillor into %s region's balcony",
                    changedCouncilor.getCouncillor().getColor(),
                    changedCouncilor.getRegion()));
            lookForAdditionalMainActions(updateState);
            mActionList.decrementFastActionCounter();
        } else {
            appendChatText(String.format("[ROOM] Player %s has sent an assistant to elect a %s councillor into %s region's balcony",
                    updateState.getNickname(),
                    changedCouncilor.getCouncillor().getColor(),
                    changedCouncilor.getRegion()));
        }
    }

    /**
     * Update updateState when an addictional main action is acquired
     * @param updateState with this information
     */
    /*package-local*/ void onAdditionalMainActionAcquired(UpdateState updateState) {
        mGameBoard.invalidate();
        if (isCurrentPlayer(updateState)) {
            updatePlayer(mCallback.getModel().getPlayer(updateState.getNickname()));
            appendChatText(String.format("[ROOM] You have released %d assistants and you can bought an additional main action",
                    updateState.getRemovedAssistants()));
            lookForAdditionalMainActions(updateState);
            mActionList.decrementFastActionCounter();
        } else {
            appendChatText(String.format("[ROOM] Player %s has released %d assistants and he can now perform an " +
                    "additional main action.",
                    updateState.getNickname(),
                    updateState.getRemovedAssistants()));
        }
    }

    /**
     * Open a dialog showing first special rewards got
     * @param count number of rewards
     */
    /*package-local*/ void showFirstSpecialRewards(int count) {
        mOpenDialog = FirstSpecialBonusPanel.createDialog(this, count, mCallback.getNickname(), mCallback.getModel(), this);
        mOpenDialog.setVisible(true);
    }

    /** Open a dialog showing second special rewards got
     * @param count number of rewards
     */
    /*package-local*/ void showSecondSpecialRewards(int count) {
        mOpenDialog = SecondSpecialBonusPanel.createDialog(this, count, mCallback.getModel(), this);
        mOpenDialog.setVisible(true);
    }

    /**
     * Open a dialog showing third special rewards got
     * @param count number of rewards
     */
    /*package-local*/ void showThirdSpecialRewards(int count) {
        Player player = mCallback.getModel().getPlayer(mCallback.getNickname());
        mOpenDialog = ThirdSpecialBonusPanel.createDialog(this, count, player, this);
        mOpenDialog.setVisible(true);
    }


    /**
     * Notify for first special rewards
     * @param updateState with this information
     */
    /*package-local*/ void onFirstSpecialRewardsEarned(UpdateState updateState) {
        mGameBoard.invalidate();
        if (isCurrentPlayer(updateState)) {
            updatePlayer(mCallback.getModel().getPlayer(updateState.getNickname()));
            appendChatText("[ROOM] You have earned a first special reward");
            lookForAdditionalMainActions(updateState);
        } else {
            appendChatText(String.format("[ROOM] Player %s has earned a first special reward", updateState.getNickname()));
        }
    }

    /**
     * Notify for second special rewards
     * @param updateState with this information
     */
    /*package-local*/ void onSecondSpecialRewardsEarned(UpdateState updateState) {
        mGameBoard.invalidate();
        if (isCurrentPlayer(updateState)) {
            updatePlayer(mCallback.getModel().getPlayer(updateState.getNickname()));
            appendChatText(String.format("[ROOM] You have earned a second special reward and you have taken %d " +
                    "business permit tiles without paying the cost", updateState.getAddedBusinessPermitTiles().size()));
            lookForAdditionalMainActions(updateState);
        } else {
            appendChatText(String.format("[ROOM] Player %s has earned a second special reward and he has taken %d " +
                    "business permit tiles without paying the cost",
                    updateState.getNickname(),
                    updateState.getAddedBusinessPermitTiles().size())
            );
        }
    }

    /**
     * Notify for third special rewards
     * @param updateState with this information
     */
    /*package-local*/ void onThirdSpecialRewardsEarned(UpdateState updateState) {
        mGameBoard.invalidate();
        if (isCurrentPlayer(updateState)) {
            updatePlayer(mCallback.getModel().getPlayer(updateState.getNickname()));
            appendChatText("[ROOM] You have earned a third special reward");
            lookForAdditionalMainActions(updateState);
        } else {
            appendChatText(String.format("[ROOM] Player %s has earned a third special reward", updateState.getNickname()));
        }
    }

    /**
     * If updateState has 'added main actions', give this possibility incrementing them on mActionList.
     * @param updateState current state.
     */
    private void lookForAdditionalMainActions(UpdateState updateState) {
        for (int i = 0; i < updateState.getAddedMainActions(); i++) {
            mActionList.incrementMainActionCounter();
        }
        if (updateState.getAddedMainActions() > 0) {
            appendChatText(String.format("[ROOM] You have %d additional main action%s for this turn",
                    updateState.getAddedMainActions(),
                    updateState.getAddedMainActions() > 1 ? "s" : ""));
        }
    }

    /**
     * Open a dialog with visible information from other players.
     * @param player clicked.
     */
    @Override
    public void onPlayerClicked(Player player) {
        Audio.play(CLICK);
        if (!player.getNickname().equals(mCallback.getNickname())) {
            JDialog dialog = PlayerDetailPanel.createDialog(this, player);
            dialog.setVisible(true);
        }
    }

    /**
     * Ask for first special rewards.
     * @param cities provided.
     */
    @Override
    public void earnFirstSpecialRewards(List<String> cities) {
        onCancel();
        mCallback.earnFirstSpecialRewards(cities);
    }

    /**
     * Ask for second special rewards.
     * @param regions provided.
     * @param indices provided.
     */
    @Override
    public void earnSecondSpecialRewards(List<String> regions, List<Integer> indices) {
        onCancel();
        mCallback.earnSecondSpecialRewards(regions, indices);
    }

    /**
     * Ask for third special rewards.
     * @param businessPermitTiles provided.
     */
    @Override
    public void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles) {
        onCancel();
        mCallback.earnThirdSpecialRewards(businessPermitTiles);
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * @return the nickname of the Player
         */
        String getNickname();

        /**
         * @param receiver if message start with @nickname, the message will be sent only to him
         * @param message  field of the message sent
         */
        void sendChatMessage(String receiver, String message);

        /**
         * Used to draw a politic card
         */
        void drawPoliticCard();

        /**
         * Used to end a turn
         */
        void endTurn();

        /**
         * Used to load GamePanel with configurations
         *
         * @return the game model generated
         */
        BaseGame getModel();

        /**
         * Retrieve the current market session from internal bus.
         *
         * @return the current market session.
         */
        MarketSession getMarketSession();

        /**
         * Used to elect a councillor.
         *
         * @param councilor going to be elect
         * @param region    in which councillor must be elect
         */
        void electCouncillor(Councilor councilor, String region);

        /**
         * Used to acquire business permit tile.
         *
         * @param politicCards    used to satisfy the region's balcony.
         * @param region          where the permit tile is placed.
         * @param permitTileIndex index of the permit tile to acquire.
         */
        void acquireBusinessPermitTile(List<PoliticCard> politicCards, String region, int permitTileIndex);

        /**
         * Callback method used to send to the server the intent to build an emporium with a permit tile.
         *
         * @param businessPermitTile to use to make the action.
         * @param city               where the emporium should be built.
         */
        void buildEmporiumWithBusinessPermitTile(BusinessPermitTile businessPermitTile, String city);

        /**
         * Callback method used to send to the server the intent to build an emporium with the help of the king.
         *
         * @param politicCards to use to satisfy the king's balcony.
         * @param cities       sorted list of the city where the king should move.
         */
        void buildEmporiumWithKingHelp(List<PoliticCard> politicCards, List<String> cities);

        /**
         * Callback method used to send to the server the intent to engage an assistant.
         */
        void engageAssistant();

        /**
         * Callback method used to send to the server the intent to change the visible permit tiles.
         *
         * @param region where the permit tiles should be changed.
         */
        void changeBusinessPermitTile(String region);

        /**
         * Callback method used to send to the server the intent to send an assistant to elect a councillor.
         *
         * @param councilor to elect.
         * @param region    where the councillor should be elected.
         */
        void sendAssistantElectCouncillor(Councilor councilor, String region);

        /**
         * Callback method used to buy an additional main action.
         */
        void performAdditionalMainAction();

        /**
         * Callback method used to earn the first special rewards.
         *
         * @param cities where the player want to retrieve the bonus.
         */
        void earnFirstSpecialRewards(List<String> cities);

        /**
         * Callback method used to earn the second special rewards.
         *
         * @param regions where the player want to take the permit tile.
         * @param indices list of index of the permit tiles to take.
         */
        void earnSecondSpecialRewards(List<String> regions, List<Integer> indices);

        /**
         * Callback method used to earn the third special rewards.
         *
         * @param businessPermitTiles list of permit tiles to use.
         */
        void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles);

        /**
         * Callback method used to sell a politic card on the market.
         *
         * @param politicCard to sell.
         * @param price       to set.
         */
        void sellPoliticCard(PoliticCard politicCard, int price);

        /**
         * Callback method used to sell a business permit tile on the market.
         *
         * @param businessPermitTile to sell.
         * @param price              to set.
         */
        void sellBusinessPermitTile(BusinessPermitTile businessPermitTile, int price);

        /**
         * Callback method used to sell an assistant on the market.
         *
         * @param price to set.
         */
        void sellAssistant(int price);

        /**
         * Callback method used to buy an item from the market.
         *
         * @param marketId of the item to buy.
         */
        void buyItem(String marketId);

        /**
         * Callback method used to show an incoming error from server or from ui-actions.
         *
         * @param code specific error code. {@link ErrorCodes};
         */
        void showError(int code);
    }
}
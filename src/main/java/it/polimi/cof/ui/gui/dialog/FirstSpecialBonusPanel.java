package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.City;
import it.polimi.cof.model.Reward;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.control.CityRewardTableCell;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Panel that handle first special bonus.
 */
public class FirstSpecialBonusPanel extends JFXPanel {

    private TableView<City> mCityTable;

    @SuppressWarnings("unchecked")
    private FirstSpecialBonusPanel(int count, String nickname, BaseGame baseGame, Callback callback) {
        Parent root = GuiUtils.loadFxml(this, "dialog_first_special_reward");
        Label helpLabel = (Label) root.lookup("#helpLabel");
        mCityTable = (TableView<City>) root.lookup("#cityTableView");
        TableColumn<City, String> cityName = new TableColumn<>("City");
        TableColumn<City, Reward> cityReward = new TableColumn<>("Reward");
        cityName.setCellValueFactory(param -> {
            SimpleObjectProperty<String> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getName());
            return simpleObjectProperty;
        });
        cityReward.setCellValueFactory(param -> {
            SimpleObjectProperty<Reward> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getReward());
            return simpleObjectProperty;
        });
        cityReward.setCellFactory(param -> new CityRewardTableCell());
        Button applyButton = (Button) root.lookup("#applyButton");
        applyButton.setOnAction(event -> {
            List<String> cities = mCityTable.getSelectionModel().getSelectedItems().stream().map(City::getName).collect(Collectors.toList());
            callback.earnFirstSpecialRewards(cities);
        });
        mCityTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        mCityTable.getSelectionModel().getSelectedItems().addListener((ListChangeListener<City>) c ->
                Platform.runLater(() -> changeButtonVisibility(count, applyButton))
        );
        Platform.runLater(() -> {
            mCityTable.getColumns().addAll(cityName, cityReward);
            helpLabel.setText(getHelpMessage(count));
            mCityTable.setItems(getAllAvailableCities(nickname, baseGame));
        });
    }

    private String getHelpMessage(int count) {
        if (count > 1) {
            return String.format("Select %d different cities where you want to take the reward", count);
        }
        return "Select a city where you want to take the reward";
    }

    private ObservableList<City> getAllAvailableCities(String nickname, BaseGame baseGame) {
        ObservableList<City> list = FXCollections.observableArrayList();
        List<City> cities = baseGame.getCitiesWherePlayerHasBuilt(nickname);
        for (City city : cities) {
            Reward reward = city.getReward();
            if (reward != null && reward.getNobilityStepCount() == 0) {
                list.add(city);
            }
        }
        return list;
    }

    private void changeButtonVisibility(int count, Button button) {
        int size = mCityTable.getSelectionModel().getSelectedIndices().size();
        button.setDisable(size == 0 || size > count);
    }

    /**
     * Handle dialog creation of first special bonus
     * @param parent root
     * @param count number of bonus
     * @param baseGame game state
     * @param callback to graphicaluserinterface
     * @return
     */
    public static JDialog createDialog(Component parent, int count, String nickname, BaseGame baseGame, Callback callback) {
        FirstSpecialBonusPanel panel = new FirstSpecialBonusPanel(count, nickname, baseGame, callback);
        return GuiUtils.createDialog(parent, "Special nobility reward", 600, 400, panel);
    }

    /**
     * Callback to the graphical user interface class.
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * Ask for first special rewards.
         * @param cities provided.
         */
        void earnFirstSpecialRewards(List<String> cities);
    }
}
package it.polimi.cof.ui.gui;

import it.polimi.cof.util.Debug;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

/**
 * This class is used at start of client instance while others component are loading.
 */
/*package-local*/ class SplashScreenPanel extends JPanel {

    private transient BufferedImage mImage;

    /*package-local*/ SplashScreenPanel() {
        URL url = this.getClass().getClassLoader().getResource("images/splash_screen.jpg");
        try {
            if (url != null) {
                mImage = ImageIO.read(url);
            }
        } catch (IOException e) {
            Debug.error("IO Exception while loading splash screen image", e);
        }
    }

    /**
     * Paint filling the entire screen with the image
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(mImage, 0, 0, getWidth(), getHeight(), this);
    }
}
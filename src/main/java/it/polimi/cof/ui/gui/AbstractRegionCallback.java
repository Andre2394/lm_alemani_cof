package it.polimi.cof.ui.gui;

/**
 * Handle mouse click on region
 */
@FunctionalInterface
public interface AbstractRegionCallback {

    /**
     * Open dialog with information balcony clicked
     * @param region of the balcony
     * @param colors on that balcony
     */
    void onBalconyClick(String region, String[] colors);
}
package it.polimi.cof.ui.gui.control;

import it.polimi.cof.model.Councilor;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

/**
 * This class is used by {@link ListView} to represent the {@link Councilor} item in the list.
 */
public class CouncillorListCell extends ListCell<Councilor> {

    /**
     * @param item coulcillor in representative list
     * @param empty true if an empty list
     */
    @Override
    protected void updateItem(Councilor item, boolean empty) {
        super.updateItem(item, empty);
        Platform.runLater(() -> {
            if (item == null || empty) {
                setGraphic(null);
            } else {
                Canvas canvas = new Canvas(54, 120);
                Painter.drawCouncillor(canvas, item);
                setGraphic(canvas);
            }
        });
    }
}
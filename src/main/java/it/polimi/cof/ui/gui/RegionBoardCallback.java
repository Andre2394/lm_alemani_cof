package it.polimi.cof.ui.gui;

import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.City;

/**
 * Handle mouse click on region
 */
public interface RegionBoardCallback extends AbstractRegionCallback {

    /**
     * This event make a call to open a dialog of city information
     * @param city clicked
     */
    void onCityClicked(City city);

    /**
     *  Open dialog with information on Business Permit Tile clicked
     * @param businessPermitTile clicked
     */
    void onPermitTileClick(BusinessPermitTile businessPermitTile);
}
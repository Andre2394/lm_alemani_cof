package it.polimi.cof.ui.gui;

import it.polimi.cof.model.*;
import it.polimi.cof.ui.gui.components.GameBoardPane;
import it.polimi.cof.ui.gui.dialog.AddLinkPanel;
import it.polimi.cof.ui.gui.dialog.EditCityRewardPanel;
import it.polimi.cof.util.Debug;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;

import javax.swing.*;
import java.util.*;

/**
 * This class handle the GUI to be the player able to edit game rules value in the map, nobility track and business permit tale.
 * It shows default configuration at start.
 * Created by Guido on 13/06/2016.
 */
/*package-local*/ class GameConfigurator extends JFXPanel implements AddLinkPanel.Callback, EditCityRewardPanel.Callback {

    private static final String PLEASE = "PLEASE";
    private static final String TABLE_CELL_STYLE = "-fx-alignment: CENTER;";
    private static final String CLICK = "click";

    private final Callback mCallback;
    private Configuration mConfig;

    private TextField mTimeField;

    // Map pane global variables
    private GameBoardPane mGameBoard;
    private ChoiceBox<RegionBoard> mLeftRegionChoiceBox;
    private ChoiceBox<City> mLeftRegionCityChoiceBox;
    private ListView<String> mLeftRegionCityList;
    private String mLeftRegionSelectedCity;
    private Button mLeftRemoveLinkButton;
    private ChoiceBox<RegionBoard> mCenterRegionChoiceBox;
    private ChoiceBox<City> mCenterRegionCityChoiceBox;
    private ListView<String> mCenterRegionCityList;
    private String mCenterRegionSelectedCity;
    private Button mCenterRemoveLinkButton;
    private ChoiceBox<RegionBoard> mRightRegionChoiceBox;
    private ChoiceBox<City> mRightRegionCityChoiceBox;
    private ListView<String> mRightRegionCityList;
    private String mRightRegionSelectedCity;
    private Button mRightRemoveLinkButton;

    // Nobility track global variables
    private TextField mNobilityPositionField;
    private TextField mNobilityAssistantsField;
    private TextField mNobilityVictoryPointsField;
    private TextField mNobilityCoinsField;
    private TextField mNobilityPoliticCardsField;
    private TextField mNobilityMainActionsField;
    private TextField mNobilityFirstSpecialRewardField;
    private TextField mNobilitySecondSpecialRewardField;
    private TextField mNobilityThirdSpecialRewardField;
    private TableView<Map.Entry<Integer, NobilityReward>> mTableNobility;
    private TableColumn<Map.Entry<Integer, NobilityReward>, Integer> mTableNobilitySortColumn;
    private TableColumn.SortType mTableNobilitySortType;
    private ObservableList<Map.Entry<Integer, NobilityReward>> mNobilityTrack;

    // Business permit tile table variables
    private TextField mPermitTileCitiesField;
    private TextField mPermitTileAssistantsField;
    private TextField mPermitTileVictoryPointsField;
    private TextField mPermitTileNobilityStepsField;
    private TextField mPermitTileCoinsField;
    private TextField mPermitTilePoliticCardsField;
    private TextField mPermitTileMainActionsField;
    private TableView<BusinessPermitTile> mTableTiles;
    private TableColumn<BusinessPermitTile, String> mTableTilesSortColumn;
    private TableColumn.SortType mTableTilesSortType;
    private ObservableList<BusinessPermitTile> mPermitTiles;

    private JDialog mAddLinkDialog;
    private JDialog mEditCityRewardDialog;
    private ListView<String> mCurrentList;

    /**
     * Prepare events and information for the scene.
     *
     * @param callback to the main class.
     */
    /*package-local*/ GameConfigurator(Callback callback) {
        mCallback = callback;
        Parent root = GuiUtils.loadFxml(this, "sGC");
        createGameBoard(root);
        createNobilityTrackTable(root);
        createBusinessPermitTileTable(root);
        // setup nobility track views
        mNobilityPositionField = (TextField) root.lookup("#nobilityPositionField");
        mNobilityAssistantsField = (TextField) root.lookup("#assField");
        mNobilityVictoryPointsField = (TextField) root.lookup("#vicField");
        mNobilityCoinsField = (TextField) root.lookup("#coinsField");
        mNobilityPoliticCardsField = (TextField) root.lookup("#polField");
        mNobilityMainActionsField = (TextField) root.lookup("#mainField");
        mNobilityFirstSpecialRewardField = (TextField) root.lookup("#firstField");
        mNobilitySecondSpecialRewardField = (TextField) root.lookup("#secondField");
        mNobilityThirdSpecialRewardField = (TextField) root.lookup("#thirdField");
        Button nobilityAddButton = (Button) root.lookup("#addButton");
        Button nobilityRemoveButton = (Button) root.lookup("#remButton");
        nobilityAddButton.setOnAction(event -> {
            Audio.play(CLICK);
            addEditNobilityReward();
        });
        nobilityRemoveButton.setOnAction(event -> {
            Audio.play(CLICK);
            removeEditNobilityReward();
        });
        // setup permit tile views
        mPermitTileCitiesField = (TextField) root.lookup("#iniTField");
        mPermitTileAssistantsField = (TextField) root.lookup("#assTField");
        mPermitTileVictoryPointsField = (TextField) root.lookup("#vicTField");
        mPermitTileNobilityStepsField = (TextField) root.lookup("#nobTField");
        mPermitTileCoinsField = (TextField) root.lookup("#coinsTField");
        mPermitTilePoliticCardsField = (TextField) root.lookup("#polTField");
        mPermitTileMainActionsField = (TextField) root.lookup("#mainTField");
        Button permitTileAddButton = (Button) root.lookup("#changeButton");
        permitTileAddButton.setOnAction(event -> {
            Audio.play(CLICK);
            addPermitTile();
        });
        // setup bottom views
        Button applyButton = (Button) root.lookup("#applyButton");
        mTimeField = (TextField) root.lookup("#timeField");
        applyButton.setOnAction(event -> applyConfiguration());
    }

    /**
     * Events and information for GameBoard in configuration
     * @param root over which GameBoard is showed
     */
    @SuppressWarnings("unchecked")
    private void createGameBoard(Parent root) {
        mGameBoard = new GameBoardPane(false, null);
        mLeftRegionChoiceBox = (ChoiceBox<RegionBoard>) root.lookup("#leftMapChoiceBox");
        mLeftRegionCityChoiceBox = (ChoiceBox<City>) root.lookup("#leftMapCityChoiceBox");
        mLeftRegionCityList = (ListView<String>) root.lookup("#leftMapCityList");
        Button leftAddLinkButton = (Button) root.lookup("#leftAddLinkButton");
        Button leftEditCityRewardButton = (Button) root.lookup("#leftEditCityRewardButton");
        mLeftRemoveLinkButton = (Button) root.lookup("#leftRemoveLinkButton");
        mCenterRegionChoiceBox = (ChoiceBox<RegionBoard>) root.lookup("#centerMapChoiceBox");
        mCenterRegionCityChoiceBox = (ChoiceBox<City>) root.lookup("#centerMapCityChoiceBox");
        mCenterRegionCityList = (ListView<String>) root.lookup("#centerMapCityList");
        Button centerAddLinkButton = (Button) root.lookup("#centerAddLinkButton");
        Button centerEditCityRewardButton = (Button) root.lookup("#centerEditCityRewardButton");
        mCenterRemoveLinkButton = (Button) root.lookup("#centerRemoveLinkButton");
        mRightRegionChoiceBox = (ChoiceBox<RegionBoard>) root.lookup("#rightMapChoiceBox");
        mRightRegionCityChoiceBox = (ChoiceBox<City>) root.lookup("#rightMapCityChoiceBox");
        mRightRegionCityList = (ListView<String>) root.lookup("#rightMapCityList");
        Button rightAddLinkButton = (Button) root.lookup("#rightAddLinkButton");
        Button rightEditCityRewardButton = (Button) root.lookup("#rightEditCityRewardButton");
        mRightRemoveLinkButton = (Button) root.lookup("#rightRemoveLinkButton");
        mLeftRegionChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mGameBoard.setLeftRegionBoard(newValue);
            mLeftRegionCityChoiceBox.setItems(FXCollections.observableList(new ArrayList<>(newValue.getCities().values())));
            mLeftRegionCityChoiceBox.getSelectionModel().selectFirst();
        });
        mLeftRegionCityChoiceBox.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> refreshCityLinks(mLeftRegionChoiceBox.getValue(), mLeftRegionCityList, newValue)
        );
        mLeftRegionCityList.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    mLeftRegionSelectedCity = newValue;
                    mLeftRemoveLinkButton.setDisable(newValue == null);
                }
        );
        leftAddLinkButton.setOnAction(event -> {
            Audio.play(CLICK);
            openAddLinkWindow(mLeftRegionChoiceBox.getValue(), mLeftRegionCityChoiceBox.getValue(), new String[]{"#1", "#2", "#3"});
            mCurrentList = mLeftRegionCityList;
        });
        mLeftRemoveLinkButton.setOnAction(event -> {
            Audio.play(CLICK);
            removeLink(mLeftRegionChoiceBox.getValue(), mLeftRegionCityList, mLeftRegionCityChoiceBox.getValue(), mLeftRegionSelectedCity);
            mGameBoard.invalidate();
        });
        leftEditCityRewardButton.setOnAction(event -> {
            Audio.play(CLICK);
            openEditCityRewardWindow(mLeftRegionCityChoiceBox.getValue());
        });

        mCenterRegionChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mGameBoard.setCenterRegionBoard(newValue);
            mCenterRegionCityChoiceBox.setItems(FXCollections.observableList(new ArrayList<>(newValue.getCities().values())));
            mCenterRegionCityChoiceBox.getSelectionModel().selectFirst();
        });
        mCenterRegionCityChoiceBox.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> refreshCityLinks(mCenterRegionChoiceBox.getValue(), mCenterRegionCityList, newValue)
        );
        mCenterRegionCityList.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    mCenterRegionSelectedCity = newValue;
                    mCenterRemoveLinkButton.setDisable(newValue == null);
                }
        );
        centerAddLinkButton.setOnAction(event -> {
            Audio.play(CLICK);
            openAddLinkWindow(mCenterRegionChoiceBox.getValue(), mCenterRegionCityChoiceBox.getValue(), new String[]{"#1", "#2", "#3", "#4", "#5", "#6"});
            mCurrentList = mCenterRegionCityList;
        });
        mCenterRemoveLinkButton.setOnAction(event -> {
            Audio.play(CLICK);
            removeLink(mCenterRegionChoiceBox.getValue(), mCenterRegionCityList, mCenterRegionCityChoiceBox.getValue(), mCenterRegionSelectedCity);
            mGameBoard.invalidate();
        });
        centerEditCityRewardButton.setOnAction(event -> {
            Audio.play(CLICK);
            openEditCityRewardWindow(mCenterRegionCityChoiceBox.getValue());
        });


        mRightRegionChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mGameBoard.setRightRegionBoard(newValue);
            mRightRegionCityChoiceBox.setItems(FXCollections.observableList(new ArrayList<>(newValue.getCities().values())));
            mRightRegionCityChoiceBox.getSelectionModel().selectFirst();
        });
        mRightRegionCityChoiceBox.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> refreshCityLinks(mRightRegionChoiceBox.getValue(), mRightRegionCityList, newValue)
        );
        mRightRegionCityList.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    mRightRegionSelectedCity = newValue;
                    mRightRemoveLinkButton.setDisable(newValue == null);
                }
        );
        rightAddLinkButton.setOnAction(event -> {
            Audio.play(CLICK);
            openAddLinkWindow(mRightRegionChoiceBox.getValue(), mRightRegionCityChoiceBox.getValue(), new String[]{"#4", "#5", "#6"});
            mCurrentList = mRightRegionCityList;
        });
        mRightRemoveLinkButton.setOnAction(event -> {
            Audio.play(CLICK);
            removeLink(mRightRegionChoiceBox.getValue(), mRightRegionCityList, mRightRegionCityChoiceBox.getValue(), mRightRegionSelectedCity);
            mGameBoard.invalidate();
        });
        rightEditCityRewardButton.setOnAction(event -> {
            Audio.play(CLICK);
            openEditCityRewardWindow(mRightRegionCityChoiceBox.getValue());
        });

        Platform.runLater(() -> {
            AnchorPane gameBoardContainer = (AnchorPane) root.lookup("#mapContainer");
            gameBoardContainer.getChildren().add(mGameBoard);
            AnchorPane.setLeftAnchor(mGameBoard, 0d);
            AnchorPane.setTopAnchor(mGameBoard, 0d);
            AnchorPane.setRightAnchor(mGameBoard, 0d);
            AnchorPane.setBottomAnchor(mGameBoard, 0d);
        });
    }

    private void openAddLinkWindow(RegionBoard regionBoard, City city, String[] gates) {
        mAddLinkDialog = AddLinkPanel.createDialog(this, regionBoard, city, gates, this);
        mAddLinkDialog.setVisible(true);
    }

    /**
     * Refresh GameBoard automatically when it happens modify to map
     * @param regionBoard
     * @param tableView
     * @param city
     */
    private void refreshCityLinks(RegionBoard regionBoard, ListView<String> tableView, City city) {
        if (city == null) {
            return;
        }
        ObservableList<String> cities = FXCollections.observableArrayList();
        for (Link link : regionBoard.getLinks()) {
            if (link.getCityFrom().equals(city.getName())) {
                cities.add(link.getCityTo());
            } else if (link.getCityTo().equals(city.getName())) {
                cities.add(link.getCityFrom());
            }
        }
        tableView.setItems(cities);
    }

    private void removeLink(RegionBoard regionBoard, ListView<String> tableView, City city, String to) {
        if (to != null) {
            Iterator<Link> iterator = regionBoard.getLinks().iterator();
            while (iterator.hasNext()) {
                Link link = iterator.next();
                if (link.getCityFrom().equals(city.getName()) && link.getCityTo().equals(to)) {
                    iterator.remove();
                } else if (link.getCityFrom().equals(to) && link.getCityTo().equals(city.getName())) {
                    iterator.remove();
                }
            }
            refreshCityLinks(regionBoard, tableView, city);
        } else {
            Debug.error("Cannot remove a 'null' city");
        }
    }

    private void openEditCityRewardWindow(City city) {
        mEditCityRewardDialog = EditCityRewardPanel.createDialog(this, city, this);
        mEditCityRewardDialog.setVisible(true);
    }

    @SuppressWarnings("unchecked")
    private void createNobilityTrackTable(Parent root) {
        mTableNobility = (TableView<Map.Entry<Integer, NobilityReward>>) root.lookup("#tableNobility");
        // create table columns
        TableColumn<Map.Entry<Integer, NobilityReward>, Integer> positionColumn = new TableColumn<>("Position");
        TableColumn<Map.Entry<Integer, NobilityReward>, Integer> assistantsColumn = new TableColumn<>("Assistants");
        TableColumn<Map.Entry<Integer, NobilityReward>, Integer> victoryPointsColumn = new TableColumn<>("Victory Points");
        TableColumn<Map.Entry<Integer, NobilityReward>, Integer> coinsColumn = new TableColumn<>("Coins");
        TableColumn<Map.Entry<Integer, NobilityReward>, Integer> politicsCardsColumn = new TableColumn<>("Politics Cards");
        TableColumn<Map.Entry<Integer, NobilityReward>, Integer> mainActionColumn = new TableColumn<>("Main Action");
        TableColumn<Map.Entry<Integer, NobilityReward>, Integer> firstSpecialColumn = new TableColumn<>("First Special");
        TableColumn<Map.Entry<Integer, NobilityReward>, Integer> secondSpecialColumn = new TableColumn<>("Second Special");
        TableColumn<Map.Entry<Integer, NobilityReward>, Integer> thirdSpecialColumn = new TableColumn<>("Third Special");
        // apply css style
        positionColumn.setStyle(TABLE_CELL_STYLE);
        assistantsColumn.setStyle(TABLE_CELL_STYLE);
        victoryPointsColumn.setStyle(TABLE_CELL_STYLE);
        coinsColumn.setStyle(TABLE_CELL_STYLE);
        politicsCardsColumn.setStyle(TABLE_CELL_STYLE);
        mainActionColumn.setStyle(TABLE_CELL_STYLE);
        firstSpecialColumn.setStyle(TABLE_CELL_STYLE);
        secondSpecialColumn.setStyle(TABLE_CELL_STYLE);
        thirdSpecialColumn.setStyle(TABLE_CELL_STYLE);
        // create cell value factory
        positionColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getKey());
            return simpleObjectProperty;
        });
        assistantsColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getValue().getAssistantCount());
            return simpleObjectProperty;
        });
        victoryPointsColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getValue().getVictoryPointCount());
            return simpleObjectProperty;
        });
        coinsColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getValue().getCoinsCount());
            return simpleObjectProperty;
        });
        politicsCardsColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getValue().getPoliticCardCount());
            return simpleObjectProperty;
        });
        mainActionColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getValue().getMainActionCount());
            return simpleObjectProperty;
        });
        firstSpecialColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getValue().getFirstSpecialBonusCount());
            return simpleObjectProperty;
        });
        secondSpecialColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getValue().getSecondSpecialBonusCount());
            return simpleObjectProperty;
        });
        thirdSpecialColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getValue().getThirdSpecialBonusCount());
            return simpleObjectProperty;
        });
        // set columns
        Platform.runLater(() -> {
            mTableNobility.getColumns().setAll(
                    positionColumn,
                    assistantsColumn,
                    victoryPointsColumn,
                    coinsColumn,
                    politicsCardsColumn,
                    mainActionColumn,
                    firstSpecialColumn,
                    secondSpecialColumn,
                    thirdSpecialColumn
            );
            // initialize sort
            mTableNobilitySortColumn = positionColumn;
            mTableNobilitySortType = TableColumn.SortType.ASCENDING;
            mTableNobilitySortColumn.setSortType(mTableNobilitySortType);
            mTableNobility.getSortOrder().add(mTableNobilitySortColumn);
        });
    }

    /**
     * Display default configuration in configuration.json
     * @param config configuration file
     */
    /*package-local*/ void showConfiguration(Configuration config) {
        mConfig = config;
        displayConfiguration();
    }

    /**
     * Use the mConfig variable to show current configuration
     */
    private void displayConfiguration() {
        mLeftRegionChoiceBox.setItems(FXCollections.observableList(mConfig.getLeftRegionBoards()));
        mCenterRegionChoiceBox.setItems(FXCollections.observableList(mConfig.getCenterRegionBoards()));
        mRightRegionChoiceBox.setItems(FXCollections.observableList(mConfig.getRightRegionBoards()));
        mLeftRegionChoiceBox.setConverter(new RegionBoardIndexConverter(mConfig.getLeftRegionBoards()));
        mLeftRegionCityChoiceBox.setConverter(new CityConverter());
        mCenterRegionChoiceBox.setConverter(new RegionBoardIndexConverter(mConfig.getCenterRegionBoards()));
        mCenterRegionCityChoiceBox.setConverter(new CityConverter());
        mRightRegionChoiceBox.setConverter(new RegionBoardIndexConverter(mConfig.getRightRegionBoards()));
        mRightRegionCityChoiceBox.setConverter(new CityConverter());
        mLeftRegionChoiceBox.getSelectionModel().selectFirst();
        mCenterRegionChoiceBox.getSelectionModel().selectFirst();
        mRightRegionChoiceBox.getSelectionModel().selectFirst();
        loadCurrentMap();
        mNobilityTrack = FXCollections.observableList(new ArrayList<>(mConfig.getNobilityTrack().entrySet()));
        mPermitTiles = FXCollections.observableList(mConfig.getBusinessPermitTiles());
        mTableNobility.setItems(mNobilityTrack);
        mTableTiles.setItems(mPermitTiles);
        Platform.runLater(() -> {
            mTableNobilitySortColumn.setSortType(mTableNobilitySortType);
            mTableNobility.getSortOrder().add(mTableNobilitySortColumn);
        });
    }

    private void loadCurrentMap() {
        mGameBoard.setLeftRegionBoard(mLeftRegionChoiceBox.getValue());
        mGameBoard.setCenterRegionBoard(mCenterRegionChoiceBox.getValue());
        mGameBoard.setRightRegionBoard(mRightRegionChoiceBox.getValue());
    }

    /**
     * Handle edit on nobility reward
     */
    private void addEditNobilityReward() {
        int position = parseNaturalInteger(mNobilityPositionField, 0);
        NobilityReward nobilityReward = getAndRemoveNobilityReward(position);
        // edit the nobility track item
        nobilityReward.addAssistants(parseNaturalInteger(mNobilityAssistantsField, 0));
        nobilityReward.addVictoryPointCount(parseNaturalInteger(mNobilityVictoryPointsField, 0));
        nobilityReward.addCoins(parseNaturalInteger(mNobilityCoinsField, 0));
        nobilityReward.addPoliticCards(parseNaturalInteger(mNobilityPoliticCardsField, 0));
        nobilityReward.addMainActions(parseNaturalInteger(mNobilityMainActionsField, 0));
        nobilityReward.addFirstSpecialBonus(parseNaturalInteger(mNobilityFirstSpecialRewardField, 0));
        nobilityReward.addSecondSpecialBonus(parseNaturalInteger(mNobilitySecondSpecialRewardField, 0));
        nobilityReward.addThirdSpecialBonus(parseNaturalInteger(mNobilityThirdSpecialRewardField, 0));
        // now check if this reward is valid
        if (nobilityReward.isValid()) {
            mNobilityTrack.add(new NobilityEntry(position, nobilityReward));
        }
    }


    /**
     * Give the Nobility Reward at position
     * @param position where is asked if there is a Nobility Reward
     * @return Nobility Reward or null if there is no one.
     */
    private NobilityReward getNobilityReward(int position) {
        for (Map.Entry<Integer, NobilityReward> item : mNobilityTrack) {
            if (item.getKey().equals(position)) {
                return item.getValue();
            }
        }
        throw new NoSuchElementException();
    }

    private void removeNobilityTrackEntry(int position) {
        Iterator<Map.Entry<Integer, NobilityReward>> iterator = mNobilityTrack.iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, NobilityReward> entry = iterator.next();
            if (entry.getKey().equals(position)) {
                iterator.remove();
                return;
            }
        }
        throw new NoSuchElementException();
    }

    private int parseNaturalInteger(TextField textField, int fallback) {
        try {
            int value = Integer.parseInt(textField.getText());
            return value < 0 ? fallback : value;
        } catch (NumberFormatException e) {
            return fallback;
        }
    }

    private void removeEditNobilityReward() {
        int position = parseNaturalInteger(mNobilityPositionField, 0);
        NobilityReward nobilityReward = getAndRemoveNobilityReward(position);
        // edit the nobility track item
        nobilityReward.removeAssistants(parseNaturalInteger(mNobilityAssistantsField, 0));
        nobilityReward.removeVictoryPointCount(parseNaturalInteger(mNobilityVictoryPointsField, 0));
        nobilityReward.removeCoins(parseNaturalInteger(mNobilityCoinsField, 0));
        nobilityReward.removePoliticCards(parseNaturalInteger(mNobilityPoliticCardsField, 0));
        nobilityReward.removeMainActions(parseNaturalInteger(mNobilityMainActionsField, 0));
        nobilityReward.removeFirstSpecialBonus(parseNaturalInteger(mNobilityFirstSpecialRewardField, 0));
        nobilityReward.removeSecondSpecialBonus(parseNaturalInteger(mNobilitySecondSpecialRewardField, 0));
        nobilityReward.removeThirdSpecialBonus(parseNaturalInteger(mNobilityThirdSpecialRewardField, 0));
        // now check if this reward is valid
        if (nobilityReward.isValid()) {
            mNobilityTrack.add(new NobilityEntry(position, nobilityReward));
        }
    }

    /**
     * Remove Nobility Reward
     * @param position of Nobility Reward that must be removed
     * @return nobility reward removed or a new one if there isn'y anyone in that position
     */
    private NobilityReward getAndRemoveNobilityReward(int position) {
        try {
            NobilityReward nobilityReward = getNobilityReward(position);
            removeNobilityTrackEntry(position);
            return nobilityReward;
        } catch (NoSuchElementException e) {
            Debug.debug("No item found on nobility track at position " + position, e);
        }
        return new NobilityReward();
    }

    /**
     * Information and events of the Table
     * @param root where is the table
     */
    @SuppressWarnings("unchecked")
    private void createBusinessPermitTileTable(Parent root) {
        mTableTiles = (TableView<BusinessPermitTile>) root.lookup("#tableTiles");
        // create columns
        TableColumn<BusinessPermitTile, String> cityColumn = new TableColumn<>("City letters");
        TableColumn<BusinessPermitTile, Integer> assistantsColumn = new TableColumn<>("Assistants");
        TableColumn<BusinessPermitTile, Integer> victoryPointsColumn = new TableColumn<>("Victory Points");
        TableColumn<BusinessPermitTile, Integer> nobilityStepsColumn = new TableColumn<>("Nobility Steps");
        TableColumn<BusinessPermitTile, Integer> coinsColumn = new TableColumn<>("Coins");
        TableColumn<BusinessPermitTile, Integer> politicsCardsColumn = new TableColumn<>("Politics Cards");
        TableColumn<BusinessPermitTile, Integer> mainActionColumn = new TableColumn<>("Main Action");
        TableColumn<BusinessPermitTile, Boolean> deleteColumn = new TableColumn<>("Action");
        // apply css style
        cityColumn.setStyle(TABLE_CELL_STYLE);
        assistantsColumn.setStyle(TABLE_CELL_STYLE);
        victoryPointsColumn.setStyle(TABLE_CELL_STYLE);
        nobilityStepsColumn.setStyle(TABLE_CELL_STYLE);
        coinsColumn.setStyle(TABLE_CELL_STYLE);
        politicsCardsColumn.setStyle(TABLE_CELL_STYLE);
        mainActionColumn.setStyle(TABLE_CELL_STYLE);
        deleteColumn.setStyle(TABLE_CELL_STYLE);
        // create cell value factory
        cityColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<String> simpleObjectProperty = new SimpleObjectProperty<>();
            StringBuilder builder = new StringBuilder();
            for (char character : param.getValue().getCities()) {
                if (builder.length() != 0) {
                    builder.append(", ");
                }
                builder.append(character);
            }
            simpleObjectProperty.setValue(builder.toString());
            return simpleObjectProperty;
        });
        assistantsColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getAssistantCount());
            return simpleObjectProperty;
        });
        victoryPointsColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getVictoryPointCount());
            return simpleObjectProperty;
        });
        nobilityStepsColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getNobilityStepCount());
            return simpleObjectProperty;
        });
        coinsColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getCoinsCount());
            return simpleObjectProperty;
        });
        politicsCardsColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getPoliticCardCount());
            return simpleObjectProperty;
        });
        mainActionColumn.setCellValueFactory(param -> {
            SimpleObjectProperty<Integer> simpleObjectProperty = new SimpleObjectProperty<>();
            simpleObjectProperty.setValue(param.getValue().getMainActionCount());
            return simpleObjectProperty;
        });
        deleteColumn.setCellFactory(param -> new DeleteTableCell());
        // set columns
        Platform.runLater(() -> {
            mTableTiles.getColumns().setAll(
                    cityColumn,
                    assistantsColumn,
                    victoryPointsColumn,
                    nobilityStepsColumn,
                    coinsColumn,
                    politicsCardsColumn,
                    mainActionColumn,
                    deleteColumn
            );
            // initialize sort
            mTableTilesSortColumn = cityColumn;
            mTableTilesSortType = TableColumn.SortType.ASCENDING;
            mTableTilesSortColumn.setSortType(mTableTilesSortType);
            mTableTiles.getSortOrder().add(mTableTilesSortColumn);
        });
    }

    /**
     * Handle the add of a permit tile
     */
    private void addPermitTile() {
        String cleanCitiesString = mPermitTileCitiesField.getText().replaceAll("[^a-zA-Z]", "");
        BusinessPermitTile businessPermitTile = new BusinessPermitTile(
                parseNaturalInteger(mPermitTileAssistantsField, 0),
                parseNaturalInteger(mPermitTileVictoryPointsField, 0),
                parseNaturalInteger(mPermitTileNobilityStepsField, 0),
                parseNaturalInteger(mPermitTileCoinsField, 0),
                parseNaturalInteger(mPermitTilePoliticCardsField, 0),
                parseNaturalInteger(mPermitTileMainActionsField, 0),
                cleanCitiesString.toCharArray()
        );
        if (businessPermitTile.isValid()) {
            mPermitTiles.add(businessPermitTile);
        }
    }

    /**
     * Using the callback send to the GraphicalUserInterface class the information that the user has given
     */
    private void applyConfiguration() {
        try {
            Configuration configuration = generateConfiguration();
            Audio.play(CLICK);
            mCallback.onGameConfigured(configuration);
        } catch (NumberFormatException e) {
            int res = JOptionPane.showOptionDialog(this, "You must insert a valid timer value", PLEASE, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
            if (res == 0) {
                Audio.play(CLICK);
            }
        }
    }

    /**
     * Generate a Configuration file from information inserted on GUI
     * @return Configuration object
     */
    private Configuration generateConfiguration() {
        return new Configuration(
                Integer.parseInt(mTimeField.getText()),
                mLeftRegionChoiceBox.getValue(),
                mCenterRegionChoiceBox.getValue(),
                mRightRegionChoiceBox.getValue(),
                createMap(),
                new ArrayList<>(mPermitTiles)
        );
    }

    /**
     * Create map of nobility reward
     * @return nobility reward map
     */
    private Map<Integer, NobilityReward> createMap() {
        Map<Integer, NobilityReward> nobilityTrack = new HashMap<>();
        for (Map.Entry<Integer, NobilityReward> entry : mNobilityTrack) {
            nobilityTrack.put(entry.getKey(), entry.getValue());
        }
        return nobilityTrack;
    }

    /**
     * Add event must refresh RegionBoard interested
     * @param regionBoard where has been added a link
     * @param city involved
     */
    @Override
    public void onLinkAdded(RegionBoard regionBoard, City city) {
        mGameBoard.invalidate();
        refreshCityLinks(regionBoard, mCurrentList, city);
        onCancel();
    }

    /**
     * Handle event: change of a reward
     */
    @Override
    public void onRewardChanged() {
        mGameBoard.invalidate();
        onCancel();
    }

    /**
     * Cancel button
     */
    @Override
    public void onCancel() {
        if (mAddLinkDialog != null) {
            mAddLinkDialog.setVisible(false);
            mCurrentList = null;
        }
        if (mEditCityRewardDialog != null) {
            mEditCityRewardDialog.setVisible(false);
        }
    }

    /**
     * Callback to the graphical user interface class.
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * @param configuration configuration files generated by the users choice
         */
        void onGameConfigured(Configuration configuration);
    }

    private class NobilityEntry implements Map.Entry<Integer, NobilityReward> {

        private int mPosition;
        private NobilityReward mNobilityReward;

        private NobilityEntry(int position, NobilityReward nobilityReward) {
            mPosition = position;
            mNobilityReward = nobilityReward;
        }

        /**
         * @return position key
         */
        @Override
        public Integer getKey() {
            return mPosition;
        }

        /**
         * @return NobilityReward object
         */
        @Override
        public NobilityReward getValue() {
            return mNobilityReward;
        }

        /**
         * @param value that must be set to NobilityReward object
         * @return Nobility Reward changed
         */
        @Override
        public NobilityReward setValue(NobilityReward value) {
            mNobilityReward = value;
            return mNobilityReward;
        }
    }

    /**
     * Handle deleting cell in Table
     */
    private class DeleteTableCell extends TableCell<BusinessPermitTile, Boolean> {

        final Button mDeleteButton;

        private DeleteTableCell() {
            mDeleteButton = new Button("Delete");
            mDeleteButton.setOnAction(event -> {
                Audio.play(CLICK);
                mPermitTiles.remove(getIndex());
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                Platform.runLater(() -> setGraphic(mDeleteButton));
            }
        }
    }

    private class RegionBoardIndexConverter extends StringConverter<RegionBoard> {

        private List<RegionBoard> mRegionBoards;

        private RegionBoardIndexConverter(List<RegionBoard> regionBoards) {
            mRegionBoards = regionBoards;
        }

        @Override
        public String toString(RegionBoard object) {
            return String.format("Map %d", mRegionBoards.indexOf(object) + 1);
        }

        @Override
        public RegionBoard fromString(String string) {
            int index = Integer.parseInt(string.substring(4));
            return mRegionBoards.get(index - 1);
        }
    }

    private class CityConverter extends StringConverter<City> {

        /**
         * Give city name
         * @param city of City object
         * @return the name of the city
         */
        @Override
        public String toString(City city) {
            return city.getName();
        }

        /**
         * @param string name of the city
         * @return City object referred to
         */
        @Override
        public City fromString(String string) {
            return null;
        }
    }
}
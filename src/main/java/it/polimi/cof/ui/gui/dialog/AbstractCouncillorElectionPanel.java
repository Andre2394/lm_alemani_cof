package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.Councilor;
import it.polimi.cof.model.RegionBoard;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.components.Painter;
import it.polimi.cof.ui.gui.control.CouncillorListCell;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;

import java.util.Arrays;

/**
 * Abstract class that handle general events on Councillor election Panel
 */
/*package-local*/ abstract class AbstractCouncillorElectionPanel extends JFXPanel {

    private final BaseGame mBaseGame;

    private Canvas mBalconyCanvas;
    private ChoiceBox<String> mRegionBoard;

    private Councilor mSelectedCouncillor;

    /**
     * * Prepare events and information for the scene.
     * @param baseGame game state
     * @param resource which fxml must be loaded by the situation, given from resource string
     */
    @SuppressWarnings("unchecked")
    /*package-local*/ AbstractCouncillorElectionPanel(BaseGame baseGame, String resource) {
        mBaseGame = baseGame;
        Parent root = GuiUtils.loadFxml(this, resource);
        mBalconyCanvas = (Canvas) root.lookup("#canvasBalcony");
        mRegionBoard = (ChoiceBox<String>) root.lookup("#regionChoiceBox");
        mRegionBoard.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> drawBalcony(newValue));
        ListView<Councilor> councillorListView = (ListView<Councilor>) root.lookup("#councillorList");
        councillorListView.setCellFactory(param -> new CouncillorListCell());
        Button applyButton = (Button) root.lookup("#applyButton");
        applyButton.setOnAction(event -> onCouncillorSelected(mSelectedCouncillor, mRegionBoard.getValue()));
        Button cancelButton = (Button) root.lookup("#cancelButton");
        cancelButton.setOnAction(event -> onCancel());
        councillorListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mSelectedCouncillor = newValue;
            applyButton.setDisable(mSelectedCouncillor == null);
        });
        Platform.runLater(() -> {
            councillorListView.setItems(FXCollections.observableArrayList(baseGame.getCouncilors()));
            mRegionBoard.setItems(FXCollections.observableList(Arrays.asList(
                    RegionBoard.LEFT_REGION,
                    RegionBoard.CENTER_REGION,
                    RegionBoard.RIGHT_REGION,
                    RegionBoard.KING_REGION
            )));
            mRegionBoard.getSelectionModel().selectFirst();
        });
    }

    private void drawBalcony(String region) {
        Painter.drawBalcony(mBalconyCanvas, mBaseGame.getBalconyColors(region));
    }

    /**
     * Abstract method for event on Councillor selected
     * @param councilor selected
     * @param region of the councillor selected
     */
    protected abstract void onCouncillorSelected(Councilor councilor, String region);

    /**
     * Abstract cancel button for panel
     */
    protected abstract void onCancel();
}
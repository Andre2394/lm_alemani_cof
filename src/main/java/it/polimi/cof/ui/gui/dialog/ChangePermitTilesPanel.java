package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.RegionBoard;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

/**
 * Panel that handle the change of permit tiles
 */
public class ChangePermitTilesPanel extends JFXPanel {

    private final BaseGame mBaseGame;
    private final Callback mCallback;

    private Canvas mPermitTilesCanvas;
    private ChoiceBox<String> mRegionBoard;

    @SuppressWarnings("unchecked")
    private ChangePermitTilesPanel(BaseGame baseGame, Callback callback) {
        mBaseGame = baseGame;
        mCallback = callback;
        Parent root = GuiUtils.loadFxml(this, "dialog_change_permit_tiles");
        mPermitTilesCanvas = (Canvas) root.lookup("#permitTilesCanvas");
        mRegionBoard = (ChoiceBox<String>) root.lookup("#regionChoiceBox");
        Button applyButton = (Button) root.lookup("#applyButton");
        Button cancelButton = (Button) root.lookup("#cancelButton");
        mRegionBoard.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> drawPermitTiles(newValue));
        applyButton.setOnAction(event -> mCallback.onBusinessPermitTilesChanged(mRegionBoard.getValue()));
        cancelButton.setOnAction(event -> mCallback.onCancel());
        Platform.runLater(() -> {
            mRegionBoard.setItems(FXCollections.observableList(Arrays.asList(RegionBoard.LEFT_REGION, RegionBoard.CENTER_REGION, RegionBoard.RIGHT_REGION)));
            mRegionBoard.getSelectionModel().selectFirst();
        });
    }

    /**
     * Handle dialog creation for change of permit tiles
     * @param parent component over
     * @param baseGame game state
     * @param callback to GraphicalUserInterface
     * @return
     */
    public static JDialog createDialog(Component parent, BaseGame baseGame, Callback callback) {
        ChangePermitTilesPanel panel = new ChangePermitTilesPanel(baseGame, callback);
        return GuiUtils.createDialog(parent, "Fast action 2", 500, 400, panel);
    }

    private void drawPermitTiles(String region) {
        List<BusinessPermitTile> permitTiles = mBaseGame.getVisiblePermitTiles(region);
        GraphicsContext context = mPermitTilesCanvas.getGraphicsContext2D();
        double permitTileWidth = 162;
        double permitTileHeight = 180;
        Painter.drawPermitTile(context, permitTiles.get(0), 39, 10, permitTileWidth, permitTileHeight);
        Painter.drawPermitTile(context, permitTiles.get(1), 279, 10, permitTileWidth, permitTileHeight);
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * Giving an assistants, a player is able to change two business permit tile and put them at the bottom and discard two new cards from cards list
         * @param region where the permit tiles are changed.
         */
        void onBusinessPermitTilesChanged(String region);

        /**
         * Disable current window
         */
        void onCancel();
    }
}
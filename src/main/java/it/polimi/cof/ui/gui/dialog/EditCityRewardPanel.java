package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.City;
import it.polimi.cof.model.Reward;
import it.polimi.cof.ui.gui.GuiUtils;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javax.swing.*;
import java.awt.*;

/**
 * Panel that permit to edit city reward
 */
public class EditCityRewardPanel extends JFXPanel {

    private final City mCity;
    private final Callback mCallback;

    private Label mLabel;
    private TextField mAssistants;
    private TextField mVictoryPoints;
    private TextField mNobilitySteps;
    private TextField mCoins;
    private TextField mPoliticCards;
    private TextField mMainActions;

    private EditCityRewardPanel(City city, Callback callback) {
        mCity = city;
        mCallback = callback;
        Parent root = GuiUtils.loadFxml(this, "dialog_edit_reward");
        mLabel = (Label) root.lookup("#labelCityName");
        mAssistants = (TextField) root.lookup("#assistantsField");
        mVictoryPoints = (TextField) root.lookup("#victoryPointsField");
        mNobilitySteps = (TextField) root.lookup("#nobilityStepsField");
        mCoins = (TextField) root.lookup("#coinsField");
        mPoliticCards = (TextField) root.lookup("#politicCardsField");
        mMainActions = (TextField) root.lookup("#mainActionsField");
        Button applyButton = (Button) root.lookup("#applyButton");
        applyButton.setOnAction(event -> addReward());
        Button cancelButton = (Button) root.lookup("#cancelButton");
        cancelButton.setOnAction(event -> mCallback.onCancel());
        Platform.runLater(() -> {
            mLabel.setText(String.format("Editing reward for %s city", city.getName()));
            loadReward(city.getReward());
        });
    }

    /**
     * Handle dialog creation of city detail
     * @param parent root
     * @param city involved
     * @param callback to GraphicalUserInterface
     * @return
     */
    public static JDialog createDialog(Component parent, City city, Callback callback) {
        EditCityRewardPanel panel = new EditCityRewardPanel(city, callback);
        return GuiUtils.createDialog(parent, "Edit city reward", 280, 350, panel);
    }

    private void loadReward(Reward reward) {
        setTextNumber(mAssistants, reward != null ? reward.getAssistantCount() : 0);
        setTextNumber(mVictoryPoints, reward != null ? reward.getVictoryPointCount() : 0);
        setTextNumber(mNobilitySteps, reward != null ? reward.getNobilityStepCount() : 0);
        setTextNumber(mCoins, reward != null ? reward.getCoinsCount() : 0);
        setTextNumber(mPoliticCards, reward != null ? reward.getPoliticCardCount() : 0);
        setTextNumber(mMainActions, reward != null ? reward.getMainActionCount() : 0);
    }

    private void addReward() {
        Reward reward = new Reward(
                parseNaturalInteger(mAssistants, 0),
                parseNaturalInteger(mVictoryPoints, 0),
                parseNaturalInteger(mNobilitySteps, 0),
                parseNaturalInteger(mCoins, 0),
                parseNaturalInteger(mPoliticCards, 0),
                parseNaturalInteger(mMainActions, 0)
        );
        mCity.setReward(reward.isValid() ? reward : null);
        mCallback.onRewardChanged();
    }

    private int parseNaturalInteger(TextField textField, int fallback) {
        try {
            int value = Integer.parseInt(textField.getText());
            return value < 0 ? fallback : value;
        } catch (NumberFormatException e) {
            return fallback;
        }
    }

    private void setTextNumber(TextField textField, int value) {
        textField.setText(String.format("%d", value));
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * Handle event: change of a reward
         */
        void onRewardChanged();

        /**
         * Cancel button
         */
        void onCancel();
    }
}
package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.RegionBoard;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.control.PermitTileListCell;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Panel that handle second special bonus
 */
public class SecondSpecialBonusPanel extends JFXPanel {

    private final BaseGame mBaseGame;
    private ListView<BusinessPermitTile> mList;
    private ChoiceBox<String> mRegionBoard;

    private BusinessPermitTile mBusinessPermitTiles;

    @SuppressWarnings("unchecked")
    private SecondSpecialBonusPanel(int count, BaseGame baseGame, Callback callback) {
        mBaseGame = baseGame;
        Parent root = GuiUtils.loadFxml(this, "dialog_second_special_reward");
        Label helpLabel = (Label) root.lookup("#helpLabel");
        mList = (ListView<BusinessPermitTile>) root.lookup("#permitTileList");
        mRegionBoard = (ChoiceBox<String>) root.lookup("#regionChoiceBox");
        Button applyButton = (Button) root.lookup("#applyButton");
        applyButton.setOnAction(event -> earnReward(callback));
        mList.setCellFactory(param -> new PermitTileListCell());
        mList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mBusinessPermitTiles = newValue;
            applyButton.setDisable(newValue == null);
        });
        mRegionBoard.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> loadPermitTiles(newValue));
        Platform.runLater(() -> {
            mRegionBoard.setItems(FXCollections.observableList(Arrays.asList(RegionBoard.LEFT_REGION, RegionBoard.CENTER_REGION, RegionBoard.RIGHT_REGION)));
            mRegionBoard.getSelectionModel().selectFirst();
            helpLabel.setText(String.format("Select %d business permit tile to take for free", count));
        });
    }

    private void earnReward(Callback callback) {
        List<String> regions = new ArrayList<>();
        List<Integer> indices = new ArrayList<>();
        regions.add(mRegionBoard.getValue());
        indices.add(mBaseGame.getVisiblePermitTiles(mRegionBoard.getValue()).indexOf(mBusinessPermitTiles) + 1);
        callback.earnSecondSpecialRewards(regions, indices);
    }

    private void loadPermitTiles(String region) {
        Platform.runLater(() -> mList.setItems(FXCollections.observableList(mBaseGame.getVisiblePermitTiles(region))));
    }

    /**
     * Handle dialog creation of second special bonus
     * @param parent root
     * @param count number of bonus
     * @param baseGame game state
     * @param callback to graphicaluserinterface
     * @return
     */
    public static JDialog createDialog(Component parent, int count, BaseGame baseGame, Callback callback) {
        SecondSpecialBonusPanel panel = new SecondSpecialBonusPanel(count, baseGame, callback);
        return GuiUtils.createDialog(parent, "Special nobility reward", 600, 500, panel);
    }

    /**
     * Callback to the graphical user interface class.
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * Ask for second special rewards.
         * @param regions provided.
         * @param indices provided.
         */
        void earnSecondSpecialRewards(List<String> regions, List<Integer> indices);
    }
}
package it.polimi.cof.ui.gui;

import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URL;

/**
 * Useful code used in class relatives to the GUI
 */
public class GuiUtils {

    private GuiUtils() {
        // hide constructor
    }


    /**
     *    This method is used to load FXML files of the GUI made with editor.
     *   It is useful to use this method because it wouldn't be nice repeat in many classes these load command.
     * @param panel used for loading FXML
     * @param resource name of the file in /resource/fxml directory
     * @return root of fxml
     */
    public static Parent loadFxml(JFXPanel panel, String resource) {
        URL url = getUrlFromFile(resource);
        return loadFxmlFromUrl(panel, url);
    }

    /**
     * Overloading method
     * @param panel used for loading FXML
     * @param classLoader class that is requesting the FXML
     * @param resource name of the file in /resource/fxml directory
     * @return root of FXML
     */
    public static Parent loadFxml(JFXPanel panel, ClassLoader classLoader, String resource) {
        URL url = getUrlFromFile(classLoader, resource);
        return loadFxmlFromUrl(panel, url);
    }

    /**
     * Get a URL from string name of resource in fxml path
     * @param resource filename in path
     * @return correct URL used to FXML loading
     */
    private static URL getUrlFromFile(String resource) {
        return GuiUtils.class.getResource(String.format("/fxml/%s.fxml", resource));
    }

    /**
     * Overloading of method
     * @param classLoader caller class for FXML loading
     * @param resource filename in path
     * @return correct url to filename in path
     */
    private static URL getUrlFromFile(ClassLoader classLoader, String resource) {
        return classLoader.getResource(String.format("/fxml/%s.fxml", resource));
    }

    /**
     * Loading FXML from url
     * @param panel over which will be loaded FXML file
     * @param url to get fxml file
     * @return parent (root) of fxml
     */
    private static Parent loadFxmlFromUrl(JFXPanel panel, URL url) {
        if (url == null) {
            throw new NullPointerException("URL is null");
        }
        try {
            Parent root = FXMLLoader.load(url);
            Scene scene = new Scene(root);
            panel.setScene(scene);
            return root;
        } catch (IOException e) {
            throw new IllegalStateException("Cannot initialize gui", e);
        }
    }

    /**
     * Convert from a type to another
     * @param color awt object given
     * @return color converted
     */
    public static Color getColor(java.awt.Color color) {
        return Color.rgb(color.getRed(), color.getGreen(), color.getBlue());
    }

    /**
     * Snippet used to create a JDialog given valid argument
     * @param parent of the dialog
     * @param title of the dialog
     * @param width dimension
     * @param height dimension
     * @param resource that must be load on dialog
     * @return
     */
    public static JDialog createJFXDialog(Component parent, String title, int width, int height, String resource) {
        JFXPanel jfxPanel = new JFXPanel();
        loadFxml(jfxPanel, resource);
        return createDialog(parent, title, width, height, jfxPanel);
    }

    /**
     * Create dialog effectively with parameters on input
     * @param parent of the dialog
     * @param title of the dialog
     * @param width dimension
     * @param height dimension
     * @param panel that is added to the JDialog
     * @return
     */
    public static JDialog createDialog(Component parent, String title, int width, int height, JFXPanel panel) {
        JDialog dialog = new JDialog((JFrame) SwingUtilities.getWindowAncestor(parent));
        dialog.setTitle(title);
        dialog.setSize(width, height);
        dialog.add(panel);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        dialog.setLocation(dim.width / 2 - dialog.getSize().width / 2, dim.height / 2 - dialog.getSize().height / 2);
        return dialog;
    }

    /**
     * Makes visible/not visible a node(for example, a node just instanced)
     * @param node who ask to appear
     * @param visible type of visibility the node ask: true -> visible , false -> not visible
     */
    public static void setVisible(Node node, boolean visible) {
        node.setVisible(visible);
        node.setManaged(visible);
    }
}
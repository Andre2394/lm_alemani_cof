package it.polimi.cof.ui.gui.components;

import it.polimi.cof.model.AbstractBoard;
import it.polimi.cof.model.KingBoard;
import it.polimi.cof.model.NobilityReward;
import it.polimi.cof.ui.gui.AbstractRegionCallback;
import it.polimi.cof.ui.gui.TextureManager;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;

import java.util.Map;

/**
* King panel
 * */
/*package-local*/ class KingPane extends Canvas implements EventHandler<MouseEvent> {

    private final AbstractRegionCallback mCallback;
    private KingBoard mKingBoard;
    private Rectangle2D mBalconyPosition;

    /*package-local*/ KingPane(AbstractRegionCallback callback) {
        mCallback = callback;
        invalidate();
        widthProperty().addListener(observable -> invalidate());
        heightProperty().addListener(observable -> invalidate());
        addEventHandler(MouseEvent.MOUSE_CLICKED, this);
    }

    /**
     * Invalidate current view.
     */
    public void invalidate() {
        GraphicsContext context = getGraphicsContext2D();
        context.clearRect(0, 0, getWidth(), getHeight());
        onDraw(context);
    }

    private void onDraw(GraphicsContext context) {
        if (mKingBoard != null) {
            drawBackground(context);
            drawBalcony(context);
            drawNobilityTrack(context);
        }
    }

    private void drawBackground(GraphicsContext context) {
        Image texture = TextureManager.get("kingboard", "jpg");
        context.drawImage(texture, 0, 0, getWidth(), getHeight());
    }

    private void drawBalcony(GraphicsContext context) {
        double parentWidth = widthProperty().doubleValue();
        double parentHeight = heightProperty().doubleValue();
        double x = parentWidth * 0.6285d;
        double width = parentWidth * 0.11666d;
        double height = parentHeight * 0.3172d;
        Painter.drawTopBalcony(context, mKingBoard.getBalconyColors(), x, 0, width, height);
        mBalconyPosition = new Rectangle2D(x, 0, width, height);
    }

    private void drawNobilityTrack(GraphicsContext context) {
        Map<Integer, NobilityReward> nobilityTrack = mKingBoard.getNobilityTrack();
        double startX = getWidth() * 0.0464527d;
        double startY = getHeight() * 0.3125d;
        double rewardWidth = getWidth() * 0.688063d / 21d;
        double rewardHeight = getHeight() * 0.28646d;
        for (int i = 0; i <= 20; i++) {
            double x = startX + i * rewardWidth;
            if (nobilityTrack.containsKey(i)) {
                Painter.drawNobilityRewardColumn(context, nobilityTrack.get(i), x, startY, rewardWidth, rewardHeight);
            }
        }
    }

    /*package-local*/ void setKingBoard(KingBoard kingBoard) {
        mKingBoard = kingBoard;
        invalidate();
    }

    /**
     * Handle event on balcony click
     * @param event listener
     */
    @Override
    public void handle(MouseEvent event) {
        if (mCallback != null) {
            Point2D point = sceneToLocal(event.getSceneX(), event.getSceneY());
            if (mBalconyPosition.contains(point)) {
                mCallback.onBalconyClick(AbstractBoard.KING_REGION, mKingBoard.getBalconyColors());
                event.consume();
            }
        }
    }
}
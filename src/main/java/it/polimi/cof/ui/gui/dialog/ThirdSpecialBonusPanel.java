package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.Player;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.control.PermitTileListCell;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Panel that handle third special bonus
 */
public class ThirdSpecialBonusPanel extends JFXPanel {

    private ListView<BusinessPermitTile> mPermitTileList;

    @SuppressWarnings("unchecked")
    private ThirdSpecialBonusPanel(int count, Player player, Callback callback) {
        Parent root = GuiUtils.loadFxml(this, "dialog_third_special_reward");
        Label helpLabel = (Label) root.lookup("#helpLabel");
        mPermitTileList = (ListView<BusinessPermitTile>) root.lookup("#permitTileList");
        Button applyButton = (Button) root.lookup("#applyButton");
        applyButton.setOnAction(event -> earnReward(callback));
        mPermitTileList.setCellFactory(param -> new PermitTileListCell());
        mPermitTileList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        mPermitTileList.getSelectionModel().getSelectedItems().addListener((ListChangeListener<BusinessPermitTile>) c ->
            Platform.runLater(() -> changeButtonVisibility(count, applyButton))
        );
        Platform.runLater(() -> {
            ObservableList<BusinessPermitTile> list = FXCollections.observableList(player.getBusinessPermitTiles());
            list.addAll(player.getUsedBusinessPermitTiles());
            mPermitTileList.setItems(list);
            helpLabel.setText(getHelpMessage(count));
        });
    }

    private String getHelpMessage(int count) {
        if (count > 1) {
            return String.format("Select %d different permit tile that you already own to take the reward", count);
        }
        return "Select a permit tile that you already own to take the reward";
    }

    private void changeButtonVisibility(int count, Button button) {
        int size = mPermitTileList.getSelectionModel().getSelectedIndices().size();
        button.setDisable(size == 0 || size > count);
    }

    private void earnReward(Callback callback) {
        callback.earnThirdSpecialRewards(new ArrayList<>(mPermitTileList.getSelectionModel().getSelectedItems()));
    }

    /**
     * Handle dialog creation of third special bonus
     * @param parent root
     * @param count number of bonus
     * @param player involved
     * @param callback to GraphicalUserInterface
     * @return
     */
    public static JDialog createDialog(Component parent, int count, Player player, Callback callback) {
        ThirdSpecialBonusPanel panel = new ThirdSpecialBonusPanel(count, player, callback);
        return GuiUtils.createDialog(parent, "Special nobility reward", 600, 500, panel);
    }

    /**
     * Callback to the graphical user interface class.
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * Ask for third special rewards.
         * @param businessPermitTiles provided.
         */
        void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles);
    }
}
package it.polimi.cof.ui.gui.control;

import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

/**
 * This class is used by {@link ListView} to represent the {@link BusinessPermitTile} item in the list.
 */
public class PermitTileListCell extends ListCell<BusinessPermitTile> {

    /**
     * @param item permit tiles of the list
     * @param empty true if an empty list
     */
    @Override
    protected void updateItem(BusinessPermitTile item, boolean empty) {
        super.updateItem(item, empty);
        Platform.runLater(() -> {
            if (item == null || empty) {
                setGraphic(null);
            } else {
                Canvas canvas = new Canvas(108, 120);
                Painter.drawPermitTile(canvas, item);
                setGraphic(canvas);
            }
        });
    }
}
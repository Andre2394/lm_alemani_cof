package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.components.Painter;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

import javax.swing.*;
import java.awt.*;

/**
 * Panel that handle permit tile details
 */
public class PermitTileDetailPanel extends JFXPanel {

    private PermitTileDetailPanel(BusinessPermitTile businessPermitTile) {
        Parent root = GuiUtils.loadFxml(this, "dialog_permit_tile_detail");
        Canvas canvas = (Canvas) root.lookup("#permitTileCanvas");
        Platform.runLater(() -> {
            GraphicsContext context = canvas.getGraphicsContext2D();
            double permitTileWidth = 162;
            double permitTileHeight = 180;
            Painter.drawPermitTile(context, businessPermitTile, 39, 10, permitTileWidth, permitTileHeight);
        });
    }

    /**
     * Handle dialog creation of permit tile panel
     * @param parent
     * @param businessPermitTile
     * @return
     */
    public static JDialog createDialog(Component parent, BusinessPermitTile businessPermitTile) {
        PermitTileDetailPanel panel = new PermitTileDetailPanel(businessPermitTile);
        return GuiUtils.createDialog(parent, "Business permit tile details", 300, 250, panel);
    }
}
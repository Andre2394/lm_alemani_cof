package it.polimi.cof.ui.gui;

import javafx.scene.image.Image;

import java.util.HashMap;
import java.util.Map;

/**
 * Class who manage images file need to load in configuration map or game map
 * Created by andre on 26/06/2016.
 */
public class TextureManager {

    private static TextureManager mInstance;

    private final Map<String, Image> mTextureCache;

    private TextureManager() {
        mTextureCache = new HashMap<>();
    }

    /*package-local*/
    static void initialize() {
        mInstance = new TextureManager();
    }

    /**
     * Static method is called to load texture in map. ( example: city gold, iron, king,...)
     *
     * @param name file name in /images need to be loaded
     * @return
     */
    public static Image get(String name) {
        return get(name, "png");
    }

    /**
     * Load and cache image
     * @param name
     * @param extension
     * @return
     */
    public static Image get(String name, String extension) {
        if (!mInstance.mTextureCache.containsKey(name)) {
            String url = TextureManager.class.getResource(String.format("/images/%s.%s", name, extension)).toExternalForm();
            mInstance.mTextureCache.put(name, new Image(url));
        }
        return mInstance.mTextureCache.get(name);
    }
}
package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.Councilor;
import it.polimi.cof.ui.gui.GuiUtils;

import javax.swing.*;
import java.awt.*;

/**
 * Panel that handle action of elect councillor thanks to assistant.
 */
public class SendAssistantElectCouncillorPanel extends AbstractCouncillorElectionPanel {

    private final Callback mCallback;

    private SendAssistantElectCouncillorPanel(BaseGame baseGame, Callback callback) {
        super(baseGame, "dialog_send_assistant_elect_councillor");
        mCallback = callback;
    }

    /**
     * Handle dialog creation of
     * @param parent root
     * @param baseGame game state
     * @param callback to GraphicalUserInterface
     * @return
     */
    public static JDialog createDialog(Component parent, BaseGame baseGame, Callback callback) {
        SendAssistantElectCouncillorPanel panel = new SendAssistantElectCouncillorPanel(baseGame, callback);
        return GuiUtils.createDialog(parent, "Fast action 3", 500, 600, panel);
    }

    /**
     * Select event
     * @param councilor selected
     * @param region    of the councillor selected
     */
    @Override
    protected void onCouncillorSelected(Councilor councilor, String region) {
        mCallback.onAssistantSentToElectCouncillor(councilor, region);
    }

    /**
     * Cancel button
     */
    @Override
    protected void onCancel() {
        mCallback.onCancel();
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * Players take an available councillor and place him in a balcony.
         * @param councilor going to be elect
         * @param region in which is elelected
         */
        void onAssistantSentToElectCouncillor(Councilor councilor, String region);

        /**
         * Disable current window
         */
        void onCancel();
    }
}
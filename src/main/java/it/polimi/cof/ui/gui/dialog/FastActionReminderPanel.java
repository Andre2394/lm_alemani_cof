package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.ui.gui.GuiUtils;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javax.swing.*;
import java.awt.*;

/**
 * Panel that handle a reminder of fast action
 */
public class FastActionReminderPanel extends JFXPanel {

    /**
     * Settings and information loading
     * @param action type of fast action
     * @param callback to GraphicalUserInterface
     */
    private FastActionReminderPanel(int action, Callback callback) {
        Parent root = GuiUtils.loadFxml(this, "dialog_generic_fast_action");
        Label title = (Label) root.lookup("#titleLabel");
        Label message = (Label) root.lookup("#messageLabel");
        Button applyButton = (Button) root.lookup("#applyButton");
        Button cancelButton = (Button) root.lookup("#cancelButton");
        applyButton.setOnAction(event -> callback.onApply(action));
        cancelButton.setOnAction(event -> callback.onCancel());
        Platform.runLater(() -> {
            title.setText(getTitle(action));
            message.setText(getMessage(action));
        });
    }

    /**
     * Handle dialog creation of fast action reminder
     * @param parent root
     * @param action type
     * @param callback to GraphicalUserInterface class
     * @return
     */
    public static JDialog createDialog(Component parent, int action, Callback callback) {
        FastActionReminderPanel panel = new FastActionReminderPanel(action, callback);
        return GuiUtils.createDialog(parent, String.format("Fast action %d", action), 500, 200, panel);
    }

    private String getTitle(int action) {
        switch (action) {
            case 1:
                return "Engage assistant";
            case 4:
                return "Perform an additional main action";
            default:
                return "Unknown";
        }
    }

    private String getMessage(int action) {
        switch (action) {
            case 1:
                return "Buy an assistant paying 3 coins";
            case 4:
                return "Pay 3 assistants to make an additional main action";
            default:
                return "Unknown";
        }
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * Apply fast action
         * @param action type of fast action
         */
        void onApply(int action);

        /**
         * Disable current window
         */
        void onCancel();
    }
}
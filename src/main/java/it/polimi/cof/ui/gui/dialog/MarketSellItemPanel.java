package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.Player;
import it.polimi.cof.model.PoliticCard;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.model.market.MarketSession;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.control.PermitTileListCell;
import it.polimi.cof.ui.gui.control.PoliticCardListCell;
import it.polimi.cof.util.Debug;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

import javax.swing.*;
import java.awt.*;

/**
 * Class handle GUI panel about market sell of item
 */
public class MarketSellItemPanel extends JFXPanel {

    private static final String UNKNOWN_TYPE = "Unknown market type";

    private TextField mPriceField;
    private Object mSelectedObject;

    /**
     * Create Market Sell of item Panel loading FXML relative file
     * Settings events and information on graphic component
     * @param type of market
     * @param marketSession type
     * @param player turn
     * @param callback to the server class
     */
    @SuppressWarnings("unchecked")
    private MarketSellItemPanel(MarketType type, MarketSession marketSession, Player player, Callback callback) {
        Parent root = GuiUtils.loadFxml(this, "dialog_market_sell_item");
        Label title = (Label) root.lookup("#title");
        Label help = (Label) root.lookup("#help");
        ListView listView = (ListView) root.lookup("#itemList");
        mPriceField = (TextField) root.lookup("#priceField");
        Button sellButton = (Button) root.lookup("#sellButton");
        Button cancelButton = (Button) root.lookup("#cancelButton");
        title.setText(getTitle(type));
        help.setText(getHelpMessage(type));
        switch (type) {
            case POLITIC_CARD:
                listView.setCellFactory(param -> new PoliticCardListCell());
                Platform.runLater(() -> listView.setItems(getPoliticCards(marketSession, player)));
                sellButton.setOnAction(event -> callback.sellPoliticCard((PoliticCard) mSelectedObject, parsePriceSafely()));
                break;
            case PERMIT_TILE:
                listView.setCellFactory(param -> new PermitTileListCell());
                Platform.runLater(() -> listView.setItems(getPermitTiles(marketSession, player)));
                sellButton.setOnAction(event -> callback.sellBusinessPermitTile((BusinessPermitTile) mSelectedObject, parsePriceSafely()));
                break;
            case ASSISTANT:
                GuiUtils.setVisible(listView, false);
                mSelectedObject = getAssistant(marketSession, player);
                checkAssistantNumber(help);
                sellButton.setOnAction(event -> callback.sellAssistant(parsePriceSafely()));
                break;
            default:
                throw new IllegalArgumentException(UNKNOWN_TYPE);
        }
        mPriceField.textProperty().addListener((observable, oldValue, newValue) -> checkVisibility(sellButton));
        listView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mSelectedObject = newValue;
            checkVisibility(sellButton);
        });
        cancelButton.setOnAction(event -> callback.onCancel());
    }

    /**
     * Method that create a dialog for different Market operations giving right parameters
     * @param parent root
     * @param type of market item
     * @param marketSession in act ( buy/sell )
     * @param player this dialog will be shown
     * @param callback to the main class
     * @return
     */
    public static JDialog createDialog(Component parent, MarketType type, MarketSession marketSession, Player player, Callback callback) {
        MarketSellItemPanel panel = new MarketSellItemPanel(type, marketSession, player, callback);
        return GuiUtils.createDialog(parent, getTitle(type), 500, type == MarketType.ASSISTANT ? 220 : 400, panel);
    }

    private static String getTitle(MarketType type) {
        switch (type) {
            case POLITIC_CARD:
                return "Sell politic card";
            case PERMIT_TILE:
                return "Sell permit tile";
            case ASSISTANT:
                return "Sell assistant";
            default:
                throw new IllegalArgumentException(UNKNOWN_TYPE);
        }
    }

    private static String getHelpMessage(MarketType type) {
        switch (type) {
            case POLITIC_CARD:
                return "Select the politic card that you want to sell and provide the price";
            case PERMIT_TILE:
                return "Select the permit tile that you want to sell and provide the price";
            case ASSISTANT:
                return "Enter the price for the assistant";
            default:
                throw new IllegalArgumentException(UNKNOWN_TYPE);
        }
    }

    private ObservableList<PoliticCard> getPoliticCards(MarketSession marketSession, Player player) {
        ObservableList<PoliticCard> politicCards = FXCollections.observableArrayList(player.getPoliticCards());
        marketSession.getItemsOnSale().stream().filter(item -> item.getSeller().equals(player.getNickname())).forEach(item -> {
            PoliticCard politicCard = item.getPoliticCard();
            if (politicCard != null) {
                politicCards.remove(politicCard);
            }
        });
        return politicCards;
    }

    private ObservableList<BusinessPermitTile> getPermitTiles(MarketSession marketSession, Player player) {
        ObservableList<BusinessPermitTile> businessPermitTiles = FXCollections.observableArrayList(player.getBusinessPermitTiles());
        marketSession.getItemsOnSale().stream().filter(item -> item.getSeller().equals(player.getNickname())).forEach(item -> {
            BusinessPermitTile permitTile = item.getBusinessPermitTile();
            if (permitTile != null) {
                businessPermitTiles.remove(permitTile);
            }
        });
        return businessPermitTiles;
    }

    private Object getAssistant(MarketSession marketSession, Player player) {
        int assistantOnSale = 0;
        for (Item item : marketSession.getItemsOnSale()) {
            if (item.getSeller().equals(player.getNickname())) {
                assistantOnSale += item.getAssistant();
            }
        }
        if (assistantOnSale < player.getAssistants()) {
            return new Object();
        }
        return null;
    }

    private int parsePriceSafely() {
        try {
            return parsePrice();
        } catch (NumberFormatException e) {
            Debug.debug("Not a valid number", e);
        }
        return 0;
    }

    private int parsePrice() {
        return Integer.parseInt(mPriceField.getText());
    }

    private boolean isValidNumber() {
        try {
            return parsePrice() >= 0;
        } catch (NumberFormatException e) {
            Debug.debug("Not a valid number", e);
        }
        return false;
    }

    private void checkVisibility(Button sellButton) {
        sellButton.setDisable(!isValidNumber() || mSelectedObject == null);
    }

    private void checkAssistantNumber(Label help) {
        if (mSelectedObject == null) {
            help.setText("You have no assistant to sell");
            help.setTextFill(Color.RED);
        }
    }

    /**
     * Type of possible item on market
     */
    public enum MarketType {
        POLITIC_CARD,
        PERMIT_TILE,
        ASSISTANT
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * Put in market sell type a politic card
         * @param politicCard on sale
         * @param price target for that card
         */
        void sellPoliticCard(PoliticCard politicCard, int price);

        /**
         * Put in market sell type a business permit tile
         * @param businessPermitTile on sale
         * @param price target for that card
         */
        void sellBusinessPermitTile(BusinessPermitTile businessPermitTile, int price);

        /**
         * Assistant on sale
         * @param  price of an assistant
         */
        void sellAssistant(int price);

        /**
         * Disable current window
         */
        void onCancel();
    }
}
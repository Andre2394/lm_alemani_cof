package it.polimi.cof.ui.gui;

import it.polimi.cof.network.NetworkType;
import it.polimi.cof.network.client.ClientConnectionException;
import it.polimi.cof.util.Debug;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;

/**
 * This class handle the request to the client about his network preference.
 * It's purpose is to save the network connection choice by the user.
 * Created by Guido on 07/06/2016.
 */
/*package-local*/ class NetworkSettingMenu extends JFXPanel {

    private final Callback mCallback;
    private RadioButton mSocketButton;
    private RadioButton mRMIButton;
    private TextField mPort;
    private TextField mHost;
    private Label mLabelError;
    private Button mConnect;


    /**
     * Scene prepared with variables and events to handle it.
     *
     * @param callback to the main class
     */
    /*package-local*/ NetworkSettingMenu(Callback callback) {
        mCallback = callback;
        Parent root = GuiUtils.loadFxml(this, "sNSM");
        GridPane pane = (GridPane) root.lookup("#pane");

        mConnect = (Button) root.lookup("#connectButton");
        mSocketButton = (RadioButton) root.lookup("#socketButton");
        mRMIButton = (RadioButton) root.lookup("#rmiButton");
        mPort = (TextField) root.lookup("#portField");
        mLabelError = (Label) root.lookup("#errorText");
        mHost = (TextField) root.lookup("#hostText");

        mConnect.setOnAction(event -> doConnection());
        mConnect.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                doConnection();
            }
        });
        mPort.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                doConnection();
            }
        });
        mHost.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                doConnection();
            }
        });
        mSocketButton.setOnAction(event -> doDefault());
        mRMIButton.setOnAction(event -> doDefault());
        ToggleGroup group = new ToggleGroup();
        mSocketButton.setToggleGroup(group);
        mRMIButton.setToggleGroup(group);
        mSocketButton.setSelected(true);
        mHost.textProperty().addListener((observableValue, s, s2) -> checkValidity());
        mPort.textProperty().addListener((observableValue, s, s2) -> checkValidity());

        Image pergamena = new Image(getClass().getResource("/images/pergamena.png").toExternalForm());
        Image sfondo = new Image(getClass().getResource("/images/sfondo1.jpg").toExternalForm(),500,500,false,false);
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSizeUnder = new BackgroundSize(pane.getWidth(), pane.getHeight(), false, false, true, true);
        BackgroundSize backgroundSize = new BackgroundSize(pane.getWidth(), pane.getHeight(), false, false, true, false);
        // new BackgroundImage(image, repeatX, repeatY, position2, size)
        BackgroundImage backgroundImage = new BackgroundImage(pergamena, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        BackgroundImage backgroundImageUnder = new BackgroundImage(sfondo, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, backgroundSizeUnder);
        // new Background(images...)
        Background background = new Background(backgroundImageUnder, backgroundImage);
        pane.setBackground(background);
    }

    private void checkValidity() {
        mConnect.setDisable(mPort.getText().isEmpty() || mHost.getText().isEmpty());
    }

    // This is my method to handle my event
    private void doConnection() {
        try {
            NetworkType networkType;
            if (mSocketButton.isSelected()) {
                networkType = NetworkType.SOCKET;
            } else {
                networkType = NetworkType.RMI;
            }
            Audio.play("click");
            mCallback.setNetworkSettings(networkType, mHost.getText(), Integer.parseInt(mPort.getText()));
        } catch (ClientConnectionException e) {
            Debug.debug("Connection with server failed", e);
            mLabelError.setVisible(true);
        } catch (NumberFormatException e) {
            Debug.debug("Not a valid port number", e);
            mLabelError.setVisible(true);
        }
    }

    private void doDefault() {
        mHost.setText("localhost");
        if (mSocketButton.isSelected()) {
            mPort.setText("3031");
        } else {
            mPort.setText("3032");
        }
    }

    /**
     * Callback to the graphical user interface class.
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * @param networkType type of network RMI or Socket connection
         * @param address     address of the server
         * @param port        port of the server
         * @throws ClientConnectionException
         */
        void setNetworkSettings(NetworkType networkType, String address, int port) throws ClientConnectionException;
    }
}

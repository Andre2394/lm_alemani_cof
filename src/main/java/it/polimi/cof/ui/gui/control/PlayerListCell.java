package it.polimi.cof.ui.gui.control;

import it.polimi.cof.model.Player;
import it.polimi.cof.ui.gui.GuiUtils;
import javafx.application.Platform;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeType;

/**
 * This class is used by {@link ListView} to represent the {@link Player} item in the list.
 */
public class PlayerListCell extends ListCell<Player> {

    private final PlayerCallback mCallback;

    /**
     * Base constructor with no callback.
     */
    public PlayerListCell() {
        mCallback = null;
    }

    /**
     * Base constructor.
     *
     * @param callback where notify the player clicks.
     */
    public PlayerListCell(PlayerCallback callback) {
        mCallback = callback;
    }

    /**
     * @param item players in the list. There's a recognizable text added to nickname if they went offline
     * @param empty true if there are no players in list
     */
    @Override
    protected void updateItem(Player item, boolean empty) {
        super.updateItem(item, empty);
        Platform.runLater(() -> {
            if (item == null || empty) {
                setText(null);
            } else {
                if (item.isOnline()) {
                    setText(item.getNickname());
                } else {
                    setText(item.getNickname() + " [OFFLINE]");
                }
                setTextFill(Color.BLACK);
                setGraphic(createLeftCircle(GuiUtils.getColor(item.getColor())));
                if (mCallback != null) {
                    setOnMouseClicked(event -> mCallback.onPlayerClicked(item));
                }
            }
        });
    }

    private Circle createLeftCircle(Color color) {
        Circle circle = new Circle();
        circle.setRadius(5);
        circle.setFill(color);
        circle.setStroke(Color.BLACK);
        circle.setStrokeType(StrokeType.INSIDE);
        return circle;
    }

    /**
     * Interface used to notify that a player has been clicked.
     */
    @FunctionalInterface
    public interface PlayerCallback {

        /**
         * Handle the player clicked.
         *
         * @param player clicked.
         */
        void onPlayerClicked(Player player);
    }
}
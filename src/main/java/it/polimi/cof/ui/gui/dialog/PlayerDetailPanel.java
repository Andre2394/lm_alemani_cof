package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.Player;
import it.polimi.cof.model.Reward;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.control.BonusListCell;
import it.polimi.cof.ui.gui.control.KingRewardListCell;
import it.polimi.cof.ui.gui.control.PermitTileListCell;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.shape.Circle;

import javax.swing.*;
import java.awt.*;
import java.util.Map;


/**
 * This class is about panel information of a player.
 * It receive information from Player object and handle it to show them on a dialog.
 */
public class PlayerDetailPanel extends JFXPanel {

    @SuppressWarnings("unchecked")
    private PlayerDetailPanel(Player player) {
        Parent root = GuiUtils.loadFxml(this, "dialog_player_details");
        Circle playerCircle = (Circle) root.lookup("#playerColor");
        Label playerNickname = (Label) root.lookup("#playerNickname");
        Label playerAssistantLabel = (Label) root.lookup("#assistantLabel");
        Label playerEmporiumLabel = (Label) root.lookup("#emporiumLabel");
        Label playerCoinLabel = (Label) root.lookup("#coinLabel");
        Label politicCardLabel = (Label) root.lookup("#politicCardsLabel");
        Label playerVictoryPointLabel = (Label) root.lookup("#victoryPointsLabel");
        Label playerNobilityPositionLabel = (Label) root.lookup("#nobilityTrackPositionLabel");
        ListView<BusinessPermitTile> permitTileList = (ListView<BusinessPermitTile>) root.lookup("#permitTileListView");
        ListView<Map.Entry<String, Reward>> bonusList = (ListView<Map.Entry<String, Reward>>) root.lookup("#bonusListView");
        ListView<Reward> rewardKingCardList = (ListView<Reward>) root.lookup("#rewardKingCardListView");
        permitTileList.setCellFactory(param -> new PermitTileListCell());
        bonusList.setCellFactory(param -> new BonusListCell());
        rewardKingCardList.setCellFactory(param -> new KingRewardListCell());
        Platform.runLater(() -> {
            playerCircle.setFill(GuiUtils.getColor(player.getColor()));
            playerNickname.setText(player.getNickname());
            playerAssistantLabel.setText(Integer.toString(player.getAssistants()));
            playerEmporiumLabel.setText(Integer.toString(player.getEmporiums()));
            playerCoinLabel.setText(Integer.toString(player.getCoins()));
            politicCardLabel.setText(Integer.toString(player.getPoliticCards().size()));
            playerVictoryPointLabel.setText(Integer.toString(player.getVictoryPoints()));
            playerNobilityPositionLabel.setText(Integer.toString(player.getNobilityTrackPosition()));
            permitTileList.setItems(FXCollections.observableList(player.getBusinessPermitTiles()));
            bonusList.setItems(FXCollections.observableArrayList(player.getBonusCards().entrySet()));
            rewardKingCardList.setItems(FXCollections.observableArrayList(player.getKingRewards()));
        });
    }

    /**
     * This public method create the dialog and call the constructor of this class
     *
     * @param parent is the GamePanel over which this dialog should open
     * @param player is the nickname clicked which the player should receive information about
     * @return create the dialog
     */
    public static JDialog createDialog(Component parent, Player player) {
        PlayerDetailPanel panel = new PlayerDetailPanel(player);
        return GuiUtils.createDialog(parent, "Player state", 310, 300, panel);
    }
}
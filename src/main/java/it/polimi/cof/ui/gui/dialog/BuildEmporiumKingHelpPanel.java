package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.City;
import it.polimi.cof.model.Player;
import it.polimi.cof.model.PoliticCard;
import it.polimi.cof.ui.gui.GuiUtils;
import it.polimi.cof.ui.gui.components.Painter;
import it.polimi.cof.ui.gui.control.CityListCell;
import it.polimi.cof.ui.gui.control.PoliticCardListCell;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Panel about building Emporium using king help
 */
public class BuildEmporiumKingHelpPanel extends JFXPanel {

    private final BaseGame mBaseGame;
    private final Callback mCallback;
    private final Player mPlayer;

    private Canvas mBalconyCanvas;
    private Label mCurrentTrack;
    private ListView<City> mLinkedCities;
    private ListView<PoliticCard> mPoliticCardList;
    private List<String> mCities;
    private Button mMoveButton;
    private Button mBackButton;
    private Button mApplyButton;

    private City mSelectedCity;

    @SuppressWarnings("unchecked")
    private BuildEmporiumKingHelpPanel(String player, BaseGame baseGame, Callback callback) {
        mPlayer = baseGame.getPlayer(player);
        mBaseGame = baseGame;
        mCallback = callback;
        Parent root = GuiUtils.loadFxml(this, "dialog_build_emporium_king_help");
        mBalconyCanvas = (Canvas) root.lookup("#canvasBalcony");
        mCurrentTrack = (Label) root.lookup("#kingTrack");
        mLinkedCities = (ListView<City>) root.lookup("#linkedCityList");
        mPoliticCardList = (ListView<PoliticCard>) root.lookup("#politicCardList");
        mMoveButton = (Button) root.lookup("#moveButton");
        mBackButton = (Button) root.lookup("#backButton");
        mApplyButton = (Button) root.lookup("#applyButton");
        Button cancelButton = (Button) root.lookup("#cancelButton");
        mLinkedCities.setCellFactory(param -> new CityListCell());
        mMoveButton.setOnAction(event -> {
            if (mSelectedCity != null) {
                Platform.runLater(() -> moveTo(mSelectedCity.getName()));
            }
        });
        mBackButton.setOnAction(event -> Platform.runLater(this::moveBack));
        mApplyButton.setOnAction(event -> buildEmporium());
        cancelButton.setOnAction(event -> mCallback.onCancel());
        mLinkedCities.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mSelectedCity = newValue;
            mMoveButton.setDisable(mSelectedCity == null);
        });
        mPoliticCardList.setCellFactory(param -> new PoliticCardListCell());
        mPoliticCardList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        mPoliticCardList.getSelectionModel().getSelectedItems().addListener((ListChangeListener<PoliticCard>) c ->
                Platform.runLater(this::updateApplyButtonVisibility)
        );
        Platform.runLater(() -> {
            mMoveButton.setDisable(true);
            mCities = new ArrayList<>();
            moveTo(mBaseGame.getKingCityName());
            mPoliticCardList.setItems(FXCollections.observableList(mPlayer.getPoliticCards()));
            Painter.drawBalcony(mBalconyCanvas, mBaseGame.getKingBalconyColors());
        });
    }

    /**
     * Handle dialog creation for Emporium building
     * @param parent component
     * @param player who's asking balcony detail request
     * @param baseGame game state
     * @param callback to GraphicalUserInterface class
     * @return
     */
    public static JDialog createDialog(Component parent, String player, BaseGame baseGame, Callback callback) {
        BuildEmporiumKingHelpPanel panel = new BuildEmporiumKingHelpPanel(player, baseGame, callback);
        return GuiUtils.createDialog(parent, "Main action 4", 800, 550, panel);
    }

    private void updateApplyButtonVisibility() {
        int politicCardCount = mPoliticCardList.getSelectionModel().getSelectedIndices().size();
        boolean alreadyBuilt = mBaseGame.findCity(mCities.get(mCities.size() - 1)).hasPlayerAnEmporium(mPlayer);
        mApplyButton.setDisable(politicCardCount <= 0 || politicCardCount > 4 || alreadyBuilt);
    }

    private void buildEmporium() {
        List<PoliticCard> politicCards = new ArrayList<>(mPoliticCardList.getSelectionModel().getSelectedItems());
        mCallback.onEmporiumBuiltWithKingHelp(politicCards, mCities);
    }

    private void moveTo(String city) {
        mCities.add(city);
        updateCurrentTrack();
        updateLinkedCitiesList(city);
        mBackButton.setDisable(mCities.size() <= 1);
        updateApplyButtonVisibility();
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void moveBack() {
        if (mCities.size() > 1) {
            mCities.remove(mCities.size() - 1);
            updateCurrentTrack();
            updateLinkedCitiesList(mCities.get(mCities.size() - 1));
            mBackButton.setDisable(mCities.size() <= 1);
            updateApplyButtonVisibility();
        }
    }

    private void updateLinkedCitiesList(String city) {
        mLinkedCities.setItems(FXCollections.observableArrayList(mBaseGame.getLinkedCities(mBaseGame.findCity(city))));
    }

    private void updateCurrentTrack() {
        StringBuilder builder = new StringBuilder();
        for (String city : mCities) {
            if (builder.length() == 0) {
                builder.append("Track: ");
            } else {
                builder.append(" -> ");
            }
            builder.append(city);
        }
        mCurrentTrack.setText(builder.toString());
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * Emporium built satisfying king advise. King moves in any city where we want to build an emporium
         * @param politicCards discarded
         * @param cities crossed
         */
        void onEmporiumBuiltWithKingHelp(List<PoliticCard> politicCards, List<String> cities);

        /**
         * Disable current window
         */
        void onCancel();
    }
}
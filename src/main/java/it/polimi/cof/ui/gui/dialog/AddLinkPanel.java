package it.polimi.cof.ui.gui.dialog;

import it.polimi.cof.model.City;
import it.polimi.cof.model.Link;
import it.polimi.cof.model.RegionBoard;
import it.polimi.cof.ui.gui.GuiUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import javax.swing.*;
import java.awt.*;
import java.util.stream.Collectors;

/**
 * Panel that handle link configuration of cities
 */
public class AddLinkPanel extends JFXPanel {

    private final RegionBoard mRegionBoard;
    private final City mCity;
    private final Callback mCallback;

    private String mSelectedCity;

    /**
     * Events and information of the panel
     * @param regionBoard of the cities
     * @param city involved in link
     * @param gates link with other regions
     * @param callback to GraphicalUserInterface class
     */
    @SuppressWarnings("unchecked")
    private AddLinkPanel(RegionBoard regionBoard, City city, String[] gates, Callback callback) {
        mRegionBoard = regionBoard;
        mCity = city;
        mCallback = callback;
        Parent root = GuiUtils.loadFxml(this, "dialog_new_link");
        Label label = (Label) root.lookup("#linkMessageLabel");
        ListView<String> list = (ListView<String>) root.lookup("#listView");
        Button applyButton = (Button) root.lookup("#applyButton");
        applyButton.setOnAction(event -> addLink());
        Button cancelButton = (Button) root.lookup("#cancelButton");
        cancelButton.setOnAction(event -> mCallback.onCancel());
        list.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            mSelectedCity = newValue;
            applyButton.setDisable(mSelectedCity == null);
        });
        ObservableList<String> cities = FXCollections.observableArrayList();
        cities.addAll(regionBoard.getCities().values().stream().filter(item -> item != city).map(City::getName).collect(Collectors.toList()));
        cities.addAll(gates);
        removeCitiesAlreadyLinked(cities);
        list.setItems(cities);
        label.setText(String.format("Select a city or a gate to link to %s", city.getName()));
    }

    /**
     * Create dialog about city link information
     * @param parent of the panel
     * @param regionBoard referred to this city
     * @param city involved
     * @param gates link
     * @param callback to the main class
     * @return
     */
    public static JDialog createDialog(Component parent, RegionBoard regionBoard, City city, String[] gates, Callback callback) {
        AddLinkPanel panel = new AddLinkPanel(regionBoard, city, gates, callback);
        return GuiUtils.createDialog(parent, "Add link", 300, 200, panel);
    }

    private void removeCitiesAlreadyLinked(ObservableList<String> cities) {
        for (Link link : mRegionBoard.getLinks()) {
            String toRemove = null;
            if (link.getCityFrom().equals(mCity.getName())) {
                toRemove = link.getCityTo();
            } else if (link.getCityTo().equals(mCity.getName())) {
                toRemove = link.getCityFrom();
            }
            if (toRemove != null) {
                cities.remove(toRemove);
            }
        }
    }

    private void addLink() {
        mRegionBoard.getLinks().add(new Link(mCity.getName(), mSelectedCity));
        mCallback.onLinkAdded(mRegionBoard, mCity);
    }

    /**
     * Callback to the graphical user interface class.
     */
    public interface Callback {

        /**
         * Add event must refresh RegionBoard interested
         * @param regionBoard where has been added a link
         * @param city involved
         */
        void onLinkAdded(RegionBoard regionBoard, City city);

        /**
         * Cancel button
         */
        void onCancel();
    }
}
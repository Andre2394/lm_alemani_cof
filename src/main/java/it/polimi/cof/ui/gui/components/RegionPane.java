package it.polimi.cof.ui.gui.components;

import it.polimi.cof.model.*;
import it.polimi.cof.ui.gui.RegionBoardCallback;
import it.polimi.cof.ui.gui.TextureManager;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.util.HashMap;
import java.util.Map;

/**
 * This class represent the single region board of the game.
 */
/*package-local*/ class RegionPane extends Canvas implements EventHandler<MouseEvent> {

    private final RegionBoardCallback mCallback;
    /**
     * Position of the region board. It is useful when creating the layout because of the position of the gates.
     */
    private Position mPosition;
    private RegionBoard mRegionBoard;
    private Map<City, Rectangle2D> mCityPositions;
    private Map<BusinessPermitTile, Rectangle2D> mPermitTilePositions;
    private Rectangle2D mBalconyPosition;
    private boolean mConfiguratorMode;

    /**
     * Create a new region board pane. It will helps with the management and the display of the cities and the link
     * between them, the display of the balcony and the visible permit tiles.
     *
     * @param position where the region is placed.
     */
    /*package-local*/ RegionPane(RegionBoardCallback callback, Position position, boolean configuratorMode) {
        mCallback = callback;
        mPosition = position;
        mCityPositions = new HashMap<>();
        mPermitTilePositions = new HashMap<>();
        mConfiguratorMode = configuratorMode;
        invalidate();
        widthProperty().addListener(observable -> invalidate());
        heightProperty().addListener(observable -> invalidate());
        addEventHandler(MouseEvent.MOUSE_CLICKED, this);
    }

    /**
     * Invalidate current view.
     */
    public void invalidate() {
        mCityPositions.clear();
        GraphicsContext context = getGraphicsContext2D();
        context.clearRect(0, 0, getWidth(), getHeight());
        onDraw(context);
    }

    private void onDraw(GraphicsContext canvas) {
        if (mRegionBoard != null) {
            drawLinks(canvas);
            drawCities(canvas);
            drawPermitTiles(canvas);
            drawBalcony(canvas);
            if (mConfiguratorMode) {
                drawGates(canvas);
            }
        }
    }

    private void drawLinks(GraphicsContext canvas) {
        double parentWidth = widthProperty().doubleValue();
        double parentHeight = heightProperty().doubleValue();
        for (Link link : mRegionBoard.getLinks()) {
            String from = link.getCityFrom();
            String to = link.getCityTo();
            drawLink(
                    canvas,
                    parentWidth / 100 * getCityX(from),
                    parentHeight / 100 * getCityY(from),
                    parentWidth / 100 * getCityX(to),
                    parentHeight / 100 * getCityY(to)
            );
        }
    }

    private double getCityX(String name) {
        if (!name.startsWith("#")) {
            return mRegionBoard.getCities().get(name).getPositionX();
        }
        return getGateX(Integer.parseInt(name.substring(1)));
    }

    private double getGateX(int gate) {
        switch (mPosition) {
            case LEFT:
                return 100;
            case RIGHT:
                return 0;
            default:
                return gate <= 3 ? 0 : 100;
        }
    }

    private double getCityY(String name) {
        if (!name.startsWith("#")) {
            return mRegionBoard.getCities().get(name).getPositionY();
        }
        return getGateY(Integer.parseInt(name.substring(1)));
    }

    private double getGateY(int gate) {
        switch (gate) {
            case 1:
            case 4:
                return 20;
            case 2:
            case 5:
                return 45;
            default:
                return 70;
        }
    }

    private void drawLink(GraphicsContext canvas, double startX, double startY, double endX, double endY) {
        double parentHeight = heightProperty().doubleValue();
        double width = Math.sqrt(Math.pow(endX - startX, 2) + Math.pow(endY - startY, 2));
        double height = parentHeight / 40;
        canvas.save();
        canvas.translate(startX, startY);
        canvas.rotate(getAngle(startX, startY, endX, endY));
        canvas.drawImage(
                TextureManager.get("route"),
                0,
                -(height / 2),
                width,
                height
        );
        canvas.restore();
    }

    private double getAngle(double p1x, double p1y, double p2x, double p2y) {
        double distanceX = p2x - p1x;
        double distanceY = p2y - p1y;
        double angle = Math.toDegrees(Math.atan(distanceY / distanceX));
        return distanceX < 0 ? 180 + angle : angle;
    }

    private void drawCities(GraphicsContext canvas) {
        mRegionBoard.getCities().values().forEach(city -> drawCity(canvas, city));
    }

    private void drawCity(GraphicsContext canvas, City city) {
        double parentWidth = widthProperty().doubleValue();
        double parentHeight = heightProperty().doubleValue();
        double centerX = parentWidth / 100 * city.getPositionX();
        double centerY = parentHeight / 100 * city.getPositionY();
        double width = parentWidth / 2.5;
        double height = parentHeight / 4;
        Painter.drawCity(canvas, city, centerX - width / 2, centerY - height / 2, width, height);
        mCityPositions.put(city, new Rectangle2D(centerX - width / 2, centerY - height / 2, width, height));
        if (city.hasKing()) {
            Image crownTexture = TextureManager.get("crown");
            canvas.drawImage(crownTexture, centerX - width / 2, centerY - height / 2, width, height);
        }
    }

    private void drawPermitTiles(GraphicsContext canvas) {
        mPermitTilePositions.clear();
        double parentWidth = widthProperty().doubleValue();
        double parentHeight = heightProperty().doubleValue();
        double x1 = parentWidth * 0.424831d - getBoardDeviation();
        double x2 = parentWidth * 0.648648d - getBoardDeviation();
        double y = parentHeight * 0.7941d;
        double width = parentWidth * 0.201858d;
        double height = parentHeight * 0.1193317d;
        Painter.drawPermitTile(canvas, mRegionBoard.getFirstVisibleTile(), x1, y, width, height);
        mPermitTilePositions.put(mRegionBoard.getFirstVisibleTile(), new Rectangle2D(x1, y, width, height));
        Painter.drawPermitTile(canvas, mRegionBoard.getSecondVisibleTile(), x2, y, width, height);
        mPermitTilePositions.put(mRegionBoard.getSecondVisibleTile(), new Rectangle2D(x2, y, width, height));
    }

    private void drawBalcony(GraphicsContext canvas) {
        double parentWidth = widthProperty().doubleValue();
        double parentHeight = heightProperty().doubleValue();
        double x = parentWidth * 0.4023d - getBoardDeviation();
        double y = parentHeight * 0.93047d;
        double width = parentWidth * 0.35d;
        double height = parentHeight * 0.0788d;
        Painter.drawTopBalcony(canvas, mRegionBoard.getBalconyColors(), x, y, width, height);
        mBalconyPosition = new Rectangle2D(x, y, width, height);
    }

    private void drawGates(GraphicsContext canvas) {
        double gateWidth = getWidth() * 0.12d;
        double gateHeight = getHeight() * 0.05d;
        switch (mPosition) {
            case LEFT:
                drawLeftRegionGates(canvas, gateWidth, gateHeight);
                break;
            case CENTER:
                drawCenterRegionGates(canvas, gateWidth, gateHeight);
                break;
            case RIGHT:
                drawRightRegionGates(canvas, gateWidth, gateHeight);
                break;
            default:
                throw new IllegalStateException("Unknown region");
        }
    }

    private void drawLeftRegionGates(GraphicsContext canvas, double gateWidth, double gateHeight) {
        drawGate(canvas, "#", getWidth() - (gateWidth / 2), getHeight() / 100 * getGateY(1) - (gateHeight / 2), gateWidth / 2, gateHeight);
        drawGate(canvas, "#", getWidth() - (gateWidth / 2), getHeight() / 100 * getGateY(2) - (gateHeight / 2), gateWidth / 2, gateHeight);
        drawGate(canvas, "#", getWidth() - (gateWidth / 2), getHeight() / 100 * getGateY(3) - (gateHeight / 2), gateWidth / 2, gateHeight);
    }

    private void drawCenterRegionGates(GraphicsContext canvas, double gateWidth, double gateHeight) {
        drawGate(canvas, "1", 0, getHeight() / 100 * getGateY(1) - (gateHeight / 2), gateWidth / 2, gateHeight);
        drawGate(canvas, "2", 0, getHeight() / 100 * getGateY(2) - (gateHeight / 2), gateWidth / 2, gateHeight);
        drawGate(canvas, "3", 0, getHeight() / 100 * getGateY(3) - (gateHeight / 2), gateWidth / 2, gateHeight);
        drawGate(canvas, "#", getWidth() - (gateWidth / 2), getHeight() / 100 * getGateY(1) - (gateHeight / 2), gateWidth / 2, gateHeight);
        drawGate(canvas, "#", getWidth() - (gateWidth / 2), getHeight() / 100 * getGateY(2) - (gateHeight / 2), gateWidth / 2, gateHeight);
        drawGate(canvas, "#", getWidth() - (gateWidth / 2), getHeight() / 100 * getGateY(3) - (gateHeight / 2), gateWidth / 2, gateHeight);
    }

    private void drawRightRegionGates(GraphicsContext canvas, double gateWidth, double gateHeight) {
        drawGate(canvas, "4", 0, getHeight() / 100 * getGateY(1) - (gateHeight / 2), gateWidth / 2, gateHeight);
        drawGate(canvas, "5", 0, getHeight() / 100 * getGateY(2) - (gateHeight / 2), gateWidth / 2, gateHeight);
        drawGate(canvas, "6", 0, getHeight() / 100 * getGateY(3) - (gateHeight / 2), gateWidth / 2, gateHeight);
    }

    private void drawGate(GraphicsContext canvas, String text, double x, double y, double width, double height) {
        canvas.setFill(Color.BLACK);
        canvas.fillRect(x, y, width, height);
        canvas.setTextAlign(TextAlignment.CENTER);
        canvas.setTextBaseline(VPos.CENTER);
        canvas.setFill(Color.WHITE);
        canvas.setFont(new Font(height / 2));
        canvas.fillText(text, x + width / 2, y + height / 2);
    }

    private double getBoardDeviation() {
        if (mPosition != Position.LEFT) {
            return widthProperty().doubleValue() * 0.0979d;
        }
        return 0d;
    }

    /*package-local*/ void setRegionBoard(RegionBoard regionBoard) {
        mRegionBoard = regionBoard;
        invalidate();
    }

    /**
     * Handle click mouse event and open the correct dialog for each component
     * @param event listener
     */
    @Override
    public void handle(MouseEvent event) {
        if (mCallback != null) {
            Point2D point = sceneToLocal(event.getSceneX(), event.getSceneY());
            for (Map.Entry<City, Rectangle2D> entry : mCityPositions.entrySet()) {
                if (entry.getValue().contains(point)) {
                    mCallback.onCityClicked(entry.getKey());
                    event.consume();
                    return;
                }
            }
            if (mBalconyPosition.contains(point)) {
                mCallback.onBalconyClick(getRegion(), mRegionBoard.getBalconyColors());
                event.consume();
                return;
            }
            for (Map.Entry<BusinessPermitTile, Rectangle2D> entry : mPermitTilePositions.entrySet()) {
                if (entry.getValue().contains(point)) {
                    mCallback.onPermitTileClick(entry.getKey());
                    event.consume();
                    return;
                }
            }
        }
    }

    private String getRegion() {
        switch (mPosition) {
            case LEFT:
                return AbstractBoard.LEFT_REGION;
            case CENTER:
                return AbstractBoard.CENTER_REGION;
            case RIGHT:
                return AbstractBoard.RIGHT_REGION;
            default:
                throw new IllegalStateException("Position unknown");
        }
    }

    /**
     * Position of the region inside the MapPane.
     */
    /*package-local*/ enum Position {

        /**
         * Left region board.
         */
        LEFT,

        /**
         * Center region board.
         */
        CENTER,

        /**
         * Right region board.
         */
        RIGHT
    }
}
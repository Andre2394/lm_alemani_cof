package it.polimi.cof.ui.cli.context;

/**
 * This class represent the login context.
 */
public class LoginContext extends BaseUiContext {

    private final Callback mCallback;

    /**
     * Base constructor.
     * @param contextInterface to use.
     * @param callback for login action.
     */
    public LoginContext(ContextInterface contextInterface, Callback callback) {
        super(contextInterface);
        mCallback = callback;
        addCommand("login", "[nickname]", this::login);
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private void login(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            mCallback.loginPlayer(arguments[0]);
        } else {
            throw new CommandNotValid();
        }
    }

    /**
     * Internal callback used for login the player.
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * Login the player with the provided nickname.
         * @param nickname to use.
         */
        /*package-local*/ void loginPlayer(String nickname);
    }
}
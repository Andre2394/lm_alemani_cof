package it.polimi.cof.ui.cli.context;

import it.polimi.cof.network.NetworkType;
import it.polimi.cof.network.client.ClientConnectionException;
import it.polimi.cof.util.Debug;

/**
 * This class represent the network setting context.
 */
public class NetworkSettingContext extends BaseUiContext {

    private final Callback mCallback;

    private NetworkType mNetworkType;
    private String mAddress;
    private int mPort;

    /**
     * Base constructor.
     * @param contextInterface to use.
     * @param callback to use.
     */
    public NetworkSettingContext(ContextInterface contextInterface, Callback callback) {
        super(contextInterface);
        mCallback = callback;
        addCommand("show-config", "", arguments -> printCurrentConfig());
        addCommand("change-network-type", "[socket|rmi]", this::changeNetworkType);
        addCommand("change-address", "[new address]", this::changeAddress);
        addCommand("change-port", "[new port]", this::changePort);
        addCommand("connect", "", arguments -> connect());
        mNetworkType = NetworkType.SOCKET;
        mAddress = "localhost";
        mPort = 3031;
        printCurrentConfig();
    }

    private void printCurrentConfig() {
        String type = mNetworkType == NetworkType.SOCKET ? "socket" : "rmi";
        print("Current network configuration:\n- type: %s\n- address: %s\n- port: %d\nType 'connect' to open a connection with server", type, mAddress, mPort);
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private void changeNetworkType(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            switch (arguments[0]) {
                case "socket":
                    mNetworkType = NetworkType.SOCKET;
                    if (mPort == 3032) {
                        mPort = 3031;
                    }
                    break;
                case "rmi":
                    mNetworkType = NetworkType.RMI;
                    if (mPort == 3031) {
                        mPort = 3032;
                    }
                    break;
                default:
                    throw new CommandNotValid();
            }
            printCurrentConfig();
        } else {
            throw new CommandNotValid();
        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private void changeAddress(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            mAddress = arguments[0];
            printCurrentConfig();
        } else {
            throw new CommandNotValid();
        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private void changePort(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            try {
                int port = Integer.parseInt(arguments[0]);
                if (port > 1024 && port <= 65535) {
                    mPort = port;
                    printCurrentConfig();
                } else {
                    throw new CommandNotValid("Port out of range");
                }
            } catch (NumberFormatException e) {
                throw new CommandNotValid(e);
            }
        } else {
            throw new CommandNotValid();
        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private void connect() throws CommandNotValid {
        try {
            mCallback.setNetworkSettings(mNetworkType, mAddress, mPort);
        } catch (ClientConnectionException e) {
            Debug.debug(e);
            print("Cannot connect to the server, please check network settings and retry.");
        }
    }

    /**
     * Interface used as callback.
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * Set the network settings and try to connect.
         * @param networkType type of the network.
         * @param address of the server.
         * @param port of the server.
         * @throws ClientConnectionException if an error occur or the server is not reachable.
         */
        void setNetworkSettings(NetworkType networkType, String address, int port) throws ClientConnectionException;
    }
}
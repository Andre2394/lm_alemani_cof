package it.polimi.cof.ui.cli.context;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represent the second special rewards context.
 */
public class SecondSpecialRewardContext extends BaseSpecialRewardContext {

    private final SecondSpecialRewardCallback mCallback;

    /**
     * Base constructor.
     * @param callback to use.
     * @param count of rewards.
     */
    public SecondSpecialRewardContext(SecondSpecialRewardCallback callback, int count) {
        super(callback, count);
        mCallback = callback;
        addCommand("take-permit-tile", getHelpText("[left|center|right]:[1|2]"), this::takePermitTile);
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void takePermitTile(String[] arguments) throws CommandNotValid {
        if (arguments.length == getCount()) {
            List<String> regions = new ArrayList<>();
            List<Integer> indices = new ArrayList<>();
            for (String argument : arguments) {
                String[] parts = argument.split(":");
                if (parts.length != 2) {
                    throw new CommandNotValid();
                }
                regions.add(parts[0]);
                indices.add(parseNumber(parts[1], "Not a valid region index"));
            }
            mCallback.earnSecondSpecialRewards(regions, indices);
        } else {
            throw new CommandNotValid();
        }
    }

    @Override
    protected void showSingleRewardTutorial() {
        print("You have obtained a special reward and you can now take a visible permit tile for free. " +
                "Type 'take-permit-tile region:index' to get the desired permit tile.");
    }

    @Override
    protected void showMultiRewardTutorial(int count) {
        print("You have obtained a special reward and you can now take %d visible permit tiles for free. " +
                "Type 'take-permit-tile' to get the permit tiles.", count);
    }

    /**
     * Interface used as callback.
     */
    public interface SecondSpecialRewardCallback extends BaseGameContext.Callback {

        /**
         * Earn a second special rewards.
         * @param regions where the player want to take the permit tile.
         * @param indices where the player want to take the permit tile.
         */
        void earnSecondSpecialRewards(List<String> regions, List<Integer> indices);
    }
}

package it.polimi.cof.ui.cli.context;

/**
 * This class represent the single command to execute.
 */
@FunctionalInterface
/*package-local*/ interface Command {

    /**
     * Execute the command with the given arguments.
     * @param arguments from the player.
     * @throws CommandNotValid if arguments are not valid to execute the command.
     */
    void execute(String[] arguments) throws CommandNotValid;
}
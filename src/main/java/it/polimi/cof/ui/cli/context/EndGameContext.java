package it.polimi.cof.ui.cli.context;

import it.polimi.cof.model.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represent the end of game context. It will handle all the base commands and some specific context commands.
 */
public class EndGameContext extends BaseGameContext {

    private final Map<String, Player> mPlayers;
    private final List<String> mRanking;

    /**
     * Public constructor.
     * @param callback to use.
     * @param players list of all players.
     * @param ranking list of all players sorted from the winner to the last looser.
     */
    public EndGameContext(Callback callback, List<Player> players, List<String> ranking) {
        super(callback);
        mPlayers = new HashMap<>();
        for (Player player : players) {
            mPlayers.put(player.getNickname(), player);
        }
        mRanking = ranking;
        addCommand("show-ranking", "", args -> showRanking());
        print("This is the final ranking:");
        showRanking();
    }

    private void showRanking() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < mRanking.size(); i++) {
            Player player = mPlayers.get(mRanking.get(i));
            if (i != 0) {
                builder.append("\n");
            }
            builder.append(i + 1);
            builder.append(") ");
            builder.append(player.getNickname());
            builder.append(" with ");
            builder.append(player.getVictoryPoints());
            builder.append(player.getVictoryPoints() <= 1 ? " victory point, " : " victory points, ");
            builder.append(player.getAssistants());
            builder.append(player.getAssistants() <= 1 ? " assistant" : " assistants");
            builder.append(" and ");
            builder.append(player.getPoliticCards().size());
            builder.append(player.getPoliticCards().size() <= 1 ? " politic card" : " politic cards");
        }
        print(builder.toString());
    }
}
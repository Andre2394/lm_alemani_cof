package it.polimi.cof.ui.cli.context;

import it.polimi.cof.network.server.game.MarketTurn;

/**
 * This class represent the market turn context.
 */
public class MarketTurnContext extends BaseMarketContext {

    private static final String NOT_VALID_PRICE = "Not a valid price";
    private static final String INDEX = "[index]";

    private final Callback mCallback;

    /**
     * Base constructor.
     * @param callback to use.
     * @param mode of the market turn.
     */
    public MarketTurnContext(Callback callback, MarketTurn.Mode mode) {
        super(callback);
        mCallback = callback;
        if (mode == MarketTurn.Mode.SELL) {
            addCommand("sell-politic-card", "[index] [price]", this::sellPoliticCard);
            addCommand("sell-permit-tile", "[index] [price]", this::sellBusinessPermitTile);
            addCommand("sell-assistant", "[price]", this::sellAssistant);
        } else if (mode == MarketTurn.Mode.BUY) {
            addCommand("buy-politic-card", INDEX, this::buyPoliticCard);
            addCommand("buy-permit-tile", INDEX, this::buyBusinessPermitTile);
            addCommand("buy-assistant", INDEX, this::buyAssistant);
        }
        addCommand("end-turn", "", arguments -> mCallback.endTurn());
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sellPoliticCard(String[] arguments) throws CommandNotValid {
        if (arguments.length == 2) {
            int index = parseIndex(arguments[0]);
            int price = parseNumber(arguments[1], NOT_VALID_PRICE);
            mCallback.sellPoliticCard(index, price);
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sellBusinessPermitTile(String[] arguments) throws CommandNotValid {
        if (arguments.length == 2) {
            int index = parseIndex(arguments[0]);
            int price = parseNumber(arguments[1], NOT_VALID_PRICE);
            mCallback.sellBusinessPermitTile(index, price);
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sellAssistant(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            int price = parseNumber(arguments[0], NOT_VALID_PRICE);
            mCallback.sellAssistant(price);
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void buyPoliticCard(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            mCallback.buyPoliticCard(parseIndex(arguments[0]));
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void buyBusinessPermitTile(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            mCallback.buyBusinessPermitTile(parseIndex(arguments[0]));
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void buyAssistant(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            mCallback.buyAssistant(parseIndex(arguments[0]));
        } else {
            throw new CommandNotValid();
        }
    }

    /**
     * Interface used as callback.
     */
    public interface Callback extends BaseMarketCallback {

        /**
         * Sell a politic card on the market.
         * @param index of the politic card.
         * @param price to set.
         */
        void sellPoliticCard(int index, int price);

        /**
         * Sell a business permit tile on the market.
         * @param index of the politic card.
         * @param price to set.
         */
        void sellBusinessPermitTile(int index, int price);

        /**
         * Sell an assistant on the market.
         * @param price to set.
         */
        void sellAssistant(int price);

        /**
         * Buy a politic card.
         * @param index of the politic card.
         */
        void buyPoliticCard(int index);

        /**
         * Buy a business permit tile.
         * @param index of the business permit tile.
         */
        void buyBusinessPermitTile(int index);

        /**
         * Buy an assistant.
         * @param index of the assistant.
         */
        void buyAssistant(int index);

        /**
         * End turn.
         */
        void endTurn();
    }
}
package it.polimi.cof.ui.cli;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * This class works as Wrapper and it is built on top of {@link PrintWriter PrintWriter} to easily print messages
 * in standard output. This class should not be used for display log information or for debugging.
 * Use Log4j library instead.
 */
public class DisplayPrinter {

    /**
     * PrintWriter instance.
     */
    private final PrintWriter mPrinter;

    /**
     * Create the PrintWriter instance.
     */
    public DisplayPrinter() {
        mPrinter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(FileDescriptor.out)));
    }

    /**
     * Print immediately a string message on standard output.
     * @param message to display.
     */
    public void print(String message) {
        mPrinter.println(message);
        mPrinter.flush();
    }

    /**
     * Print immediately a string message on standard output.
     * @param message formatted string.
     * @param args arguments for formatted string.
     */
    public void print(String message, Object... args) {
        mPrinter.println(String.format(message, args));
        mPrinter.flush();
    }

    /**
     * Print immediately a string message on standard output.
     * @param message to display.
     */
    public void print(String message, PrintColor color) {
        mPrinter.println(String.format("%s%s%s", color.getAnsiColor(), message, PrintColor.CLOSE_COLOR_TAG));
        mPrinter.flush();
    }
}
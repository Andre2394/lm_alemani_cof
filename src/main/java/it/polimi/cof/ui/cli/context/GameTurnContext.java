package it.polimi.cof.ui.cli.context;

/**
 * This class represent the game context.
 */
public class GameTurnContext extends BaseGameContext {

    private final GameTurnCallback mCallback;

    /**
     * Base constructor.
     * @param callback to use.
     */
    public GameTurnContext(GameTurnCallback callback) {
        super(callback);
        mCallback = callback;
        addCommand("get-action-list", "", arguments -> getActionList());
        addCommand("draw-politic-card", "", arguments -> drawPoliticCard());
        // main actions
        addCommand("elect-councillor", "[index] [left|center|right|king]", this::electCouncillor);
        addCommand("acquire-permit-tile", "[index/index/index/index] [left|center|right] [1|2]", this::acquireBusinessPermitTile);
        addCommand("build-emporium-with-permit-tile", "[index] [city]", this::buildEmporiumWithPermitTile);
        addCommand("build-emporium-with-king-help", "[index/index/index/index] [city->city->city...]", this::buildEmporiumWithKingHelp);
        // fast actions
        addCommand("engage-assistant", "", arguments -> engageAssistant());
        addCommand("change-business-permit-tiles", "[left|center|right]", this::changeBusinessPermitTiles);
        addCommand("send-assistant-elect-councillor", "[index] [left|center|right|king]", this::sendAssistantToElectCouncillor);
        addCommand("additional-main-action", "", arguments -> performAdditionalMainAction());
        addCommand("end-turn", "", arguments -> endTurn());
    }

    private void getActionList() {
        mCallback.getActionList();
    }

    private void drawPoliticCard() {
        if (mCallback.canDrawPoliticCard()) {
            mCallback.drawPoliticCard();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void electCouncillor(String[] arguments) throws CommandNotValid {
        if (arguments.length == 2) {
            if (mCallback.canDoMainAction()) {
                int index = parseIndex(arguments[0]);
                String region = arguments[1];
                mCallback.electCouncillor(index, region);
            }
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void acquireBusinessPermitTile(String[] arguments) throws CommandNotValid {
        if (arguments.length == 3) {
            if (mCallback.canDoMainAction()) {
                int[] indices = parseIntArray(arguments[0]);
                String region = arguments[1];
                int index = parseIndex(arguments[2]);
                mCallback.acquireBusinessPermitTile(indices, region, index);
            }
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void buildEmporiumWithPermitTile(String[] arguments) throws CommandNotValid {
        if (arguments.length == 2) {
            if (mCallback.canDoMainAction()) {
                int index = parseIndex(arguments[0]);
                String city = arguments[1];
                mCallback.buildEmporiumWithPermitTile(index, city);
            }
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void buildEmporiumWithKingHelp(String[] arguments) throws CommandNotValid {
        if (arguments.length == 2) {
            if (mCallback.canDoMainAction()) {
                int[] indices = parseIntArray(arguments[0]);
                String[] cities = parseCities(arguments[1]);
                mCallback.buildEmporiumWithKingHelp(indices, cities);
            }
        } else {
            throw new CommandNotValid();
        }
    }

    private void engageAssistant() {
        if (mCallback.canDoFastAction()) {
            mCallback.engageAssistant();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void changeBusinessPermitTiles(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            if (mCallback.canDoFastAction()) {
                mCallback.changeBusinessPermitTiles(arguments[0]);
            }
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sendAssistantToElectCouncillor(String[] arguments) throws CommandNotValid {
        if (arguments.length == 2) {
            if (mCallback.canDoFastAction()) {
                int index = parseIndex(arguments[0]);
                String region = arguments[1];
                mCallback.sendAssistantToElectCouncillor(index, region);
            }
        } else {
            throw new CommandNotValid();
        }
    }

    private void performAdditionalMainAction() {
        if (mCallback.canDoFastAction()) {
            mCallback.performAdditionalMainAction();
        }
    }

    private int[] parseIntArray(String text) throws CommandNotValid {
        String[] textIndices = text.split("/");
        int[] indices = new int[textIndices.length];
        for (int i = 0; i < textIndices.length; i++) {
            int index = parseIndex(textIndices[i]);
            indices[i] = index;
            for (int j = 0; j < i; j++) {
                if (indices[j] == index) {
                    throw new CommandNotValid("Repeated index");
                }
            }
        }
        if (indices.length > 4) {
            throw new CommandNotValid("[index/index/index/index] should be max 4 different indices");
        }
        return indices;
    }

    private String[] parseCities(String text) throws CommandNotValid {
        return text.split("->");
    }

    private void endTurn() {
        if (mCallback.canEndTurn()) {
            mCallback.endTurn();
        }
    }

    /**
     * Interface used as callback for the game context.
     */
    public interface GameTurnCallback extends BaseGameContext.Callback {

        /**
         * Get the action list.
         */
        void getActionList();

        /**
         * Check if player can draw a politic card.
         * @return true if it can, false otherwise.
         */
        boolean canDrawPoliticCard();

        /**
         * Draw a politic card.
         */
        void drawPoliticCard();

        /**
         * Check if player can do a main action.
         * @return true if it can, false otherwise.
         */
        boolean canDoMainAction();

        /**
         * Elect a councillor.
         * @param index of the councillor.
         * @param region where elect the councillor.
         */
        void electCouncillor(int index, String region);

        /**
         * Acquire a business permit tile.
         * @param indices of the politic cards to use.
         * @param region where the permit tile is place.
         * @param permitTileIndex index of the permit tile to take.
         */
        void acquireBusinessPermitTile(int[] indices, String region, int permitTileIndex);

        /**
         * Build an emporium with a permit tile.
         * @param index of the permit tile.
         * @param city where build the emporium.
         */
        void buildEmporiumWithPermitTile(int index, String city);

        /**
         * Build an emporium with the help of the king.
         * @param indices of the politic cards to use.
         * @param cities list of cities.
         */
        void buildEmporiumWithKingHelp(int[] indices, String[] cities);

        /**
         * Check if player can do a fast action.
         * @return true if it can, false otherwise.
         */
        boolean canDoFastAction();

        /**
         * Engage an assistant.
         */
        void engageAssistant();

        /**
         * Change the visible permit tiles of the region.
         * @param region where the visible permit tiles should be changed.
         */
        void changeBusinessPermitTiles(String region);

        /**
         * Send an assistant to elect a councillor.
         * @param index of the councillor.
         * @param region where elect the councillor.
         */
        void sendAssistantToElectCouncillor(int index, String region);

        /**
         * Buy an additional main action.
         */
        void performAdditionalMainAction();

        /**
         * Check if player can end the turn.
         * @return true if it can, false otherwise.
         */
        boolean canEndTurn();

        /**
         * End turn.
         */
        void endTurn();
    }
}
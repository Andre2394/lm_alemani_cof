package it.polimi.cof.ui.cli.context;

/**
 * This class represent the market context. It is built on top of {@link BaseGameContext} and it will handle all the base
 * commands and the market specific commands.
 */
public class BaseMarketContext extends BaseGameContext {

    private final BaseMarketCallback mCallback;

    /**
     * Base constructor.
     * @param callback to use.
     */
    public BaseMarketContext(BaseMarketCallback callback) {
        super(callback);
        mCallback = callback;
        addCommand("show-market-items", "", arguments -> mCallback.showAllMarketItems());
        addCommand("show-market-items-on-sale", "", arguments -> mCallback.showMarketItemsOnSale());
        addCommand("show-market-items-sold", "", arguments -> mCallback.showMarketItemsSold());
        addCommand("show-market-politic-cards", "", arguments -> mCallback.showPoliticCards());
        addCommand("show-market-permit-tiles", "", arguments -> mCallback.showBusinessPermitTile());
        addCommand("show-market-assistants", "", arguments -> mCallback.showAssistants());
    }

    /**
     * This interface is built on top of (@link BaseGameContext.Callback}
     */
    /*package-local*/ interface BaseMarketCallback extends BaseGameContext.Callback {

        /**
         * Show a list of all market items.
         */
        void showAllMarketItems();

        /**
         * Show a list of all items on sale.
         */
        void showMarketItemsOnSale();

        /**
         * Show a list of all items sold.
         */
        void showMarketItemsSold();

        /**
         * Show a list of politic cards on sale.
         */
        void showPoliticCards();

        /**
         * Show a list of business permit tiles on sale.
         */
        void showBusinessPermitTile();

        /**
         * Show a list of all assistants on sale.
         */
        void showAssistants();
    }
}
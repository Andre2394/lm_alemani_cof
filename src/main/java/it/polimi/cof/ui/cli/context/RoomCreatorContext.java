package it.polimi.cof.ui.cli.context;

import it.polimi.cof.util.Debug;

/**
 * This class represent the room creator context.
 */
public class RoomCreatorContext extends BaseUiContext {

    private final Callback mCallback;

    /**
     * Base constructor.
     * @param contextInterface to use.
     * @param callback to use.
     */
    public RoomCreatorContext(ContextInterface contextInterface, Callback callback) {
        super(contextInterface);
        mCallback = callback;
        print("Type 'set-max-players [number]' to create a room");
        addCommand("set-max-players", "[2+ players]", this::setMaxPlayers);
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void setMaxPlayers(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            try {
                int maxPlayers = Integer.parseInt(arguments[0]);
                if (maxPlayers >= 2) {
                    mCallback.setMaxPlayers(maxPlayers);
                } else {
                    throw new CommandNotValid("Player number must be >= 2");
                }
            } catch (NumberFormatException e) {
                Debug.debug(e);
                throw new CommandNotValid("Not a valid number, please retry.");
            }
        } else {
            throw new CommandNotValid();
        }
    }

    /**
     * Interface used as callback.
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * Create a room with the given number of players.
         * @param maxPlayers number of players.
         */
        void setMaxPlayers(int maxPlayers);
    }
}
package it.polimi.cof.ui.cli.context;

/**
 * This exception is thrown if the provided arguments are not valid.
 */
/*package-local*/ class CommandNotValid extends Exception {

    /**
     * Base constructor.
     */
    public CommandNotValid() {
        super();
    }

    /**
     * Base constructor.
     * @param message of the error.
     */
    public CommandNotValid(String message) {
        super(message);
    }

    /**
     * Base constructor.
     * @param throwable of the error.
     */
    public CommandNotValid(Throwable throwable) {
        super(throwable);
    }
}
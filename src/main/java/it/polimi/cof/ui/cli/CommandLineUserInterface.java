package it.polimi.cof.ui.cli;

import it.polimi.cof.model.*;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.model.market.MarketSession;
import it.polimi.cof.network.server.game.MarketTurn;
import it.polimi.cof.ui.AbstractUi;
import it.polimi.cof.ui.UiController;
import it.polimi.cof.ui.cli.context.*;
import it.polimi.cof.util.Debug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * This class is built on top of {@link AbstractUi}. It will manage the user interface into a command line window.
 * The MVC pattern interface can be accessed via {@link #getController()} method.
 */
public class CommandLineUserInterface extends AbstractUi implements GameTurnContext.GameTurnCallback, MarketTurnContext.Callback, FirstSpecialRewardContext.FirstSpecialRewardCallback, SecondSpecialRewardContext.SecondSpecialRewardCallback, ThirdSpecialRewardContext.ThirdSpecialRewardCallback {

    /**
     * Constants.
     */
    private static final String LIST_PATTERN = "%d) %s";
    private static final String PLAYER_NOT_FOUND = "Player %s not found";
    private static final String FIRST_PERMIT_TILE = "- first business permit tile: %s";
    private static final String SECOND_PERMIT_TILE = "- second business permit tile: %s";
    private static final String INVALID_REGION_BALCONY = "[region balcony] must be left|center|right";
    private static final String YOU_HAVE = "You have";

    /**
     * Print driver used for writing on Display.
     */
    private final DisplayPrinter mPrinter;

    /**
     * Current context of the game. This will allow to easily create different custom commands for every status.
     */
    private BaseUiContext mContext;

    /**
     * Keep a reference to current turn's available actions.
     */
    private ActionList mActionList;

    /**
     * Splash screen declared as constants.
     */
    private static final String SPLASH_SCREEN = "   ___                       _ _   ___  __   ___                \n" +
                                                "  / __\\___  _   _ _ __   ___(_) | /___\\/ _| / __\\__  _   _ _ __ \n" +
                                                " / /  / _ \\| | | | '_ \\ / __| | |//  // |_ / _\\/ _ \\| | | | '__|\n" +
                                                "/ /__| (_) | |_| | | | | (__| | / \\_//|  _/ / | (_) | |_| | |   \n" +
                                                "\\____/\\___/ \\__,_|_| |_|\\___|_|_\\___/ |_| \\/   \\___/ \\__,_|_|   \n" +
                                                "                                                                \n";

    /**
     * Create a CLI user interface for the game.
     * @param controller interface used as callback and MVC.
     */
    public CommandLineUserInterface(UiController controller) {
        super(controller);
        mPrinter = new DisplayPrinter();
        new KeyboardHandler().start();
        printSplashScreen();
    }

    private void printSplashScreen() {
        mPrinter.print(SPLASH_SCREEN);
    }

    /**
     * Called when ui should show to user a form where he can put network settings for client-server connection.
     * Remember to call the appropriate method on UiController when network setting are ready.
     */
    @Override
    public void showNetworkSettingMenu() {
        mContext = new NetworkSettingContext(this, getController()::setNetworkSettings);
    }

    /**
     * Called when ui should show the login menu to user.
     */
    @Override
    public void showLoginMenu() {
        mPrinter.print("You are successfully connected with server!\nLogin to start playing.");
        mContext = new LoginContext(this, getController()::loginPlayer);
    }

    /**
     * Called when ui should show a login error.
     */
    @Override
    public void showLoginErrorMessage() {
        mPrinter.print("This nickname is not valid or already in use, try another one.", PrintColor.RED);
    }

    /**
     * Called when ui should show a "join successful" message to user.
     */
    @Override
    public void showJoinRoomSuccess() {
        mPrinter.print("You are joined in a room, waiting for other players...");
        mContext = null; // disable user interaction until game starts.
    }

    /**
     * Called when ui should show a "join failed" message to user.
     */
    @Override
    public void showJoinRoomFailed() {
        mPrinter.print("No rooms available, you are going to create a new one.");
        mContext = new RoomCreatorContext(this, getController()::createRoom);
    }

    /**
     * Called when ui should show a "creating room successful" message to user.
     */
    @Override
    public void showCreatingRoomSuccess() {
        mPrinter.print("You have successfully created a new room and you are the admin.");
        mContext = null; // disable user interaction until game starts.
    }

    /**
     * Called when ui should show a "creating room failed" message to user.
     */
    @Override
    public void showCreatingRoomFailed() {
        mPrinter.print("An error occur and you have been added to another room. Waiting for other players... ", PrintColor.RED);
        mContext = null; // disable user interaction until game starts.
    }

    /**
     * Called when player is the admin of the match and he has to configure the game.
     * @param configuration bundle that contains all configurable data.
     */
    @Override
    public void showGameConfigurator(Configuration configuration) {
        mPrinter.print("You can now configure the match.");
        mContext = new GameConfiguratorContext(this, getController()::onGameConfigured, configuration);
    }

    /**
     *  Apply configuration chosen by the player
     */
    @Override
    public void showGameConfigurationDone() {
        mPrinter.print("Game has been configured with success, the match will start as soon as possible.");
        mContext = null; // disable user interaction until game starts.
    }

    /** Called when player try to play with a fault configuration.
     * @param errorMessage of the configuration
     */
    @Override
    public void showInvalidGameConfiguration(String errorMessage) {
        mPrinter.print(String.format("Invalid configuration: %s", errorMessage), PrintColor.RED);
    }

    /**
     * Notify ui that game is started.
     */
    @Override
    public void notifyGameStarted() {
        mContext = new BaseGameContext(this);
    }

    /**
     * Called when another player's turn is started.
     * @param nickname of the player that should start the turn.
     */
    @Override
    public void showGameTurnStarted(String nickname, int remainingTime) {
        if (getController().isCurrentPlayer(nickname)) {
            mPrinter.print("Your game turn is started, you have %d seconds to complete it.", remainingTime);
            mContext = new GameTurnContext(this);
            mActionList = new ActionList();
        } else {
            mPrinter.print("%s's game turn is started.", nickname);
            mContext = new BaseGameContext(this);
            mActionList = null;
        }
    }

    /**
     * Notify ui that the market session is started.
     */
    @Override
    public void showMarketSessionStarted() {
        mPrinter.print("All players have done their turn, market session is started");
        mContext = new BaseMarketContext(this);
    }

    /**
     * Notify ui that a new market sell turn is started.
     * @param nickname of the player.
     * @param remainingTime remaining time.
     */
    @Override
    public void showMarketSellTurnStarted(String nickname, int remainingTime) {
        if (getController().isCurrentPlayer(nickname)) {
            mPrinter.print("Your marked turn is started, you have %d seconds to select what you want to sell.", remainingTime);
            mContext = new MarketTurnContext(this, MarketTurn.Mode.SELL);
        } else {
            mPrinter.print("%s's market selling turn is started.", nickname);
            mContext = new BaseMarketContext(this);
        }
    }

    /**
     * Notify ui that a new market buy turn is started.
     * @param nickname of the player.
     * @param remainingTime remaining time.
     */
    @Override
    public void showMarketBuyTurnStarted(String nickname, int remainingTime) {
        if (getController().isCurrentPlayer(nickname)) {
            mPrinter.print("Your marked turn is started, you have %d seconds to buy what you want.", remainingTime);
            mContext = new MarketTurnContext(this, MarketTurn.Mode.BUY);
        } else {
            mPrinter.print("%s's market buying turn is started.", nickname);
            mContext = new BaseMarketContext(this);
        }
    }

    /**
     * Notify ui that a new market item has been added.
     * @param item added.
     */
    @Override
    public void showMarketItemAddedOnSale(Item item) {
        String line = (getController().isCurrentPlayer(item.getSeller()) ? YOU_HAVE : item.getSeller() + " has") +
                " put on sale " +
                getGenericMarketItemBody(item) +
                " for " +
                item.getPrice() +
                (item.getPrice() > 1 ? " coins" : " coin");
        mPrinter.print(line);
    }

    /**
     * Notify ui that a market item has been bought.
     * @param item bought.
     */
    @Override
    public void showMarketItemBought(Item item) {
        String line = (getController().isCurrentPlayer(item.getBuyer()) ? YOU_HAVE : item.getBuyer() + " has") +
                " bought " +
                getGenericMarketItemBody(item) +
                " from " +
                (getController().isCurrentPlayer(item.getSeller()) ? "you" : item.getSeller()) +
                " for " +
                item.getPrice() +
                (item.getPrice() > 1 ? " coins" : " coin");
        mPrinter.print(line);
    }

    private String getGenericMarketItemBody(Item item) {
        StringBuilder builder = new StringBuilder();
        if (item.getPoliticCard() != null) {
            builder.append("a ");
            builder.append(getPoliticCard(item.getPoliticCard()));
            builder.append(" politic card");
        }
        if (item.getBusinessPermitTile() != null) {
            builder.append("a business permit tile");
        }
        if (item.getAssistant() > 0) {
            builder.append("an assistant");
        }
        return builder.toString();
    }

    /**
     * Notify ui that the market session is finished.
     */
    @Override
    public void showMarketSessionFinished() {
        mPrinter.print("Market session is finished");
    }

    /**
     * Called when turn's timer should be updated with new remaining time.
     * @param remainingTime updated time value in seconds.
     */
    @Override
    public void updateTurnRemainingTime(int remainingTime) {
        if (remainingTime <= 0) {
            mPrinter.print("Your turn is over!", PrintColor.RED);
        }
    }

    /**
     * Notify ui that a politic card has been drawn.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyPoliticCardDrawn(UpdateState updateState) {
        if (mActionList != null) {
            mActionList.setPoliticCardDrawn();
        }
        PoliticCard politicCard = updateState.getAddedPoliticCards().get(0);
        if (getController().isCurrentPlayer(updateState)) {
            mPrinter.print("You have drawn a %s politic card", getPoliticCard(politicCard));
        } else {
            mPrinter.print("%s has drawn a politic card", updateState.getNickname());
        }
    }

    /**
     * Notify ui that the action list is ready.
     * @param actionList from server.
     */
    @Override
    public void showActionList(ActionList actionList) {
        mActionList = actionList; // sync with server state
        mPrinter.print("Available actions:");
        if (actionList.isPoliticCardDrawn()) {
            int fastActions = actionList.getFastActionCount();
            int mainActions = actionList.getMainActionCount();
            if (fastActions > 0) {
                mPrinter.print("- Do %d fast action%s", fastActions, fastActions > 1 ? "s" : "");
            }
            if (mainActions > 0) {
                mPrinter.print("- Do %d main action%s", mainActions, mainActions > 1 ? "s" : "");
            } else {
                mPrinter.print("- End turn");
            }
        } else {
            mPrinter.print("- Draw a politic card");
        }
    }

    /**
     * Notify ui that a councillor has been added.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyCouncilorAdded(UpdateState updateState) {
        UpdateState.ChangedCouncilor changedCouncilor = updateState.getChangedCouncillors().get(0);
        if (getController().isCurrentPlayer(updateState)) {
            lookForAdditionalMainActions(updateState);
            decrementMainActionCounter();
            mPrinter.print("You have elected a %s councillor into %s region's balcony and you have gained %d coins",
                    getCouncillor(changedCouncilor.getCouncillor()),
                    changedCouncilor.getRegion(),
                    updateState.getAddedCoins()
            );
        } else {
            mPrinter.print("%s has elected a %s councillor into %s region's balcony and he gained %d coins",
                    updateState.getNickname(),
                    getCouncillor(changedCouncilor.getCouncillor()),
                    changedCouncilor.getRegion(),
                    updateState.getAddedCoins()
            );
        }
    }

    /**
     * Notify ui that a business permit tile has been acquired.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyBusinessPermitTileAcquired(UpdateState updateState) {
        UpdateState.ChangedBusinessPermitTile changedBusinessPermitTile = updateState.getChangedBusinessPermitTiles().get(0);
        if (getController().isCurrentPlayer(updateState)) {
            lookForAdditionalMainActions(updateState);
            decrementMainActionCounter();
            mPrinter.print("You have satisfied the %s region's balcony, you have payed %d coins and you have acquired the %s business permit tile of the region",
                    changedBusinessPermitTile.getRegion(),
                    updateState.getRemovedCoins(),
                    changedBusinessPermitTile.getIndex() == 1 ? "first" : "second"
            );
            printRewards(null, updateState.getRewards());
            printNobilityTrackReward(null, updateState.getAddedNobilitySteps(), updateState.getNobilityReward());
        } else {
            mPrinter.print("%s has satisfied the %s region's balcony, he payed %d coins and he acquired the %s business permit tile of the region",
                    updateState.getNickname(),
                    changedBusinessPermitTile.getRegion(),
                    updateState.getRemovedCoins(),
                    changedBusinessPermitTile.getIndex() == 1 ? "first" : "second"
            );
            printRewards(updateState.getNickname(), updateState.getRewards());
            printNobilityTrackReward(updateState.getNickname(), updateState.getAddedNobilitySteps(), updateState.getNobilityReward());
        }
    }

    /**
     * Notify ui that an emporium has been built with a permit tile.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyEmporiumBuiltWithBusinessPermitTile(UpdateState updateState) {
        if (getController().isCurrentPlayer(updateState)) {
            lookForAdditionalMainActions(updateState);
            decrementMainActionCounter();
            mPrinter.print("You have built an emporium on %s city using a business permit tile",
                    updateState.getAddedEmporiums().get(0)
            );
            printRewards(null, updateState.getRewards());
            printBonus(null, updateState.getBonus(), updateState.getKingRewards());
            printNobilityTrackReward(null, updateState.getAddedNobilitySteps(), updateState.getNobilityReward());
        } else {
            mPrinter.print("%s has built an emporium on %s city using a business permit tile",
                    updateState.getNickname(),
                    updateState.getAddedEmporiums().get(0)
            );
            printRewards(updateState.getNickname(), updateState.getRewards());
            printBonus(updateState.getNickname(), updateState.getBonus(), updateState.getKingRewards());
            printNobilityTrackReward(updateState.getNickname(), updateState.getAddedNobilitySteps(), updateState.getNobilityReward());
        }
    }

    /**
     * Notify ui that an emporium has been built with the help of the king.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyEmporiumBuiltWithKingHelp(UpdateState updateState) {
        if (getController().isCurrentPlayer(updateState)) {
            lookForAdditionalMainActions(updateState);
            decrementMainActionCounter();
            mPrinter.print("You have satisfied the king region's balcony, you have payed %d coins and you have moved the king to %s city where you have built an emporium",
                    updateState.getRemovedCoins(),
                    updateState.getNewKingCity()
            );
            printRewards(null, updateState.getRewards());
            printBonus(null, updateState.getBonus(), updateState.getKingRewards());
            printNobilityTrackReward(null, updateState.getAddedNobilitySteps(), updateState.getNobilityReward());
        } else {
            mPrinter.print("%s has satisfied the king region's balcony, he payed %d coins and he moved the king to %s city where he built an emporium",
                    updateState.getNickname(),
                    updateState.getRemovedCoins(),
                    updateState.getNewKingCity()
            );
            printRewards(updateState.getNickname(), updateState.getRewards());
            printBonus(updateState.getNickname(), updateState.getBonus(), updateState.getKingRewards());
            printNobilityTrackReward(updateState.getNickname(), updateState.getAddedNobilitySteps(), updateState.getNobilityReward());
        }
    }

    private void printRewards(String nickname, List<Reward> rewards) {
        mPrinter.print("%s gained the following rewards: ", nickname != null ? nickname + " has" : YOU_HAVE);
        for (Reward reward : rewards) {
            mPrinter.print("- %s", getReward(reward));
        }
    }

    private void printBonus(String nickname, Map<String, Reward> bonus, List<Reward> kingRewards) {
        if (!bonus.isEmpty()) {
            mPrinter.print("%s gained the following bonus: ", nickname != null ? nickname + " has" : YOU_HAVE);
            for (Map.Entry<String, Reward> entry : bonus.entrySet()) {
                mPrinter.print("- %s: %s", entry.getKey(), getReward(entry.getValue()));
            }
            if (!kingRewards.isEmpty()) {
                mPrinter.print("%s gained the following king rewards: ", nickname != null ? nickname + " has" : YOU_HAVE);
                for (Reward kingReward : kingRewards) {
                    mPrinter.print("- %s", getReward(kingReward));
                }
            }
        }
    }

    private void printNobilityTrackReward(String nickname, int steps, NobilityReward nobilityReward) {
        if (steps > 0) {
            mPrinter.print("%s moved %d steps on nobility track", nickname != null ? nickname + " has" : YOU_HAVE, steps);
            if (nobilityReward != null) {
                mPrinter.print("Here %s gained the following reward:", nickname != null ? nickname + " has" : YOU_HAVE, steps);
                mPrinter.print("- %s", getNobilityReward(nobilityReward));
            }
        }
    }

    private void decrementMainActionCounter() {
        if (mActionList != null) {
            mActionList.decrementMainActionCounter();
        }
    }

    /**
     * Notify ui that an assistant has been engaged.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyAssistantEngaged(UpdateState updateState) {
        if (getController().isCurrentPlayer(updateState)) {
            lookForAdditionalMainActions(updateState);
            decrementFastActionCounter();
            mPrinter.print("You have payed %d coins and You have engaged %d assistant",
                    updateState.getRemovedCoins(),
                    updateState.getAddedAssistants()
            );
        } else {
            mPrinter.print("%s has payed %d coins and he has engaged %d assistant",
                    updateState.getNickname(),
                    updateState.getRemovedCoins(),
                    updateState.getAddedAssistants()
            );
        }
    }

    /**
     * Notify ui that a the visible permit tiles of a region are changed.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyBusinessPermitTilesChanged(UpdateState updateState) {
        String region = updateState.getChangedBusinessPermitTiles().get(0).getRegion();
        if (getController().isCurrentPlayer(updateState)) {
            lookForAdditionalMainActions(updateState);
            decrementFastActionCounter();
            mPrinter.print("You have released %d assistant and you have changed %s region's visible business permit tiles",
                    updateState.getRemovedAssistants(),
                    region
            );
        } else {
            mPrinter.print("%s has released %d assistant and has changed %s region's visible business permit tiles",
                    updateState.getNickname(),
                    updateState.getRemovedAssistants(),
                    region
            );
        }
    }

    /**
     * Notify ui that an assistant has been sent to elect a councillor.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyAssistantSentToElectCouncillor(UpdateState updateState) {
        UpdateState.ChangedCouncilor changedCouncilor = updateState.getChangedCouncillors().get(0);
        if (getController().isCurrentPlayer(updateState)) {
            lookForAdditionalMainActions(updateState);
            decrementFastActionCounter();
            mPrinter.print("You have sent an assistant to elect a %s councillor into %s region's balcony",
                    getCouncillor(changedCouncilor.getCouncillor()),
                    changedCouncilor.getRegion()
            );
        } else {
            mPrinter.print("%s has sent an assistant to elect a %s councillor into %s region's balcony",
                    updateState.getNickname(),
                    getCouncillor(changedCouncilor.getCouncillor()),
                    changedCouncilor.getRegion()
            );
        }
    }

    /**
     * Notify ui that a player has bought an additional main action.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyNewAdditionalMainActionAvailable(UpdateState updateState) {
        if (getController().isCurrentPlayer(updateState)) {
            decrementFastActionCounter();
            lookForAdditionalMainActions(updateState);
            mPrinter.print("You have released %d assistants and you can now perform an additional main action.",
                    updateState.getRemovedAssistants()
            );
        } else {
            mPrinter.print("%s has released %d assistants and he can now perform an additional main action.",
                    updateState.getNickname(),
                    updateState.getRemovedAssistants()
            );
        }
    }

    private void decrementFastActionCounter() {
        if (mActionList != null) {
            mActionList.decrementFastActionCounter();
        }
    }

    private void lookForAdditionalMainActions(UpdateState updateState) {
        for (int i = 0; i < updateState.getAddedMainActions(); i++) {
            mActionList.incrementMainActionCounter();
        }
    }

    /**
     * Notify ui that an action is not valid.
     * @param errorCode that represent the error.
     */
    @Override
    public void showActionNotValid(int errorCode) {
        String errorMessage = getErrorMessage(errorCode);
        if (errorMessage == null) {
            errorMessage = String.format("Unknown error code: %d", errorCode);
        }
        mPrinter.print(errorMessage, PrintColor.RED);
    }

    /**
     * Notify ui that a first special rewards is available.
     * @param count of the cities where the player can get the bonus.
     */
    @Override
    public void showFirstSpecialRewards(int count) {
        mContext = new FirstSpecialRewardContext(this, count);
    }

    /**
     * Notify ui that a second special rewards is available.
     * @param count of the permit tile that the user can take without paying nothing.
     */
    @Override
    public void showSecondSpecialRewards(int count) {
        mContext = new SecondSpecialRewardContext(this, count);
    }

    /**
     * Notify ui that a third special rewards is available.
     * @param count of the permit tiles where the player can get the bonus.
     */
    @Override
    public void showThirdSpecialRewards(int count) {
        List<BusinessPermitTile> businessPermitTiles = new ArrayList<>();
        businessPermitTiles.addAll(getCurrentPlayer().getBusinessPermitTiles());
        businessPermitTiles.addAll(getCurrentPlayer().getUsedBusinessPermitTiles());
        mContext = new ThirdSpecialRewardContext(this, count, businessPermitTiles);
    }

    /**
     * Notify ui that a first special rewards has been done.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyFirstSpecialRewardsEarned(UpdateState updateState) {
        if (getController().isCurrentPlayer(updateState)) {
            printRewards(null, updateState.getRewards());
            mContext = new GameTurnContext(this);
        } else {
            printRewards(updateState.getNickname(), updateState.getRewards());
        }
    }

    /**
     * Notify ui that a second special rewards has been done.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifySecondSpecialRewardsEarned(UpdateState updateState) {
        if (getController().isCurrentPlayer(updateState)) {
            mPrinter.print("You have taken %d permit tiles without paying the cost", updateState.getAddedBusinessPermitTiles().size());
            mContext = new GameTurnContext(this);
        } else {
            mPrinter.print("%s has taken %d business permit tiles without paying the cost", updateState.getNickname(), updateState.getAddedBusinessPermitTiles().size());
        }
    }

    /**
     * Notify ui that a third special rewards has been done.
     * @param updateState from server that contains everything changed during the action.
     */
    @Override
    public void notifyThirdSpecialRewardsEarned(UpdateState updateState) {
        if (getController().isCurrentPlayer(updateState)) {
            printRewards(null, updateState.getRewards());
            mContext = new GameTurnContext(this);
        } else {
            printRewards(updateState.getNickname(), updateState.getRewards());
        }
    }

    /**
     * Notify ui that a new chat message is arrived.
     * @param privateMessage true if message is private, false if public.
     * @param author of the message.
     * @param message body of the message.
     */
    @Override
    public void showChatMessage(boolean privateMessage, String author, String message) {
        if (privateMessage) {
            mPrinter.print("[chat][%s -> you] %s", author, message);
        } else {
            mPrinter.print("[chat][%s] %s", author, message);
        }
    }

    /**
     * Notify ui that a player has disconnected.
     * @param nickname of the player that has disconnected.
     */
    @Override
    public void showPlayerDisconnected(String nickname) {
        mPrinter.print(String.format("Player %s has disconnected", nickname), PrintColor.PURPLE);
    }

    /**
     * Notify ui that the last turn is started.
     * @param nickname of the player that has built all his emporiums.
     */
    @Override
    public void showLastTurnStarted(String nickname) {
        mPrinter.print("Player %s has built all his emporiums. The last game turn is starting.", nickname);
    }

    /**
     * Notify ui that the game is over.
     * @param updateStates list of final update states.
     * @param ranking list of players from the winner to the last loser.
     */
    @Override
    public void notifyGameEnded(UpdateState[] updateStates, List<String> ranking) {
        mPrinter.print("Game is ended, the players has owned the following victory points: ");
        for (UpdateState updateState : updateStates) {
            mPrinter.print("- %s -> %d victory points", updateState.getNickname(), updateState.getAddedVictoryPoints());
        }
        mContext = new EndGameContext(this, getController().getGameModel().getPlayers(), ranking);
    }

    /**
     * Print something on the display.
     * @param message to print.
     */
    @Override
    public void print(String message) {
        mPrinter.print(message);
    }

    /**
     * Send a chat message.
     * @param message body to send.
     */
    @Override
    public void sendChatMessage(String message) {
        mPrinter.print("[chat][you] %s", message);
        getController().sendChatMessage(null, message);
    }

    /**
     * Send a private chat message.
     * @param receiver of the message.
     * @param message body to send.
     */
    @Override
    public void sendPrivateChatMessage(String receiver, String message) {
        mPrinter.print("[chat][you -> %s] %s", receiver, message);
        getController().sendChatMessage(receiver, message);
    }

    /**
     * Show the current map.
     */
    @Override
    public void showMap() {
        BaseGame baseGame = getController().getGameModel();
        mPrinter.print("Left region board:");
        for (City city : baseGame.getLeftRegionCities()) {
            printCity(city, baseGame.getLinkedCities(city));
        }
        mPrinter.print(FIRST_PERMIT_TILE, getBusinessPermitTile(baseGame.getLeftRegionFirstVisibleTile()));
        mPrinter.print(SECOND_PERMIT_TILE, getBusinessPermitTile(baseGame.getLeftRegionSecondVisibleTile()));
        mPrinter.print("Center region board:");
        for (City city : baseGame.getCenterRegionCities()) {
            printCity(city, baseGame.getLinkedCities(city));
        }
        mPrinter.print(FIRST_PERMIT_TILE, getBusinessPermitTile(baseGame.getCenterRegionFirstVisibleTile()));
        mPrinter.print(SECOND_PERMIT_TILE, getBusinessPermitTile(baseGame.getCenterRegionSecondVisibleTile()));
        mPrinter.print("Right region board:");
        for (City city : baseGame.getRightRegionCities()) {
            printCity(city, baseGame.getLinkedCities(city));
        }
        mPrinter.print(FIRST_PERMIT_TILE, getBusinessPermitTile(baseGame.getRightRegionFirstVisibleTile()));
        mPrinter.print(SECOND_PERMIT_TILE, getBusinessPermitTile(baseGame.getRightRegionSecondVisibleTile()));
        mPrinter.print("Balconies:");
        printBalcony("Left balcony", baseGame.getLeftBalconyColors());
        printBalcony("Center balcony", baseGame.getCenterBalconyColors());
        printBalcony("Right balcony", baseGame.getRightBalconyColors());
        printBalcony("King balcony", baseGame.getKingBalconyColors());
        mPrinter.print("King is in %s city", baseGame.getKingCityName());
    }

    private void printCity(City city, List<City> linkedCities) {
        mPrinter.print("- %s (%s), linked to: %s", city.getName(), city.getType(), getLinkedCities(linkedCities));
    }

    private String getLinkedCities(List<City> cities) {
        StringBuilder builder = new StringBuilder();
        for (City city : cities) {
            if (builder.length() > 0) {
                builder.append(", ");
            }
            builder.append(city.getName());
        }
        return builder.toString();
    }

    private void printBalcony(String prefix, String[] colors) {
        mPrinter.print("- %s: [%s|%s|%s|%s]", prefix,
                colors[3],
                colors[2],
                colors[1],
                colors[0]);
    }

    /**
     * Show the nobility track.
     */
    @Override
    public void showNobilityTrack() {
        BaseGame baseGame = getController().getGameModel();
        Map<Integer, NobilityReward> nobilityTrack = baseGame.getNobilityTrack();
        Set<Integer> keySet = nobilityTrack.keySet();
        List<Integer> sortedKeys = new ArrayList<>(keySet);
        Collections.sort(sortedKeys);
        mPrinter.print("Nobility track:");
        for (Integer position : sortedKeys) {
            mPrinter.print("- position: %d, reward: %s", position, getNobilityReward(nobilityTrack.get(position)));
        }
    }

    /**
     * Show a given city.
     * @param name of the city.
     */
    @Override
    public void showCity(String name) {
        BaseGame baseGame = getController().getGameModel();
        City city = baseGame.findCity(name);
        if (city != null) {
            mPrinter.print("%s: ", name);
            mPrinter.print("- type: %s", city.getType());
            mPrinter.print("- reward: %s", city.getReward() != null ? getReward(city.getReward()) : "no reward");
            mPrinter.print("- linked to: %s", getLinkedCities(baseGame.getLinkedCities(city)));
            mPrinter.print("- emporiums: %s", getEmporiums(city.getEmporiums()));
            if (city.hasKing()) {
                mPrinter.print("- king is in this city");
            }
        } else {
            mPrinter.print("City %s not found", name);
        }
    }

    private String getEmporiums(Set<String> players) {
        StringBuilder builder = new StringBuilder();
        for (String player : players) {
            if (builder.length() > 0) {
                builder.append(", ");
            }
            builder.append(player);
        }
        if (builder.length() == 0) {
            builder.append("no emporium built here");
        }
        return builder.toString();
    }

    /**
     * Show the councillors.
     */
    @Override
    public void showCouncillors() {
        List<Councilor> councilors = getController().getGameModel().getCouncilors();
        mPrinter.print("Councilors:");
        for (int i = 0; i < councilors.size(); i++) {
            mPrinter.print(LIST_PATTERN, i + 1, getCouncillor(councilors.get(i)));
        }
    }

    /**
     * Show the players.
     */
    @Override
    public void showPlayers() {
        mPrinter.print("Player list:");
        BaseGame baseGame = getController().getGameModel();
        List<Player> players = baseGame.getPlayers();
        for (int i = 0; i < players.size(); i++) {
            mPrinter.print(LIST_PATTERN, i + 1, players.get(i).getNickname());
        }
    }

    /**
     * Show a given player.
     * @param nickname of the player to show.
     */
    @Override
    public void showPlayer(String nickname) {
        Player player = getPlayer(nickname);
        if (player != null) {
            mPrinter.print("Player %s:", player.getNickname());
            mPrinter.print("- assistants: %d", player.getAssistants());
            mPrinter.print("- emporiums to build: %d", player.getEmporiums());
            mPrinter.print("- coins: %d", player.getCoins());
            mPrinter.print("- victory points: %d", player.getVictoryPoints());
            mPrinter.print("- nobility track position: %d", player.getNobilityTrackPosition());
            mPrinter.print("- politic cards: %d", player.getPoliticCards().size());
            mPrinter.print("- usable business permit tiles: %d", player.getBusinessPermitTiles().size());
            mPrinter.print("- used business permit tiles: %d", player.getUsedBusinessPermitTiles().size());
            mPrinter.print("- reward king cards: %d", player.getKingRewards().size());
            mPrinter.print("- bonus cards: %d", player.getBonusCards().size());
        } else {
            mPrinter.print(PLAYER_NOT_FOUND, nickname);
        }
    }

    /**
     * Show current player politic cards.
     */
    @Override
    public void showPlayerPoliticCards() {
        Player player = getCurrentPlayer();
        List<PoliticCard> politicCards = player.getPoliticCards();
        mPrinter.print("Politic cards: ");
        for (int i = 0; i < politicCards.size(); i++) {
            mPrinter.print(LIST_PATTERN, i + 1, getPoliticCard(politicCards.get(i)));
        }
    }

    /**
     * Show the business permit tile of a given player.
     * @param nickname of the player to show.
     */
    @Override
    public void showPlayerUsableBusinessPermitTiles(String nickname) {
        Player player = getPlayer(nickname);
        if (player != null) {
            List<BusinessPermitTile> businessPermitTiles = player.getBusinessPermitTiles();
            mPrinter.print("Usable business permit tiles: ");
            for (int i = 0; i < businessPermitTiles.size(); i++) {
                mPrinter.print(LIST_PATTERN, i + 1, getBusinessPermitTile(businessPermitTiles.get(i)));
            }
        } else {
            mPrinter.print(PLAYER_NOT_FOUND, nickname);
        }
    }

    /**
     * Show the used business permit tile of a given player.
     * @param nickname of the player to show.
     */
    @Override
    public void showPlayerUsedBusinessPermitTiles(String nickname) {
        Player player = getPlayer(nickname);
        if (player != null) {
            List<BusinessPermitTile> businessPermitTiles = player.getUsedBusinessPermitTiles();
            mPrinter.print("Used business permit tiles: ");
            for (int i = 0; i < businessPermitTiles.size(); i++) {
                mPrinter.print(LIST_PATTERN, i + 1, getBusinessPermitTile(businessPermitTiles.get(i)));
            }
        } else {
            mPrinter.print(PLAYER_NOT_FOUND, nickname);
        }
    }

    /**
     * Show the reward king cards of a given player.
     * @param nickname of the player to show.
     */
    @Override
    public void showRewardKingCards(String nickname) {
        Player player = getPlayer(nickname);
        if (player != null) {
            List<Reward> rewards = player.getKingRewards();
            mPrinter.print("Reward king cards:");
            for (int i = 0; i < rewards.size(); i++) {
                mPrinter.print(LIST_PATTERN, i + 1, getReward(rewards.get(i)));
            }
        } else {
            mPrinter.print(PLAYER_NOT_FOUND, nickname);
        }
    }

    /**
     * Show the bonus cards of a given player.
     * @param nickname of the player to show.
     */
    @Override
    public void showBonusCards(String nickname) {
        Player player = getPlayer(nickname);
        if (player != null) {
            Map<String, Reward> bonusCards = player.getBonusCards();
            mPrinter.print("Bonus cards:");
            for (Map.Entry<String, Reward> entry : bonusCards.entrySet()) {
                mPrinter.print("- %s: %s", entry.getKey(), getReward(entry.getValue()));
            }
        } else {
            mPrinter.print(PLAYER_NOT_FOUND, nickname);
        }
    }

    private Player getCurrentPlayer() {
        UiController controller = getController();
        String nickname = controller.getNickname();
        return controller.getGameModel().getPlayer(nickname);
    }

    private Player getPlayer(String nickname) {
        BaseGame baseGame = getController().getGameModel();
        return baseGame.getPlayer(nickname != null ? nickname : getController().getNickname());
    }

    private String getCouncillor(Councilor councilor) {
        return councilor.getColor();
    }

    private String getPoliticCard(PoliticCard politicCard) {
        if (!politicCard.isJolly()) {
            return politicCard.getColor();
        }
        return "jolly";
    }

    private String getNobilityReward(NobilityReward reward) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        StringBuilder rewardBuilder = getRewardBuilder(reward);
        boolean appendDivider = rewardBuilder.length() > 0;
        builder.append(getRewardBuilder(reward));
        if (reward.getFirstSpecialBonusCount() > 0) {
            if (appendDivider) {
                builder.append(", ");
            }
            builder.append(String.format("+%d special action 1", reward.getFirstSpecialBonusCount()));
            appendDivider = true;
        }
        if (reward.getSecondSpecialBonusCount() > 0) {
            if (appendDivider) {
                builder.append(", ");
            }
            builder.append(String.format("+%d special action 2", reward.getSecondSpecialBonusCount()));
            appendDivider = true;
        }
        if (reward.getThirdSpecialBonusCount() > 0) {
            if (appendDivider) {
                builder.append(", ");
            }
            builder.append(String.format("+%d special action 3", reward.getThirdSpecialBonusCount()));
        }
        builder.append("]");
        return builder.toString();
    }

    private String getReward(Reward reward) {
        return String.format("[%s]", getRewardBuilder(reward).toString());
    }

    private StringBuilder getRewardBuilder(Reward reward) {
        StringBuilder builder = new StringBuilder();
        if (reward.getAssistantCount() > 0) {
            builder.append(String.format("+%d assistants", reward.getAssistantCount()));
        }
        if (reward.getCoinsCount() > 0) {
            builder.append(getDivider(builder));
            builder.append(String.format("+%d coins", reward.getCoinsCount()));
        }
        if (reward.getNobilityStepCount() > 0) {
            builder.append(getDivider(builder));
            builder.append(String.format("+%d nobility steps", reward.getNobilityStepCount()));
        }
        if (reward.getPoliticCardCount() > 0) {
            builder.append(getDivider(builder));
            builder.append(String.format("+%d politic cards", reward.getPoliticCardCount()));
        }
        if (reward.getVictoryPointCount() > 0) {
            builder.append(getDivider(builder));
            builder.append(String.format("+%d victory points", reward.getVictoryPointCount()));
        }
        if (reward.getMainActionCount() > 0) {
            builder.append(getDivider(builder));
            builder.append(String.format("+%d main actions", reward.getMainActionCount()));
        }
        return builder;
    }

    private String getDivider(StringBuilder builder) {
        return builder.length() > 0 ? ", " : "";
    }

    /**
     * Get the action list.
     */
    @Override
    public void getActionList() {
        getController().getActionList();
    }

    /**
     * Check if player can draw a politic card.
     * @return true if it can, false otherwise.
     */
    @Override
    public boolean canDrawPoliticCard() {
        if (mActionList.isPoliticCardDrawn()) {
            mPrinter.print("You have already drawn a politic card. Type 'get-action-list' to get a list of available actions", PrintColor.RED);
            return false;
        }
        return true;
    }

    /**
     * Draw a politic card.
     */
    @Override
    public void drawPoliticCard() {
        getController().drawPoliticCard();
    }

    /**
     * Check if player can do a main action.
     * @return true if it can, false otherwise.
     */
    @Override
    public boolean canDoMainAction() {
        if (mActionList.isPoliticCardDrawn()) {
            if (mActionList.getMainActionCount() <= 0) {
                mPrinter.print("No main action available. Type 'get-action-list' to get a list of available actions", PrintColor.RED);
                return false;
            }
            return true;
        } else {
            mPrinter.print("You should draw a politic card before. Type 'get-action-list' to get a list of available actions", PrintColor.RED);
            return false;
        }
    }

    /**
     * Elect a councillor.
     * @param index of the councillor.
     * @param region where elect the councillor.
     */
    @Override
    public void electCouncillor(int index, String region) {
        BaseGame baseGame = getController().getGameModel();
        List<Councilor> councilors = baseGame.getCouncilors();
        if (isValidRange(index, 1, councilors.size())) {
            if (isValidRegion(region, true)) {
                getController().actionElectCouncillor(councilors.get(index - 1), region);
            } else {
                mPrinter.print(INVALID_REGION_BALCONY);
            }
        } else {
            mPrinter.print(String.format("[index] must be a number between %d-%d. Type 'show-councillors' to show a list of available councillors.", 1, councilors.size()), PrintColor.RED);
        }
    }

    /**
     * Acquire a business permit tile.
     * @param indices of the politic cards to use.
     * @param region where the permit tile is place.
     * @param permitTileIndex index of the permit tile to take.
     */
    @Override
    public void acquireBusinessPermitTile(int[] indices, String region, int permitTileIndex) {
        BaseGame baseGame = getController().getGameModel();
        Player player = baseGame.getPlayer(getController().getNickname());
        List<PoliticCard> politicCards = player.getPoliticCards();
        if (areValidRanges(indices, 1, politicCards.size())) {
            if (isValidRegion(region)) {
                if (isValidRange(permitTileIndex, 1, 2)) {
                    getController().actionAcquireBusinessPermitTile(getChosenPoliticCards(politicCards, indices), region, permitTileIndex);
                } else {
                    mPrinter.print("[index] must be 1 or 2 that indicates which permit tile do you want to acquire.", PrintColor.RED);
                }
            } else {
                mPrinter.print(INVALID_REGION_BALCONY, PrintColor.RED);
            }
        } else {
            mPrinter.print(String.format("[index/index/index/index] must be 4 numbers between %d-%d. Type 'show-politic-cards' to show a list of available politic cards.", 1, politicCards.size()), PrintColor.RED);
        }
    }

    /**
     * Build an emporium with a permit tile.
     * @param index of the permit tile.
     * @param city where build the emporium.
     */
    @Override
    public void buildEmporiumWithPermitTile(int index, String city) {
        BaseGame baseGame = getController().getGameModel();
        Player player = baseGame.getPlayer(getController().getNickname());
        List<BusinessPermitTile> businessPermitTiles = player.getBusinessPermitTiles();
        if (isValidRange(index, 1, businessPermitTiles.size())) {
            if (baseGame.findCity(city) != null) {
                BusinessPermitTile permitTile = businessPermitTiles.get(index - 1);
                if (isValidCity(city, permitTile)) {
                    getController().actionBuildEmporiumWithBusinessPermitTile(permitTile, city);
                } else {
                    mPrinter.print(String.format("This permit tile allows you to build only in cities where the name starts with: %s", getString(permitTile.getCities())), PrintColor.RED);
                }
            } else {
                mPrinter.print(String.format("Unknown city %s", city), PrintColor.RED);
            }
        } else {
            mPrinter.print(String.format("[index] must be a number between %d-%d. Type 'show-usable-business-permit-tiles' to show a list of available business permit tiles.", 1, businessPermitTiles.size()), PrintColor.RED);
        }
    }

    private String getString(char[] chars) {
        StringBuilder builder = new StringBuilder();
        for (char character : chars) {
            if (builder.length() > 0) {
                builder.append(", ");
            }
            builder.append(character);
        }
        return builder.toString();
    }

    /**
     * Build an emporium with the help of the king.
     * @param indices of the politic cards to use.
     * @param cities list of cities.
     */
    @Override
    public void buildEmporiumWithKingHelp(int[] indices, String[] cities) {
        BaseGame baseGame = getController().getGameModel();
        Player player = baseGame.getPlayer(getController().getNickname());
        List<PoliticCard> politicCards = player.getPoliticCards();
        if (areValidRanges(indices, 1, politicCards.size())) {
            if (areValidCities(baseGame, cities)) {
                getController().actionBuildEmporiumWithKingHelp(getChosenPoliticCards(politicCards, indices), Arrays.asList(cities));
            } else {
                mPrinter.print("Invalid route. Please check city names and retry.", PrintColor.RED);
            }
        } else {
            mPrinter.print(String.format("[index/index/index/index] must be 4 numbers between %d-%d. Type 'show-politic-cards' to show a list of available politic cards.", 1, politicCards.size()), PrintColor.RED);
        }
    }

    private boolean isValidRange(int index, int min, int max) {
        return index >= min && index <= max;
    }

    private boolean isValidRegion(String region) {
        return "left".equals(region) || "center".equals(region) || "right".equals(region);
    }

    private boolean isValidRegion(String region, boolean withKing) {
        return isValidRegion(region) || (withKing && "king".equals(region));
    }

    private boolean isValidCity(String city, BusinessPermitTile businessPermitTile) {
        if (city.length() > 0) {
            char firstChar = city.charAt(0);
            for (char character : businessPermitTile.getCities()) {
                if (Character.toUpperCase(firstChar) == Character.toUpperCase(character)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean areValidRanges(int[] indices, int min, int max) {
        for (int index : indices) {
            if (!isValidRange(index, min, max)) {
                return false;
            }
        }
        return true;
    }

    private boolean areValidCities(BaseGame baseGames, String[] cities) {
        for (String city : cities) {
            if (baseGames.findCity(city) == null) {
                return false;
            }
        }
        return true;
    }

    private List<PoliticCard> getChosenPoliticCards(List<PoliticCard> politicCardList, int[] indices) {
        List<PoliticCard> politicCards = new ArrayList<>();
        for (int index : indices) {
            politicCards.add(politicCardList.get(index - 1));
        }
        return politicCards;
    }

    /**
     * Check if player can do a fast action.
     * @return true if it can, false otherwise.
     */
    @Override
    public boolean canDoFastAction() {
        if (mActionList.isPoliticCardDrawn()) {
            if (mActionList.getFastActionCount() <= 0) {
                mPrinter.print("No fast action available. Type 'get-action-list' to get a list of available actions", PrintColor.RED);
                return false;
            }
            return true;
        } else {
            mPrinter.print("You should draw a politic card before. Type 'get-action-list' to get a list of available actions", PrintColor.RED);
            return false;
        }
    }

    /**
     * Engage an assistant.
     */
    @Override
    public void engageAssistant() {
        getController().actionEngageAssistant();
    }

    /**
     * Change the visible permit tiles of the region.
     * @param region where the visible permit tiles should be changed.
     */
    @Override
    public void changeBusinessPermitTiles(String region) {
        if (isValidRegion(region)) {
            getController().actionChangeBusinessPermitTile(region);
        } else {
            mPrinter.print(INVALID_REGION_BALCONY, PrintColor.RED);
        }
    }

    /**
     * Send an assistant to elect a councillor.
     * @param index of the councillor.
     * @param region where elect the councillor.
     */
    @Override
    public void sendAssistantToElectCouncillor(int index, String region) {
        BaseGame baseGame = getController().getGameModel();
        List<Councilor> councilors = baseGame.getCouncilors();
        if (isValidRange(index, 1, councilors.size())) {
            if (isValidRegion(region, true)) {
                getController().actionSendAssistantElectCouncillor(councilors.get(index - 1), region);
            } else {
                mPrinter.print(INVALID_REGION_BALCONY);
            }
        } else {
            mPrinter.print(String.format("[index] must be a number between %d-%d. Type 'show-councillors' to show a list of available councillors.", 1, councilors.size()), PrintColor.RED);
        }
    }

    /**
     * Buy an additional main action.
     */
    @Override
    public void performAdditionalMainAction() {
        getController().actionPerformAdditionalMainAction();
    }

    /**
     * Check if player can end the turn.
     * @return true if it can, false otherwise.
     */
    @Override
    public boolean canEndTurn() {
        if (mActionList.isPoliticCardDrawn()) {
            if (mActionList.getMainActionCount() > 0) {
                mPrinter.print("You should do all available main actions before ending the turn. Type 'get-action-list' to get a list of available actions", PrintColor.RED);
                return false;
            }
            return true;
        } else {
            mPrinter.print("You should draw a politic card and do a main action before ending the turn. Type 'get-action-list' to get a list of available actions", PrintColor.RED);
            return false;
        }
    }

    /**
     * End turn.
     */
    @Override
    public void endTurn() {
        getController().endTurn();
    }

    /**
     * Show a list of all market items.
     */
    @Override
    public void showAllMarketItems() {
        mPrinter.print("Market items: ");
        MarketSession marketSession = getController().getMarketSession();
        for (Item item : marketSession.getAllItems()) {
            mPrinter.print("- %s", getMarketItem(item));
        }
    }

    /**
     * Show a list of all items on sale.
     */
    @Override
    public void showMarketItemsOnSale() {
        mPrinter.print("Items on sale: ");
        MarketSession marketSession = getController().getMarketSession();
        for (Item item : marketSession.getItemsOnSale()) {
            mPrinter.print("- %s", getMarketItem(item));
        }
    }

    /**
     * Show a list of all items sold.
     */
    @Override
    public void showMarketItemsSold() {
        mPrinter.print("Items sold: ");
        MarketSession marketSession = getController().getMarketSession();
        for (Item item : marketSession.getItemsSold()) {
            mPrinter.print("- %s", getMarketItem(item));
        }
    }

    private String getMarketItem(Item item) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("[%s] ", item.getSeller()));
        builder.append(item.isSold() ? "sold " : "is selling ");
        if (item.getBusinessPermitTile() != null) {
            builder.append("this permit tile: ");
            builder.append(getBusinessPermitTile(item.getBusinessPermitTile()));
        }
        if (item.getPoliticCard() != null) {
            builder.append(String.format("a %s politic card", getPoliticCard(item.getPoliticCard())));
        }
        if (item.getAssistant() > 0) {
            builder.append("an assistant");
        }
        if (item.isSold()) {
            builder.append(String.format(" to %s", item.getBuyer()));
        }
        builder.append(String.format(" for %d coins", item.getPrice()));
        return builder.toString();
    }

    /**
     * Sell a politic card on the market.
     * @param index of the politic card.
     * @param price to set.
     */
    @Override
    public void sellPoliticCard(int index, int price) {
        List<PoliticCard> politicCards = getCurrentPlayer().getPoliticCards();
        if (isValidRange(index, 1, politicCards.size())) {
            if (checkPrice(price)) {
                PoliticCard politicCard = politicCards.get(index - 1);
                getController().sellPoliticCard(politicCard, price);
            }
        } else {
            mPrinter.print(String.format("[index] must be %d-%d. Type 'show-politic-cards' to display available politic cards", 1, politicCards.size()), PrintColor.RED);
        }
    }

    /**
     * Sell a business permit tile on the market.
     * @param index of the politic card.
     * @param price to set.
     */
    @Override
    public void sellBusinessPermitTile(int index, int price) {
        List<BusinessPermitTile> businessPermitTiles = getCurrentPlayer().getBusinessPermitTiles();
        if (isValidRange(index, 1, businessPermitTiles.size())) {
            if (checkPrice(price)) {
                BusinessPermitTile businessPermitTile = businessPermitTiles.get(index - 1);
                getController().sellBusinessPermitTile(businessPermitTile, price);
            }
        } else {
            mPrinter.print(String.format("[index] must be %d-%d. Type 'show-permit-tiles' to display available permit tiles", 1, businessPermitTiles.size()), PrintColor.RED);
        }
    }

    /**
     * Sell an assistant on the market.
     * @param price to set.
     */
    @Override
    public void sellAssistant(int price) {
        if (checkPrice(price)) {
            getController().sellAssistant(price);
        }
    }

    private boolean checkPrice(int price) {
        if (price > 0) {
            return true;
        }
        mPrinter.print("Price should be greater than 0", PrintColor.RED);
        return false;
    }

    /**
     * Show a list of politic cards on sale.
     */
    @Override
    public void showPoliticCards() {
        MarketSession marketSession = getController().getMarketSession();
        List<Item> items = marketSession.getPoliticCards();
        mPrinter.print("Politic cards on sale: ");
        for (int i = 0; i < items.size(); i++) {
            mPrinter.print(LIST_PATTERN, i + 1, getMarketItem(items.get(i)));
        }
    }

    /**
     * Show a list of business permit tiles on sale.
     */
    @Override
    public void showBusinessPermitTile() {
        MarketSession marketSession = getController().getMarketSession();
        List<Item> items = marketSession.getBusinessPermitTiles();
        mPrinter.print("Business permit tiles on sale: ");
        for (int i = 0; i < items.size(); i++) {
            mPrinter.print(LIST_PATTERN, i + 1, getMarketItem(items.get(i)));
        }
    }

    /**
     * Show a list of all assistants on sale.
     */
    @Override
    public void showAssistants() {
        MarketSession marketSession = getController().getMarketSession();
        List<Item> items = marketSession.getAssistants();
        mPrinter.print("Assistants on sale: ");
        for (int i = 0; i < items.size(); i++) {
            mPrinter.print(LIST_PATTERN, i + 1, getMarketItem(items.get(i)));
        }
    }

    /**
     * Buy a politic card.
     * @param index of the politic card.
     */
    @Override
    public void buyPoliticCard(int index) {
        MarketSession marketSession = getController().getMarketSession();
        List<Item> items = marketSession.getPoliticCards();
        if (isValidRange(index, 1, items.size())) {
            getController().buyItem(items.get(index - 1).getMarketId());
        } else {
            mPrinter.print(String.format("[index] must be %d-%d. Type 'show-market-politic-cards' to display politic cards on sale", 1, items.size()), PrintColor.RED);
        }
    }

    /**
     * Buy a business permit tile.
     * @param index of the business permit tile.
     */
    @Override
    public void buyBusinessPermitTile(int index) {
        MarketSession marketSession = getController().getMarketSession();
        List<Item> items = marketSession.getBusinessPermitTiles();
        if (isValidRange(index, 1, items.size())) {
            getController().buyItem(items.get(index - 1).getMarketId());
        } else {
            mPrinter.print(String.format("[index] must be %d-%d. Type 'show-market-permit-tiles' to display permit tiles on sale", 1, items.size()), PrintColor.RED);
        }
    }

    /**
     * Buy an assistant.
     * @param index of the assistant.
     */
    @Override
    public void buyAssistant(int index) {
        MarketSession marketSession = getController().getMarketSession();
        List<Item> items = marketSession.getAssistants();
        if (isValidRange(index, 1, items.size())) {
            getController().buyItem(items.get(index - 1).getMarketId());
        } else {
            mPrinter.print(String.format("[index] must be %d-%d. Type 'show-market-assistants' to display assistants on sale", 1, items.size()), PrintColor.RED);
        }
    }

    /**
     * Earn a first special rewards.
     * @param cities where the player want to get the reward.
     */
    @Override
    public void earnFirstSpecialRewards(List<String> cities) {
        // check if cities has no nobility track rewards
        String invalidCity = null;
        for (String name : cities) {
            City city = getController().getGameModel().findCity(name);
            if (city == null || city.getReward() == null || city.getReward().getNobilityStepCount() > 0) {
                invalidCity = name;
                break;
            }
        }
        if (invalidCity != null) {
            mPrinter.print(String.format("%s is not a valid city", invalidCity), PrintColor.RED);
        } else {
            getController().earnFirstSpecialRewards(cities);
        }
    }

    /**
     * Earn a second special rewards.
     * @param regions where the player want to take the permit tile.
     * @param indices where the player want to take the permit tile.
     */
    @Override
    public void earnSecondSpecialRewards(List<String> regions, List<Integer> indices) {
        for (String region : regions) {
            if (!isValidRegion(region)) {
                mPrinter.print("Region %s is not a valid region", region);
                return;
            }
        }
        for (Integer index : indices) {
            if (!isValidRange(index, 1, 2)) {
                mPrinter.print("Index must be 1 or 2");
                return;
            }
        }
        getController().earnSecondSpecialRewards(regions, indices);
    }

    /**
     * Get the business permit tile as string.
     * @param businessPermitTile to get.
     * @return the given business permit tile as string.
     */
    @Override
    public String getBusinessPermitTile(BusinessPermitTile businessPermitTile) {
        StringBuilder builder = new StringBuilder();
        if (businessPermitTile != null) {
            builder.append("[cities: ");
            for (char city : businessPermitTile.getCities()) {
                builder.append(city);
                builder.append(", ");
            }
            builder.append("reward: ");
            builder.append(getRewardBuilder(businessPermitTile));
            builder.append("]");
        } else {
            builder.append("no tile found");
        }
        return builder.toString();
    }

    /**
     * Earn a third special rewards.
     * @param businessPermitTiles permit tiles where the player want to take the reward.
     */
    @Override
    public void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles) {
        getController().earnThirdSpecialRewards(businessPermitTiles);
    }

    /**
     * This class in an extension of a thread and it will stay always listening on keyboards events.
     * It will delegate the user input to the current Context that will handle it if it can.
     */
    private class KeyboardHandler extends Thread {

        /**
         * Thread implementation.
         */
        @Override
        public void run() {
            BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                try {
                    String line = keyboard.readLine();
                    if (mContext != null) {
                        mContext.handle(line);
                    }
                } catch (UnknownCommandException e) {
                    Debug.debug("Unknown command", e);
                    mPrinter.print("Unknown command, type 'help' for command list");
                } catch (IOException e) {
                    Debug.error("Cannot read from keyboard", e);
                    break;
                }
            }
        }
    }
}
package it.polimi.cof.ui.cli.context;

import it.polimi.cof.model.BusinessPermitTile;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represent the third special rewards context.
 */
public class ThirdSpecialRewardContext extends BaseSpecialRewardContext {

    private final ThirdSpecialRewardCallback mCallback;
    private final List<BusinessPermitTile> mBusinessPermitTiles;

    /**
     * Base constructor.
     * @param callback to use.
     * @param count of rewards.
     * @param businessPermitTiles to use.
     */
    public ThirdSpecialRewardContext(ThirdSpecialRewardCallback callback, int count, List<BusinessPermitTile> businessPermitTiles) {
        super(callback, count);
        mCallback = callback;
        mBusinessPermitTiles = businessPermitTiles;
        addCommand("show-all-permit-tiles", "", arguments -> showAllPermitTiles());
        addCommand("take-permit-tile-bonus", getHelpText("[index]"), this::takePermitTileBonus);
    }

    private void showAllPermitTiles() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < mBusinessPermitTiles.size(); i++) {
            if (i != 0) {
                builder.append("\n");
            }
            builder.append(i + 1);
            builder.append(") ");
            builder.append(mCallback.getBusinessPermitTile(mBusinessPermitTiles.get(i)));
        }
        print(builder.toString());
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void takePermitTileBonus(String[] arguments) throws CommandNotValid {
        if (arguments.length == getCount()) {
            List<BusinessPermitTile> businessPermitTiles = new ArrayList<>();
            for (String argument : arguments) {
                int index = parseIndex(argument);
                if (index >= 1 && index <= mBusinessPermitTiles.size()) {
                    mBusinessPermitTiles.add(mBusinessPermitTiles.get(index - 1));
                } else {
                    throw new CommandNotValid("Index not valid");
                }
            }
            mCallback.earnThirdSpecialRewards(businessPermitTiles);
        } else {
            throw new CommandNotValid();
        }
    }

    @Override
    protected void showSingleRewardTutorial() {
        print("You have obtained a special reward and you can now take a bonus from a permit tile that you have " +
                "already acquired. Type 'show-all-permit-tiles' to list all available permit tiles and type " +
                "'take-permit-tile-bonus' to get the bonus from the selected permit tile");
    }

    @Override
    protected void showMultiRewardTutorial(int count) {
        print("You have obtained a special reward and you can now take a bonus from %d permit tiles that you have " +
                "already acquired. Type 'show-all-permit-tiles' to list all available permit tiles and type " +
                "'take-permit-tile-bonus' to get the bonus from the selected permit tiles", count);
    }

    /**
     * Interface used as callback.
     */
    public interface ThirdSpecialRewardCallback extends BaseGameContext.Callback {

        /**
         * Get the business permit tile as string.
         * @param businessPermitTile to get.
         * @return the given business permit tile as string.
         */
        String getBusinessPermitTile(BusinessPermitTile businessPermitTile);

        /**
         * Earn a third special rewards.
         * @param businessPermitTiles permit tiles where the player want to take the reward.
         */
        void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles);
    }
}
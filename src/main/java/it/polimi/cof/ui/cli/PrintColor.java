package it.polimi.cof.ui.cli;

/**
 * This enum represent all possible color that can be printed in a console window.
 */
public enum PrintColor {

    /**
     * List of available colors.
     */
    BLACK("\u001B[30m"),
    RED("\u001B[31m"),
    GREEN("\u001B[32m"),
    YELLOW("\u001B[33m"),
    BLUE("\u001B[34m"),
    PURPLE("\u001B[35m"),
    CYAN("\u001B[36m"),
    WHITE("\u001B[37m");

    /**
     * This should be appended at the end of the string to reset color.
     */
    public static final String CLOSE_COLOR_TAG = "\u001B[0m";

    /**
     * Internal enum value.
     */
    private String mAnsiColor;

    /**
     * Enum constructor.
     * @param ansiColor to append in console window.
     */
    PrintColor(String ansiColor) {
        this.mAnsiColor = ansiColor;
    }

    /**
     * Get the color to append in the string.
     * @return the color as string to append in the string.
     */
    public String getAnsiColor() {
        return mAnsiColor;
    }
}
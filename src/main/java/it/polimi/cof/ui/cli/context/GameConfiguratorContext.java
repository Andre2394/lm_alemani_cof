package it.polimi.cof.ui.cli.context;

import it.polimi.cof.model.*;
import it.polimi.cof.util.LinkedList;

import java.util.*;

/**
 * This class represent the game configurator context.
 */
public class GameConfiguratorContext extends BaseUiContext {

    private static final String GATE_LEFT_CENTER_1 = "#1";
    private static final String GATE_LEFT_CENTER_2 = "#2";
    private static final String GATE_LEFT_CENTER_3 = "#3";
    private static final String GATE_CENTER_RIGHT_1 = "#4";
    private static final String GATE_CENTER_RIGHT_2 = "#5";
    private static final String GATE_CENTER_RIGHT_3 = "#6";
    private static final String INVALID_ASSISTANTS = "Invalid assistants number";
    private static final String INVALID_VICTORY_POINTS = "Invalid victory points number";
    private static final String INVALID_NOBILITY_STEPS = "Invalid nobility steps number";
    private static final String INVALID_COINS = "Invalid coins number";
    private static final String INVALID_POLITIC_CARDS = "Invalid politic cards number";
    private static final String INVALID_MAIN_ACTIONS = "Invalid main actions number";

    private final Callback mCallback;
    private final Configuration mConfigurations;

    // current map indices
    private int mLeftMapIndex;
    private int mCenterMapIndex;
    private int mRightMapIndex;
    private LinkedList<String, City> mCurrentMap;

    private RegionBoard mLeftRegionBoard;
    private RegionBoard mCenterRegionBoard;
    private RegionBoard mRightRegionBoard;
    private Map<Integer, NobilityReward> mNobilityTrack;
    private List<BusinessPermitTile> mBusinessPermitTiles;
    private int mWaitingTime;

    /**
     * Base constructor.
     * @param contextInterface to use.
     * @param callback to use.
     * @param configurations bundle that contains all default configurations.
     */
    public GameConfiguratorContext(ContextInterface contextInterface, Callback callback, Configuration configurations) {
        super(contextInterface);
        mCallback = callback;
        mConfigurations = configurations;
        mLeftRegionBoard = configurations.getLeftRegionBoards().get(mLeftMapIndex);
        mCenterRegionBoard = configurations.getCenterRegionBoards().get(mCenterMapIndex);
        mRightRegionBoard = configurations.getRightRegionBoards().get(mRightMapIndex);
        mNobilityTrack = new HashMap<>(configurations.getNobilityTrack());
        mBusinessPermitTiles = new ArrayList<>(configurations.getBusinessPermitTiles());
        mWaitingTime = 180;
        addCommand("show-configuration", "", arguments -> showCurrentConfiguration());
        addCommand("show-map", "", arguments -> showMapConfiguration());
        addCommand("show-nobility-track","", arguments -> showNobilityTrackConfiguration());
        addCommand("show-permit-tiles", "", arguments -> showBusinessPermitTiles());
        addCommand("show-waiting-time", "", arguments -> showWaitingTime());
        addCommand("change-map", "[left|center|right]", this::changeMap);
        addCommand("change-waiting-time", "[time in seconds (30s <-> 300s)]", this::changeWaitingTime);
        addCommand("add-city-reward", "[city] [assistants] [victory points] [nobility steps] [coins] [politic cards] [main actions]", this::addCityReward);
        addCommand("remove-city-reward", "[city] [assistants] [victory points] [nobility steps] [coins] [politic cards] [main actions]", this::removeCityReward);
        addCommand("add-link", "[city1] [city2]", this::addLink);
        addCommand("remove-link", "[city1] [city2]", this::removeLink);
        addCommand("add-nobility-reward", "[position] [assistants] [victory points] [coins] [politic cards] [main actions] [special reward 1] [special reward 2] [special reward 3]", this::addNobilityReward);
        addCommand("remove-nobility-reward", "[position] [assistants] [victory points] [coins] [politic cards] [main actions] [special reward 1] [special reward 2] [special reward 3]", this::removeNobilityReward);
        addCommand("add-permit-tile", "[characters] [assistants] [victory points] [coins] [politic cards] [main actions]", this::addBusinessPermitTile);
        addCommand("remove-permit-tile", "[index]", this::removeBusinessPermitTile);
        addCommand("apply-configuration", "", arguments -> applyConfiguration());
        loadCurrentMap();
        print("Type 'help' for command list, type 'apply-configuration' to set current configuration.");
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void changeMap(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            switch (arguments[0]) {
                case "left":
                    mLeftMapIndex = mConfigurations.getLeftRegionBoards().size() > (mLeftMapIndex + 1) ? mLeftMapIndex + 1 : 0;
                    mLeftRegionBoard = mConfigurations.getLeftRegionBoards().get(mLeftMapIndex);
                    break;
                case "center":
                    mCenterMapIndex = mConfigurations.getCenterRegionBoards().size() > (mCenterMapIndex + 1) ? mCenterMapIndex + 1 : 0;
                    mCenterRegionBoard = mConfigurations.getCenterRegionBoards().get(mCenterMapIndex);
                    break;
                case "right":
                    mRightMapIndex = mConfigurations.getRightRegionBoards().size() > (mRightMapIndex + 1) ? mRightMapIndex + 1 : 0;
                    mRightRegionBoard = mConfigurations.getRightRegionBoards().get(mRightMapIndex);
                    break;
                default:
                    throw new CommandNotValid();
            }
            loadCurrentMap();
            showMapConfiguration();
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void changeWaitingTime(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            try {
                int time = Integer.parseInt(arguments[0]);
                if (time >= 30 && time <= 300) {
                    mWaitingTime = time;
                } else {
                    throw new CommandNotValid("Time out of range, must be [30-300]");
                }
            } catch (NumberFormatException e) {
                throw new CommandNotValid("Not a valid number");
            }
        } else {
            throw new CommandNotValid();
        }
    }

    private void loadCurrentMap() {
        mCurrentMap = new LinkedList<>();
        // add gates
        mCurrentMap.addNode(GATE_LEFT_CENTER_1, null);
        mCurrentMap.addNode(GATE_LEFT_CENTER_2, null);
        mCurrentMap.addNode(GATE_LEFT_CENTER_3, null);
        mCurrentMap.addNode(GATE_CENTER_RIGHT_1, null);
        mCurrentMap.addNode(GATE_CENTER_RIGHT_2, null);
        mCurrentMap.addNode(GATE_CENTER_RIGHT_3, null);
        // add cities
        mCurrentMap.addNodes(mLeftRegionBoard.getCities());
        mCurrentMap.addNodes(mCenterRegionBoard.getCities());
        mCurrentMap.addNodes(mRightRegionBoard.getCities());
        // add links
        addLinks(mLeftRegionBoard.getLinks());
        addLinks(mCenterRegionBoard.getLinks());
        addLinks(mRightRegionBoard.getLinks());
    }

    private void addLinks(List<Link> links) {
        for (Link link : links) {
            mCurrentMap.linkNode(link.getCityFrom(), link.getCityTo());
        }
    }

    private void showCurrentConfiguration() {
        showMapConfiguration();
        showBusinessPermitTiles();
        showNobilityTrackConfiguration();
        showWaitingTime();
    }

    private void showMapConfiguration() {
        StringBuilder builder = new StringBuilder();
        builder.append("Map configuration:\n");
        builder.append("Left region board: \n");
        for (String name : mLeftRegionBoard.getCities().keySet()) {
            appendCity(builder, name);
            builder.append("\n");
        }
        builder.append("Center region board: \n");
        for (String name : mCenterRegionBoard.getCities().keySet()) {
            appendCity(builder, name);
            builder.append("\n");
        }
        builder.append("Right region board: \n");
        for (String name : mRightRegionBoard.getCities().keySet()) {
            appendCity(builder, name);
            builder.append("\n");
        }
        print(builder.toString());
    }

    private void appendCity(StringBuilder builder, String name) {
        City city = mCurrentMap.getValue(name);
        builder.append("- ");
        builder.append(name);
        builder.append(" (");
        builder.append(city.getType());
        builder.append(") coordinates: (");
        builder.append(city.getPositionX());
        builder.append(", ");
        builder.append(city.getPositionY());
        builder.append(") linked to: ");
        for (String linkedCity : mCurrentMap.getLinkedKeys(name)) {
            builder.append(linkedCity);
            builder.append(", ");
        }
        builder.append("reward: ");
        Reward reward = city.getReward();
        if (reward != null) {
            builder.append("[");
            appendFullReward(builder, reward);
            builder.append("]");
        } else {
            builder.append("No reward set");
        }
    }

    private void appendFullReward(StringBuilder builder, Reward reward) {
        builder.append(String.format("assistants: %d, ", reward.getAssistantCount()));
        builder.append(String.format("victory points: %d, ", reward.getVictoryPointCount()));
        if (!(reward instanceof NobilityReward)) {
            builder.append(String.format("nobility steps: %d, ", reward.getNobilityStepCount()));
        }
        builder.append(String.format("coins: %d, ", reward.getCoinsCount()));
        builder.append(String.format("politic cards: %d, ", reward.getPoliticCardCount()));
        builder.append(String.format("main actions: %d", reward.getMainActionCount()));
    }

    private void showNobilityTrackConfiguration() {
        StringBuilder builder = new StringBuilder();
        builder.append("Nobility track:\n");
        for (int i = 0; i <= Collections.max(mNobilityTrack.keySet()); i++) {
            builder.append(i);
            builder.append(") reward: ");
            if (mNobilityTrack.containsKey(i)) {
                builder.append("[");
                appendNobilityReward(builder, mNobilityTrack.get(i));
                builder.append("]");
            } else {
                builder.append("No reward set");
            }
            builder.append("\n");
        }
        print(builder.toString());
    }

    private void appendNobilityReward(StringBuilder builder, NobilityReward nobilityReward) {
        appendFullReward(builder, nobilityReward);
        builder.append(String.format(", special reward 1: %d, ", nobilityReward.getFirstSpecialBonusCount()));
        builder.append(String.format("special reward 2: %d, ", nobilityReward.getSecondSpecialBonusCount()));
        builder.append(String.format("special reward 3: %d", nobilityReward.getThirdSpecialBonusCount()));
    }

    private void showBusinessPermitTiles() {
        StringBuilder builder = new StringBuilder();
        builder.append("Business permit tiles:\n");
        for (int i = 0; i < mBusinessPermitTiles.size(); i++) {
            builder.append(i + 1);
            builder.append(") [");
            appendBusinessPermitTile(builder, mBusinessPermitTiles.get(i));
            builder.append("]\n");
        }
        print(builder.toString());
    }

    private void appendBusinessPermitTile(StringBuilder builder, BusinessPermitTile businessPermitTile) {
        builder.append("cities: ");
        for (char initial : businessPermitTile.getCities()) {
            builder.append(initial);
            builder.append(", ");
        }
        appendFullReward(builder, businessPermitTile);
    }

    private void showWaitingTime() {
        print("Current waiting time (seconds): %d", mWaitingTime);
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void addCityReward(String[] arguments) throws CommandNotValid {
        if (arguments.length == 7) {
            City city = parseCity(arguments[0]);
            int assistants = parseNumber(arguments[1], INVALID_ASSISTANTS);
            int victoryPoints = parseNumber(arguments[2], INVALID_VICTORY_POINTS);
            int nobilitySteps = parseNumber(arguments[3], INVALID_NOBILITY_STEPS);
            int coins = parseNumber(arguments[4], INVALID_COINS);
            int politicCards = parseNumber(arguments[5], INVALID_POLITIC_CARDS);
            int mainActions = parseNumber(arguments[6], INVALID_MAIN_ACTIONS);
            city.addReward(
                    assistants,
                    victoryPoints,
                    nobilitySteps,
                    coins,
                    politicCards,
                    mainActions
            );
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void removeCityReward(String[] arguments) throws CommandNotValid {
        if (arguments.length == 7) {
            City city = parseCity(arguments[0]);
            int assistants = parseNumber(arguments[1], INVALID_ASSISTANTS);
            int victoryPoints = parseNumber(arguments[2], INVALID_VICTORY_POINTS);
            int nobilitySteps = parseNumber(arguments[3], INVALID_NOBILITY_STEPS);
            int coins = parseNumber(arguments[4], INVALID_COINS);
            int politicCards = parseNumber(arguments[5], INVALID_POLITIC_CARDS);
            int mainActions = parseNumber(arguments[6], INVALID_MAIN_ACTIONS);
            city.removeReward(
                    assistants,
                    victoryPoints,
                    nobilitySteps,
                    coins,
                    politicCards,
                    mainActions
            );
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void addLink(String[] arguments) throws CommandNotValid {
        if (arguments.length == 2) {
            validateCity(arguments[0]);
            validateCity(arguments[1]);
            addLink(arguments[0], arguments[1]);
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void removeLink(String[] arguments) throws CommandNotValid {
        if (arguments.length == 2) {
            validateCity(arguments[0]);
            validateCity(arguments[1]);
            removeLink(arguments[0], arguments[1]);
        } else {
            throw new CommandNotValid();
        }
    }

    private void addLink(String from, String to) throws CommandNotValid {
        RegionBoard regionBoard = getRegionAndValidateLink(from, to, "add");
        regionBoard.getLinks().add(new Link(from, to));
        loadCurrentMap();
    }

    private void removeLink(String from, String to) throws CommandNotValid {
        RegionBoard regionBoard = getRegionAndValidateLink(from, to, "remove");
        removeLink(regionBoard, from, to);
        loadCurrentMap();
    }

    private RegionBoard getRegionAndValidateLink(String from, String to, String action) throws CommandNotValid {
        RegionBoard regionBoard;
        if (!from.startsWith("#")) {
            regionBoard = getRegion(from);
            if (!to.startsWith("#") && !regionBoard.hasCity(to)) {
                throw new CommandNotValid("Cannot " + action + " link between two cities that do not belong to the same region");
            }
        } else if (!to.startsWith("#")) {
            regionBoard = getRegion(to);
            if (!from.startsWith("#") && !regionBoard.hasCity(from)) {
                throw new CommandNotValid("Cannot " + action + " link between two cities that do not belong to the same region");
            }
        } else {
            throw new CommandNotValid("Gates are never linked together");
        }
        return regionBoard;
    }

    private void removeLink(RegionBoard regionBoard, String from, String to) {
        List<Link> links = regionBoard.getLinks();
        for (Iterator<Link> iterator = links.iterator(); iterator.hasNext();) {
            Link link = iterator.next();
            if (link.getCityFrom().equals(from) && link.getCityTo().equals(to) || link.getCityFrom().equals(to) && link.getCityTo().equals(from)) {
                // Remove the current element from the iterator and the list. (safe)
                iterator.remove();
            }
        }
    }

    private RegionBoard getRegion(String city) throws CommandNotValid {
        if (mLeftRegionBoard.hasCity(city)) {
            return mLeftRegionBoard;
        }
        if (mCenterRegionBoard.hasCity(city)) {
            return mCenterRegionBoard;
        }
        if (mRightRegionBoard.hasCity(city)) {
            return mRightRegionBoard;
        }
        throw new CommandNotValid("Cannot link cities that belongs to different regions");
    }

    private City parseCity(String name) throws CommandNotValid {
        City city = mCurrentMap.getValue(name);
        if (city == null) {
            throw new CommandNotValid("Unknown city");
        }
        return city;
    }

    private void validateCity(String name) throws CommandNotValid {
        if (!name.startsWith("#")) {
            parseCity(name);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void addNobilityReward(String[] arguments) throws CommandNotValid {
        if (arguments.length == 9) {
            int position = parseNumber(arguments[0], "Invalid position");
            if (position < 0 || position > 20) {
                throw new CommandNotValid("Position must be 0 <= x <= 20");
            }
            NobilityReward nobilityReward = getAndRemoveNobilityReward(position);
            nobilityReward.addAssistants(parseNumber(arguments[1], INVALID_ASSISTANTS));
            nobilityReward.addVictoryPointCount(parseNumber(arguments[2], INVALID_VICTORY_POINTS));
            nobilityReward.addCoins(parseNumber(arguments[3], INVALID_COINS));
            nobilityReward.addPoliticCards(parseNumber(arguments[4], INVALID_POLITIC_CARDS));
            nobilityReward.addMainActions(parseNumber(arguments[5], INVALID_MAIN_ACTIONS));
            nobilityReward.addFirstSpecialBonus(parseNumber(arguments[6], "Invalid special reward 1 number"));
            nobilityReward.addSecondSpecialBonus(parseNumber(arguments[7], "Invalid special reward 2 number"));
            nobilityReward.addThirdSpecialBonus(parseNumber(arguments[8], "Invalid special reward 3 number"));
            if (nobilityReward.isValid()) {
                mNobilityTrack.put(position, nobilityReward);
            }
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void removeNobilityReward(String[] arguments) throws CommandNotValid {
        if (arguments.length == 9) {
            int position = parseNumber(arguments[0], "Invalid position");
            if (position < 0 || position > 20) {
                throw new CommandNotValid("Position must be 0 <= x <= 20");
            }
            NobilityReward nobilityReward = getAndRemoveNobilityReward(position);
            nobilityReward.removeAssistants(parseNumber(arguments[1], INVALID_ASSISTANTS));
            nobilityReward.removeVictoryPointCount(parseNumber(arguments[2], INVALID_VICTORY_POINTS));
            nobilityReward.removeCoins(parseNumber(arguments[3], INVALID_COINS));
            nobilityReward.removePoliticCards(parseNumber(arguments[4], INVALID_POLITIC_CARDS));
            nobilityReward.removeMainActions(parseNumber(arguments[5], INVALID_MAIN_ACTIONS));
            nobilityReward.removeFirstSpecialBonus(parseNumber(arguments[6], "Invalid special reward 1 number"));
            nobilityReward.removeSecondSpecialBonus(parseNumber(arguments[7], "Invalid special reward 2 number"));
            nobilityReward.removeThirdSpecialBonus(parseNumber(arguments[8], "Invalid special reward 3 number"));
            if (nobilityReward.isValid()) {
                mNobilityTrack.put(position, nobilityReward);
            }
        } else {
            throw new CommandNotValid();
        }
    }

    private NobilityReward getAndRemoveNobilityReward(int position) {
        if (mNobilityTrack.containsKey(position)) {
            NobilityReward nobilityReward = mNobilityTrack.get(position);
            mNobilityTrack.remove(position);
            return nobilityReward;
        }
        return new NobilityReward();
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void addBusinessPermitTile(String[] arguments) throws CommandNotValid {
        if (arguments.length == 7) {
            mBusinessPermitTiles.add(new BusinessPermitTile(
                    parseNumber(arguments[1], INVALID_ASSISTANTS),
                    parseNumber(arguments[2], INVALID_VICTORY_POINTS),
                    parseNumber(arguments[3], INVALID_NOBILITY_STEPS),
                    parseNumber(arguments[4], INVALID_COINS),
                    parseNumber(arguments[5], INVALID_POLITIC_CARDS),
                    parseNumber(arguments[6], INVALID_MAIN_ACTIONS),
                    arguments[0].toCharArray()
            ));
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void removeBusinessPermitTile(String[] arguments) throws CommandNotValid {
        if (arguments.length == 1) {
            int index = parseIndex(arguments[0]);
            if (index > 0 && index <= mBusinessPermitTiles.size()) {
                mBusinessPermitTiles.remove(index - 1);
            } else {
                throw new CommandNotValid("Index not valid");
            }
        } else {
            throw new CommandNotValid();
        }
    }

    private void applyConfiguration() throws CommandNotValid {
        Configuration configuration = new Configuration(mWaitingTime, mLeftRegionBoard, mCenterRegionBoard, mRightRegionBoard, mNobilityTrack, mBusinessPermitTiles);
        mCallback.setConfiguration(configuration);
    }

    /**
     * Interface used as callback.
     */
    @FunctionalInterface
    public interface Callback {

        /**
         * Apply the configuration to the room.
         * @param configuration to apply.
         */
        void setConfiguration(Configuration configuration);
    }
}
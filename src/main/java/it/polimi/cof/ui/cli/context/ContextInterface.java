package it.polimi.cof.ui.cli.context;

/**
 * This interface is the base callback of every CLI Context.
 */
@FunctionalInterface
/*package-local*/ interface ContextInterface {

    /**
     * Print something on the display.
     * @param message to print.
     */
    void print(String message);
}
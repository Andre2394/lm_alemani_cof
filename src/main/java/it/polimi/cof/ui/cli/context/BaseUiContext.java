package it.polimi.cof.ui.cli.context;

import it.polimi.cof.util.Debug;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This class represent the base context of the cli.
 */
public class BaseUiContext {

    /**
     * Callback used to communicate with the cli.
     */
    private final ContextInterface mCallback;

    /**
     * Map of all possible commands that the context can handle.
     */
    private final Map<String, Command> mCommands;

    /**
     * Map of all help text to show.
     */
    private final Map<String, String> mHelps;

    /**
     * Base constructor.
     * @param contextInterface used as callback.
     */
    /*package-local*/ BaseUiContext(ContextInterface contextInterface) {
        mCallback = contextInterface;
        mCommands = new HashMap<>();
        mHelps = new LinkedHashMap<>();
        mCommands.put("help", line -> mCallback.print(getHelpMenu()));
    }

    /**
     * Add a command to the context.
     * @param key of the command.
     * @param helpLine help text to show.
     * @param command handler to execute when command is triggered.
     */
    /*package-local*/ void addCommand(String key, String helpLine, Command command) {
        mCommands.put(key, command);
        mHelps.put(key, helpLine);
    }

    /**
     * Handle an incoming command.
     * @param line from the keyboard.
     * @throws UnknownCommandException if the command has not been found.
     */
    public void handle(String line) throws UnknownCommandException {
        String[] parts = line.split(" ");
        if (parts.length > 0 && mCommands.containsKey(parts[0])) {
            try {
                mCommands.get(parts[0]).execute(Arrays.copyOfRange(parts, 1, parts.length));
            } catch (CommandNotValid e) {
                Debug.debug("Command not valid", e);
                mCallback.print(String.format("Command not valid, it must be: %s", mHelps.get(parts[0])));
            }
        } else {
            throw new UnknownCommandException();
        }
    }

    /**
     * Generate an help menu to show when 'help' is typed by the user.
     * @return the help menu to show.
     */
    private String getHelpMenu() {
        StringBuilder builder = new StringBuilder();
        builder.append("Command list:");
        for (Map.Entry<String, String> entry : mHelps.entrySet()) {
            builder.append("\n");
            builder.append(entry.getKey());
            builder.append(" ");
            builder.append(entry.getValue());
        }
        return builder.toString();
    }

    /**
     * Parse an index from a string.
     * @param text to parse.
     * @return the integer parsed.
     * @throws CommandNotValid if the text is not a valid number.
     */
    /*package-local*/ int parseIndex(String text) throws CommandNotValid {
        return parseNumber(text, "Not a valid index");
    }

    /**
     * Parse a number from a string.
     * @param text to parse.
     * @param error message of the error if the string is not a valid number.
     * @return the integer parsed.
     * @throws CommandNotValid if the text is not a valid number.
     */
    /*package-local*/ int parseNumber(String text, String error) throws CommandNotValid {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            Debug.debug(error, e);
            throw new CommandNotValid(error);
        }
    }

    /**
     * Print something on the Display driver.
     * @param message to print.
     */
    /*package-local*/ void print(String message) {
        mCallback.print(message);
    }

    /**
     * Print something on the Display driver.
     * @param message to print.
     * @param args to bind in the string.
     */
    /*package-local*/ void print(String message, Object... args) {
        mCallback.print(String.format(message, args));
    }
}
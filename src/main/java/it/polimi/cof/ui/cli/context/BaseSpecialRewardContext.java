package it.polimi.cof.ui.cli.context;

/**
 * This class represent the base context for the special rewards.
 */
/*package-local*/ abstract class BaseSpecialRewardContext extends BaseGameContext {

    private final int mCount;

    /*package-local*/ BaseSpecialRewardContext(Callback callback, int count) {
        super(callback);
        mCount = count;
        if (count <= 1) {
            showSingleRewardTutorial();
        } else {
            showMultiRewardTutorial(count);
        }
    }

    protected String getHelpText(String help) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < mCount; i++) {
            if (builder.length() != 0) {
                builder.append(" ");
            }
            builder.append(help);
        }
        return builder.toString();
    }

    protected abstract void showSingleRewardTutorial();

    protected abstract void showMultiRewardTutorial(int count);

    protected int getCount() {
        return mCount;
    }
}

package it.polimi.cof.ui.cli.context;

import java.io.IOException;

/**
 * This exception is thrown if no command to execute is found.
 */
public class UnknownCommandException extends IOException {

    /**
     * Base constructor.
     */
    /*package-local*/ UnknownCommandException() {
        super();
    }
}

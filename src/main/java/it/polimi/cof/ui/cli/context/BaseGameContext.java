package it.polimi.cof.ui.cli.context;

/**
 * This class represent the base game context. It can handle all the base action during a match.
 */
public class BaseGameContext extends BaseUiContext {

    private static final String NICKNAME_HELP = "{nickname}";

    private final Callback mCallback;

    /**
     * Base constructor.
     * @param callback to use.
     */
    public BaseGameContext(Callback callback) {
        super(callback);
        mCallback = callback;
        addCommand("chat", "[message]", this::sendRoomMessage);
        addCommand("chat-private", "[receiver] [message]", this::sendPrivateMessage);
        addCommand("show-map", "", arguments -> mCallback.showMap());
        addCommand("show-nobility-track", "", arguments -> mCallback.showNobilityTrack());
        addCommand("show-city", "[city]", this::showCity);
        addCommand("show-councillors", "", arguments -> mCallback.showCouncillors());
        addCommand("show-players", "", arguments -> mCallback.showPlayers());
        addCommand("show-player", NICKNAME_HELP, this::showPlayer);
        addCommand("show-politic-cards", "", arguments -> mCallback.showPlayerPoliticCards());
        addCommand("show-usable-business-permit-tiles", NICKNAME_HELP, this::showPlayerUsableBusinessPermitTiles);
        addCommand("show-used-business-permit-tiles", NICKNAME_HELP, this::showPlayerUsedBusinessPermitTiles);
        addCommand("show-reward-king-cards", NICKNAME_HELP, this::showRewardKingCards);
        addCommand("show-bonus-cards", NICKNAME_HELP, this::showBonusCards);
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sendRoomMessage(String[] arguments) throws CommandNotValid {
        if (arguments.length > 0) {
            mCallback.sendChatMessage(getTextFromArray(arguments, 0));
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sendPrivateMessage(String[] arguments) throws CommandNotValid {
        if (arguments.length > 1) {
            mCallback.sendPrivateChatMessage(arguments[0], getTextFromArray(arguments, 1));
        } else {
            throw new CommandNotValid();
        }
    }

    private String getTextFromArray(String[] arguments, int skip) {
        StringBuilder builder = new StringBuilder();
        for (int i = skip; i < arguments.length; i++) {
            if (builder.length() > 0) {
                builder.append(" ");
            }
            builder.append(arguments[i]);
        }
        return builder.toString();
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void showCity(String[] arguments) throws CommandNotValid {
        if (arguments.length > 0) {
            mCallback.showCity(getTextFromArray(arguments, 0));
        } else {
            throw new CommandNotValid();
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void showPlayer(String[] arguments) throws CommandNotValid {
        if (arguments.length > 0) {
            mCallback.showPlayer(getTextFromArray(arguments, 0));
        } else {
            mCallback.showPlayer(null);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void showPlayerUsableBusinessPermitTiles(String[] arguments) throws CommandNotValid {
        if (arguments.length > 0) {
            mCallback.showPlayerUsableBusinessPermitTiles(getTextFromArray(arguments, 0));
        } else {
            mCallback.showPlayerUsableBusinessPermitTiles(null);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void showPlayerUsedBusinessPermitTiles(String[] arguments) throws CommandNotValid {
        if (arguments.length > 0) {
            mCallback.showPlayerUsedBusinessPermitTiles(getTextFromArray(arguments, 0));
        } else {
            mCallback.showPlayerUsedBusinessPermitTiles(null);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void showRewardKingCards(String[] arguments) throws CommandNotValid {
        if (arguments.length > 0) {
            mCallback.showRewardKingCards(getTextFromArray(arguments, 0));
        } else {
            mCallback.showRewardKingCards(null);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void showBonusCards(String[] arguments) throws CommandNotValid {
        if (arguments.length > 0) {
            mCallback.showBonusCards(getTextFromArray(arguments, 0));
        } else {
            mCallback.showBonusCards(null);
        }
    }

    /**
     * This interface is built on top of (@link ContextInterface}
     */
    /*package-local*/ interface Callback extends ContextInterface {

        /**
         * Send a chat message.
         * @param message body to send.
         */
        void sendChatMessage(String message);

        /**
         * Send a private chat message.
         * @param receiver of the message.
         * @param message body to send.
         */
        void sendPrivateChatMessage(String receiver, String message);

        /**
         * Show the current map.
         */
        void showMap();

        /**
         * Show the nobility track.
         */
        void showNobilityTrack();

        /**
         * Show a given city.
         * @param name of the city.
         */
        void showCity(String name);

        /**
         * Show the councillors.
         */
        void showCouncillors();

        /**
         * Show the players.
         */
        void showPlayers();

        /**
         * Show a given player.
         * @param nickname of the player to show.
         */
        void showPlayer(String nickname);

        /**
         * Show current player politic cards.
         */
        void showPlayerPoliticCards();

        /**
         * Show the business permit tile of a given player.
         * @param nickname of the player to show.
         */
        void showPlayerUsableBusinessPermitTiles(String nickname);

        /**
         * Show the used business permit tile of a given player.
         * @param nickname of the player to show.
         */
        void showPlayerUsedBusinessPermitTiles(String nickname);

        /**
         * Show the reward king cards of a given player.
         * @param nickname of the player to show.
         */
        void showRewardKingCards(String nickname);

        /**
         * Show the bonus cards of a given player.
         * @param nickname of the player to show.
         */
        void showBonusCards(String nickname);
    }
}
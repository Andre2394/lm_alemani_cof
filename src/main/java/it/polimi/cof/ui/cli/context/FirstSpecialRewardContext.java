package it.polimi.cof.ui.cli.context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * This class represent the first special rewards context.
 */
public class FirstSpecialRewardContext extends BaseSpecialRewardContext {

    private final FirstSpecialRewardCallback mCallback;

    /**
     * Base constructor.
     * @param callback to use.
     * @param count of rewards.
     */
    public FirstSpecialRewardContext(FirstSpecialRewardCallback callback, int count) {
        super(callback, count);
        mCallback = callback;
        addCommand("take-city-rewards", getHelpText("[city]"), this::takeCityReward);
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void takeCityReward(String[] arguments) throws CommandNotValid {
        HashSet<String> cities = new HashSet<>(Arrays.asList(arguments));
        if (cities.size() == getCount()) {
            mCallback.earnFirstSpecialRewards(new ArrayList<>(cities));
        } else {
            throw new CommandNotValid();
        }
    }

    @Override
    protected void showSingleRewardTutorial() {
        print("You have obtained a special reward and you can now take a bonus from a city where you have already built " +
                "an emporium. Type 'take-city-rewards' and the city to get the city's reward");
    }

    @Override
    protected void showMultiRewardTutorial(int count) {
        print("You have obtained a special reward and you can now take %d bonus from different cities where you have " +
                "already built an emporium. Type 'take-city-rewards' and the cities to get all the cities rewards", count);
    }

    /**
     * Interface used as callback.
     */
    public interface FirstSpecialRewardCallback extends BaseGameContext.Callback {

        /**
         * Earn a first special rewards.
         * @param cities where the player want to get the reward.
         */
        void earnFirstSpecialRewards(List<String> cities);
    }
}
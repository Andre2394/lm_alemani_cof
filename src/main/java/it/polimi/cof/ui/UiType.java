package it.polimi.cof.ui;

/**
 * This enum represent all supported UiTypes.
 */
public enum UiType {

    /**
     * Command Line Interface (CLI).
     */
    CLI,

    /**
     * Graphical User Interface (GUI).
     */
    GUI
}
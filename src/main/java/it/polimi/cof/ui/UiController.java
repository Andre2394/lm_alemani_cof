package it.polimi.cof.ui;

import it.polimi.cof.model.*;
import it.polimi.cof.model.market.MarketSession;
import it.polimi.cof.network.NetworkType;
import it.polimi.cof.network.client.ClientConnectionException;

import java.util.List;

/**
 * This interface is used as callback by {@link AbstractUi UserInterface} for sending data back to the main class, and
 * as MVC pattern for updating the ui in real-time and being notified of every change to data.
 */
public interface UiController {

    /**
     * This method is triggered by {@link AbstractUi#showNetworkSettingMenu()}.
     * @param networkType desired network type.
     * @param address address of the server.
     * @param port valid port of the server.
     * @throws ClientConnectionException if client cannot establish a connection to the server.
     */
    void setNetworkSettings(NetworkType networkType, String address, int port) throws ClientConnectionException;

    /**
     * This method is triggered by {@link AbstractUi#showLoginMenu()}.
     * @param nickname to use for login session.
     */
    void loginPlayer(String nickname);

    /**
     * This method is triggered by {@link AbstractUi#showJoinRoomFailed()}.
     * @param maxPlayers is the limit to set internally the room.
     */
    void createRoom(int maxPlayers);

    /**
     * Retrieve the nickname of the current player.
     * @return the nickname of the current player.
     */
    String getNickname();

    /**
     * Check if the provided nickname belongs to the current player.
     * @param nickname to check.
     * @return true if matches.
     */
    boolean isCurrentPlayer(String nickname);

    /**
     * Check if the provided update state belongs to the current player.
     * @param updateState to check.
     * @return true if belongs to him.
     */
    boolean isCurrentPlayer(UpdateState updateState);

    /**
     * This method is triggered by {@link AbstractUi#showGameConfigurator(Configuration)}.
     * @param configuration bundle that contains all admin choices.
     */
    void onGameConfigured(Configuration configuration);

    /**
     * MVC pattern: this method will return to {@link AbstractUi} the current state of the model.
     * @return the updated model that represent the game session on the client.
     */
    BaseGame getGameModel();

    /**
     * Retrieve the current market session stored into internal bus.
     * @return the internal market session if the game is in a market session, null otherwise.
     */
    MarketSession getMarketSession();

    /**
     * Ui callback used to notify the bus to draw a politic card.
     */
    void drawPoliticCard();

    /**
     * Ui callback used to notify the bus to answer the server for the player action list.
     */
    void getActionList();

    /**
     * Ui callback used to end the player turn.
     */
    void endTurn();

    /**
     * Ui callback used to elect a councillor.
     * @param councilor to elect.
     * @param region where make the election.
     */
    void actionElectCouncillor(Councilor councilor, String region);

    /**
     * Ui callback used to acquire a business permit tile.
     * @param politicCards to use to satisfy the balcony.
     * @param region where satisfy the balcony.
     * @param permitTileIndex of the permit tile to take.
     */
    void actionAcquireBusinessPermitTile(List<PoliticCard> politicCards, String region, int permitTileIndex);

    /**
     * Ui callback used to build an emporium with a business permit tile.
     * @param businessPermitTile to use to build the emporium.
     * @param city where the emporium should be built.
     */
    void actionBuildEmporiumWithBusinessPermitTile(BusinessPermitTile businessPermitTile, String city);

    /**
     * Ui callback used to build an emporium with the help of the king.
     * @param politicCards to use to satisfy the king's balcony.
     * @param cities where the king should move.
     */
    void actionBuildEmporiumWithKingHelp(List<PoliticCard> politicCards, List<String> cities);

    /**
     * Ui callback used to engage an assistant.
     */
    void actionEngageAssistant();

    /**
     * Ui callback used to change the visible permit tiles of the given region.
     * @param region where make the substitution.
     */
    void actionChangeBusinessPermitTile(String region);

    /**
     * Ui callback used to send an assistant to elect a councillor.
     * @param councilor to elect.
     * @param region where the councillor should be elected.
     */
    void actionSendAssistantElectCouncillor(Councilor councilor, String region);

    /**
     * Ui callback used to answer for an additional main action.
     */
    void actionPerformAdditionalMainAction();

    /**
     * Ui callback used to sell a politic card on the market.
     * @param politicCard to sell.
     * @param price of the politic card.
     */
    void sellPoliticCard(PoliticCard politicCard, int price);

    /**
     * Ui callback used to sell a business permit tile on the market.
     * @param businessPermitTile to sell.
     * @param price of the business permit tile.
     */
    void sellBusinessPermitTile(BusinessPermitTile businessPermitTile, int price);

    /**
     * Ui callback used to sell an assistant on the market.
     * @param price of the assistant.
     */
    void sellAssistant(int price);

    /**
     * Ui callback used to buy an item on the market.
     * @param marketId id of the item to buy.
     */
    void buyItem(String marketId);

    /**
     * Ui callback used to earn a first special rewards.
     * @param cities where the player want to take the reward.
     */
    void earnFirstSpecialRewards(List<String> cities);

    /**
     * Ui callback used to earn a second special rewards.
     * @param regions where the player want to take a permit tile.
     * @param indices where the player want to take a permit tile.
     */
    void earnSecondSpecialRewards(List<String> regions, List<Integer> indices);

    /**
     * Ui callback used to earn a third special rewards.
     * @param businessPermitTiles list of business permit tile to use.
     */
    void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles);

    /**
     * Ui callback to send a chat message.
     * @param receiver nickname of the receiver if it is a private message, null otherwise.
     * @param message to send.
     */
    void sendChatMessage(String receiver, String message);
}
package it.polimi.cof.ui;

import it.polimi.cof.model.ActionList;
import it.polimi.cof.model.Configuration;
import it.polimi.cof.model.UpdateState;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.network.protocol.ErrorCodes;

import java.util.HashMap;
import java.util.List;

/**
 * This class represent the abstraction of a user interface. Extending this class you will be able to implements whatever
 * UserInterface you want, just following the documentation.
 */
public abstract class AbstractUi {

    /**
     * UiController used as callback for menus and user interactions, and as MVC for real-time ui update.
     */
    private final UiController mController;
    private final HashMap<Integer, String> mErrorCodes;

    /**
     * Abstract constructor. You must provide a valid UiController interface while extending this class.
     * @param controller interface used as callback and MVC.
     */
    public AbstractUi(UiController controller) {
        mController = controller;
        mErrorCodes = new HashMap<>();
        loadErrorMessages();
    }

    /**
     * Load all possible incoming error codes with the linked message to show to the user.
     */
    private void loadErrorMessages() {
        mErrorCodes.put(ErrorCodes.ERROR_NOT_PLAYER_TURN, "Not your turn!");
        mErrorCodes.put(ErrorCodes.ERROR_POLITIC_CARD_NOT_YET_DRAWN, "Draw a politic card before");
        mErrorCodes.put(ErrorCodes.ERROR_POLITIC_CARD_ALREADY_DRAWN, "You have already drawn the politic card");
        mErrorCodes.put(ErrorCodes.ERROR_MAIN_ACTION_NOT_AVAILABLE, "No more main action available");
        mErrorCodes.put(ErrorCodes.ERROR_NOT_ENOUGH_COINS, "No enough coins to make this action");
        mErrorCodes.put(ErrorCodes.ERROR_NO_POLITIC_CARD_CAN_SATISFY_REGION_BALCONY, "None of the provided politic cards can satisfy the selected balcony");
        mErrorCodes.put(ErrorCodes.ERROR_CHAT_PLAYER_NOT_FOUND, "Player not found");
        mErrorCodes.put(ErrorCodes.ERROR_CITY_NOT_FOUND, "City not found");
        mErrorCodes.put(ErrorCodes.ERROR_EMPORIUM_ALREADY_BUILT, "You have already built an emporium here");
        mErrorCodes.put(ErrorCodes.ERROR_NOT_ENOUGH_ASSISTANTS, "No enough assistants to make this action");
        mErrorCodes.put(ErrorCodes.ERROR_CITY_NOT_VALID, "City not valid");
        mErrorCodes.put(ErrorCodes.ERROR_FAST_ACTION_NOT_AVAILABLE, "No more fast action available");
        mErrorCodes.put(ErrorCodes.ERROR_ROUTE_NOT_VALID, "Route not valid");
        mErrorCodes.put(ErrorCodes.ERROR_MARKET_ITEM_ALREADY_ON_SALE, "This item is already on sale");
        mErrorCodes.put(ErrorCodes.ERROR_MARKET_ITEM_ALREADY_SOLD, "This item is already sold");
        mErrorCodes.put(ErrorCodes.ERROR_MAIN_ACTION_NOT_DONE, "You cannot end your turn if at least one main action is available");
        mErrorCodes.put(ErrorCodes.ERROR_GENERIC_SERVER_ERROR, "Generic server error");
    }

    /**
     * Get the error message associated to the given error code.
     * @param errorCode to look for.
     * @return the related error message.
     */
    protected String getErrorMessage(int errorCode) {
        return mErrorCodes.get(errorCode);
    }

    /**
     * Get the UiController passed by constructor.
     * @return the cached UiController interface.
     */
    protected UiController getController() {
        return mController;
    }

    /**
     * Called when ui should show to user a form where he can put network settings for client-server connection.
     * Remember to call the appropriate method on UiController when network setting are ready.
     */
    public abstract void showNetworkSettingMenu();

    /**
     * Called when ui should show the login menu to user.
     */
    public abstract void showLoginMenu();

    /**
     * Called when ui should show a login error.
     */
    public abstract void showLoginErrorMessage();

    /**
     * Called when ui should show a "join successful" message to user.
     */
    public abstract void showJoinRoomSuccess();

    /**
     * Called when ui should show a "join failed" message to user.
     */
    public abstract void showJoinRoomFailed();

    /**
     * Called when ui should show a "creating room successful" message to user.
     */
    public abstract void showCreatingRoomSuccess();

    /**
     * Called when ui should show a "creating room failed" message to user.
     */
    public abstract void showCreatingRoomFailed();

    /**
     * Called when player is the admin of the match and he has to configure the game.
     * @param configuration bundle that contains all configurable data.
     */
    public abstract void showGameConfigurator(Configuration configuration);

    /**
     *  Apply configuration chosen by the player
     */
    public abstract void showGameConfigurationDone();

    /** Called when player try to play with a fault configuration.
     * @param errorMessage of the configuration
     */
    public abstract void showInvalidGameConfiguration(String errorMessage);

    /**
     * Notify ui that game is started.
     */
    public abstract void notifyGameStarted();

    /**
     * Called when another player's turn is started.
     * @param nickname of the player that should start the turn.
     */
    public abstract void showGameTurnStarted(String nickname, int remainingTime);

    /**
     * Notify ui that the market session is started.
     */
    public abstract void showMarketSessionStarted();

    /**
     * Notify ui that a new market sell turn is started.
     * @param nickname of the player.
     * @param remainingTime remaining time.
     */
    public abstract void showMarketSellTurnStarted(String nickname, int remainingTime);

    /**
     * Notify ui that a new market buy turn is started.
     * @param nickname of the player.
     * @param remainingTime remaining time.
     */
    public abstract void showMarketBuyTurnStarted(String nickname, int remainingTime);

    /**
     * Notify ui that a new market item has been added.
     * @param item added.
     */
    public abstract void showMarketItemAddedOnSale(Item item);

    /**
     * Notify ui that a market item has been bought.
     * @param item bought.
     */
    public abstract void showMarketItemBought(Item item);

    /**
     * Notify ui that the market session is finished.
     */
    public abstract void showMarketSessionFinished();

    /**
     * Called when turn's timer should be updated with new remaining time.
     * @param remainingTime updated time value in seconds.
     */
    public abstract void updateTurnRemainingTime(int remainingTime);

    /**
     * Notify ui that a politic card has been drawn.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyPoliticCardDrawn(UpdateState updateState);

    /**
     * Notify ui that the action list is ready.
     * @param actionList from server.
     */
    public abstract void showActionList(ActionList actionList);

    /**
     * Notify ui that a councillor has been added.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyCouncilorAdded(UpdateState updateState);

    /**
     * Notify ui that a business permit tile has been acquired.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyBusinessPermitTileAcquired(UpdateState updateState);

    /**
     * Notify ui that an emporium has been built with a permit tile.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyEmporiumBuiltWithBusinessPermitTile(UpdateState updateState);

    /**
     * Notify ui that an emporium has been built with the help of the king.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyEmporiumBuiltWithKingHelp(UpdateState updateState);

    /**
     * Notify ui that an assistant has been engaged.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyAssistantEngaged(UpdateState updateState);

    /**
     * Notify ui that a the visible permit tiles of a region are changed.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyBusinessPermitTilesChanged(UpdateState updateState);

    /**
     * Notify ui that an assistant has been sent to elect a councillor.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyAssistantSentToElectCouncillor(UpdateState updateState);

    /**
     * Notify ui that a player has bought an additional main action.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyNewAdditionalMainActionAvailable(UpdateState updateState);

    /**
     * Notify ui that an action is not valid.
     * @param errorCode that represent the error.
     */
    public abstract void showActionNotValid(int errorCode);

    /**
     * Notify ui that a first special rewards is available.
     * @param count of the cities where the player can get the bonus.
     */
    public abstract void showFirstSpecialRewards(int count);

    /**
     * Notify ui that a second special rewards is available.
     * @param count of the permit tile that the user can take without paying nothing.
     */
    public abstract void showSecondSpecialRewards(int count);

    /**
     * Notify ui that a third special rewards is available.
     * @param count of the permit tiles where the player can get the bonus.
     */
    public abstract void showThirdSpecialRewards(int count);

    /**
     * Notify ui that a first special rewards has been done.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyFirstSpecialRewardsEarned(UpdateState updateState);

    /**
     * Notify ui that a second special rewards has been done.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifySecondSpecialRewardsEarned(UpdateState updateState);

    /**
     * Notify ui that a third special rewards has been done.
     * @param updateState from server that contains everything changed during the action.
     */
    public abstract void notifyThirdSpecialRewardsEarned(UpdateState updateState);

    /**
     * Notify ui that a new chat message is arrived.
     * @param privateMessage true if message is private, false if public.
     * @param author of the message.
     * @param message body of the message.
     */
    public abstract void showChatMessage(boolean privateMessage, String author, String message);

    /**
     * Notify ui that a player has disconnected.
     * @param nickname of the player that has disconnected.
     */
    public abstract void showPlayerDisconnected(String nickname);

    /**
     * Notify ui that the last turn is started.
     * @param nickname of the player that has built all his emporiums.
     */
    public abstract void showLastTurnStarted(String nickname);

    /**
     * Notify ui that the game is over.
     * @param updateStates list of final update states.
     * @param ranking list of players from the winner to the last loser.
     */
    public abstract void notifyGameEnded(UpdateState[] updateStates, List<String> ranking);
}
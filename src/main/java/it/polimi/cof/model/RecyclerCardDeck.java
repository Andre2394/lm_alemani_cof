package it.polimi.cof.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Extension of a {@link CardDeck}. It support a sort of recycler-bin where used cards can be temporary-stored and,
 * when the deck is empty, remixed end added back to the deck.
 */
public class RecyclerCardDeck<T extends Serializable> extends CardDeck<T> {

    /**
     * List of used cards. They will be randomly added back to the deck when it will be emptied.
     */
    private ArrayList<T> mUsedCards;

    /**
     * Create a new instance of an empty card deck.
     */
    public RecyclerCardDeck() {
        this(null);
    }

    /**
     * Create a new instance of a card deck with provided cards. The cards will be added with a random order.
     * @param cards to add to the deck.
     */
    private RecyclerCardDeck(List<T> cards) {
        this(cards, true);
    }

    /**
     * Create a new instance of a card deck with provided cards.
     * @param cards to add to the deck.
     * @param random true if cards should be added randomly, false if order should not be modified.
     */
    private RecyclerCardDeck(List<T> cards, boolean random) {
        super(cards, random);
        mUsedCards = new ArrayList<>();
    }

    /**
     * Retrieve the last card of the deck. This method will not remove this card from the deck.
     * If the deck is empty and recycler bin contains some cards, it will be remixed and cards will be added back
     * to the deck. In this case the recycler bin will be cleared.
     * @return the last card of the deck if it is not empty, Null if both deck and recycler bin are empty.
     */
    @Override
    protected T getLast() {
        T last = super.getLast();
        if (last == null && !mUsedCards.isEmpty()) {
            add(mUsedCards, true);
            mUsedCards.clear();
            return super.getLast();
        }
        return last;
    }

    /**
     * Add all provided cards to the recycler bin.
     * @param politicCards to recycle.
     */
    public void recycle(List<T> politicCards) {
        mUsedCards.addAll(politicCards);
    }
}
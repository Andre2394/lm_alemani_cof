package it.polimi.cof.model;

import java.io.Serializable;

/**
 * This class represent the politic card object of the game. It contains a string with the name of the color or "jolly"
 * if is a jolly card.
 */
public class PoliticCard implements Serializable {

    /**
     * Color of the card.
     */
    private final String mColor;

    /**
     * Create a new instance of a jolly card.
     */
    public PoliticCard() {
        mColor = "jolly";
    }

    /**
     * Create a new instance of a stock card.
     * @param color of the card.
     */
    public PoliticCard(String color) {
        mColor = color;
    }

    /**
     * Check if card is a jolly.
     * @return true if jolly, false if a stock card.
     */
    public boolean isJolly() {
        return "jolly".equals(mColor);
    }

    /**
     * Get the color of the card.
     * @return the color or "jolly".
     */
    public String getColor() {
        return mColor;
    }

    /**
     * Check if this politic cards equals the provided one.
     * @param object to check.
     * @return true if equals, false otherwise.
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if ((object == null) || (object.getClass() != this.getClass())) {
            return false;
        }
        PoliticCard politicCard = (PoliticCard) object;
        return mColor.equals(politicCard.getColor());
    }

    /**
     * Calculate an hashcode of this politic card.
     * @return the hashcode of the object.
     */
    @Override
    public int hashCode() {
        return mColor.hashCode();
    }
}
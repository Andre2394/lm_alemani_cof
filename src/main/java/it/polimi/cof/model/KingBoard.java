package it.polimi.cof.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the KingBoard of the map. This class is build on top of {@link #AbstractBoard AbstractBoard}.
 * It contains all the rewards king cards.
 */
public class KingBoard extends AbstractBoard implements Serializable {

    /**
     * Reward king cards.
     */
    private final CardDeck<Reward> mRewardKingCards;

    /**
     * Nobility track's rewards.
     */
    private final Map<Integer, NobilityReward> mNobilityTrackRewards;

    /**
     * Create a new instance of a king board.
     * @param rewardKingCards list of all rewards king cards of this region.
     */
    public KingBoard(List<Reward> rewardKingCards, Map<Integer, NobilityReward> nobilityTrackRewards) {
        mRewardKingCards = new CardDeck<>(rewardKingCards, false);
        mNobilityTrackRewards = new HashMap<>(nobilityTrackRewards);
    }

    /**
     * Get the first rewards king card of the deck.
     * @return the first reward king card if found (it will be removed from the deck), Null otherwise.
     */
    public Reward takeKingReward() {
        return mRewardKingCards.takeLast();
    }

    /**
     * Get the nobility track's rewards for position.
     * @param position where the player is placed.
     * @return the reward if something is found on that position, Null otherwise.
     */
    public NobilityReward getNobilityTrackReward(int position) {
        return mNobilityTrackRewards.get(position);
    }

    /**
     * Get the nobility track.
     * @return the map that represent the nobility track.
     */
    public Map<Integer, NobilityReward> getNobilityTrack() {
        return mNobilityTrackRewards;
    }
}
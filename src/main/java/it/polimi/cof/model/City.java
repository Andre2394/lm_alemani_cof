package it.polimi.cof.model;

import it.polimi.cof.model.exceptions.EmporiumAlreadyBuilt;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * This class represent the city of a RegionBoard. It will contains basic information such as the name, the color,
 * the locations on the map, the reward, if it is hosting the king and all links to the others cities.
 */
public class City implements Serializable {

    /**
     * JSON defines used for serialization / deserialization.
     */
    private static final String JSON_NAME = "name";
    private static final String JSON_TYPE = "type";
    private static final String JSON_POSITION_X = "positionX";
    private static final String JSON_POSITION_Y = "positionY";
    private static final String JSON_REWARD = "reward";

    /**
     * Card types.
     */
    public static final String TYPE_KING = "king";

    /**
     * Name of the city.
     */
    private String mName;

    /**
     * Type of the city.
     */
    private String mType;

    /**
     * Position X of the city.
     */
    private int mPositionX;

    /**
     * Position Y of the city.
     */
    private int mPositionY;

    /**
     * Reward of the city.
     */
    private Reward mReward;

    /**
     * Boolean representation of the position of the king. If true this city is hosting the king.
     */
    private boolean mKing;

    /**
     * Set of all emporiums built on this city.
     */
    private HashSet<String> mEmporiums;

    /**
     * Create a new instance of a city.
     * @param name of the city.
     * @param type of the city.
     * @param reward if the city will contains a reward. If no reward is provided, this city is hosting the king
     *               at game startup. [see game-logic].
     */
    public City(String name, String type, int positionX, int positionY, Reward reward) {
        mName = name;
        mType = type;
        mPositionX = positionX;
        mPositionY = positionY;
        mReward = reward;
        mKing = false;
        mEmporiums = new HashSet<>();
    }

    /**
     * Create a new instance from a JSON object.
     * @param jsonObject json object to deserialize.
     */
    /*package-local*/ City(JSONObject jsonObject) {
        mName = jsonObject.getString(JSON_NAME);
        mType = jsonObject.getString(JSON_TYPE);
        mPositionX = jsonObject.getInt(JSON_POSITION_X);
        mPositionY = jsonObject.getInt(JSON_POSITION_Y);
        if (jsonObject.has(JSON_REWARD)) {
            mReward = new Reward(jsonObject.getJSONObject(JSON_REWARD));
        }
        mKing = TYPE_KING.equals(mType);
        mEmporiums = new HashSet<>();
    }

    /**
     * Build an emporium on this city. The provided emporium will be added to the city.
     * @param nickname player that is building the emporium.
     */
    public void addEmporium(String nickname) {
        mEmporiums.add(nickname);
    }

    /**
     * Add a reward to the city. If the final reward is not valid it will be removed.
     * @param assistants to add.
     * @param victoryPoints to add.
     * @param nobilitySteps to add.
     * @param coins to add.
     * @param politicCards to add.
     * @param mainActions to add.
     */
    public void addReward(int assistants, int victoryPoints, int nobilitySteps, int coins, int politicCards, int mainActions) {
        if (mReward == null) {
            mReward = new Reward();
        }
        mReward.addAssistants(assistants);
        mReward.addVictoryPointCount(victoryPoints);
        mReward.addNobilitySteps(nobilitySteps);
        mReward.addCoins(coins);
        mReward.addPoliticCards(politicCards);
        mReward.addMainActions(mainActions);
        if (!mReward.isValid()) {
            mReward = null;
        }
    }

    /**
     * Remove a reward from the city. If the final reward is not valid it will be removed.
     * @param assistants to remove.
     * @param victoryPoints to remove.
     * @param nobilitySteps to remove.
     * @param coins to remove.
     * @param politicCards to remove.
     * @param mainActions to remove.
     */
    public void removeReward(int assistants, int victoryPoints, int nobilitySteps, int coins, int politicCards, int mainActions) {
        if (mReward == null) {
            return;
        }
        mReward.removeAssistants(assistants);
        mReward.removeVictoryPointCount(victoryPoints);
        mReward.removeNobilitySteps(nobilitySteps);
        mReward.removeCoins(coins);
        mReward.removePoliticCards(politicCards);
        mReward.removeMainActions(mainActions);
        if (!mReward.isValid()) {
            mReward = null;
        }
    }

    /**
     * Build an emporium on this city from player.
     * @param player that is building the emporium.
     * @return the reward of the city.
     * @throws EmporiumAlreadyBuilt if player has already built an emporium on this city.
     */
    public Reward buildEmporium(Player player) throws EmporiumAlreadyBuilt {
        if (mEmporiums.contains(player.getNickname())) {
            throw new EmporiumAlreadyBuilt();
        }
        mEmporiums.add(player.getNickname());
        player.removeEmporiums(1);
        return mReward;
    }

    /**
     * Check if player has already built an emporium here.
     * @param player to check.
     * @return true if he has already built, false otherwise.
     */
    public boolean hasPlayerAnEmporium(Player player) {
        return mEmporiums.contains(player.getNickname());
    }

    /**
     * Try to build an emporium in this city without throwing an exception if it fails.
     * @param nickname of the player that is building the emporium.
     */
    /*package-local*/ void buildEmporiumSilently(String nickname) {
        mEmporiums.add(nickname);
    }

    /**
     * Check how many players has already built an emporium on this city and return the number of assistants necessary
     * to build an emporium here.
     * @param nickname of the player that is building the emporium.
     * @return the number of assistants necessary.
     * @throws EmporiumAlreadyBuilt if the provided player has already built an emporium here.
     */
    public int getAssistantCountToBuildEmporium(String nickname) throws EmporiumAlreadyBuilt {
        if (!mEmporiums.contains(nickname)) {
            return mEmporiums.size();
        }
        throw new EmporiumAlreadyBuilt();
    }

    /**
     * Check if the city has one of the provided initials (not case sensitive).
     * @param initials list of initials to match.
     * @return true if one of the following initials matches, false otherwise.
     */
    public boolean hasInitial(char[] initials) {
        char initial = mName.charAt(0);
        for (char character : initials) {
            if (Character.toLowerCase(initial) == Character.toLowerCase(character)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the set of all emporiums.
     * @return all emporiums built on this city.
     */
    public Set<String> getEmporiums() {
        return mEmporiums;
    }

    /**
     * Get the name of the city.
     * @return the name of the city.
     */
    public String getName() {
        return mName;
    }

    /**
     * Get the type of the city.
     * @return the type of the city.
     */
    public String getType() {
        return mType;
    }

    /**
     * Get position of city on X axis.
     * @return the x position.
     */
    public int getPositionX() {
        return mPositionX;
    }

    /**
     * Get position of city on Y axis.
     * @return the y position.
     */
    public int getPositionY() {
        return mPositionY;
    }

    /**
     * Set reward of the city.
     * @param reward of the city.
     */
    public void setReward(Reward reward) {
        mReward = reward;
    }

    /**
     * Get the reward of this city.
     * @return the reward of the city if a reward is provided during the creation, Null otherwise.
     */
    public Reward getReward() {
        return mReward;
    }

    /**
     * Check if city is currently hosting the king.
     * @return true if city is hosting the king, false otherwise.
     */
    public boolean hasKing() {
        return mKing;
    }

    /**
     * Set if city is hosting the king.
     * @param king true if city is hosting the king, false if not.
     */
    public void setKing(boolean king) {
        mKing = king;
    }
}
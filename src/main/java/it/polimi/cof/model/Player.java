package it.polimi.cof.model;

import it.polimi.cof.model.exceptions.NotEnoughAssistants;
import it.polimi.cof.model.exceptions.NotEnoughCoins;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represent the single player of the game. It contains all info about his state.
 */
public class Player implements Serializable {

    /**
     * Nickname of the player.
     */
    private String mNickname;

    /**
     * Color of the player.
     */
    private Color mColor;

    /**
     * Victory point indicator.
     */
    private int mVictoryPoints;

    /**
     * Position on nobility track.
     */
    private int mNobilityTrackPosition;

    /**
     * Coins indicator.
     */
    private int mCoins;

    /**
     * Count of the assistants owned.
     */
    private int mAssistants;

    /**
     * Count of the emporiums.
     */
    private int mEmporiums;

    /**
     * List of player's politic cards.
     */
    private ArrayList<PoliticCard> mPoliticCards;

    /**
     * List of player's business permit tiles.
     */
    private ArrayList<BusinessPermitTile> mBusinessPermitTiles;

    /**
     * List of used player's business permit tiles.
     */
    private ArrayList<BusinessPermitTile> mUsedBusinessPermitTiles;

    /**
     * List of all earned king rewards.
     */
    private ArrayList<Reward> mKingRewards;

    /**
     * Map that contains all earned bonus.
     */
    private Map<String, Reward> mBonus;

    /**
     * Flag that indicates if player is online.
     */
    private boolean mOnline;

    /**
     * Protected constructor.
     */
    protected Player() {
        mPoliticCards = new ArrayList<>();
        mBusinessPermitTiles = new ArrayList<>();
        mUsedBusinessPermitTiles = new ArrayList<>();
        mKingRewards = new ArrayList<>();
        mBonus = new HashMap<>();
        mEmporiums = 0;
        mOnline = true;
    }

    /**
     * Set the nickname of the player. This is used as uniqueId in game to identify the player.
     * @param nickname to set to the player.
     */
    public void setNickname(String nickname) {
        mNickname = nickname;
    }

    /**
     * Get the nickname of the player.
     * @return the nickname of the player.
     */
    public String getNickname() {
        return mNickname;
    }

    /**
     * Set the color of the player.
     * @param color to set to the player.
     */
    public void setColor(Color color) {
        mColor = color;
    }

    /**
     * Get the color of the player.
     * @return the color of the player.
     */
    public Color getColor() {
        return mColor;
    }

    /**
     * Add some victory points to internal indicator.
     */
    public void addVictoryPoints(int points) {
        mVictoryPoints += points;
    }

    /**
     * Get the victory's point count.
     * @return the victory's point count.
     */
    public int getVictoryPoints() {
        return mVictoryPoints;
    }

    /**
     * Move nobility track position.
     * @param count number of movements.
     */
    public void moveNobilityTrack(int count) {
        mNobilityTrackPosition += count;
    }

    /**
     * Get the position on nobility track.
     * @return the nobility track position.
     */
    public int getNobilityTrackPosition() {
        return mNobilityTrackPosition;
    }

    /**
     * Add some coins.
     * @param coins number of coins to add.
     */
    public void addCoins(int coins) {
        mCoins += coins;
    }

    /**
     * Get coins count.
     * @return the count of coins.
     */
    public int getCoins() {
        return mCoins;
    }

    /**
     * Try to pay coins.
     * @param count number of coins to pay.
     * @throws NotEnoughCoins if player has no enough coins.
     */
    public void payCoins(int count) throws NotEnoughCoins {
        if (mCoins >= count) {
            mCoins -= count;
        } else {
            throw new NotEnoughCoins();
        }
    }

    /**
     * Add assistants.
     * @param assistants number of assistants to add.
     */
    public void addAssistants(int assistants) {
        mAssistants += assistants;
    }

    /**
     * Try to pay assistants.
     * @param count number of assistants to pay.
     * @throws NotEnoughAssistants if player has no enough assistants.
     */
    public void payAssistants(int count) throws NotEnoughAssistants {
        if (hasEnoughAssistants(count)) {
            mAssistants -= count;
        } else {
            throw new NotEnoughAssistants();
        }
    }

    /**
     * Remove assistants from player.
     * @param count number of assistants to remove.
     */
    public void removeAssistants(int count) {
        mAssistants -= count;
    }

    /**
     * Check if player has enough assistant to pay.
     * @param count number of assistants to check.
     * @return true if player can pay, false otherwise.
     */
    public boolean hasEnoughAssistants(int count) {
        return mAssistants >= count;
    }

    /**
     * Use all provided business permit tiles.
     * @param businessPermitTiles list of permit tile to use.
     */
    public void useBusinessPermitTiles(List<BusinessPermitTile> businessPermitTiles) {
        businessPermitTiles.forEach(this::useBusinessPermitTile);
    }

    /**
     * Use the provided business permit tile.
     * @param businessPermitTile to use.
     */
    public void useBusinessPermitTile(BusinessPermitTile businessPermitTile) {
        mUsedBusinessPermitTiles.add(businessPermitTile);
        for (BusinessPermitTile permitTile : mBusinessPermitTiles) {
            if (permitTile.equals(businessPermitTile)) {
                mBusinessPermitTiles.remove(businessPermitTile);
                return;
            }
        }
    }

    /**
     * Add emporiums to player.
     * @param emporiums count of emporiums to add.
     */
    public void addEmporiums(int emporiums) {
        mEmporiums += emporiums;
    }

    /**
     * Remove an emporium from the list.
     * @param count number of emporiums to remove.
     */
    public void removeEmporiums(int count) {
        mEmporiums -= count;
    }

    /**
     * Get the number of available emporiums.
     * @return the number of available emporiums.
     */
    public int getEmporiums() {
        return mEmporiums;
    }

    /**
     * Get assistants count.
     * @return the assistants count.
     */
    public int getAssistants() {
        return mAssistants;
    }

    /**
     * Add politic card to player's deck.
     * @param politicCard card to add.
     */
    public void addPoliticCard(PoliticCard politicCard) {
        if (politicCard != null) {
            mPoliticCards.add(politicCard);
        }
    }

    /**
     * Add politic cards to the player deck.
     * @param politicCards list of politic cards to add.
     */
    public void addPoliticCards(List<PoliticCard> politicCards) {
        mPoliticCards.addAll(politicCards);
    }

    /**
     * Remove politic cards from the deck.
     * @param politicCard list of politic cards to remove.
     */
    public void removePoliticCard(PoliticCard politicCard) {
        if (politicCard != null) {
            mPoliticCards.remove(politicCard);
        }
    }

    /**
     * Discard only the first matching politic card of the list.
     * @param politicCards list of politic cards to discard.
     */
    public void discardPoliticCards(List<PoliticCard> politicCards) {
        for (PoliticCard politicCard : politicCards) {
            mPoliticCards.remove(politicCard);
        }
    }

    /**
     * Get the list of politic cards owned by the user.
     * @return the list of politic cards.
     */
    public List<PoliticCard> getPoliticCards() {
        return mPoliticCards;
    }

    /**
     * Add the business permit tile to the deck.
     * @param businessPermitTile to add to the deck.
     */
    public void addBusinessPermitTile(BusinessPermitTile businessPermitTile) {
        if (businessPermitTile != null) {
            mBusinessPermitTiles.add(businessPermitTile);
        }
    }

    /**
     * Add the business permit tiles to the deck.
     * @param businessPermitTiles to add to the deck.
     */
    public void addBusinessPermitTiles(List<BusinessPermitTile> businessPermitTiles) {
        mBusinessPermitTiles.addAll(businessPermitTiles);
    }

    /**
     * Get the list of all available business permit tiles.
     * @return the list of all available business permit tiles.
     */
    public List<BusinessPermitTile> getBusinessPermitTiles() {
        return mBusinessPermitTiles;
    }

    /**
     * Remove the business permit tile from the deck.
     * @param businessPermitTile to remove.
     */
    public void removeBusinessPermitTile(BusinessPermitTile businessPermitTile) {
        if (businessPermitTile != null) {
            mBusinessPermitTiles.remove(businessPermitTile);
        }
    }

    /**
     * Get the list of all used business permit tiles.
     * @return the list of all used business permit tiles.
     */
    public List<BusinessPermitTile> getUsedBusinessPermitTiles() {
        return mUsedBusinessPermitTiles;
    }

    /**
     * Add all provided not null king rewards to the list
     * @param rewards to add to the list.
     */
    public void addKingRewards(List<Reward> rewards) {
        rewards.stream().filter(reward -> reward != null).forEach(this::addKingReward);
    }

    /**
     * Add a king reward to the list if not null.
     * @param reward to add.
     */
    public void addKingReward(Reward reward) {
        if (reward != null) {
            mKingRewards.add(reward);
        }
    }

    /**
     * Get the list of all king rewards earned by the player.
     * @return the list of all king rewards earned by the player.
     */
    public List<Reward> getKingRewards() {
        return mKingRewards;
    }

    /**
     * Add all the rewards of the map into the internal map.
     * @param rewards to add.
     */
    public void addBonus(Map<String, Reward> rewards) {
        mBonus.putAll(rewards);
    }

    /**
     * Get the map of all bonus earned by the player.
     * @return the map of all bonus earned by the player.
     */
    public Map<String, Reward> getBonusCards() {
        return mBonus;
    }

    /**
     * Set the user online.
     * @param online status of the player.
     */
    public void setOnline(boolean online) {
        mOnline = online;
    }

    /**
     * Check the current player state.
     * @return true if player is online, false if offline.
     */
    public boolean isOnline() {
        return mOnline;
    }
}
package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when a city is not found.
 */
public class CityNotFound extends LogicException {

    /**
     * Base constructor.
     */
    public CityNotFound() {
        super();
    }
}
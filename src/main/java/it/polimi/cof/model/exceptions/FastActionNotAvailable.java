package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when someone is trying to make a fast action when he has no more fast actions available.
 */
public class FastActionNotAvailable extends LogicException {

    /**
     * Base constructor.
     */
    public FastActionNotAvailable() {
        super();
    }
}
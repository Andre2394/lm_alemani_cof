package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when someone is trying to build an emporium on a city where he has already built.
 */
public class EmporiumAlreadyBuilt extends LogicException {

    /**
     * Base constructor.
     */
    public EmporiumAlreadyBuilt() {
        super();
    }
}

package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when a player is not found.
 */
public class PlayerNotFound extends LogicException {

    /**
     * Base constructor.
     */
    public PlayerNotFound() {
        super();
    }
}

package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when a city is not found or is not expected.
 */
public class CityNotValid extends LogicException {

    /**
     * Base constructor.
     */
    public CityNotValid() {
        super();
    }
}

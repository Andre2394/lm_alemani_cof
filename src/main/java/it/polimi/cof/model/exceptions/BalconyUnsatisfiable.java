package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when a balcony is not satisfiable with the provided politic cards.
 */
public class BalconyUnsatisfiable extends LogicException {

    /**
     * Base constructor.
     */
    public BalconyUnsatisfiable() {
        super();
    }
}

package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when the provided city route is not valid due to unknown cities or cities that are not
 * directly linked.
 */
public class RouteNotValid extends LogicException {

    /**
     * Base constructor.
     */
    public RouteNotValid() {
        super();
    }

    /**
     * Base constructor.
     * @param message of the exception
     */
    public RouteNotValid(String message) {
        super(message);
    }

    /**
     * Base constructor.
     * @param cause of the exception
     */
    public RouteNotValid(Throwable cause) {
        super(cause);
    }
}

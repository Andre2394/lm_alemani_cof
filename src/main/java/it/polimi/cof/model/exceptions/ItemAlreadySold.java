package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when someone is trying to buy an item that has been already bought.
 */
public class ItemAlreadySold extends LogicException {

    /**
     * Base constructor.
     */
    public ItemAlreadySold() {
        super();
    }
}

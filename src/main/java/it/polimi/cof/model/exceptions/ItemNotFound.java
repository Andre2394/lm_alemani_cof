package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when a market item is not found.
 */
public class ItemNotFound extends LogicException {

    /**
     * Base constructor.
     */
    public ItemNotFound() {
        super();
    }
}

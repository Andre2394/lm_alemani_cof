package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when someone is trying to make an action but he has no enough assistants.
 */
public class NotEnoughAssistants extends LogicException {

    /**
     * Base constructor.
     */
    public NotEnoughAssistants() {
        super();
    }
}

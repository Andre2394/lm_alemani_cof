package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when someone is trying to make a main action but he has no more main actions available.
 */
public class MainActionNotAvailable extends LogicException {

    /**
     * Base constructor.
     */
    public MainActionNotAvailable() {
        super();
    }
}

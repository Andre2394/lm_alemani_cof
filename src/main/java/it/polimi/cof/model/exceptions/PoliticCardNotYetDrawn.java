package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when someone is trying to make a main/fast action but he has not drawn the politic card yet.
 */
public class PoliticCardNotYetDrawn extends LogicException {

    /**
     * Base constructor.
     */
    public PoliticCardNotYetDrawn() {
        super();
    }
}

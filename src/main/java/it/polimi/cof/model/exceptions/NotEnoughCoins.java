package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when someone is trying to make an action but he has no enough coins.
 */
public class NotEnoughCoins extends LogicException {

    /**
     * Base constructor.
     */
    public NotEnoughCoins() {
        super();
    }
}

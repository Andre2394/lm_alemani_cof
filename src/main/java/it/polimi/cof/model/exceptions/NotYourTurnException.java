package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when someone is trying to make an action but is not his turn.
 */
public class NotYourTurnException extends LogicException {

    /**
     * Base constructor.
     */
    public NotYourTurnException() {
        super();
    }
}
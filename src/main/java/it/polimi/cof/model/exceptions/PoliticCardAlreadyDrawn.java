package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when someone is trying to draw a politic card but he has already drawn one.
 */
public class PoliticCardAlreadyDrawn extends LogicException {

    /**
     * Base constructor.
     */
    public PoliticCardAlreadyDrawn() {
        super();
    }
}
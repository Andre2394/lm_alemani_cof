package it.polimi.cof.model.exceptions;

/**
 * This exception is thrown when someone is trying to put on sale an item that is already on sale.
 */
public class ItemAlreadyOnSale extends LogicException {

    /**
     * Base constructor.
     */
    public ItemAlreadyOnSale() {
        super();
    }
}

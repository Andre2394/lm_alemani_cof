package it.polimi.cof.model;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * This class represent a single reward. It contains the number of single objects as rewards.
 * For example: if {@link #mAssistants} = 3, {@link #mVictoryPoints} = 2, {@link #mPoliticCards} = 4, the reward
 * consists in 3 assistants, 2 victory points and 4 political cards.
 */
public class Reward implements Serializable {

    /**
     * JSONObject's constants
     */
    private static final String JSON_ASSISTANTS = "assistants";
    private static final String JSON_VICTORY_POINTS = "victoryPoints";
    private static final String JSON_NOBILITY_STEPS = "nobilitySteps";
    private static final String JSON_COINS = "coins";
    private static final String JSON_POLITIC_CARDS = "politicCards";
    private static final String JSON_MAIN_ACTIONS = "mainActions";

    /**
     * Number of assistants of the reward.
     */
    private int mAssistants;

    /**
     * Number of victory points of the reward.
     */
    private int mVictoryPoints;

    /**
     * Number of nobility steps of the reward.
     */
    private int mNobilitySteps;

    /**
     * Number of coins of the reward.
     */
    private int mCoins;

    /**
     * Number of political cards of the reward.
     */
    private int mPoliticCards;

    /**
     * Number of main actions of the reward.
     */
    private int mMainActions;

    /**
     * Create new instance of Reward.
     * @param assistants number of assistants.
     * @param victoryPoints number of victory points.
     * @param nobilitySteps number of nobility steps.
     * @param coins number of coins.
     * @param politicCards number of political cards.
     * @param mainActions number of main actions.
     */
    public Reward(int assistants, int victoryPoints, int nobilitySteps, int coins, int politicCards, int mainActions) {
        mAssistants = assistants;
        mVictoryPoints = victoryPoints;
        mNobilitySteps = nobilitySteps;
        mCoins = coins;
        mPoliticCards = politicCards;
        mMainActions = mainActions;
    }

    /**
     * Create new instance of Reward from JSONObject.
     * @param jsonObject to deserialize.
     */
    public Reward(JSONObject jsonObject) {
        mAssistants = jsonObject.optInt(JSON_ASSISTANTS, 0);
        mVictoryPoints = jsonObject.optInt(JSON_VICTORY_POINTS, 0);
        mNobilitySteps = jsonObject.optInt(JSON_NOBILITY_STEPS, 0);
        mCoins = jsonObject.optInt(JSON_COINS, 0);
        mPoliticCards = jsonObject.optInt(JSON_POLITIC_CARDS, 0);
        mMainActions = jsonObject.optInt(JSON_MAIN_ACTIONS, 0);
    }

    /**
     * Empty constructor used by java framework for serialization.
     */
    protected Reward() {
        // required for java serialization
    }

    /**
     * Add assistants to the reward.
     * @param assistants number of assistants to add.
     */
    public void addAssistants(int assistants) {
        mAssistants += assistants;
    }

    /**
     * Remove assistants from the reward.
     * @param assistants number of assistants to remove.
     */
    public void removeAssistants(int assistants) {
        mAssistants -= Math.min(mAssistants, assistants);
    }

    /**
     * Get number of rewards assistants.
     * @return the number of assistants.
     */
    public int getAssistantCount() {
        return mAssistants;
    }

    /**
     * Add victory points to the reward.
     * @param victoryPoints number of victory points to add.
     */
    public void addVictoryPointCount(int victoryPoints) {
        mVictoryPoints += victoryPoints;
    }

    /**
     * Remove victory points from the reward.
     * @param victoryPoints number of victory points to remove.
     */
    public void removeVictoryPointCount(int victoryPoints) {
        mVictoryPoints -= Math.min(mVictoryPoints, victoryPoints);
    }

    /**
     * Get number of rewards victory points.
     * @return the number of victory points.
     */
    public int getVictoryPointCount() {
        return mVictoryPoints;
    }

    /**
     * Add nobility steps to the reward.
     * @param nobilitySteps number of nobility steps to add.
     */
    public void addNobilitySteps(int nobilitySteps) {
        mNobilitySteps += nobilitySteps;
    }

    /**
     * Remove nobility steps from the reward.
     * @param nobilitySteps number of nobility steps to remove.
     */
    public void removeNobilitySteps(int nobilitySteps) {
        mNobilitySteps -= Math.min(mNobilitySteps, nobilitySteps);
    }

    /**
     * Get number of rewards nobility steps.
     * @return the number of nobility steps.
     */
    public int getNobilityStepCount() {
        return mNobilitySteps;
    }

    /**
     * Add coins to the reward.
     * @param coins number of coins to add.
     */
    public void addCoins(int coins) {
        mCoins += coins;
    }

    /**
     * Remove coins from the reward.
     * @param coins number of coins to remove.
     */
    public void removeCoins(int coins) {
        mCoins -= Math.min(mCoins, coins);
    }

    /**
     * Get number of rewards coins.
     * @return the number of coins.
     */
    public int getCoinsCount() {
        return mCoins;
    }

    /**
     * Add politic cards to the reward.
     * @param politicCards number of politic cards to add.
     */
    public void addPoliticCards(int politicCards) {
        mPoliticCards += politicCards;
    }

    /**
     * Remove politic cards from the reward.
     * @param politicCards number of politic cards to remove.
     */
    public void removePoliticCards(int politicCards) {
        mPoliticCards -= Math.min(mPoliticCards, politicCards);
    }

    /**
     * Get number of rewards political cards.
     * @return the number of political cards.
     */
    public int getPoliticCardCount() {
        return mPoliticCards;
    }

    /**
     * Add main actions as reward.
     * @param mainActions number of main actions to add.
     */
    public void addMainActions(int mainActions) {
        mMainActions += mainActions;
    }

    /**
     * Remove main actions from the reward.
     * @param mainActions number of main actions to remove.
     */
    public void removeMainActions(int mainActions) {
        mMainActions -= Math.min(mMainActions, mainActions);
    }

    /**
     * Check if the following reward is valid.
     * @return true if valid, false if not.
     */
    public boolean isValid() {
        return mAssistants + mVictoryPoints + mNobilitySteps + mCoins + mPoliticCards + mMainActions > 0;
    }

    /**
     * Get number of rewards main actions.
     * @return the number of main actions.
     */
    public int getMainActionCount() {
        return mMainActions;
    }

    /**
     * Generate a fingerprint string of the reward. This is useful to fast compare two rewards.
     * @return the reward fingerprint.
     */
    /*package-local*/ String generateFingerprint() {
        return String.valueOf(mAssistants) + String.valueOf(mVictoryPoints)
                + String.valueOf(mNobilitySteps) + String.valueOf(mCoins)
                + String.valueOf(mPoliticCards) + String.valueOf(mMainActions);
    }
}
package it.polimi.cof.model;

import it.polimi.cof.model.exceptions.BalconyUnsatisfiable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class that represent the base of every region.
 * It's main goal is to manage the balcony where councilors are inserted.
 */
public abstract class AbstractBoard implements Serializable {

    /**
     * Balcony of the region.
     */
    private Balcony mBalcony;

    public static final String LEFT_REGION = "left";
    public static final String CENTER_REGION = "center";
    public static final String RIGHT_REGION = "right";
    public static final String KING_REGION = "king";

    /**
     * Abstract constructor: required for every class that extends this one.
     */
    /*package-local*/ AbstractBoard() {
        mBalcony = new Balcony();
    }

    /**
     * Add new councilor to the balcony.
     * @param councilor to add.
     * @return the oldest councilor if the balcony is full, NULL otherwise.
     */
    public Councilor addCouncilor(Councilor councilor) {
        return mBalcony.addCouncilor(councilor);
    }

    /**
     * Fill balcony from list of councilors. Every councilor added to balcony will be removed from list.
     * @param councilors list of councilors from which to extract required councilors to fill the balcony.
     */
    public void fillBalcony(List<Councilor> councilors) {
        while (!mBalcony.isFull() && !councilors.isEmpty()) {
            Councilor councilor = councilors.get(0);
            mBalcony.addCouncilor(councilor);
            councilors.remove(0);
        }
    }

    /**
     * Satisfy balcony with provided politic cards.
     * @param politicCards given politic cards to satisfy this balcony.
     * @return a list with politic cards used to satisfy this balcony.
     * @throws BalconyUnsatisfiable if none of the given politic cards can satisfy this balcony.
     */
    public List<PoliticCard> satisfyBalcony(List<PoliticCard> politicCards) throws BalconyUnsatisfiable {
        List<String> councilors = mBalcony.getColors();
        ArrayList<PoliticCard> usedPoliticCards = new ArrayList<>();
        for (PoliticCard politicCard : politicCards) {
            if (!politicCard.isJolly() && canThisCardSatisfyCouncilor(politicCard, councilors)) {
                councilors.remove(politicCard.getColor());
                usedPoliticCards.add(politicCard);
            }
        }
        for (PoliticCard politicCard : politicCards) {
            if (councilors.isEmpty()) {
                break;
            }
            if (politicCard.isJolly()) {
                councilors.remove(0);
                usedPoliticCards.add(politicCard);
            }
        }
        if (!usedPoliticCards.isEmpty()) {
            return usedPoliticCards;
        }
        throw new BalconyUnsatisfiable();
    }

    /**
     * Check if a politic card can satisfy one of the provided councillors.
     * @param politicCard to use.
     * @param councilors list of councillors.
     * @return true if the card can be used, false otherwise.
     */
    private boolean canThisCardSatisfyCouncilor(PoliticCard politicCard, List<String> councilors) {
        for (String councilor : councilors) {
            if (politicCard.getColor().equals(councilor)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieve the color of the balcony councillors.
     * @return an array of string that represent the color of the councillors.
     */
    public String[] getBalconyColors() {
        String[] colors = new String[Balcony.BALCONY_SIZE];
        List<String> colorList = mBalcony.getColors();
        for (int i = 0; i < colorList.size(); i++) {
            colors[i] = colorList.get(i);
        }
        return colors;
    }
}
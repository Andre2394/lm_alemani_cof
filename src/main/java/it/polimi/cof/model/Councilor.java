package it.polimi.cof.model;

import java.io.Serializable;

/**
 * This class represent the councillor. This is just a wrapper class on top of the color.
 */
public class Councilor implements Serializable {

    /**
     * Color of the councilor. This is directly linked to owner player.
     */
    private final String mColor;

    /**
     * Create new councilor whit provided color.
     * @param color of the councilor.
     */
    public Councilor(String color) {
        mColor = color;
    }

    /**
     * Get the councilor color.
     * @return the councilor color.
     */
    public String getColor() {
        return mColor;
    }

    /**
     * Check if this councillor equals another object.
     * @param object to check.
     * @return true if two object are equals, false otherwise.
     */
    @Override
    public boolean equals(Object object) {
        if(this == object) {
            return true;
        }
        if((object == null) || (object.getClass() != this.getClass())) {
            return false;
        }
        Councilor councilor = (Councilor) object;
        return mColor.equals(councilor.getColor());
    }

    /**
     * Generate an hashcode of the class.
     * @return the generated hashcode according to JavaDoc.
     */
    @Override
    public int hashCode() {
        return mColor.hashCode();
    }
}
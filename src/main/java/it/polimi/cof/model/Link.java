package it.polimi.cof.model;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * This class represent the link between cities (or city and #gate).
 */
/*package-local*/ public class Link implements Serializable {

    /**
     * JSON defines used for serialization / deserialization.
     */
    private static final String JSON_LINK_FROM = "from";
    private static final String JSON_LINK_TO = "to";

    /**
     * Name or #gate where link starts.
     */
    private final String mCityFrom;

    /**
     * Name or #gate where link ends.
     */
    private final String mCityTo;

    /**
     * Create a new instance of a link.
     * @param cityFrom name or #gate where link starts.
     * @param cityTo name or #gate where link ends.
     */
    public Link(String cityFrom, String cityTo) {
        mCityFrom = cityFrom;
        mCityTo = cityTo;
    }

    /**
     * Create a new instance from a JSON object.
     * @param jsonObject json object to deserialize.
     */
    /*package-local*/ Link(JSONObject jsonObject) {
        mCityFrom = jsonObject.getString(JSON_LINK_FROM);
        mCityTo = jsonObject.getString(JSON_LINK_TO);
    }

    /**
     * Get name of the city or #gate where the link starts.
     * @return name / #gate where link starts.
     */
    public String getCityFrom() {
        return mCityFrom;
    }

    /**
     * Get name of the city or #gate where the link ends.
     * @return name / #gate where link ends.
     */
    public String getCityTo() {
        return mCityTo;
    }
}
package it.polimi.cof.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Implementations of a card deck. It contains all card of the deck. Only the last card can be retrieved.
 * When the last card is retrieved with {@link #takeLast() takeLast}, the card will be removed from the deck.
 */
public class CardDeck<T extends Serializable> implements Serializable, Iterable<T> {

    /**
     * List of the cards of the deck.
     */
    private ArrayList<T> mNewCards;

    /**
     * Create a new instance of an empty card deck.
     */
    /*package-local*/ CardDeck() {
        this(null);
    }

    /**
     * Create a new instance of a card deck with provided cards. The cards will be added with a random order.
     * @param cards to add to the deck.
     */
    /*package-local*/ CardDeck(List<T> cards) {
        this(cards, true);
    }

    /**
     * Create a new instance of a card deck with provided cards.
     * @param cards to add to the deck.
     * @param random true if cards should be added randomly, false if order should not be modified.
     */
    /*package-local*/ CardDeck(List<T> cards, boolean random) {
        mNewCards = new ArrayList<>();
        if (cards != null) {
            add(cards, random);
        }
    }

    /**
     * Retrieve the last card of the deck. This method will not remove this card from the deck.
     * If the deck is empty and recycler bin contains some cards, it will be remixed and cards will be added back
     * to the deck. In this case the recycler bin will be cleared.
     * @return the last card of the deck if it is not empty, Null if both deck and recycler bin are empty.
     */
    protected T getLast() {
        if (!mNewCards.isEmpty()) {
            return mNewCards.get(mNewCards.size() - 1);
        }
        return null;
    }

    /**
     * Take last card from the deck. This card will be removed from it. If the deck is empty and recycler bin contains
     * some cards, it will be remixed and cards will be added back to the deck.
     * In this case the recycler bin will be cleared.
     * @return the last card of the deck if it is not empty, Null if both deck and recycler bin are empty.
     */
    public T takeLast() {
        T last = getLast();
        if (last != null) {
            mNewCards.remove(mNewCards.size() - 1);
        }
        return last;
    }

    /**
     * Add card to the deck.
     * @param card to add to the deck.
     */
    public void add(T card) {
        mNewCards.add(card);
    }

    /**
     * Add card to the deck at bottom.
     * @param card to add to the deck.
     */
    public void addAtBottom(T card) {
        mNewCards.add(0, card);
    }

    /**
     * Add cards to the deck.
     * @param cards to add to the deck.
     * @param random true if the deck should be shuffled, false if the order should not be modified.
     */
    public void add(List<T> cards, boolean random) {
        mNewCards.addAll(cards);
        if (random) {
            Collections.shuffle(mNewCards);
        }
    }

    /**
     * Get an iterator of all cards in the deck.
     * @return the main list iterator.
     */
    @Override
    public Iterator<T> iterator() {
        return mNewCards.iterator();
    }
}
package it.polimi.cof.model;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * This class is built on top of {@link Reward} class because is an extension. The nobility track can have the generic
 * game's rewards and three special additional rewards.
 */
public class NobilityReward extends Reward implements Serializable {

    /**
     * JSON Defines.
     */
    private static final String JSON_FIRST_SPECIAL_BONUS = "firstSpecialBonus";
    private static final String JSON_SECOND_SPECIAL_BONUS = "secondSpecialBonus";
    private static final String JSON_THIRD_SPECIAL_BONUS = "thirdSpecialBonus";

    /**
     * First special rewards count.
     */
    private int mFirstSpecialBonus;

    /**
     * Second special rewards count.
     */
    private int mSecondSpecialBonus;

    /**
     * Third special rewards count.
     */
    private int mThirdSpecialBonus;

    /**
     * Constructor from jsonObject.
     * @param jsonObject to parse.
     */
    public NobilityReward(JSONObject jsonObject) {
        super(jsonObject);
        mFirstSpecialBonus = jsonObject.getInt(JSON_FIRST_SPECIAL_BONUS);
        mSecondSpecialBonus = jsonObject.getInt(JSON_SECOND_SPECIAL_BONUS);
        mThirdSpecialBonus = jsonObject.getInt(JSON_THIRD_SPECIAL_BONUS);
    }

    /**
     * Constructor required for java serialization and to build an empty nobility reward.
     */
    public NobilityReward() {
        // empty constructor
    }

    /**
     * Add first special rewards.
     * @param firstSpecialBonus number of first special reward to add.
     */
    public void addFirstSpecialBonus(int firstSpecialBonus) {
        mFirstSpecialBonus += firstSpecialBonus;
    }

    /**
     * Remove first special rewards.
     * @param firstSpecialBonus number of first special reward to remove.
     */
    public void removeFirstSpecialBonus(int firstSpecialBonus) {
        mFirstSpecialBonus -= Math.min(mFirstSpecialBonus, firstSpecialBonus);
    }

    /**
     * Get the number of first special rewards.
     * @return the number of first special rewards.
     */
    public int getFirstSpecialBonusCount() {
        return mFirstSpecialBonus;
    }

    /**
     * Add second special rewards.
     * @param secondSpecialBonus number of second special reward to add.
     */
    public void addSecondSpecialBonus(int secondSpecialBonus) {
        mSecondSpecialBonus += secondSpecialBonus;
    }

    /**
     * Remove second special rewards.
     * @param secondSpecialBonus number of second special reward to remove.
     */
    public void removeSecondSpecialBonus(int secondSpecialBonus) {
        mSecondSpecialBonus -= Math.min(mSecondSpecialBonus, secondSpecialBonus);
    }

    /**
     * Get the number of second special rewards.
     * @return the number of second special rewards.
     */
    public int getSecondSpecialBonusCount() {
        return mSecondSpecialBonus;
    }

    /**
     * Add third special rewards.
     * @param thirdSpecialBonus number of third special reward to add.
     */
    public void addThirdSpecialBonus(int thirdSpecialBonus) {
        mThirdSpecialBonus += thirdSpecialBonus;
    }

    /**
     * Remove third special rewards.
     * @param thirdSpecialBonus number of third special reward to remove.
     */
    public void removeThirdSpecialBonus(int thirdSpecialBonus) {
        mThirdSpecialBonus -= Math.min(mThirdSpecialBonus, thirdSpecialBonus);
    }

    /**
     * Get the number of third special rewards.
     * @return the number of third special rewards.
     */
    public int getThirdSpecialBonusCount() {
        return mThirdSpecialBonus;
    }

    /**
     * Check if this nobility reward is valid.
     * @return true if valid, false otherwise.
     */
    @Override
    public boolean isValid() {
        return super.isValid() || (mFirstSpecialBonus + mSecondSpecialBonus + mThirdSpecialBonus > 0);
    }
}
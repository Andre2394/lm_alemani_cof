package it.polimi.cof.model;

import it.polimi.cof.util.FifoList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represent the balcony of every region in game. It will contains the councilors.
 * It's main goal is to manage the councilors lifecycle. When the balcony is full and a new councilor is added
 * then the oldest one will be removed.
 */
/*package-local*/ class Balcony implements Serializable {

    /**
     * Max number of councilors in the balcony.
     */
    /*package-local*/ static final int BALCONY_SIZE = 4;

    /**
     * Fifo list to help with the councilors lifecycle management.
     */
    private FifoList<Councilor> mCouncilors;

    /**
     * Base constructor. Balcony will be empty until a councilor is not added.
     */
    /*package-local*/ Balcony() {
        mCouncilors = new FifoList<>(BALCONY_SIZE);
    }

    /**
     * Add new councilor to the balcony.
     * @param councilor to add.
     * @return the oldest councilor if the balcony is full, NULL otherwise.
     */
    /*package-local*/ Councilor addCouncilor(Councilor councilor) {
        return mCouncilors.add(councilor);
    }

    /**
     * Check is balcony is full.
     * @return true if balcony is full, false otherwise.
     */
    /*package-local*/ boolean isFull() {
        return mCouncilors.isFull();
    }

    /**
     * Get a list of the colors of the councillors.
     * @return the colors of the councillors.
     */
    /*package-local*/ List<String> getColors() {
        ArrayList<String> colors = new ArrayList<>();
        for (Councilor councilor : mCouncilors) {
            colors.add(councilor.getColor());
        }
        return colors;
    }
}
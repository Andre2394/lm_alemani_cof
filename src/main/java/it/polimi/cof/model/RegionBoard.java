package it.polimi.cof.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the RegionBoard of the map. This class is build on top of {@link #AbstractBoard AbstractBoard}.
 * It contains all the city of the region, all the business permit tiles and it will manage their state using
 * a card deck list. It also contains information between how cities are linked together.
 */
public class RegionBoard extends AbstractBoard implements Serializable {

    /**
     * Index of the first visible business permit tile.
     */
    public static final int INDEX_FIRST_BPT = 1;

    /**
     * Index of the second visible business permit tile.
     */
    public static final int INDEX_SECOND_BPT = 2;

    /**
     * JSON defines used for serialization / deserialization.
     */
    private static final String JSON_TYPE = "type";
    private static final String JSON_CITIES = "cities";
    private static final String JSON_LINKS = "links";

    /**
     * Type of the region board.
     */
    private final String mRegionType;

    /**
     * Map of the cities of the region.
     */
    private final HashMap<String, City> mCities;

    /**
     * Map of all links between cities.
     */
    private final ArrayList<Link> mLinks;

    /**
     * Card deck of the business permit tiles.
     */
    private final CardDeck<BusinessPermitTile> mBusinessPermitTiles;

    /**
     * Array of two business permit tiles. item at index 0 represent the first visible tile, item at index 1 the second one.
     */
    private final BusinessPermitTile[] mVisibleTiles;

    /**
     * Create a new instance of a region board from JSONObject.
     * @param jsonObject to deserialize.
     */
    public RegionBoard(JSONObject jsonObject) {
        mCities = new HashMap<>();
        mLinks = new ArrayList<>();
        mBusinessPermitTiles = new CardDeck<>();
        mVisibleTiles = new BusinessPermitTile[2];
        mRegionType = jsonObject.getString(JSON_TYPE);
        parseCities(jsonObject.getJSONArray(JSON_CITIES));
        parseLinks(jsonObject.getJSONArray(JSON_LINKS));
    }

    /**
     * Deserialize list of cities from JSONArray.
     * @param jsonArray to deserialize.
     */
    private void parseCities(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            City city = new City(jsonObject);
            mCities.put(city.getName(), city);
        }
    }

    /**
     * Deserialize list of links.
     * @param jsonArray to deserialize.
     */
    private void parseLinks(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            mLinks.add(new Link(jsonObject));
        }
    }

    public String getRegionType() {
        return mRegionType;
    }

    /**
     * Get the map of all the cities in the region.
     * @return the map of all the cities in the region.
     */
    public Map<String, City> getCities() {
        return mCities;
    }

    /**
     * Check if this region contains the provided city.
     * @param name of the city to check.
     * @return true if this region contains this city, false otherwise.
     */
    public boolean hasCity(String name) {
        return mCities.containsKey(name);
    }

    /**
     * Get the map of all links between cities in the region.
     * @return the map of all links.
     */
    public List<Link> getLinks() {
        return mLinks;
    }

    /**
     * Take the first visible business permit tile.
     * @return the first visible business permit tile. It will be replaced by the last card of the hidden deck if it is
     * not empty.
     */
    public BusinessPermitTile takeFirstVisibleTile() {
        return takeVisibleTile(0);
    }

    /**
     * Get the first visible business permit tile.
     * @return the first visible business permit tile. It will not be replaced.
     */
    public BusinessPermitTile getFirstVisibleTile() {
        return getVisibleTile(0);
    }

    /**
     * Take the second visible business permit tile.
     * @return the second visible business permit tile. It will be replaced by the last card of the hidden deck if it is
     * not empty.
     */
    public BusinessPermitTile takeSecondVisibleTile() {
        return takeVisibleTile(1);
    }

    /**
     * Get the second visible business permit tile.
     * @return the second visible business permit tile. It will not be replaced.
     */
    public BusinessPermitTile getSecondVisibleTile() {
        return getVisibleTile(1);
    }

    /**
     * Take the visible card specified by the index. It should be a valid index or it will thrown an IndexOutOfBoundsException.
     * This tile will be immediately replaced by the last card of the hidden deck if it is not empty.
     * @param index of the visible tile to take.
     * @return the visible tile required if found, null otherwise.
     */
    private BusinessPermitTile takeVisibleTile(int index) {
        BusinessPermitTile tile = mVisibleTiles[index];
        mVisibleTiles[index] = mBusinessPermitTiles.takeLast();
        return tile;
    }

    /**
     * Get the visible card specified by the index. It should be a valid index or it will thrown an IndexOutOfBoundsException.
     * This tile will not be removed or replaced by another one.
     * @param index of the visible tile to get.
     * @return the visible tile required if found, null otherwise.
     */
    private BusinessPermitTile getVisibleTile(int index) {
        return mVisibleTiles[index];
    }

    /**
     * Replace the visible business permit tile of the region at the specified index.
     * @param businessPermitTile to set at the specified index.
     * @param index where place the permit tile.
     */
    /*package-local*/ void changeVisibleBusinessPermitTile(BusinessPermitTile businessPermitTile, int index) {
        mVisibleTiles[index - 1] = businessPermitTile;
    }

    /**
     * Change the visible business permit tiles of the region.
     */
    public void changeVisibleBusinessTiles() {
        mBusinessPermitTiles.addAtBottom(takeFirstVisibleTile());
        mBusinessPermitTiles.addAtBottom(takeSecondVisibleTile());
    }

    /**
     * Add business permit tiles to internal card deck. The first two cards are extracted from the deck and put on
     * first and second card-place if required.
     * @param businessPermitTiles list of card to add.
     */
    public void addBusinessPermitTiles(List<BusinessPermitTile> businessPermitTiles) {
        mBusinessPermitTiles.add(businessPermitTiles, true);
        mVisibleTiles[0] = mVisibleTiles[0] == null ? mBusinessPermitTiles.takeLast() : mVisibleTiles[0];
        mVisibleTiles[1] = mVisibleTiles[1] == null ? mBusinessPermitTiles.takeLast() : mVisibleTiles[1];
    }
}
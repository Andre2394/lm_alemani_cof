package it.polimi.cof.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class represent the information bundle that the server sends to client every time there is something new to
 * update local data state. (For example when a player make an action).
 */
public class UpdateState implements Serializable {

    /**
     * Nickname of the player that has made the changes.
     */
    private final String mNickname;

    /**
     * List of all rewards that the player has earned.
     */
    private List<Reward> mRewards;

    /**
     * Nobility reward if the player has moved along the nobility track and has earned a nobility reward.
     */
    private NobilityReward mNobilityReward;

    /**
     * Number of added assistants (All rewards are already applied).
     */
    private int mAddedAssistants;

    /**
     * Number of added victory points (All rewards are already applied).
     */
    private int mAddedVictoryPoints;

    /**
     * Number of added nobility steps (All rewards are already applied).
     */
    private int mAddedNobilitySteps;

    /**
     * Number of added coins (All rewards are already applied).
     */
    private int mAddedCoins;

    /**
     * Number of added main actions (All rewards are already applied).
     */
    private int mAddedMainActions;

    /**
     * Number of added assistants (All rewards are already applied).
     */
    private List<PoliticCard> mAddedPoliticCards;

    /**
     * List of all emporiums built.
     */
    private List<String> mAddedEmporiums;

    /**
     * List of business permit tiles earned.
     */
    private List<BusinessPermitTile> mAddedBusinessPermitTiles;

    /**
     * List of king rewards earned.
     */
    private List<Reward> mAddedKingRewards;

    /**
     * List of bonus earned.
     */
    private Map<String, Reward> mAddedBonus;

    /**
     * Number of removed assistants.
     */
    private int mRemovedAssistants;

    /**
     * Number of removed coins.
     */
    private int mRemovedCoins;

    /**
     * List of removed business permit tiles.
     */
    private List<BusinessPermitTile> mRemovedBusinessPermitTiles;

    /**
     * List of removed politic cards.
     */
    private List<PoliticCard> mRemovedPoliticCards;

    /**
     * Assigned if the king city is changed.
     */
    private String mChangedKingCity;

    /**
     * List of changed business permit tiles.
     */
    private List<ChangedBusinessPermitTile> mChangedBusinessPermitTiles;

    /**
     * List of changed councillors.
     */
    private List<ChangedCouncilor> mChangedCouncillors;

    /**
     * Base constructor.
     * @param nickname of the player that is making the changes.
     */
    public UpdateState(String nickname) {
        mNickname = nickname;
        mAddedPoliticCards = new ArrayList<>();
        mRewards = new ArrayList<>();
        mAddedEmporiums = new ArrayList<>();
        mAddedBusinessPermitTiles = new ArrayList<>();
        mAddedKingRewards = new ArrayList<>();
        mAddedBonus = new HashMap<>();
        mRemovedBusinessPermitTiles = new ArrayList<>();
        mRemovedPoliticCards = new ArrayList<>();
        mChangedBusinessPermitTiles = new ArrayList<>();
        mChangedCouncillors = new ArrayList<>();
    }

    /**
     * Get the nickname of the player that has made the changes.
     * @return the nickname of the player that has made the changes.
     */
    public String getNickname() {
        return mNickname;
    }

    /**
     * Check if the update state belong to a given nickname.
     * @param nickname to check.
     * @return true if this update state belongs to him, false otherwise.
     */
    public boolean isPlayer(String nickname) {
        return mNickname != null && mNickname.equals(nickname);
    }

    /**
     * Add a reward to the update state if it is not null.
     * @param reward to add.
     */
    public void addReward(Reward reward) {
        if (reward != null) {
            mRewards.add(reward);
        }
    }

    /**
     * Set the nobility reward.
     * @param nobilityReward to set.
     */
    public void setNobilityReward(NobilityReward nobilityReward) {
        mNobilityReward = nobilityReward;
    }

    /**
     * Get the nobility reward.
     * @return the nobility reward.
     */
    public NobilityReward getNobilityReward() {
        return mNobilityReward;
    }

    /**
     * Add a list of rewards if they are not null.
     * @param rewards to add.
     */
    public void addRewards(List<Reward> rewards) {
        mRewards.addAll(rewards.stream().filter(reward -> reward != null).collect(Collectors.toList()));
    }

    /**
     * Get the list of added rewards.
     * @return the list of added rewards.
     */
    public List<Reward> getRewards() {
        return mRewards;
    }

    /**
     * Add assistants.
     * @param count of the assistants to add.
     */
    public void addAssistants(int count) {
        mAddedAssistants += count;
    }

    /**
     * Retrieve the number of added assistants.
     * @return the number of added assistants.
     */
    public int getAddedAssistants() {
        return mAddedAssistants;
    }

    /**
     * Add victory points.
     * @param count of the victory points to add.
     */
    public void addVictoryPoints(int count) {
        mAddedVictoryPoints += count;
    }

    /**
     * Retrieve the number of added assistants.
     * @return the number of added assistants.
     */
    public int getAddedVictoryPoints() {
        return mAddedVictoryPoints;
    }

    /**
     * Add nobility steps.
     * @param steps of the nobility steps to add.
     */
    public void addNobilitySteps(int steps) {
        mAddedNobilitySteps += steps;
    }

    /**
     * Retrieve the number of added nobility steps.
     * @return the number of added nobility steps.
     */
    public int getAddedNobilitySteps() {
        return mAddedNobilitySteps;
    }

    /**
     * Add coins.
     * @param count of the coins to add.
     */
    public void addCoins(int count) {
        mAddedCoins += count;
    }

    /**
     * Retrieve the number of added coins.
     * @return the number of added coins.
     */
    public int getAddedCoins() {
        return mAddedCoins;
    }

    /**
     * Add emporium of the give city.
     * @param city where build an emporium.
     */
    public void addEmporium(String city) {
        mAddedEmporiums.add(city);
    }

    /**
     * Retrieve a list of added emporiums.
     * @return the list of added emporiums.
     */
    public List<String> getAddedEmporiums() {
        return mAddedEmporiums;
    }

    /**
     * Add business permit tile.
     * @param businessPermitTile of the business permit tile to add.
     */
    public void addBusinessPermitTile(BusinessPermitTile businessPermitTile) {
        mAddedBusinessPermitTiles.add(businessPermitTile);
    }

    /**
     * Retrieve the list of added business permit tile.
     * @return the list of added business permit tile.
     */
    public List<BusinessPermitTile> getAddedBusinessPermitTiles() {
        return mAddedBusinessPermitTiles;
    }

    /**
     * Add politic card.
     * @param politicCard to add.
     */
    public void addPoliticCard(PoliticCard politicCard) {
        mAddedPoliticCards.add(politicCard);
    }

    /**
     * Retrieve the list of added politic cards.
     * @return the list of added politic cards.
     */
    public List<PoliticCard> getAddedPoliticCards() {
        return mAddedPoliticCards;
    }

    /**
     * Add a king reward if not null.
     * @param reward to add.
     */
    public void addKingReward(Reward reward) {
        if (reward != null) {
            mAddedKingRewards.add(reward);
        }
    }

    /**
     * Retrieve the list of added king rewards.
     * @return the list of added king rewards.
     */
    public List<Reward> getKingRewards() {
        return mAddedKingRewards;
    }

    /**
     * Add bonus.
     * @param bonus of the bonus to add.
     */
    public void addBonus(Map<String, Reward> bonus) {
        mAddedBonus.putAll(bonus);
    }

    /**
     * Retrieve the map of added bonus.
     * @return the map of added bonus.
     */
    public Map<String, Reward> getBonus() {
        return mAddedBonus;
    }

    /**
     * Add main actions.
     * @param count of the main actions to add.
     */
    public void addMainAction(int count) {
        mAddedMainActions += count;
    }

    /**
     * Retrieve the number of added main actions.
     * @return the number of added main actions.
     */
    public int getAddedMainActions() {
        return mAddedMainActions;
    }

    /**
     * Remove assistants.
     * @param count of the assistants to remove.
     */
    public void removeAssistants(int count) {
        mRemovedAssistants += count;
    }

    /**
     * Retrieve the number of removed assistants.
     * @return the number of removed assistants.
     */
    public int getRemovedAssistants() {
        return mRemovedAssistants;
    }

    /**
     * Remove coins.
     * @param count of the coins to remove.
     */
    public void removeCoins(int count) {
        mRemovedCoins += count;
    }

    /**
     * Retrieve the number of removed coins.
     * @return the number of removed coins.
     */
    public int getRemovedCoins() {
        return mRemovedCoins;
    }

    /**
     * Remove business permit tile.
     * @param businessPermitTile to remove.
     */
    public void removeBusinessPermitTile(BusinessPermitTile businessPermitTile) {
        mRemovedBusinessPermitTiles.add(businessPermitTile);
    }

    /**
     * Retrieve the list of business permit tiles to remove.
     * @return the list of business permit tiles to remove.
     */
    public List<BusinessPermitTile> getRemovedBusinessPermitTiles() {
        return mRemovedBusinessPermitTiles;
    }

    /**
     * Remove politic card.
     * @param politicCards to remove.
     */
    public void removePoliticCards(List<PoliticCard> politicCards) {
        mRemovedPoliticCards.addAll(politicCards);
    }

    /**
     * Retrieve the list of politic cards to remove.
     * @return the list of politic cards to remove.
     */
    public List<PoliticCard> getRemovedPoliticCards() {
        return mRemovedPoliticCards;
    }

    /**
     * Change business permit tile at given region and index.
     * @param newPermitTile instance of the permit tile.
     * @param region where the permit tile should be placed.
     * @param permitTileIndex where the permit tile should be placed.
     */
    public void changeBusinessPermitTileAtIndex(BusinessPermitTile newPermitTile, String region, int permitTileIndex) {
        mChangedBusinessPermitTiles.add(new ChangedBusinessPermitTile(region, permitTileIndex, newPermitTile));
    }

    /**
     * Get a list of changed business permit tiles.
     * @return the list of changed business permit tiles.
     */
    public List<ChangedBusinessPermitTile> getChangedBusinessPermitTiles() {
        return mChangedBusinessPermitTiles;
    }

    /**
     * Change councillor at the given region.
     * @param addedCouncilor instance of the councillor.
     * @param region where the councillor should be changed.
     */
    public void changeCouncillor(Councilor addedCouncilor, String region) {
        mChangedCouncillors.add(new ChangedCouncilor(addedCouncilor, region));
    }

    /**
     * Get a list of changed councillors.
     * @return the list of changed councillors.
     */
    public List<ChangedCouncilor> getChangedCouncillors() {
        return mChangedCouncillors;
    }

    /**
     * Change the current king city.
     * @param kingCity new king city.
     */
    public void changeKingCity(String kingCity) {
        mChangedKingCity = kingCity;
    }

    /**
     * Check if the king city is changed.
     * @return true if changed, false otherwise.
     */
    public boolean isKingCityChanged() {
        return mChangedKingCity != null;
    }

    /**
     * Retrieve the new king city.
     * @return the name of the new king city if changed, null otherwise.
     */
    public String getNewKingCity() {
        return mChangedKingCity;
    }

    /**
     * This class works like a struct to identify the changed business permit tile and the region and index where it
     * should be placed.
     */
    public class ChangedBusinessPermitTile implements Serializable {

        /**
         * Region where the permit tile has been changed.
         */
        private String mRegion;

        /**
         * Index of the changed permit tile.
         */
        private int mPermitTileIndex;

        /**
         * The changed business permit tile.
         */
        private BusinessPermitTile mBusinessPermitTile;

        /**
         * Base constructor.
         * @param region where the permit tile is changed.
         * @param permitTileIndex index of the changed permit tile.
         * @param newPermitTile instance of the new permit tile.
         */
        private ChangedBusinessPermitTile(String region, int permitTileIndex, BusinessPermitTile newPermitTile) {
            mRegion = region;
            mPermitTileIndex = permitTileIndex;
            mBusinessPermitTile = newPermitTile;
        }

        /**
         * Retrieve the region where is has been added.
         * @return the region to update.
         */
        public String getRegion() {
            return mRegion;
        }

        /**
         * Retrieve the index where is has been added.
         * @return the index to update.
         */
        public int getIndex() {
            return mPermitTileIndex;
        }

        /**
         * Retrieve the added permit tile.
         * @return the new permit tile.
         */
        public BusinessPermitTile getBusinessPermitTile() {
            return mBusinessPermitTile;
        }
    }

    /**
     * This class works like a struct to identify the changed councillor and the region where it has been added.
     */
    public class ChangedCouncilor implements Serializable {

        /**
         * Added councillor.
         */
        private Councilor mCouncilor;

        /**
         * Region where it has been added.
         */
        private String mRegion;

        /**
         * Base constructor.
         * @param councilor changed.
         * @param region where it has been added.
         */
        private ChangedCouncilor(Councilor councilor, String region) {
            mCouncilor = councilor;
            mRegion = region;
        }

        /**
         * Retrieve the added councillor.
         * @return the new councillor.
         */
        public Councilor getCouncillor() {
            return mCouncilor;
        }

        /**
         * Retrieve the region where is has been added.
         * @return the region to update.
         */
        public String getRegion() {
            return mRegion;
        }
    }
}
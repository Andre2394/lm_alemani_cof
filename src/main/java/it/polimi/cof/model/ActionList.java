package it.polimi.cof.model;

import java.io.Serializable;

/**
 * This class represent the possible action that the player can make during his game turn. It contains all details
 * about the current turn.
 */
public class ActionList implements Serializable {

    /**
     * Boolean that represent if the player has drawn the politic card.
     */
    private boolean mPoliticCardDrawn;

    /**
     * Number of available main actions.
     */
    private int mMainActions;

    /**
     * Number of available fast actions.
     */
    private int mFastActions;

    /**
     * Public constructor.
     */
    public ActionList() {
        mPoliticCardDrawn = false;
        mMainActions = 1;
        mFastActions = 1;
    }

    /**
     * Set politic card drawn.
     */
    public void setPoliticCardDrawn() {
        mPoliticCardDrawn = true;
    }

    /**
     * Check if politic card has been drawn.
     * @return true if politic cards has been drawn, false otherwise.
     */
    public boolean isPoliticCardDrawn() {
        return mPoliticCardDrawn;
    }

    /**
     * Increment main action counter by one units.
     */
    public void incrementMainActionCounter() {
        mMainActions += 1;
    }

    /**
     * Decrement main action counter by one units.
     */
    public void decrementMainActionCounter() {
        mMainActions -= 1;
    }

    /**
     * Get the number of available main actions.
     * @return the number of available main actions.
     */
    public int getMainActionCount() {
        return mMainActions;
    }

    /**
     * Decrement fast action counter by one units.
     */
    public void decrementFastActionCounter() {
        mFastActions -= 1;
    }

    /**
     * Get the number of available fast actions.
     * @return the number of available fast actions.
     */
    public int getFastActionCount() {
        return mFastActions;
    }
}
package it.polimi.cof.model;

import it.polimi.cof.model.exceptions.CityNotFound;
import it.polimi.cof.model.exceptions.LogicException;
import it.polimi.cof.util.Debug;
import it.polimi.cof.util.LinkedList;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 *  This class represent the base of the game. It contains the map of the game.
 */
public class BaseGame implements Serializable {

    /**
     * Map configurator gates.
     */
    private static final String GATE_LEFT_CENTER_1 = "#1";
    private static final String GATE_LEFT_CENTER_2 = "#2";
    private static final String GATE_LEFT_CENTER_3 = "#3";
    private static final String GATE_CENTER_RIGHT_1 = "#4";
    private static final String GATE_CENTER_RIGHT_2 = "#5";
    private static final String GATE_CENTER_RIGHT_3 = "#6";

    /**
     * Players of the game.
     */
    protected Map<String, Player> mPlayers;

    /**
     * Left region board.
     */
    protected RegionBoard mLeftRegionBoard;

    /**
     * Center region board.
     */
    protected RegionBoard mCenterRegionBoard;

    /**
     * Right region board.
     */
    protected RegionBoard mRightRegionBoard;

    /**
     * King region board.
     */
    protected KingBoard mKingBoard;

    /**
     * Councilor list.
     */
    protected ArrayList<Councilor> mCouncilors;

    /**
     * Linked city map. Every city is linked to other cities.
     */
    private LinkedList<String, City> mLinkedCityMap;

    /**
     * Abstract constructor: required for every class that extends this one.
     */
    protected BaseGame() {
        mPlayers = new LinkedHashMap<>();
    }

    /**
     * Get councillor list.
     * @return the list of all councillors.
     */
    public List<Councilor> getCouncilors() {
        return mCouncilors;
    }

    /**
     * Get player with provided nickname.
     * @param nickname of the player to get.
     * @return the player if found, null otherwise.
     */
    public Player getPlayer(String nickname) {
        return mPlayers.get(nickname);
    }

    /**
     * Get all players as list.
     * @return the list of all players.
     */
    public List<Player> getPlayers() {
        List<Player> players = new ArrayList<>();
        players.addAll(mPlayers.values());
        return players;
    }

    /**
     * Get city from name.
     * @param name of the city to get.
     * @return the city.
     * @throws CityNotFound if city is not found.
     */
    protected City getCity(String name) throws CityNotFound {
        City city = mLinkedCityMap.getValue(name);
        if (city != null) {
            return city;
        }
        throw new CityNotFound();
    }

    /**
     * Look for the city with the given name.
     * @param name of the city to get.
     * @return the city if found, null otherwise.
     */
    public City findCity(String name) {
        return mLinkedCityMap.getValue(name);
    }

    /**
     * Apply an update state to the game session on client.
     * @param updateState to apply.
     * @throws LogicException if something is wrong while applying the update state.
     */
    public void applyUpdate(UpdateState updateState) throws LogicException {
        Player player = getPlayer(updateState.getNickname());
        // add
        player.addAssistants(updateState.getAddedAssistants());
        player.addVictoryPoints(updateState.getAddedVictoryPoints());
        player.moveNobilityTrack(updateState.getAddedNobilitySteps());
        player.addCoins(updateState.getAddedCoins());
        player.addPoliticCards(updateState.getAddedPoliticCards());
        player.addBusinessPermitTiles(updateState.getAddedBusinessPermitTiles());
        player.addKingRewards(updateState.getKingRewards());
        player.addBonus(updateState.getBonus());
        // remove
        player.payAssistants(updateState.getRemovedAssistants());
        player.payCoins(updateState.getRemovedCoins());
        player.discardPoliticCards(updateState.getRemovedPoliticCards());
        player.useBusinessPermitTiles(updateState.getRemovedBusinessPermitTiles());
        // update
        addEmporiums(player, updateState.getAddedEmporiums());
        applyChangedBusinessPermitTiles(updateState.getChangedBusinessPermitTiles());
        applyChangedCouncillors(updateState.getChangedCouncillors());
        if (updateState.isKingCityChanged()) {
            moveKingTo(getCity(updateState.getNewKingCity()));
        }
        // remove king reward cards from card deck (only for ui)
        int kingRewardCards = updateState.getKingRewards().size();
        for (int i = 0; i < kingRewardCards; i++) {
            mKingBoard.takeKingReward();
        }
    }

    /**
     * Add an emporium in provided cities.
     * @param player that has built the emporium.
     * @param cities where the player has built the emporium.
     * @throws CityNotFound if a city is not found.
     */
    private void addEmporiums(Player player, List<String> cities) throws CityNotFound {
        for (String name : cities) {
            City city = getCity(name);
            city.buildEmporiumSilently(player.getNickname());
        }
        player.removeEmporiums(cities.size());
    }

    /**
     * Apply a change of business permit tiles.
     * @param changedBusinessPermitTiles changed permit tile.
     */
    private void applyChangedBusinessPermitTiles(List<UpdateState.ChangedBusinessPermitTile> changedBusinessPermitTiles) {
        for (UpdateState.ChangedBusinessPermitTile changed : changedBusinessPermitTiles) {
            RegionBoard regionBoard = (RegionBoard) getAbstractBoard(changed.getRegion());
            regionBoard.changeVisibleBusinessPermitTile(changed.getBusinessPermitTile(), changed.getIndex());
        }
    }

    /**
     * Apply a change in councillors of a balcony.
     * @param changedCouncilors changed councillor.
     */
    private void applyChangedCouncillors(List<UpdateState.ChangedCouncilor> changedCouncilors) {
        for (UpdateState.ChangedCouncilor changed : changedCouncilors) {
            addCouncilor(changed.getCouncillor(), changed.getRegion());
        }
    }

    /**
     * Get appropriate abstract board from region name.
     * @param region to retrieve.
     * @return the correct abstract board.
     * @throws IllegalArgumentException if region is not recognized.
     */
    protected AbstractBoard getAbstractBoard(String region) {
        switch (region) {
            case AbstractBoard.LEFT_REGION:
                return mLeftRegionBoard;
            case AbstractBoard.CENTER_REGION:
                return mCenterRegionBoard;
            case AbstractBoard.RIGHT_REGION:
                return mRightRegionBoard;
            case AbstractBoard.KING_REGION:
                return mKingBoard;
            default:
                throw new IllegalArgumentException("Unknown region");
        }
    }

    /**
     * Add councilor to region balcony and recycle the old one.
     * @param councilor to add to the balcony.
     * @param region where the councilor should be added.
     */
    protected void addCouncilor(Councilor councilor, String region) {
        mCouncilors.add(getAbstractBoard(region).addCouncilor(councilor));
        mCouncilors.remove(councilor);
    }

    /**
     * Generate linked map of cities.
     */
    protected void generateLinkedCityMap() {
        mLinkedCityMap = new LinkedList<>();
        addGateNodes();
        addCityNodes();
        addLinks();
        mergeRemoveGateNodes();
    }

    /**
     * Add gates into the map.
     */
    private void addGateNodes() {
        mLinkedCityMap.addNode(GATE_LEFT_CENTER_1, null);
        mLinkedCityMap.addNode(GATE_LEFT_CENTER_2, null);
        mLinkedCityMap.addNode(GATE_LEFT_CENTER_3, null);
        mLinkedCityMap.addNode(GATE_CENTER_RIGHT_1, null);
        mLinkedCityMap.addNode(GATE_CENTER_RIGHT_2, null);
        mLinkedCityMap.addNode(GATE_CENTER_RIGHT_3, null);
    }

    /**
     * Add all cities of every regions into the map.
     */
    private void addCityNodes() {
        mLinkedCityMap.addNodes(mLeftRegionBoard.getCities());
        mLinkedCityMap.addNodes(mCenterRegionBoard.getCities());
        mLinkedCityMap.addNodes(mRightRegionBoard.getCities());
    }

    /**
     * Add links between cities into the map.
     */
    private void addLinks() {
        addLinks(mLeftRegionBoard.getLinks());
        addLinks(mCenterRegionBoard.getLinks());
        addLinks(mRightRegionBoard.getLinks());
    }

    /**
     * Add a single link between cities.
     * @param links between cities.
     */
    private void addLinks(List<Link> links) {
        for (Link link : links) {
            mLinkedCityMap.linkNode(link.getCityFrom(), link.getCityTo());
        }
    }

    /**
     * Remove gates and merge the linked nodes.
     */
    private void mergeRemoveGateNodes() {
        mLinkedCityMap.mergeNodes(GATE_LEFT_CENTER_1);
        mLinkedCityMap.mergeNodes(GATE_LEFT_CENTER_2);
        mLinkedCityMap.mergeNodes(GATE_LEFT_CENTER_3);
        mLinkedCityMap.mergeNodes(GATE_CENTER_RIGHT_1);
        mLinkedCityMap.mergeNodes(GATE_CENTER_RIGHT_2);
        mLinkedCityMap.mergeNodes(GATE_CENTER_RIGHT_3);
    }

    /**
     * Get the city where is the king.
     * @return the city where the king is placed.
     * @throws CityNotFound if no city with king has been found.
     */
    protected City getKingCity() throws CityNotFound {
        for (City city : mLinkedCityMap.getValues()) {
            if (city.hasKing()) {
                return city;
            }
        }
        throw new CityNotFound();
    }

    /**
     * Get the name of the city where the king is placed.
     * @return the name of the city where the king is placed.
     */
    public String getKingCityName() {
        try {
            return getKingCity().getName();
        } catch (CityNotFound e) {
            Debug.critical("Where is king??", e);
        }
        return "unknown?";
    }

    /**
     * Get all cities of the left region board.
     * @return all cities of the left region board.
     */
    public List<City> getLeftRegionCities() {
        return getRegionCity(mLeftRegionBoard);
    }

    /**
     * Get all cities of the center region board.
     * @return all cities of the center region board.
     */
    public List<City> getCenterRegionCities() {
        return getRegionCity(mCenterRegionBoard);
    }

    /**
     * Get all cities of the right region board.
     * @return all cities of the right region board.
     */
    public List<City> getRightRegionCities() {
        return getRegionCity(mRightRegionBoard);
    }

    /**
     * Get a list of all cities where the name starts with one of the provided characters.
     * @param initials array of characters.
     * @return the list where the name starts with one of the provided characters.
     */
    public List<City> getCitiesWithInitial(char[] initials) {
        return mLinkedCityMap.getValues().stream().filter(city -> city.hasInitial(initials)).collect(Collectors.toList());
    }

    /**
     * Get the list of cities where the player has built an emporium.
     * @param nickname of the player.
     * @return the list of cities where the player has built an emporium.
     */
    public List<City> getCitiesWherePlayerHasBuilt(String nickname) {
        return mLinkedCityMap.getValues().stream().filter(city -> city.getEmporiums().contains(nickname)).collect(Collectors.toList());
    }

    /**
     * Get all the cities of the provided regions.
     * @param regionBoard region to unpack.
     * @return the list of all cities of the region.
     */
    private List<City> getRegionCity(RegionBoard regionBoard) {
        List<City> cities = new ArrayList<>();
        cities.addAll(regionBoard.getCities().values());
        return cities;
    }

    /**
     * Get a list of all the linked cities.
     * @param city to look for.
     * @return the list of all linked cities.
     */
    public List<City> getLinkedCities(City city) {
        return mLinkedCityMap.getLinkedNodes(city.getName());
    }

    /**
     * Check if two cities are directly linked.
     * @param city1 city 1
     * @param city2 city 2
     * @return true if directly linked.
     */
    protected boolean areCitiesLinked(String city1, String city2) {
        return mLinkedCityMap.areNodesLinked(city1, city2);
    }

    /**
     * Get the list of the cities of the map with the same type.
     * @param type of the cities.
     * @return the list of the cities of the map with the same type.
     */
    protected List<City> getAllCitiesByType(String type) {
        return mLinkedCityMap.getValues().stream().filter(city -> city.getType().equals(type)).collect(Collectors.toList());
    }

    /**
     * Get the region of a city.
     * @param city to look for.
     * @return the region of the city.
     */
    protected RegionBoard getCityRegion(City city) {
        if (mLeftRegionBoard.hasCity(city.getName())) {
            return mLeftRegionBoard;
        }
        if (mCenterRegionBoard.hasCity(city.getName())) {
            return mCenterRegionBoard;
        }
        if (mRightRegionBoard.hasCity(city.getName())) {
            return mRightRegionBoard;
        }
        throw new IllegalStateException("City not belong to any region?");
    }

    /**
     * Move the king from one city to another.
     * @param city where the king should be placed.
     * @throws CityNotFound if the city is not found.
     */
    protected void moveKingTo(City city) throws CityNotFound {
        City kingCity = getKingCity();
        kingCity.setKing(false);
        city.setKing(true);
    }

    /**
     * Get nobility track.
     * @return the map of the nobility track.
     */
    public Map<Integer, NobilityReward> getNobilityTrack() {
        return mKingBoard.getNobilityTrack();
    }

    /**
     * Get the left balcony colors.
     * @return the array of 4 colors.
     */
    public String[] getLeftBalconyColors() {
        return mLeftRegionBoard.getBalconyColors();
    }

    /**
     * Get the center balcony colors.
     * @return the array of 4 colors.
     */
    public String[] getCenterBalconyColors() {
        return mCenterRegionBoard.getBalconyColors();
    }

    /**
     * Get the right balcony colors.
     * @return the array of 4 colors.
     */
    public String[] getRightBalconyColors() {
        return mRightRegionBoard.getBalconyColors();
    }

    /**
     * Get the king balcony colors.
     * @return the array of 4 colors.
     */
    public String[] getKingBalconyColors() {
        return mKingBoard.getBalconyColors();
    }

    /**
     * Get the provided balcony colors.
     * @return the array of 4 colors.
     */
    public String[] getBalconyColors(String region) {
        return getAbstractBoard(region).getBalconyColors();
    }

    /**
     * Get the list of visible permit tile of the provided region.
     * @param region to look for.
     * @return the list of permit tiles.
     */
    public List<BusinessPermitTile> getVisiblePermitTiles(String region) {
        RegionBoard regionBoard = (RegionBoard) getAbstractBoard(region);
        return Arrays.asList(regionBoard.getFirstVisibleTile(), regionBoard.getSecondVisibleTile());
    }

    /**
     * Get the index of the provided visible permit tile of the region.
     * @param region to look for.
     * @param businessPermitTile to look for.
     * @return the matching index.
     */
    public int getVisiblePermitTileIndex(String region, BusinessPermitTile businessPermitTile) {
        RegionBoard regionBoard = (RegionBoard) getAbstractBoard(region);
        if (businessPermitTile.equals(regionBoard.getFirstVisibleTile())) {
            return 1;
        } else if (businessPermitTile.equals(regionBoard.getSecondVisibleTile())) {
            return 2;
        }
        throw new IllegalArgumentException("Provided business permit tile not found in this region");
    }

    /**
     * Get the first visible permit tile of left region.
     * @return the first visible permit tile of left region.
     */
    public BusinessPermitTile getLeftRegionFirstVisibleTile() {
        return mLeftRegionBoard.getFirstVisibleTile();
    }

    /**
     * Get the second visible permit tile of left region.
     * @return the second visible permit tile of left region.
     */
    public BusinessPermitTile getLeftRegionSecondVisibleTile() {
        return mLeftRegionBoard.getSecondVisibleTile();
    }

    /**
     * Get the first visible permit tile of center region.
     * @return the first visible permit tile of center region.
     */
    public BusinessPermitTile getCenterRegionFirstVisibleTile() {
        return mCenterRegionBoard.getFirstVisibleTile();
    }

    /**
     * Get the second visible permit tile of center region.
     * @return the second visible permit tile of center region.
     */
    public BusinessPermitTile getCenterRegionSecondVisibleTile() {
        return mCenterRegionBoard.getSecondVisibleTile();
    }

    /**
     * Get the first visible permit tile of right region.
     * @return the first visible permit tile of right region.
     */
    public BusinessPermitTile getRightRegionFirstVisibleTile() {
        return mRightRegionBoard.getFirstVisibleTile();
    }

    /**
     * Get the second visible permit tile of right region.
     * @return the second visible permit tile of right region.
     */
    public BusinessPermitTile getRightRegionSecondVisibleTile() {
        return mRightRegionBoard.getSecondVisibleTile();
    }

    /**
     * Get the left region board.
     * @return the left region board.
     */
    public RegionBoard getLeftRegionBoard() {
        return mLeftRegionBoard;
    }

    /**
     * Get the center region board.
     * @return the center region board.
     */
    public RegionBoard getCenterRegionBoard() {
        return mCenterRegionBoard;
    }

    /**
     * Get the right region board.
     * @return the right region board.
     */
    public RegionBoard getRightRegionBoard() {
        return mRightRegionBoard;
    }

    /**
     * Get the king region board.
     * @return the king region board.
     */
    public KingBoard getKingRegionBoard() {
        return mKingBoard;
    }
}
package it.polimi.cof.model;

import it.polimi.cof.model.exceptions.CityNotValid;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Arrays;

/**
 * This class extends the simple Reward class. It contains the initial characters of city where is possible to
 * use this tile.
 */
public class BusinessPermitTile extends Reward implements Serializable {

    /**
     * JSONObject's constants
     */
    private static final String JSON_CITIES = "cities";

    /**
     * City/Cities initial character
     */
    private char[] mCities;

    /**
     * Create a new instance of BusinessPermitTile.
     * @param assistants number of assistant of the rewards.
     * @param victoryPoints number of victoryPoints of the rewards.
     * @param nobilitySteps number of nobilitySteps of the rewards.
     * @param coins number of coins of the rewards.
     * @param politicCards number of politicCards of the rewards.
     * @param mainActions number of mainActions of the rewards.
     * @param cities initial character/s of the city/cities where this tile can be used.
     */
    public BusinessPermitTile(int assistants, int victoryPoints, int nobilitySteps, int coins, int politicCards, int mainActions, char... cities) {
        super(assistants, victoryPoints, nobilitySteps, coins, politicCards, mainActions);
        mCities = cities;
    }

    /**
     * Create a new instance from a JSON object.
     * @param jsonObject json object to deserialize.
     */
    public BusinessPermitTile(JSONObject jsonObject) {
        super(jsonObject);
        JSONArray jsonArray = jsonObject.getJSONArray(JSON_CITIES);
        mCities = new char[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++) {
            mCities[i] = jsonArray.optString(i, "?").charAt(0);
        }
    }

    /**
     * Constructor required by Java for serialization.
     */
    protected BusinessPermitTile() {
        super();
    }

    /**
     * Get list of all characters of this tile.
     * @return all characters of the tile as array.
     */
    public char[] getCities() {
        return mCities;
    }

    /**
     * Check if this permit tile can build an emporium on the given city.
     * @param city to check.
     * @throws CityNotValid if this business permit tile cannot be use to build an emporium on this city.
     */
    public void validateCity(City city) throws CityNotValid {
        String name = city.getName().toUpperCase();
        if (!name.isEmpty()) {
            for (char initial : mCities) {
                if (name.startsWith(String.valueOf(initial).toUpperCase())) {
                    return;
                }
            }
        }
        throw new CityNotValid();
    }

    /**
     * Check if this business permit tile equals another object.
     * @param object to check.
     * @return true if the two objects are equals, false otherwise.
     */
    @Override
    public boolean equals(Object object) {
        if(this == object) {
            return true;
        }
        if((object == null) || (object.getClass() != this.getClass())) {
            return false;
        }
        BusinessPermitTile businessPermitTile = (BusinessPermitTile) object;
        return Arrays.equals(mCities, businessPermitTile.getCities()) && generateFingerprint().equals(businessPermitTile.generateFingerprint());
    }

    /**
     * Check if this business permit tile is valid.
     * @return true if valid, false otherwise.
     */
    @Override
    public boolean isValid() {
        return super.isValid() && mCities.length != 0;
    }

    /**
     * Generate an hashcode of the class.
     * @return the generated hashcode according to JavaDoc.
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash = hash * 31 + Arrays.hashCode(mCities);
        hash = hash * 31 + getAssistantCount();
        hash = hash * 31 + getVictoryPointCount();
        hash = hash * 31 + getNobilityStepCount();
        hash = hash * 31 + getCoinsCount();
        hash = hash * 31 + getPoliticCardCount();
        hash = hash * 31 + getMainActionCount();
        return hash;
    }
}
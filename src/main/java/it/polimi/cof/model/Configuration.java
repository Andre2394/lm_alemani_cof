package it.polimi.cof.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class represent the data bundle that can be transferred by server to client to give to the admin different
 * possible choice about how to configure the game, and from client to server to send the admin's choices.
 */
public class Configuration implements Serializable {

    /**
     * List of all default left region boards.
     */
    private List<RegionBoard> mLeftRegionBoards;

    /**
     * List of all default left center boards.
     */
    private List<RegionBoard> mCenterRegionBoards;

    /**
     * List of all default left right boards.
     */
    private List<RegionBoard> mRightRegionBoards;

    /**
     * Map that contains the nobility track configuration.
     */
    private Map<Integer, NobilityReward> mNobilityTrack;

    /**
     * List of all business permit tiles.
     */
    private List<BusinessPermitTile> mBusinessPermitTiles;

    /**
     * Integer that represent the waiting time in seconds.
     */
    private Integer mWaitingTime;

    /**
     * Server constructor. This is used only by the server to create a bundle to send to the client.
     * @param leftRegions list of all default left region boards.
     * @param centerRegions list of all default center region boards.
     * @param rightRegions list of all default right region boards.
     * @param nobilityTrack map that contains the default nobility track configuration.
     * @param businessPermitTiles list of all default business permit tiles.
     */
    public Configuration(List<RegionBoard> leftRegions, List<RegionBoard> centerRegions, List<RegionBoard> rightRegions, Map<Integer, NobilityReward> nobilityTrack, List<BusinessPermitTile> businessPermitTiles) {
        mLeftRegionBoards = leftRegions;
        mCenterRegionBoards = centerRegions;
        mRightRegionBoards = rightRegions;
        mNobilityTrack = nobilityTrack;
        mBusinessPermitTiles = businessPermitTiles;
    }

    /**
     * Client constructor. This is used only by the client to send the desired configuration to the server.
     * @param waitingTime max waiting time in seconds.
     * @param leftRegion chosen and configured left region board.
     * @param centerRegion chosen and configured center region board.
     * @param rightRegion chosen and configured right region board.
     * @param nobilityTrack configured map that represent the nobility track.
     * @param businessPermitTiles list of chosen and configured business permit tiles.
     */
    public Configuration(int waitingTime, RegionBoard leftRegion, RegionBoard centerRegion, RegionBoard rightRegion, Map<Integer, NobilityReward> nobilityTrack, List<BusinessPermitTile> businessPermitTiles) {
        mLeftRegionBoards = new ArrayList<>();
        mCenterRegionBoards = new ArrayList<>();
        mRightRegionBoards = new ArrayList<>();
        mNobilityTrack = nobilityTrack;
        mBusinessPermitTiles = businessPermitTiles;
        mLeftRegionBoards.add(leftRegion);
        mCenterRegionBoards.add(centerRegion);
        mRightRegionBoards.add(rightRegion);
        mWaitingTime = waitingTime;
    }

    /**
     * Get a list of all left region boards of the bundle.
     * @return the list of all left region boards of the bundle.
     */
    public List<RegionBoard> getLeftRegionBoards() {
        return mLeftRegionBoards;
    }

    /**
     * Get a list of all center region boards of the bundle.
     * @return the list of all center region boards of the bundle.
     */

    public List<RegionBoard> getCenterRegionBoards() {
        return mCenterRegionBoards;
    }

    /**
     * Get a list of all right region boards of the bundle.
     * @return the list of all right region boards of the bundle.
     */

    public List<RegionBoard> getRightRegionBoards() {
        return mRightRegionBoards;
    }

    /**
     * Get the current nobility track map.
     * @return the current representation of the nobility track.
     */
    public Map<Integer, NobilityReward> getNobilityTrack() {
        return mNobilityTrack;
    }

    /**
     * Get a list of all business permit tiles of the bundle.
     * @return the list of all business permit tiles of the bundle.
     */
    public List<BusinessPermitTile> getBusinessPermitTiles() {
        return mBusinessPermitTiles;
    }

    /**
     * Retrieve the chosen left region board.
     * @return the region board if found, null otherwise.
     */
    public RegionBoard getLeftRegionBoard() {
        if (!mLeftRegionBoards.isEmpty()) {
            return mLeftRegionBoards.get(0);
        }
        return null;
    }

    /**
     * Retrieve the chosen center region board.
     * @return the region board if found, null otherwise.
     */
    public RegionBoard getCenterRegionBoard() {
        if (!mCenterRegionBoards.isEmpty()) {
            return mCenterRegionBoards.get(0);
        }
        return null;
    }

    /**
     * Retrieve the chosen right region board.
     * @return the region board if found, null otherwise.
     */
    public RegionBoard getRightRegionBoard() {
        if (!mRightRegionBoards.isEmpty()) {
            return mRightRegionBoards.get(0);
        }
        return null;
    }

    /**
     * Retrieve the chosen max waiting time.
     * @return the chosen waiting time in seconds if specified, 0 otherwise.
     */
    public int getWaitingTime() {
        return mWaitingTime;
    }
}
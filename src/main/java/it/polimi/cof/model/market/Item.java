package it.polimi.cof.model.market;

import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.PoliticCard;

import java.io.Serializable;

/**
 * This object represent a single market item that can be:
 * - a politic card
 * - a business permit tile
 * - an assistant
 * It contains also information about the seller, the price and the nickname of the player that has bought it.
 * It is identified by a {@link #mMarketId String}.
 */
public class Item implements Serializable {

    /**
     * Unique id of the item during the market session.
     */
    private final String mMarketId;

    /**
     * Nickname of the player that is selling the item.
     */
    private final String mSellerNickname;

    /**
     * Politic card of the item.
     */
    private final PoliticCard mPoliticCard;

    /**
     * Permit tile of the item.
     */
    private final BusinessPermitTile mBusinessPermitTile;

    /**
     * Assistant of the item.
     */
    private final boolean mAssistant;

    /**
     * Price of the item.
     */
    private final int mCoins;

    /**
     * Nickname of the player that has bought the item.
     */
    private String mBuyerNickname;

    /**
     * This constructor creates a market item over a politic card.
     * @param marketId of the item.
     * @param nickname of the player that is selling the politic card.
     * @param coins price of the politic card.
     * @param politicCard that should be sold.
     */
    /*package-local*/ Item(String marketId, String nickname, int coins, PoliticCard politicCard) {
        mMarketId = marketId;
        mSellerNickname = nickname;
        mPoliticCard = politicCard;
        mBusinessPermitTile = null;
        mAssistant = false;
        mCoins = coins;
    }

    /**
     * This constructor creates a market item over a business permit tile.
     * @param marketId of the item.
     * @param nickname of the player that is selling the business permit tile.
     * @param coins price of the business permit tile.
     * @param businessPermitTile that should be sold.
     */
    /*package-local*/ Item(String marketId, String nickname, int coins, BusinessPermitTile businessPermitTile) {
        mMarketId = marketId;
        mSellerNickname = nickname;
        mPoliticCard = null;
        mBusinessPermitTile = businessPermitTile;
        mAssistant = false;
        mCoins = coins;
    }

    /**
     * This constructor creates a market item over an assistant.
     * @param marketId of the item.
     * @param nickname of the player that is selling the assistant.
     * @param coins price of the assistant.
     */
    /*package-local*/ Item(String marketId, String nickname, int coins) {
        mMarketId = marketId;
        mSellerNickname = nickname;
        mPoliticCard = null;
        mBusinessPermitTile = null;
        mAssistant = true;
        mCoins = coins;
    }

    /**
     * Retrieve the nickname of the player that is selling the item.
     * @return the nickname of the player that is selling the item.
     */
    public String getSeller() {
        return mSellerNickname;
    }

    /**
     * Check if the current item has been sold.
     * @return true if sold, false if available.
     */
    public boolean isSold() {
        return mBuyerNickname != null;
    }

    /**
     * Retrieve the nickname of the player that has bought the item.
     * @return the nickname of the player that has bought the item.
     */
    public String getBuyer() {
        return mBuyerNickname;
    }

    /**
     * Retrieve the unique id of the item.
     * @return the unique id of the item.
     */
    public String getMarketId() {
        return mMarketId;
    }

    /**
     * Retrieve the price of the item.
     * @return the price of the item.
     */
    public int getPrice() {
        return mCoins;
    }

    /**
     * Retrieve the politic card of the item.
     * @return the politic card if the item is a politic card, null otherwise.
     */
    public PoliticCard getPoliticCard() {
        return mPoliticCard;
    }

    /**
     * Retrieve the business permit tile of the item.
     * @return the business permit tile if the item is a business permit tile, null otherwise.
     */
    public BusinessPermitTile getBusinessPermitTile() {
        return mBusinessPermitTile;
    }

    /**
     * Retrieve the assistant of the item.
     * @return 1 if the item is an assistant, 0 otherwise.
     */
    public int getAssistant() {
        return mAssistant ? 1 : 0;
    }

    /**
     * Set this item has sold.
     * @param buyer nickname of the player that has bought this item.
     */
    public void setBought(String buyer) {
        mBuyerNickname = buyer;
    }
}
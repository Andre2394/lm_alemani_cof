package it.polimi.cof.model.market;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.Player;
import it.polimi.cof.model.PoliticCard;
import it.polimi.cof.model.exceptions.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class represent a single market session during the match. It contains all items of the market.
 */
public class MarketSession {

    /**
     * Internal map of all market items.
     */
    private final Map<String, Item> mItems;

    /**
     * Base constructor.
     */
    public MarketSession() {
        mItems = new HashMap<>();
    }

    /**
     * Check if all items are sold.
     * @return true if no available item if found, false otherwise.
     */
    public boolean isMarketFinished() {
        for (Item item : mItems.values()) {
            if (!item.isSold()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Sell a politic card on the market.
     * @param player that would sell the politic card.
     * @param politicCard that the player want sell.
     * @param coins that the player want for the item.
     * @return the item added to the market.
     * @throws ItemNotFound if player has not the provided item.
     * @throws ItemAlreadyOnSale if player has already putted on sale this item.
     */
    public Item sellPoliticCard(Player player, PoliticCard politicCard, int coins) throws LogicException {
        // check if player can sell this politic card
        int cardFrequency = Collections.frequency(player.getPoliticCards(), politicCard);
        if (cardFrequency == 0) {
            throw new ItemNotFound();
        }
        // now count how many occurrence of this card the same player is selling
        if (getPoliticCardsOnSale(player.getNickname(), politicCard) >= cardFrequency) {
            throw new ItemAlreadyOnSale();
        }
        String marketId = UUID.randomUUID().toString();
        Item item = new Item(marketId, player.getNickname(), coins, politicCard);
        mItems.put(marketId, item);
        return item;
    }

    /**
     * Count how many politic cards of the same type the player is already on sale.
     * @param seller nickname of the player to check.
     * @param politicCard to check.
     * @return the number of politic cards of the same type already on sale (from the same nickname).
     */
    private int getPoliticCardsOnSale(String seller, PoliticCard politicCard) {
        int count = 0;
        for (Item item : mItems.values()) {
            if (item.getSeller().equals(seller)) {
                PoliticCard card = item.getPoliticCard();
                if (card != null && card.equals(politicCard)) {
                    count += 1;
                }
            }
        }
        return count;
    }

    /**
     * Sell a business permit tile on the market.
     * @param player that would sell the business permit tile.
     * @param businessPermitTile that the player want sell.
     * @param coins that the player want for the item.
     * @return the item added to the market.
     * @throws ItemNotFound if player has not the provided item.
     * @throws ItemAlreadyOnSale if player has already putted on sale this item.
     */
    public Item sellBusinessPermitTile(Player player, BusinessPermitTile businessPermitTile, int coins) throws LogicException {
        // check if player can sell this business permit tile
        int tileFrequency = Collections.frequency(player.getBusinessPermitTiles(), businessPermitTile);
        if (tileFrequency == 0) {
            throw new ItemNotFound();
        }
        // now count how many occurrence of this business permit tile he is already selling
        if (getPermitTilesOnSale(player.getNickname(), businessPermitTile) >= tileFrequency) {
            throw new ItemAlreadyOnSale();
        }
        String marketId = UUID.randomUUID().toString();
        Item item = new Item(marketId, player.getNickname(), coins, businessPermitTile);
        mItems.put(marketId, item);
        return item;
    }

    /**
     * Count how many business permit tile of the same type the player is already on sale.
     * @param seller nickname of the player to check.
     * @param businessPermitTile to check.
     * @return the number of business permit tile of the same type already on sale (from the same nickname).
     */
    private int getPermitTilesOnSale(String seller, BusinessPermitTile businessPermitTile) {
        int count = 0;
        for (Item item : mItems.values()) {
            if (item.getSeller().equals(seller)) {
                BusinessPermitTile tile = item.getBusinessPermitTile();
                if (tile != null && tile.equals(businessPermitTile)) {
                    count += 1;
                }
            }
        }
        return count;
    }

    /**
     * Sell an assistant on the market.
     * @param player that would sell the assistant.
     * @param coins that the player want for the item.
     * @return the item added to the market.
     * @throws ItemNotFound if player has not the provided item.
     * @throws ItemAlreadyOnSale if player has already putted on sale this item.
     */
    public Item sellAssistant(Player player, int coins) throws LogicException {
        // check if player can sell this assistant
        int assistants = player.getAssistants();
        if (assistants == 0) {
            throw new ItemNotFound();
        }
        // now count how many assistants the player is already selling
        if (getAssistantsOnSale(player.getNickname()) >= assistants) {
            throw new ItemAlreadyOnSale();
        }
        String marketId = UUID.randomUUID().toString();
        Item item = new Item(marketId, player.getNickname(), coins);
        mItems.put(marketId, item);
        return item;
    }

    /**
     * Count how many assistants the player has already on sale.
     * @param seller nickname of the player to check.
     * @return the number of assistants of the same type already on sale (from the same nickname).
     */
    private int getAssistantsOnSale(String seller) {
        int count = 0;
        for (Item item : mItems.values()) {
            if (item.getSeller().equals(seller)) {
                count += item.getAssistant();
            }
        }
        return count;
    }

    /**
     * Buy an item by his market id.
     * @param buyer player that is trying to buy the item.
     * @param marketId of the item to buy.
     * @param baseGame instance of the game.
     * @throws ItemAlreadySold if the item has already been sold.
     * @throws NotEnoughCoins if the player has no enough coins to buy the item.
     *
     */
    public void buyItem(Player buyer, String marketId, BaseGame baseGame) throws LogicException {
        Item item = getItem(marketId);
        if (item.isSold()) {
            throw new ItemAlreadySold();
        }
        buyer.payCoins(item.getPrice());
        buyer.addPoliticCard(item.getPoliticCard());
        buyer.addBusinessPermitTile(item.getBusinessPermitTile());
        buyer.addAssistants(item.getAssistant());
        Player seller = baseGame.getPlayer(item.getSeller());
        seller.addCoins(item.getPrice());
        seller.removePoliticCard(item.getPoliticCard());
        seller.removeBusinessPermitTile(item.getBusinessPermitTile());
        seller.removeAssistants(item.getAssistant());
        item.setBought(buyer.getNickname());
    }

    /**
     * Apply an update of a market item.
     * @param item updated copy of the item.
     */
    public void applyUpdate(Item item) {
        mItems.put(item.getMarketId(), item);
    }

    /**
     * Query a list of all items in the market.
     * @return the list of all market items.
     */
    public List<Item> getAllItems() {
        return new ArrayList<>(mItems.values());
    }

    /**
     * Query a list of all items on sale.
     * @return the list of all market items on sale.
     */
    public List<Item> getItemsOnSale() {
        return mItems.values().stream().filter(item -> !item.isSold()).collect(Collectors.toList());
    }

    /**
     * Query a list of all items sold.
     * @return the list of all market items sold.
     */
    public List<Item> getItemsSold() {
        return mItems.values().stream().filter(Item::isSold).collect(Collectors.toList());
    }

    /**
     * Query a list of all available politic cards.
     * @return the list of all available politic cards.
     */
    public List<Item> getPoliticCards() {
        return mItems.values().stream().filter(item -> !item.isSold() && item.getPoliticCard() != null).collect(Collectors.toList());
    }

    /**
     * Query a list of all available business permit tiles.
     * @return the list of all available business permit tiles.
     */
    public List<Item> getBusinessPermitTiles() {
        return mItems.values().stream().filter(item -> !item.isSold() && item.getBusinessPermitTile() != null).collect(Collectors.toList());
    }

    /**
     * Query a list of all available assistants.
     * @return the list of all available assistants.
     */
    public List<Item> getAssistants() {
        return mItems.values().stream().filter(item -> !item.isSold() && item.getAssistant() != 0).collect(Collectors.toList());
    }

    /**
     * Retrieve an item by his marketId.
     * @param marketId of the item to get.
     * @return the required item.
     * @throws ItemNotFound if the item has not been found.
     */
    public Item getItem(String marketId) throws ItemNotFound {
        if (mItems.containsKey(marketId)) {
            return mItems.get(marketId);
        }
        throw new ItemNotFound();
    }
}
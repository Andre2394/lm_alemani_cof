package it.polimi.cof.network.server.game;

import it.polimi.cof.model.*;
import it.polimi.cof.network.exceptions.InvalidConfigurationException;
import it.polimi.cof.network.server.RemotePlayer;
import it.polimi.cof.util.Debug;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * This class implements the singleton pattern. At the creation of the server it must initialize the singleton instance.
 * During the initialization it will load the configuration file into java objects.
 */
public class Configurator {

    /**
     * Configuration file encoding.
     */
    private static final Charset FILE_ENCODING = StandardCharsets.UTF_8;

    /**
     * Configuration file keys for loading data.
     */
    private static final String ARRAY_COLORS = "Colors";
    private static final String ARRAY_BUSINESS_PERMIT_TILES = "BusinessPermitTiles";
    private static final String ARRAY_NOBILITY_TRACK = "NobilityTrack";
    private static final String ARRAY_REWARD_KING_CARDS = "RewardKingCards";
    private static final String ARRAY_BONUS_CARDS = "BonusCards";
    private static final String JSON_REGION_BOARDS = "RegionBoards";
    private static final String ARRAY_LEFT_REGION_BOARDS = "LeftBoards";
    private static final String ARRAY_CENTER_REGION_BOARDS = "CenterBoards";
    private static final String ARRAY_RIGHT_REGION_BOARDS = "RightBoards";
    private static final String JSON_BONUS_CARDS_TYPE = "type";
    private static final String JSON_BONUS_CARDS_REWARD = "reward";
    private static final String JSON_NOBILITY_TRACK_POSITION = "position";
    private static final String JSON_NOBILITY_TRACK_REWARD = "reward";
    private static final String JSON_POLITIC_CARD = "PoliticCards";
    private static final String JSON_POLITIC_CARD_PER_COLOR = "cardsPerColor";
    private static final String JSON_POLITIC_CARD_JOLLY = "jolly";
    private static final String JSON_COUNCILOR = "CouncilorsPerColor";

    /**
     * Singleton instance.
     */
    private static Configurator mInstance;

    /**
     * Color loaded from configuration file.
     */
    private ArrayList<String> mColors;

    /**
     * Politic Cards generated while loading configuration file.
     */
    private ArrayList<PoliticCard> mPoliticCards;

    /**
     * Councilors generated while loading configuration file.
     */
    private ArrayList<Councilor> mCouncilors;

    /**
     * Business permit tiles loaded from configuration file.
     */
    private ArrayList<BusinessPermitTile> mBusinessPermitTiles;

    /**
     * Map of nobility track position and linked reward loaded from configuration file.
     */
    private HashMap<Integer, NobilityReward> mNobilityTrack;

    /**
     * Map of bonus cards loaded from configuration file.
     */
    private HashMap<String, Reward> mBonusCards;

    /**
     * List of reward king cards loaded from configuration file.
     */
    private ArrayList<Reward> mRewardKingCards;

    /**
     * List of available left region boards.
     */
    private ArrayList<RegionBoard> mLeftRegionBoards;

    /**
     * List of available center region boards.
     */
    private ArrayList<RegionBoard> mCenterRegionBoards;

    /**
     * List of available right region boards.
     */
    private ArrayList<RegionBoard> mRightRegionBoards;

    /**
     * Private constructor, it can be instantiated only by singleton pattern with {@link #loadConfigurationFile}.
     * @param filePath to load.
     * @throws InvalidConfigurationException if file is not readable, contains some error or is not valid.
     */
    private Configurator(String filePath) throws InvalidConfigurationException {
        Debug.verbose("Start loading configuration file at: %s", filePath);
        try {
            String fileContent = IOUtils.toString(getClass().getResourceAsStream(filePath), FILE_ENCODING);
            parseFile(fileContent);
            checkValidity();
        } catch (IOException | JSONException e) {
            throw new InvalidConfigurationException(e);
        }
    }

    /**
     * Load file contents into a JSONObject and parse it into java objects.
     * @param fileContent content to load.
     * @throws JSONException if not a valid JSON string or something is not found.
     */
    private void parseFile(String fileContent) {
        JSONObject jsonObject = new JSONObject(fileContent);
        parseColors(jsonObject.getJSONArray(ARRAY_COLORS));
        parsePoliticCards(jsonObject.getJSONObject(JSON_POLITIC_CARD));
        parseCouncilor(jsonObject);
        parseBusinessPermitTiles(jsonObject.getJSONArray(ARRAY_BUSINESS_PERMIT_TILES));
        parseNobilityTrack(jsonObject.getJSONArray(ARRAY_NOBILITY_TRACK));
        parseRewardKingCards(jsonObject.getJSONArray(ARRAY_REWARD_KING_CARDS));
        parseBonusCards(jsonObject.getJSONArray(ARRAY_BONUS_CARDS));
        parseRegionBoards(jsonObject.getJSONObject(JSON_REGION_BOARDS));
    }

    /**
     * Load colors from JSONArray.
     * @param jsonArray to deserialize.
     */
    private void parseColors(JSONArray jsonArray) {
        mColors = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            mColors.add(jsonArray.getString(i));
        }
    }

    /**
     * Generate politic cards from JSONObject.
     * @param jsonObject to deserialize.
     */
    private void parsePoliticCards(JSONObject jsonObject) {
        mPoliticCards = new ArrayList<>();
        int politicCardsPerColor = jsonObject.getInt(JSON_POLITIC_CARD_PER_COLOR);
        for (String color : mColors) {
            for (int i = 0; i < politicCardsPerColor; i++) {
                mPoliticCards.add(new PoliticCard(color));
            }
        }
        int politicCardsJolly = jsonObject.getInt(JSON_POLITIC_CARD_JOLLY);
        for (int i = 0; i < politicCardsJolly; i++) {
            mPoliticCards.add(new PoliticCard());
        }
    }

    /**
     * Generate councilors from JSONObject.
     * @param jsonObject to deserialize.
     */
    private void parseCouncilor(JSONObject jsonObject) {
        mCouncilors = new ArrayList<>();
        int councilorsPerColor = jsonObject.getInt(JSON_COUNCILOR);
        for (String color : mColors) {
            for (int i = 0; i < councilorsPerColor; i++) {
                mCouncilors.add(new Councilor(color));
            }
        }
    }

    /**
     * Load business permit tiles from JSONArray.
     * @param jsonArray to deserialize.
     */
    private void parseBusinessPermitTiles(JSONArray jsonArray) {
        mBusinessPermitTiles = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            mBusinessPermitTiles.add(new BusinessPermitTile(object));
        }
    }

    /**
     * Load nobility track from JSONArray.
     * @param jsonArray to deserialize.
     */
    private void parseNobilityTrack(JSONArray jsonArray) {
        mNobilityTrack = new HashMap<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            int position = object.getInt(JSON_NOBILITY_TRACK_POSITION);
            JSONObject rewardObject = object.getJSONObject(JSON_NOBILITY_TRACK_REWARD);
            mNobilityTrack.put(position, new NobilityReward(rewardObject));
        }
    }

    /**
     * Load reward king cards from JSONArray.
     * @param jsonArray to deserialize.
     */
    private void parseRewardKingCards(JSONArray jsonArray) {
        mRewardKingCards = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            mRewardKingCards.add(new Reward(object));
        }
        Collections.reverse(mRewardKingCards);
    }

    /**
     * Load bonus cards from JSONArray.
     * @param jsonArray to deserialize.
     */
    private void parseBonusCards(JSONArray jsonArray) {
        mBonusCards = new HashMap<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            String bonusType = object.getString(JSON_BONUS_CARDS_TYPE);
            JSONObject rewardObject = object.getJSONObject(JSON_BONUS_CARDS_REWARD);
            mBonusCards.put(bonusType, new Reward(rewardObject));
        }
    }

    /**
     * Load available region boards from JSONObject.
     * @param jsonObject to deserialize.
     */
    private void parseRegionBoards(JSONObject jsonObject) {
        mLeftRegionBoards = new ArrayList<>();
        mCenterRegionBoards = new ArrayList<>();
        mRightRegionBoards = new ArrayList<>();
        parseRegionBoards(mLeftRegionBoards, jsonObject.getJSONArray(ARRAY_LEFT_REGION_BOARDS));
        parseRegionBoards(mCenterRegionBoards, jsonObject.getJSONArray(ARRAY_CENTER_REGION_BOARDS));
        parseRegionBoards(mRightRegionBoards, jsonObject.getJSONArray(ARRAY_RIGHT_REGION_BOARDS));
    }

    /**
     * Load region board into array.
     * @param array where put the loaded region board.
     * @param jsonArray array where read serialized region boards.
     */
    private void parseRegionBoards(ArrayList<RegionBoard> array, JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            array.add(new RegionBoard(jsonArray.getJSONObject(i)));
        }
    }

    /**
     * Analyze the structure of the models loaded from configuration file and check if they are valid.
     * @throws InvalidConfigurationException if data is not valid.
     */
    private void checkValidity() throws InvalidConfigurationException {
        String error = getError();
        if (error != null) {
            Debug.critical("Configuration file not valid! Error: " + error);
            throw new InvalidConfigurationException(error);
        }
    }

    /**
     * Check validity of the data and return the first error found.
     * @return a string with error if something is not valid, Null if no error has been founded.
     */
    private String getError() {
        if (mBusinessPermitTiles.isEmpty()) {
            return "No business permit tile found!";
        }
        if (mLeftRegionBoards.isEmpty()) {
            return "No left region board found!";
        }
        if (mCenterRegionBoards.isEmpty()) {
            return "No center region board found!";
        }
        if (mRightRegionBoards.isEmpty()) {
            return "No right region board found!";
        }
        return null; // no error
    }

    /**
     * Load configuration file and instantiate the singleton class.
     * @param path of the file to load.
     * @throws InvalidConfigurationException if file is not readable, contains some error or is not valid.
     */
    public static void loadConfigurationFile(String path) throws InvalidConfigurationException {
        mInstance = new Configurator(path);
    }

    /**
     * Create a configuration bundle with data to send to admin.
     * @return the created configuration bundle.
     */
    public static Configuration getConfigurationBundle() {
        return new Configuration(
                mInstance.mLeftRegionBoards,
                mInstance.mCenterRegionBoards,
                mInstance.mRightRegionBoards,
                mInstance.mNobilityTrack,
                mInstance.mBusinessPermitTiles
        );
    }

    /**
     * Generate ServerGame state based on admin choices.
     * @param players list of remote players in the match.
     * @param configuration bundle sent by admin player.
     * @return the built game state.
     */
    /*package-local*/ static ServerGame getGameInstance(List<RemotePlayer> players, Configuration configuration) {
        return new ServerGame.Builder(players)
                .setLeftRegion(configuration.getLeftRegionBoard())
                .setCenterRegion(configuration.getCenterRegionBoard())
                .setRightRegion(configuration.getRightRegionBoard())
                .setPoliticCards(mInstance.mPoliticCards)
                .setCouncilors(mInstance.mCouncilors)
                .setBusinessPermitTiles(configuration.getBusinessPermitTiles())
                .setNobilityTrack(configuration.getNobilityTrack())
                .setBonusCards(mInstance.mBonusCards)
                .setRewardKingCards(mInstance.mRewardKingCards)
                .build();
    }

    /**
     * Validate the provided configuration bundle.
     * @param configuration that should be validated.
     * @throws InvalidConfigurationException if is not valid.
     */
    public static void validateConfiguration(Configuration configuration) throws InvalidConfigurationException {
        int waitingTime = configuration.getWaitingTime();
        if (waitingTime < 30 || waitingTime > 300) {
            throw new InvalidConfigurationException("Waiting time must be 30s <-> 300s");
        }
        if (configuration.getBusinessPermitTiles().size() < 45) {
            throw new InvalidConfigurationException("Number of business permit tile must be >= 45");
        }
    }
}
package it.polimi.cof.network.server.socket;

import it.polimi.cof.model.*;
import it.polimi.cof.model.exceptions.*;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.network.NetworkException;
import it.polimi.cof.network.exceptions.CreateRoomException;
import it.polimi.cof.network.exceptions.InvalidConfigurationException;
import it.polimi.cof.network.exceptions.JoinRoomException;
import it.polimi.cof.network.exceptions.LoginException;
import it.polimi.cof.network.protocol.ErrorCodes;
import it.polimi.cof.network.protocol.socket.ServerProtocol;
import it.polimi.cof.network.protocol.socket.ServerSocketProtocolInt;
import it.polimi.cof.network.server.IServer;
import it.polimi.cof.network.server.RemotePlayer;
import it.polimi.cof.network.server.game.MarketTurn;
import it.polimi.cof.util.Debug;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * Extension of {@link RemotePlayer}. This implementation can communicate to his referenced client.
 */
/*package-local*/ class SocketPlayer extends RemotePlayer implements Runnable, ServerSocketProtocolInt {

    /**
     * Debug constants.
     */
    private static final String DEBUG_NO_ENOUGH_COINS = "[socket player] no enough coins";
    private static final String DEBUG_NO_ENOUGH_ASSISTANTS = "[socket player] no enough assistants";
    private static final String DEBUG_BALCONY_UNSATISFIABLE = "[socket player] balcony unsatisfiable";
    private static final String DEBUG_EMPORIUM_ALREADY_BUILT = "[socket player] emporium already built";
    private static final String DEBUG_CITY_NOT_FOUND = "[socket player] city not found";
    private static final String DEBUG_CITY_NOT_VALID = "[socket player] city not valid";
    private static final String DEBUG_ROUTE_NOT_VALID = "[socket player] route not valid";
    private static final String DEBUG_POLITIC_CARD_NOT_YET_DRAWN = "[socket player] politic card not yet drawn";
    private static final String DEBUG_MAIN_ACTION_NOT_AVAILABLE = "[socket player] main action not available";
    private static final String DEBUG_FAST_ACTION_NOT_AVAILABLE = "[socket player] fast action not available";
    private static final String DEBUG_NOT_YOUR_TURN = "[socket player] not your turn";
    private static final String DEBUG_UNCAUGHT = "[socket player] uncaught exception";

    /**
     * Server interface.
     */
    private final transient IServer mServer;

    /**
     * Socket where player can communicate with the server and vice versa.
     */
    private final transient Socket mSocket;

    /**
     * Input stream for receiving object from the client.
     */
    private final transient ObjectInputStream mInput;

    /**
     * Output stream to send object to the client.
     */
    private final transient ObjectOutputStream mOutput;

    /**
     * Socket protocol used for communication between client and server.
     */
    private final transient ServerProtocol mSocketProtocol;

    /**
     * Create a new instance of a socket player.
     * @param server server interface.
     * @param socket used for communication.
     * @throws IOException if some error occurs while opening input and output streams.
     */
    /*package-local*/ SocketPlayer(IServer server, Socket socket) throws IOException {
        mServer = server;
        mSocket = socket;
        mOutput = new ObjectOutputStream(new BufferedOutputStream(mSocket.getOutputStream()));
        mOutput.flush();
        mInput = new ObjectInputStream(new BufferedInputStream(mSocket.getInputStream()));
        mSocketProtocol = new ServerProtocol(mInput, mOutput, this);
    }

    /**
     * Listen forever on input stream for reading messages.
     */
    @Override
    public void run() {
        try {
            // noinspection InfiniteLoopStatement
            while (true) {
                Object object = mInput.readObject();
                mSocketProtocol.handleClientRequest(object);
            }
        } catch (IOException | ClassNotFoundException e) {
            Debug.critical(e);
        } finally {
            closeSafely(mInput, "I/O error occurs when closing input stream");
            closeSafely(mOutput, "I/O error occurs when closing output stream");
            closeSafely(mSocket, "I/O error occurs when closing socket");
        }
    }

    /**
     * Close safely the connection.
     * @param closeable object that implements {@link Closeable} interface.
     * @param message to print in case an exception is thrown while closing.
     */
    private void closeSafely(Closeable closeable, String message) {
        try {
            closeable.close();
        } catch (IOException e) {
            Debug.error(message, e);
        }
    }

    /**
     * Dispatch base game session to the player at the beginning of the match.
     * @param baseGame to dispatch to the player.
     */
    @Override
    public void dispatchGameSession(BaseGame baseGame) throws NetworkException {
        mSocketProtocol.notifyGameStarted(baseGame);
    }

    /**
     * Notify player that a new game turn is started.
     * @param nickname of the player that is starting the turn.
     * @param seconds that the player has to make the actions.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onGameTurnStarted(String nickname, int seconds) throws NetworkException {
        mSocketProtocol.notifyGameTurnStarted(nickname, seconds);
    }

    /**
     * Notify player of remaining seconds to make the turn actions.
     * @param remainingSeconds remaining time in seconds to make the actions.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onUpdateTurnCountdown(int remainingSeconds) throws NetworkException {
        mSocketProtocol.notifyTurnCountdownUpdated(remainingSeconds);
    }

    /**
     * Notify player that a politic card has been drawn.
     * @param updateState to send to the player.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onPoliticCardDrawn(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyPoliticCardDrawn(updateState);
    }

    /**
     * Send action list to the player.
     * @param actionList that should be sent.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onActionList(ActionList actionList) throws NetworkException {
        mSocketProtocol.sendActionList(actionList);
    }

    /**
     * Notify player that someone has made a main action.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onCouncillorElected(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyCouncillorElected(updateState);
    }

    /**
     * Notify player that someone has acquired a business permit tile.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onBusinessPermitTileAcquired(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyBusinessPermitTileAcquired(updateState);
    }

    /**
     * Notify player that someone has built an emporium using a business permit tile.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onEmporiumBuiltWithPermitTile(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyEmporiumBuiltWithPermitTile(updateState);
    }

    /**
     * Notify player that someone has built an emporium with king helps.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onEmporiumBuiltWithKingHelp(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyEmporiumBuiltWithKingHelp(updateState);
    }

    /**
     * Notify player that someone has engaged an assistant.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onAssistantEngaged(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyAssistantEngaged(updateState);
    }

    /**
     * Notify player that someone has changed the business permit tiles of a region.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onBusinessPermitTilesChanged(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyBusinessPermitTilesChanged(updateState);
    }

    /**
     * Notify player that someone has sent an assistant to elect a councillor.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onAssistantSentToElectCouncillor(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyAssistantSentToElectCouncillor(updateState);
    }

    /**
     * Notify player that someone has obtained an additional main action.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onAdditionalMainActionGranted(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyAdditionalMainActionGranted(updateState);
    }

    /**
     * Notify player that someone has earned one or more first special rewards.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onFirstSpecialRewardsEarned(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyFirstSpecialRewardsEarned(updateState);
    }

    /**
     * Notify player that someone has earned one or more second special rewards.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onSecondSpecialRewardsEarned(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifySecondSpecialRewardsEarned(updateState);
    }

    /**
     * Notify player that someone has earned one or more third special rewards.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onThirdSpecialRewardsEarned(UpdateState updateState) throws NetworkException {
        mSocketProtocol.notifyThirdSpecialRewardsEarned(updateState);
    }

    /**
     * Notify player that market session is started.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onMarketSessionStarted() throws NetworkException {
        mSocketProtocol.notifyMarketSessionStarted();
    }

    /**
     * Notify player that a new market turn is started.
     * @param nickname of the player that is starting the turn.
     * @param seconds that the player has to make the actions.
     * @param mode of the market turn.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onMarketTurnStarted(String nickname, int seconds, MarketTurn.Mode mode) throws NetworkException {
        mSocketProtocol.notifyMarketTurnStarted(nickname, seconds, mode);
    }

    /**
     * Notify player that a new item has been added to the market.
     * @param item that has been added to the market.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onMarketItemAddedOnSale(Item item) throws NetworkException {
        mSocketProtocol.notifyMarketItemAddedOnSale(item);
    }

    /**
     * Notify player that a market item has been bought.
     * @param nickname of the player that bought the item.
     * @param marketId id of the market item that has been bought.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onMarketItemBought(String nickname, String marketId) throws NetworkException {
        mSocketProtocol.notifyMarketItemBought(nickname, marketId);
    }

    /**
     * Notify player that market session is finished.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onMarketSessionFinished() throws NetworkException {
        mSocketProtocol.notifyMarketSessionFinished();
    }

    /**
     * Send a chat message to the player.
     * @param author nickname of the player that sent the message.
     * @param message that the author has sent.
     * @param privateMessage if message is private, false if public.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onChatMessage(String author, String message, boolean privateMessage) throws NetworkException {
        mSocketProtocol.sendChatMessage(author, message, privateMessage);
    }

    /**
     * Notify player that another player has disconnected.
     * @param nickname of the disconnected player.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onPlayerDisconnected(String nickname) throws NetworkException {
        mSocketProtocol.notifyPlayerDisconnected(nickname);
    }

    /**
     * Notify player that a player has built all his emporiums and the last turn is starting.
     * @param nickname of the player has built all his emporiums
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onLastTurn(String nickname) throws NetworkException {
        mSocketProtocol.notifyLastTurnStarted(nickname);
    }

    /**
     * Notify player that the game is finished and dispatch all last update of all players.
     * @param updateStates of all players (bonus and end-game rewards).
     * @param ranking list of players nickname sorted by winner to loser.
     * @throws NetworkException if client is not reachable.
     */
    @Override
    public void onGameEnd(UpdateState[] updateStates, List<String> ranking) throws NetworkException {
        mSocketProtocol.notifyGameEnded(updateStates, ranking);
    }

    /**
     * Try to login the player into server with the given nickname.
     * @param nickname to use.
     * @throws LoginException if another player with the same nickname is already logged.
     */
    @Override
    public void loginPlayer(String nickname) throws LoginException {
        mServer.loginPlayer(nickname, this);
    }

    /**
     * Try to join an existing room.
     * @throws JoinRoomException if no available room is found.
     */
    @Override
    public void joinRoom() throws JoinRoomException {
        mServer.joinFirstAvailableRoom(this);
    }

    /**
     * Try to create a room on the server.
     * @param maxPlayers to set.
     * @return a configuration bundle that contains all default configurations.
     * @throws CreateRoomException if another room has been created in the meanwhile.
     */
    @Override
    public Configuration createRoom(int maxPlayers) throws CreateRoomException {
        return mServer.createNewRoom(this, maxPlayers);
    }

    /**
     * Try to apply the provided configuration.
     * @param configuration to set.
     * @throws InvalidConfigurationException if the configuration is not valid.
     */
    @Override
    public void applyConfiguration(Configuration configuration) throws InvalidConfigurationException {
        mServer.applyGameConfiguration(getRoom(), configuration);
    }

    /**
     * Draw a politic card.
     */
    @Override
    public void drawPoliticCard() {
        try {
            getRoom().drawPoliticCard(this);
        } catch (PoliticCardAlreadyDrawn e) {
            Debug.debug("[socket-player] Politic card already drawn", e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_POLITIC_CARD_ALREADY_DRAWN);
        } catch (NotYourTurnException e) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } catch (LogicException e) {
            Debug.debug(DEBUG_UNCAUGHT, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_GENERIC_SERVER_ERROR);
        }
    }

    /**
     * Ask for the action list.
     */
    @Override
    public void sendActionList() {
        try {
            getRoom().sendActionList(this);
        } catch (NotYourTurnException e) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        }
    }

    /**
     * Elect a councillor.
     * @param councilor to elect.
     * @param region where it should be elected.
     */
    @Override
    public void electCouncillor(Councilor councilor, String region) {
        try {
            getRoom().electCouncillor(this, councilor, region);
        } catch (LogicException e) {
            handleActionExceptions(e);
        }
    }

    /**
     * Acquire a business permit tile.
     * @param politicCards to use to satisfy the balcony.
     * @param region where the permit tile is placed.
     * @param permitTileIndex index of the permit tile to take.
     */
    @Override
    public void acquireBusinessPermitTile(List<PoliticCard> politicCards, String region, int permitTileIndex) {
        try {
            getRoom().acquireBusinessPermitTiles(this, politicCards, region, permitTileIndex);
        } catch (LogicException e) {
            handleActionExceptions(e);
        }
    }

    /**
     * Build an emporium with a business permit tile.
     * @param businessPermitTile to use.
     * @param city where the emporium should be built.
     */
    @Override
    public void buildEmporiumWithBusinessPermitTile(BusinessPermitTile businessPermitTile, String city) {
        try {
            getRoom().buildEmporiumWithBusinessPermitTile(this, businessPermitTile, city);
        } catch (LogicException e) {
            handleActionExceptions(e);
        }
    }

    /**
     * Build an emporium with the help of the king.
     * @param politicCards to use to satisfy the king's balcony.
     * @param cities that represent the cities where the king should move along side.
     */
    @Override
    public void buildEmporiumWithKingHelp(List<PoliticCard> politicCards, List<String> cities) {
        try {
            getRoom().buildEmporiumWithKingHelp(this, politicCards, cities);
        } catch (LogicException e) {
            handleActionExceptions(e);
        }
    }

    /**
     * Engage an assistant.
     */
    @Override
    public void engageAssistant() {
        try {
            getRoom().engageAssistant(this);
        } catch (LogicException e) {
            handleActionExceptions(e);
        }
    }

    /**
     * Change the visible permit tiles of a region.
     * @param region where the visible permit tiles should be changed.
     */
    @Override
    public void changeBusinessPermitTiles(String region) {
        try {
            getRoom().changeBusinessPermitTiles(this, region);
        } catch (LogicException e) {
            handleActionExceptions(e);
        }
    }

    /**
     * Send an assistant to elect a councillor in the given region.
     * @param councilor to elect.
     * @param region where the councillor should be elected.
     */
    @Override
    public void sendAssistantElectCouncillor(Councilor councilor, String region) {
        try {
            getRoom().sendAssistantToElectCouncillor(this, councilor, region);
        } catch (LogicException e) {
            handleActionExceptions(e);
        }
    }

    /**
     * Buy an additional main action.
     */
    @Override
    public void performAdditionalMainAction() {
        try {
            getRoom().performAdditionalMainAction(this);
        } catch (LogicException e) {
            handleActionExceptions(e);
        }
    }

    /**
     * Earn a first special rewards.
     * @param cities where the player want to takes the reward.
     */
    @Override
    public void earnFirstSpecialRewards(List<String> cities) {
        try {
            getRoom().earnFirstSpecialRewards(this, cities);
        } catch (LogicException e) {
            handleActionExceptions(e);
        }
    }

    /**
     * Earn a second special rewards.
     * @param regions where the player want to take the visible permit tile.
     * @param indices of the permit tiles that the player want to take.
     */
    @Override
    public void earnSecondSpecialRewards(List<String> regions, List<Integer> indices) {
        try {
            getRoom().earnSecondSpecialRewards(this, regions, indices);
        } catch (NotYourTurnException e) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        }
    }

    /**
     * Earn a third special rewards.
     * @param businessPermitTiles list of permit tiles from which the player want to take the rewards.
     */
    @Override
    public void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles) {
        try {
            getRoom().earnThirdSpecialRewards(this, businessPermitTiles);
        } catch (NotYourTurnException e) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        }
    }

    /**
     * Handle every exception that can be thrown while doing a main / fast action.
     * @param e exception to handle.
     */
    private void handleActionExceptions(LogicException e) {
        if (e instanceof PoliticCardNotYetDrawn) {
            Debug.debug(DEBUG_POLITIC_CARD_NOT_YET_DRAWN, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_POLITIC_CARD_NOT_YET_DRAWN);
        } else if (e instanceof MainActionNotAvailable) {
            Debug.debug(DEBUG_MAIN_ACTION_NOT_AVAILABLE, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_MAIN_ACTION_NOT_AVAILABLE);
        } else if (e instanceof FastActionNotAvailable) {
            Debug.debug(DEBUG_FAST_ACTION_NOT_AVAILABLE, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_FAST_ACTION_NOT_AVAILABLE);
        } else if (e instanceof NotYourTurnException) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } else {
            handleGameLogicExceptions(e);
        }
    }

    /**
     * Handle every possible logic exception that can be thrown while doing a main / fast action.
     * @param e logic exception to handle.
     */
    private void handleGameLogicExceptions(LogicException e) {
        if (e instanceof BalconyUnsatisfiable) {
            Debug.debug(DEBUG_BALCONY_UNSATISFIABLE, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NO_POLITIC_CARD_CAN_SATISFY_REGION_BALCONY);
        } else if (e instanceof NotEnoughCoins) {
            Debug.debug(DEBUG_NO_ENOUGH_COINS, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_ENOUGH_COINS);
        } else if (e instanceof NotEnoughAssistants) {
            Debug.debug(DEBUG_NO_ENOUGH_ASSISTANTS, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_ENOUGH_ASSISTANTS);
        } else if (e instanceof CityNotFound) {
            Debug.debug(DEBUG_CITY_NOT_FOUND, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_CITY_NOT_FOUND);
        } else if (e instanceof CityNotValid) {
            Debug.debug(DEBUG_CITY_NOT_VALID, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_CITY_NOT_VALID);
        } else if (e instanceof EmporiumAlreadyBuilt) {
            Debug.debug(DEBUG_EMPORIUM_ALREADY_BUILT, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_EMPORIUM_ALREADY_BUILT);
        } else if (e instanceof RouteNotValid) {
            Debug.debug(DEBUG_ROUTE_NOT_VALID, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_ROUTE_NOT_VALID);
        }
    }

    /**
     * Sell a politic card on the market.
     * @param politicCard to sell.
     * @param price to set.
     */
    @Override
    public void sellPoliticCard(PoliticCard politicCard, int price) {
        try {
            getRoom().sellPoliticCard(this, politicCard, price);
        } catch (LogicException e) {
            handleMarketSellExceptions(e);
        }
    }

    /**
     * Sell a business permit tile on the market.
     * @param businessPermitTile to sell.
     * @param price to set.
     */
    @Override
    public void sellBusinessPermitTile(BusinessPermitTile businessPermitTile, int price) {
        try {
            getRoom().sellBusinessPermitTile(this, businessPermitTile, price);
        } catch (LogicException e) {
            handleMarketSellExceptions(e);
        }
    }

    /**
     * Sell an assistant on the market.
     * @param price to set.
     */
    @Override
    public void sellAssistant(int price) {
        try {
            getRoom().sellAssistant(this, price);
        } catch (LogicException e) {
            handleMarketSellExceptions(e);
        }
    }

    /**
     * Handle all possible exception from a Login exception.
     * @param e exception to handle.
     */
    private void handleMarketSellExceptions(LogicException e) {
        if (e instanceof ItemNotFound) {
            Debug.debug("[socket player] Item not found", e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_MARKET_ITEM_NOT_FOUND);
        } else if (e instanceof ItemAlreadyOnSale) {
            Debug.debug("[socket player] Item already on sale", e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_MARKET_ITEM_ALREADY_ON_SALE);
        } else if (e instanceof NotYourTurnException) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } else {
            Debug.debug(DEBUG_UNCAUGHT, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_GENERIC_SERVER_ERROR);
        }
    }

    /**
     * Buy an item from the market.
     * @param marketId that identify the item to buy.
     */
    @Override
    public void buyItem(String marketId) {
        try {
            getRoom().buyItem(this, marketId);
        } catch (ItemNotFound e) {
            Debug.debug("[socket player] Item not found", e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_MARKET_ITEM_NOT_FOUND);
        } catch (ItemAlreadySold e) {
            Debug.debug("[socket player] Item already sold", e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_MARKET_ITEM_ALREADY_SOLD);
        } catch (NotEnoughCoins e) {
            Debug.debug(DEBUG_NO_ENOUGH_COINS, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_ENOUGH_COINS);
        } catch (NotYourTurnException e) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } catch (IOException e) {
            Debug.debug(DEBUG_UNCAUGHT, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_GENERIC_SERVER_ERROR);
        }
    }

    /**
     * End turn.
     */
    @Override
    public void endTurn() {
        try {
            getRoom().endTurn(this);
        } catch (NotYourTurnException e) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        }
    }

    /**
     * Dispatch a chat message to the receiver or to all players.
     * @param receiver nickname of the receiver, null if message is public.
     * @param message body of the message.
     */
    @Override
    public void sendChatMessage(String receiver, String message) {
        try {
            getRoom().sendChatMessage(this, receiver, message);
        } catch (PlayerNotFound e) {
            Debug.debug("[socket player] cannot dispatch message to a player that cannot be found", e);
            mSocketProtocol.actionNotValid(ErrorCodes.ERROR_CHAT_PLAYER_NOT_FOUND);
        }
    }
}
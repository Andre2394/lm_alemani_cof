package it.polimi.cof.network.server;

import it.polimi.cof.model.Configuration;
import it.polimi.cof.network.exceptions.CreateRoomException;
import it.polimi.cof.network.exceptions.InvalidConfigurationException;
import it.polimi.cof.network.exceptions.JoinRoomException;
import it.polimi.cof.network.exceptions.LoginException;
import it.polimi.cof.network.server.game.Configurator;
import it.polimi.cof.network.server.game.Room;
import it.polimi.cof.network.server.game.RoomFullException;
import it.polimi.cof.network.server.rmi.RMIServer;
import it.polimi.cof.network.server.socket.SocketServer;
import it.polimi.cof.util.Debug;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class represent the server of the game.
 */
public class Server implements IServer {

    /**
     * Port where socket communication is open.
     */
    private static final int SOCKET_PORT = 3031;

    /**
     * Port where RMI communication is open.
     */
    private static final int RMI_PORT = 3032;

    /**
     * This object works as mutex to avoid concurrency race between player login.
     */
    private static final Object PLAYERS_MUTEX = new Object();

    /**
     * This object works as mutex to avoid concurrency race between room joining.
     */
    private static final Object ROOMS_MUTEX = new Object();

    /**
     * Players cache.
     */
    private HashMap<String, RemotePlayer> mPlayers;

    /**
     * List of all server room.
     */
    private ArrayList<Room> mRooms;

    /**
     * Socket server.
     */
    private SocketServer mSocketServer;

    /**
     * RMI server.
     */
    private RMIServer mRmiServer;

    /**
     * Create a new instance of the class.
     * @throws ServerException if some error occurs.
     */
    private Server() throws ServerException {
        mPlayers = new HashMap<>();
        mRooms = new ArrayList<>();
        mSocketServer = new SocketServer(this);
        mRmiServer = new RMIServer(this);
        loadConfigurationFile();
    }

    /**
     * Static method to execute the server.
     * @param args to pass to the server.
     */
    public static void main(String[] args) {
        Debug.initialize(Debug.LEVEL_DEBUG);
        try {
            Server server = new Server();
            server.startServer(SOCKET_PORT, RMI_PORT);
        } catch (ServerException e) {
            Debug.critical(e);
        }
    }

    /**
     * Start server connections.
     * @param socketPort port where start Socket connection.
     * @param rmiPort port where start RMI connection.
     * @throws ServerException if some error occurs.
     */
    private void startServer(int socketPort, int rmiPort) throws ServerException {
        mSocketServer.startServer(socketPort);
        mRmiServer.startServer(rmiPort);
    }

    /**
     * Load configuration file.
     * @throws ServerException if some error occurs.
     */
    private void loadConfigurationFile() throws ServerException {
        try {
            Configurator.loadConfigurationFile("/files/configuration.json");
        } catch (InvalidConfigurationException e) {
            throw new ServerException("Cannot load configuration file", e);
        }
    }

    /**
     * Login player with nickname.
     * @param nickname of the player that would login.
     * @param player reference to the player that made the request.
     * @throws LoginException if a player with this nickname already exists.
     */
    @Override
    public void loginPlayer(String nickname, RemotePlayer player) throws LoginException {
        synchronized (PLAYERS_MUTEX) {
            if (!mPlayers.containsKey(nickname)) {
                mPlayers.put(nickname, player);
                player.setNickname(nickname);
            } else {
                throw new LoginException();
            }
        }
    }

    /**
     * Get the player associated to required nickname.
     * @param nickname of the player to retrieve.
     * @return the associated remote player if found.
     */
    @Override
    public RemotePlayer getPlayer(String nickname) {
        return mPlayers.get(nickname);
    }

    /**
     * Join player to the first available room.
     * @param player reference to the player that made the request.
     * @throws RoomFullException if no room is available for the join request.
     */
    private void joinLastRoom(RemotePlayer player) throws RoomFullException {
        Room lastRoom = mRooms.isEmpty() ? null : mRooms.get(mRooms.size() - 1);
        if (lastRoom != null) {
            lastRoom.joinPlayer(player);
            player.setRoom(lastRoom);
        } else {
            throw new RoomFullException("No available room found!");
        }
    }

    /**
     * Join player to the first available room.
     * @param remotePlayer that would join.
     * @throws JoinRoomException if no available room has been found.
     */
    @Override
    public void joinFirstAvailableRoom(RemotePlayer remotePlayer) throws JoinRoomException {
        synchronized (ROOMS_MUTEX) {
            try {
                joinLastRoom(remotePlayer);
            } catch (RoomFullException e) {
                throw new JoinRoomException(e);
            }
        }
    }

    /**
     * Create a new room on server.
     * @param remotePlayer that made the request.
     * @param maxPlayers that player would like to add in the room.
     * @throws CreateRoomException if another player has created a new room in the meanwhile.
     * @return configuration bundle that contains all default configurations.
     */
    @Override
    public Configuration createNewRoom(RemotePlayer remotePlayer, int maxPlayers) throws CreateRoomException {
        synchronized (ROOMS_MUTEX) {
            boolean hasJoinRoom = false;
            try {
                joinLastRoom(remotePlayer);
                hasJoinRoom = true;
            } catch (RoomFullException e) {
                Debug.verbose("No room has been created in the meanwhile, Player is going to create his room", e);
            }
            if (!hasJoinRoom) {
                Room room = new Room(maxPlayers, remotePlayer);
                mRooms.add(room);
                remotePlayer.setRoom(room);
                return Configurator.getConfigurationBundle();
            } else {
                throw new CreateRoomException();
            }
        }
    }

    /**
     * Check and apply the provided configuration into the player's room.
     * @param room where should be applied the configuration.
     * @param configuration that should be validated and applied.
     * @throws InvalidConfigurationException if provided configuration is not valid.
     */
    @Override
    public void applyGameConfiguration(Room room, Configuration configuration) throws InvalidConfigurationException {
        Configurator.validateConfiguration(configuration);
        room.configureGame(configuration);
    }
}
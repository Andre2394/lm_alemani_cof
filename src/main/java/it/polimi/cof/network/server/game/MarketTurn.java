package it.polimi.cof.network.server.game;

import it.polimi.cof.network.server.RemotePlayer;

/**
 * This class represent a single player market turn.
 */
public class MarketTurn extends Turn {

    /**
     * Mode of the market turn.
     */
    private final Mode mMode;

    /**
     * Create a new instance of a turn.
     * @param player   that should play the turn.
     * @param callback interface to send update notification to client.
     */
    /*package-local*/ MarketTurn(RemotePlayer player, Mode mode, TurnCallback callback) {
        super(player, callback);
        mMode = mode;
    }

    /**
     * Retrieve the mode of the market turn.
     * @return the mode of the turn.
     */
    /*package-local*/ Mode getMode() {
        return mMode;
    }

    /**
     * Representation of the all possible modes of the market turn.
     */
    public enum Mode {

        /**
         * Sell mode: the player can sell politic cards, business permit tiles and assistants.
         */
        SELL,

        /**
         * Buy mode: the player can buy all the available items of the market session.
         */
        BUY
    }
}
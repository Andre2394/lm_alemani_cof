package it.polimi.cof.network.server.game;

import it.polimi.cof.model.ActionList;
import it.polimi.cof.model.NobilityReward;
import it.polimi.cof.network.server.RemotePlayer;

/**
 * This class represent a single player game turn.
 */
/*package-local*/ class GameTurn extends Turn {

    /**
     * Keep a reference of the turn status.
     */
    private ActionList mActionList;

    /**
     * First special rewards that the player can take.
     */
    private int mFirstSpecialBonus;

    /**
     * Second special rewards that the player can take.
     */
    private int mSecondSpecialBonus;

    /**
     * Third special rewards that the player can take.
     */
    private int mThirdSpecialBonus;

    /**
     * Create a new instance of a game turn.
     * @param player that should play the turn.
     * @param callback interface to send update notification to client.
     */
    /*package-local*/ GameTurn(RemotePlayer player, TurnCallback callback) {
        super(player, callback);
        mActionList = new ActionList();
    }

    /**
     * Notify turn that a main action has been done.
     */
    /*package-local*/ void onMainActionDone() {
        mActionList.decrementMainActionCounter();
    }

    /**
     * Notify turn that an additional main action can be done.
     */
    /*package-local*/ void addAdditionalMainAction() {
        mActionList.incrementMainActionCounter();
    }

    /**
     * Notify turn that a fast action has been done.
     */
    /*package-local*/ void onFastActionDone() {
        mActionList.decrementFastActionCounter();
    }

    /**
     * Retrieve the list of actions that the player can do.
     * @return the server copy of the action list.
     */
    /*package-local*/ ActionList getActionList() {
        return mActionList;
    }

    /**
     * Notify turn that a politic card has been drawn.
     */
    /*package-local*/ void setPoliticCardDrawn() {
        mActionList.setPoliticCardDrawn();
    }

    /**
     * Notify turn that a nobility reward has been earned by the player.
     * @param nobilityReward earned by the player.
     */
    /*package-local*/ void setNobilityReward(NobilityReward nobilityReward) {
        mFirstSpecialBonus = nobilityReward.getFirstSpecialBonusCount();
        mSecondSpecialBonus = nobilityReward.getSecondSpecialBonusCount();
        mThirdSpecialBonus = nobilityReward.getThirdSpecialBonusCount();
    }

    /**
     * Retrieve the number of cities where the player can gets the reward.
     * @return the number of cities where the player can gets the reward.
     */
    /*package-local*/ int getFirstSpecialBonusCount() {
        return mFirstSpecialBonus;
    }

    /**
     * Notify the turn of the number of cities where the player has taken the reward.
     * @param count the number of cities where the player has taken the reward.
     */
    /*package-local*/ void decrementFirstSpecialRewards(int count) {
        mFirstSpecialBonus -= count;
    }

    /**
     * Retrieve the number of permit tiles that the player can take without paying.
     * @return the number of permit tiles that the player can take without paying.
     */
    /*package-local*/ int getSecondSpecialBonusCount() {
        return mSecondSpecialBonus;
    }

    /**
     * Notify the turn of the number of permit tiles that the player has taken without paying.
     * @param count the number of permit tiles that the player has taken without paying.
     */
    /*package-local*/ void decrementSecondSpecialRewards(int count) {
        mSecondSpecialBonus -= count;
    }

    /**
     * Retrieve the number of permit tiles from which the player can take the rewards.
     * @return the number of permit tiles from which the player can take the rewards.
     */
    /*package-local*/ int getThirdSpecialBonusCount() {
        return mThirdSpecialBonus;
    }

    /**
     * Notify the turn of the number of permit tiles from which the player can take the rewards.
     * @param count the number of permit tiles from which the player can take the rewards.
     */
    /*package-local*/ void decrementThirdSpecialRewards(int count) {
        mThirdSpecialBonus -= count;
    }
}
package it.polimi.cof.network.server.game;

import it.polimi.cof.network.server.RemotePlayer;
import it.polimi.cof.util.Debug;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

/**
 * This class represent a single player turn. It will help with the management of the match flow.
 */
/*package-local*/ class Turn {

    /**
     * Internal mutex used to avoid thread race concurrency when a player is ending his turn and the timer is over.
     */
    private static final Object TURN_MUTEX = new Object();

    /**
     * Player that is playing the turn.
     */
    private final RemotePlayer mRemotePlayer;

    /**
     * Callback used to notify to client the updated count down timer.
     */
    private final TurnCallback mTurnCallback;

    /**
     * Internal timer used to implement the countdown.
     */
    private final Timer mTimer;

    /**
     * This works like semaphores: it is initialized to an initial number N and every second it is decremented by 1.
     * The main thread will sleep until this counter not reaches 0 by the Timer or by user interaction.
     */
    private CountDownLatch mLatch;

    /**
     * Create a new instance of a turn.
     * @param player that should play the turn.
     * @param callback interface to send update notification to client.
     */
    /*package-local*/ Turn(RemotePlayer player, TurnCallback callback) {
        mRemotePlayer = player;
        mTurnCallback = callback;
        mTimer = new Timer();
    }

    /**
     * Start the countdown and block the caller thread until the {@link #mLatch} not reaches 0.
     * @param waitingTime time [in seconds] to wait until the turn expires.
     */
    /*package-local*/ void startCountDown(int waitingTime) {
        mLatch = new CountDownLatch(waitingTime);
        mTimer.scheduleAtFixedRate(new CountDownTask(), 1000, 1000);
        try {
            mLatch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            Debug.error(e);
        }
    }

    /**
     * Stop the countdown and end the player's turn.
     */
    /*package-local*/ void stopCountDown() {
        synchronized (TURN_MUTEX) {
            mTimer.cancel();
            mTimer.purge();
            while (mLatch.getCount() > 0) {
                mLatch.countDown();
            }
        }
    }

    /**
     * Check if the current turn belongs to the provided player.
     * @param player to check.
     * @return true if player matches.
     */
    /*package-local*/ boolean isCurrentPlayer(RemotePlayer player) {
        return player == mRemotePlayer;
    }

    /**
     * This interface is used as callback to notify client when the countdown timer is changed.
     */
    @FunctionalInterface
    /*package-local*/ interface TurnCallback {

        /**
         * Notify that countdown is changed.
         * @param player that is playing the turn.
         * @param remainingTime remaining time to wait in seconds.
         */
        void onUpdateCountdown(RemotePlayer player, int remainingTime);
    }

    /**
     * This class represent what the timer should do every seconds until the timer ends.
     */
    private class CountDownTask extends TimerTask {

        /**
         * Code executed by the timer every seconds on a secondary thread.
         */
        @Override
        public void run() {
            synchronized (TURN_MUTEX) {
                if (mLatch.getCount() > 0) {
                    mTurnCallback.onUpdateCountdown(mRemotePlayer, (int) mLatch.getCount() - 1);
                    if (mLatch.getCount() == 1) {
                        mTimer.cancel();
                        mTimer.purge();
                    }
                    mLatch.countDown();
                }
            }
        }
    }
}
package it.polimi.cof.network.client.socket;

import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.Configuration;
import it.polimi.cof.model.Councilor;
import it.polimi.cof.model.PoliticCard;
import it.polimi.cof.network.NetworkException;
import it.polimi.cof.network.client.AbstractClient;
import it.polimi.cof.network.client.ClientConnectionException;
import it.polimi.cof.network.client.IClient;
import it.polimi.cof.network.exceptions.CreateRoomException;
import it.polimi.cof.network.exceptions.InvalidConfigurationException;
import it.polimi.cof.network.exceptions.JoinRoomException;
import it.polimi.cof.network.exceptions.LoginException;
import it.polimi.cof.network.protocol.socket.ClientProtocol;
import it.polimi.cof.util.Debug;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * This class is the implementation of {@link AbstractClient} class. It manages the network connection with sockets.
 */
public class SocketClient extends AbstractClient {

    /**
     * Socket endpoint of client.
     */
    private Socket mSocket;

    /**
     * Object input stream for receiving serialized objects from server socket.
     */
    private ObjectInputStream mInput;

    /**
     * Object output stream for sending serialized objects to server socket.
     */
    private ObjectOutputStream mOutput;

    /**
     * Socket protocol used for communication between client and server.
     */
    private ClientProtocol mSocketProtocol;

    /**
     * Create a socket client instance.
     * @param controller client controller.
     * @param address of the server.
     * @param port of the server.
     */
    public SocketClient(IClient controller, String address, int port) {
        super(controller, address, port);
    }

    /**
     * Open a connection with the server and initialize socket protocol.
     * @throws ClientConnectionException if server is not reachable or something went wrong.
     */
    @Override
    public void connect() throws ClientConnectionException {
        try {
            mSocket = new Socket(getAddress(), getPort());
            mOutput = new ObjectOutputStream(new BufferedOutputStream(mSocket.getOutputStream()));
            mOutput.flush();
            mInput = new ObjectInputStream(new BufferedInputStream(mSocket.getInputStream()));
        } catch (IOException e) {
            throw new ClientConnectionException(e);
        }
    }

    /**
     * Initialize socket protocol.
     */
    @Override
    public void initializeConnection() {
        mSocketProtocol = new ClientProtocol(mInput, mOutput, getController());
    }

    /**
     * Blocking request to server for user login.
     * @param nickname to use for login.
     * @throws LoginException if provided nickname is already in use.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void loginPlayer(String nickname) throws NetworkException {
        mSocketProtocol.loginPlayer(nickname);
    }

    /**
     * Blocking request to server for join user in the first available room.
     * @throws JoinRoomException if no available room has been found where join the player.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void joinFirstAvailableRoom() throws NetworkException {
        mSocketProtocol.joinFirstAvailableRoom();
        startNetworkMessageHandlerThread();
    }

    /**
     * Blocking request to server for creating a new room.
     * @param maxPlayers that should be accepted in this new room.
     * @throws CreateRoomException if a new room has been created in the meanwhile and the player has been added.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public Configuration createNewRoom(int maxPlayers) throws NetworkException {
        try {
            return mSocketProtocol.createNewRoom(maxPlayers);
        } catch (NetworkException e) {
            startNetworkMessageHandlerThread();
            throw e;
        }
    }

    /**
     * Try to apply game configuration to the player room.
     * @param configuration that should be applied to the room.
     * @throws InvalidConfigurationException if configuration is not valid.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void applyGameConfiguration(Configuration configuration) throws NetworkException {
        mSocketProtocol.applyConfiguration(configuration);
        startNetworkMessageHandlerThread();
    }

    /**
     * Retrieve a list that contains all possible actions.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void getActionList() throws NetworkException {
        mSocketProtocol.getActionList();
    }

    /**
     * Draw a politic card.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void drawPoliticCard() throws NetworkException {
        mSocketProtocol.drawPoliticCard();
    }

    /**
     * [MAIN ACTION 1]: Elect a councillor.
     * @param councilor to add to the balcony.
     * @param region where the balcony where insert the councillor is placed.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void electCouncillor(Councilor councilor, String region) throws NetworkException {
        mSocketProtocol.electCouncillor(councilor, region);
    }

    /**
     * [MAIN ACTION 2]: Acquire a business permit tile.
     * @param politicCards list of politic cards to satisfy the balcony.
     * @param region where the balcony to satisfy is placed.
     * @param permitTileIndex first or second permit tile index.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void acquireBusinessPermitTile(List<PoliticCard> politicCards, String region, int permitTileIndex) throws NetworkException {
        mSocketProtocol.acquireBusinessPermitTile(politicCards, region, permitTileIndex);
    }

    /**
     * [MAIN ACTION 3]: Build an emporium with a business permit tile.
     * @param businessPermitTile to use to build the emporium.
     * @param city where the emporium should be built.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void buildEmporiumWithBusinessPermitTile(BusinessPermitTile businessPermitTile, String city) throws NetworkException {
        mSocketProtocol.buildEmporiumWithBusinessPermitTile(businessPermitTile, city);
    }

    /**
     * [MAIN ACTION 4]: Build an emporium with the king helps.
     * @param politicCards to use to satisfy the king's balcony.
     * @param cities which represent the track that the king should do for moving.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void buildEmporiumWithKingHelp(List<PoliticCard> politicCards, List<String> cities) throws NetworkException {
        mSocketProtocol.buildEmporiumWithKingHelp(politicCards, cities);
    }

    /**
     * [FAST ACTION 1]: Engage an assistant.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void engageAssistant() throws NetworkException {
        mSocketProtocol.engageAssistant();
    }

    /**
     * [FAST ACTION 2]: Change business permit tile of the given region.
     * @param region where business permit tiles should be changed.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void changeBusinessPermitTiles(String region) throws NetworkException {
        mSocketProtocol.changeBusinessPermitTiles(region);
    }

    /**
     * [FAST ACTION 3]: Send an assistant to elect a councillor in a given region's balcony.
     * @param councilor to add to the balcony.
     * @param region where the balcony where insert the councillor is placed.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void sendAssistantElectCouncillor(Councilor councilor, String region) throws NetworkException {
        mSocketProtocol.sendAssistantElectCouncillor(councilor, region);
    }

    /**
     * [FAST ACTION 4]: Perform an additional main action.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void performAdditionalMainAction() throws NetworkException {
        mSocketProtocol.performAdditionalMainAction();
    }

    /**
     * Earn first special reward.
     * @param cities where the player has built an emporium and want to retrieve the reward.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void earnFirstSpecialRewards(List<String> cities) throws NetworkException {
        mSocketProtocol.earnFirstSpecialRewards(cities);
    }

    /**
     * Earn second special reward.
     * @param regions list of region where the permit tiles are placed.
     * @param indices list of indices that represent the position of the visible permit tiles.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void earnSecondSpecialRewards(List<String> regions, List<Integer> indices) throws NetworkException {
        mSocketProtocol.earnSecondSpecialRewards(regions, indices);
    }

    /**
     * Earn third special reward.
     * @param businessPermitTiles list of player's business permit tiles.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles) throws NetworkException {
        mSocketProtocol.earnThirdSpecialRewards(businessPermitTiles);
    }

    /**
     * Sell a politic card on market.
     * @param politicCard to sell on market.
     * @param price for that politic card.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void sellPoliticCard(PoliticCard politicCard, int price) throws NetworkException {
        mSocketProtocol.sellPoliticCard(politicCard, price);
    }

    /**
     * Sell a business permit tile on market.
     * @param businessPermitTile to sell on market.
     * @param price for that business permit tile.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void sellBusinessPermitTile(BusinessPermitTile businessPermitTile, int price) throws NetworkException {
        mSocketProtocol.sellBusinessPermitTile(businessPermitTile, price);
    }

    /**
     * Sell an assistant on market.
     * @param price for that assistant.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void sellAssistant(int price) throws NetworkException {
        mSocketProtocol.sellAssistant(price);
    }

    /**
     * Buy a market item with the given market id.
     * @param marketId of the object to buy.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void buyItem(String marketId) throws NetworkException {
        mSocketProtocol.buyItem(marketId);
    }

    /**
     * Send a request for ending the current turn.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void endTurn() throws NetworkException {
        mSocketProtocol.endTurn();
    }

    /**
     * Send a chat message to other players or a specified player.
     * @param receiver nickname of the specific player if a private message, null if should be delivered to all room players.
     * @param message to deliver.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void sendChatMessage(String receiver, String message) throws NetworkException {
        mSocketProtocol.sendChatMessage(receiver, message);
    }

    /**
     * Start a new thread that will listen for all incoming messages on socket input stream and will process them
     * according to the defined socket protocol.
     */
    private void startNetworkMessageHandlerThread() {
        ResponseHandler responseHandler = new ResponseHandler();
        responseHandler.start();
    }

    /**
     * Internal thread that will listen on {@link #mInput inputStream} for server messages.
     */
    private class ResponseHandler extends Thread {

        @Override
        public void run() {
            while (true) {
                boolean quit = false;
                try {
                    Object object = mInput.readObject();
                    mSocketProtocol.handleResponse(object);
                } catch (ClassNotFoundException | IOException e) {
                    Debug.critical("Cannot read server response", e);
                    quit = true;
                }
                if (quit) {
                    break;
                }
            }
            closeSafely(mInput, "I/O error occurs when closing input stream");
            closeSafely(mOutput, "I/O error occurs when closing output stream");
            closeSafely(mSocket, "I/O error occurs when closing socket");
        }

        /**
         * Close safely the connection.
         * @param closeable object that implements {@link Closeable} interface.
         * @param message to print in case an exception is thrown while closing.
         */
        private void closeSafely(Closeable closeable, String message) {
            try {
                closeable.close();
            } catch (IOException e) {
                Debug.error(message, e);
            }
        }
    }
}
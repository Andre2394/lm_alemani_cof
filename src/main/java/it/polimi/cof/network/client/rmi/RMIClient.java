package it.polimi.cof.network.client.rmi;

import it.polimi.cof.model.*;
import it.polimi.cof.model.exceptions.*;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.network.NetworkException;
import it.polimi.cof.network.client.AbstractClient;
import it.polimi.cof.network.client.ClientConnectionException;
import it.polimi.cof.network.client.IClient;
import it.polimi.cof.network.exceptions.CreateRoomException;
import it.polimi.cof.network.exceptions.InvalidConfigurationException;
import it.polimi.cof.network.exceptions.JoinRoomException;
import it.polimi.cof.network.exceptions.LoginException;
import it.polimi.cof.network.protocol.ErrorCodes;
import it.polimi.cof.network.server.game.MarketTurn;
import it.polimi.cof.network.server.rmi.RMIServerInterface;
import it.polimi.cof.util.Debug;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 * This class is the implementation of {@link AbstractClient} class. It manages the network connection with RMI.
 */
public class RMIClient extends AbstractClient implements RMIPlayerInterface {

    /**
     * Debug constants.
     */
    private static final String DEBUG_ITEM_NOT_FOUND = "[rmi client] item not found";
    private static final String DEBUG_ITEM_ALREADY_ON_SALE = "[rmi client] item already on sale";
    private static final String DEBUG_ITEM_ALREADY_SOLD = "[rmi client] item already sold";
    private static final String DEBUG_NO_ENOUGH_COINS = "[rmi client] no enough coins";
    private static final String DEBUG_NO_ENOUGH_ASSISTANTS = "[rmi client] no enough assistants";
    private static final String DEBUG_CITY_NOT_VALID = "[rmi client] city not valid";
    private static final String DEBUG_CITY_NOT_FOUND = "[rmi client] city not found";
    private static final String DEBUG_ROUTE_NOT_VALID = "[rmi client] route not valid";
    private static final String DEBUG_EMPORIUM_ALREADY_BUILT = "[rmi client] emporium already built";
    private static final String DEBUG_PLAYER_NOT_FOUND = "[rmi client] player not found";
    private static final String DEBUG_BALCONY_UNSATISFIABLE = "[rmi client] balcony unsatisfiable";
    private static final String DEBUG_POLITIC_CARD_ALREADY_DRAWN = "[rmi client] politic card already drawn";
    private static final String DEBUG_POLITIC_CARD_NOT_YET_DRAWN = "[rmi client] politic card not yet drawn";
    private static final String DEBUG_MAIN_ACTION_NOT_AVAILABLE = "[rmi client] main action not available";
    private static final String DEBUG_FAST_ACTION_NOT_AVAILABLE = "[rmi client] fast action not available";
    private static final String DEBUG_NOT_YOUR_TURN = "[rmi client] not your turn";
    private static final String DEBUG_UNCAUGHT = "[rmi client] uncaught exception";

    /**
     * Remote interface of the client that will be cached by the server for server to client communication.
     */
    private RMIServerInterface mServer;

    /**
     * Cached session token that uniquely identify the player on the RMIServer.
     */
    private String mSessionToken;

    /**
     * Create a RMI client instance.
     * @param controller client controller.
     * @param address of the server.
     * @param port of the server.
     */
    public RMIClient(IClient controller, String address, int port) {
        super(controller, address, port);
    }

    /**
     * Open a connection with {@link it.polimi.cof.network.server.rmi.RMIServer}.
     * @throws ClientConnectionException if server is not reachable or something went wrong.
     */
    @Override
    public void connect() throws ClientConnectionException {
        try {
            Registry registry = LocateRegistry.getRegistry(getAddress(), getPort());
            mServer = (RMIServerInterface) registry.lookup("RMIServerInterface");
            UnicastRemoteObject.exportObject(this, 0);
        } catch (RemoteException | NotBoundException e) {
            throw new ClientConnectionException(e);
        }
    }

    /**
     * Login player to RMIServer with provided nickname.
     * @param nickname to use for login.
     * @throws NetworkException if server is not reachable.
     */
    @Override
    public void loginPlayer(String nickname) throws NetworkException {
        try {
            mSessionToken = mServer.loginPlayer(nickname, this);
        } catch (LoginException e) {
            throw e;
        } catch (IOException e) {
            throw new NetworkException(e);
        }
    }

    /**
     * Try to join the first available room.
     * @throws JoinRoomException if no available room where join player has been found.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void joinFirstAvailableRoom() throws NetworkException {
        try {
            mServer.joinFirstAvailableRoom(mSessionToken);
        } catch (JoinRoomException e) {
            throw e;
        } catch (IOException e) {
            throw new NetworkException(e);
        }
    }

    /**
     * Try to create a new room on server side.
     * @param maxPlayers that should be accepted in this new room.
     * @throws CreateRoomException if another room has been created in the meanwhile.
     * @throws NetworkException if server is not reachable or something went wrong.
     * @return configuration bundle that contains all default configurations.
     */
    @Override
    public Configuration createNewRoom(int maxPlayers) throws NetworkException {
        try {
            return mServer.createNewRoom(mSessionToken, maxPlayers);
        } catch (CreateRoomException e) {
            throw e;
        } catch (IOException e) {
            throw new NetworkException(e);
        }
    }

    /**
     * Try to apply game configuration to the player room.
     * @param configuration that should be applied to the room.
     * @throws InvalidConfigurationException if configuration is not valid.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void applyGameConfiguration(Configuration configuration) throws NetworkException {
        try {
            mServer.applyGameConfiguration(mSessionToken, configuration);
        } catch (InvalidConfigurationException e) {
            throw e;
        } catch (IOException e) {
            throw new NetworkException(e);
        }
    }

    /**
     * Retrieve a list that contains all possible actions.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void getActionList() throws NetworkException {
        try {
            mServer.getActionList(mSessionToken);
        } catch (RemoteException e) {
            throw new NetworkException(e);
        } catch (NotYourTurnException e) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            getController().onActionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } catch (IOException e) {
            Debug.debug(DEBUG_UNCAUGHT, e);
            getController().onActionNotValid(ErrorCodes.ERROR_GENERIC_SERVER_ERROR);
        }
    }

    /**
     * Draw a politic card.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void drawPoliticCard() throws NetworkException {
        try {
            mServer.drawPoliticCard(mSessionToken);
        } catch (RemoteException e) {
            throw new NetworkException(e);
        } catch (PoliticCardAlreadyDrawn e) {
            Debug.debug(DEBUG_POLITIC_CARD_ALREADY_DRAWN, e);
            getController().onActionNotValid(ErrorCodes.ERROR_POLITIC_CARD_ALREADY_DRAWN);
        } catch (NotYourTurnException e) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            getController().onActionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } catch (IOException e) {
            Debug.debug(DEBUG_UNCAUGHT, e);
            getController().onActionNotValid(ErrorCodes.ERROR_GENERIC_SERVER_ERROR);
        }
    }

    /**
     * [MAIN ACTION 1]: Elect a councillor.
     * @param councilor to add to the balcony.
     * @param region where the balcony where insert the councillor is placed.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void electCouncillor(Councilor councilor, String region) throws NetworkException {
        try {
            mServer.electCouncillor(mSessionToken, councilor, region);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * [MAIN ACTION 2]: Acquire a business permit tile.
     * @param politicCards list of politic cards to satisfy the balcony.
     * @param region where the balcony to satisfy is placed.
     * @param permitTileIndex first or second permit tile index.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void acquireBusinessPermitTile(List<PoliticCard> politicCards, String region, int permitTileIndex) throws NetworkException {
        try {
            mServer.acquireBusinessPermitTile(mSessionToken, politicCards, region, permitTileIndex);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * [MAIN ACTION 3]: Build an emporium with a business permit tile.
     * @param businessPermitTile to use to build the emporium.
     * @param city where the emporium should be built.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void buildEmporiumWithBusinessPermitTile(BusinessPermitTile businessPermitTile, String city) throws NetworkException {
        try {
            mServer.buildEmporiumWithBusinessPermitTile(mSessionToken, businessPermitTile, city);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * [MAIN ACTION 4]: Build an emporium with the king helps.
     * @param politicCards to use to satisfy the king's balcony.
     * @param cities which represent the track that the king should do for moving.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void buildEmporiumWithKingHelp(List<PoliticCard> politicCards, List<String> cities) throws NetworkException {
        try {
            mServer.buildEmporiumWithKingHelp(mSessionToken, politicCards, cities);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * [FAST ACTION 1]: Engage an assistant.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void engageAssistant() throws NetworkException {
        try {
            mServer.engageAssistant(mSessionToken);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * [FAST ACTION 2]: Change business permit tile of the given region.
     * @param region where business permit tiles should be changed.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void changeBusinessPermitTiles(String region) throws NetworkException {
        try {
            mServer.changeBusinessPermitTiles(mSessionToken, region);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * [FAST ACTION 3]: Send an assistant to elect a councillor in a given region's balcony.
     * @param councilor to add to the balcony.
     * @param region where the balcony where insert the councillor is placed.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void sendAssistantElectCouncillor(Councilor councilor, String region) throws NetworkException {
        try {
            mServer.sendAssistantElectCouncillor(mSessionToken, councilor, region);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * [FAST ACTION 4]: Perform an additional main action.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void performAdditionalMainAction() throws NetworkException {
        try {
            mServer.performAdditionalMainAction(mSessionToken);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * Earn first special reward.
     * @param cities where the player has built an emporium and want to retrieve the reward.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void earnFirstSpecialRewards(List<String> cities) throws NetworkException {
        try {
            mServer.earnFirstSpecialRewards(mSessionToken, cities);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * Earn second special reward.
     * @param regions list of region where the permit tiles are placed.
     * @param indices list of indices that represent the position of the visible permit tiles.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void earnSecondSpecialRewards(List<String> regions, List<Integer> indices) throws NetworkException {
        try {
            mServer.earnSecondSpecialRewards(mSessionToken, regions, indices);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * Earn third special reward.
     * @param businessPermitTiles list of player's business permit tiles.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles) throws NetworkException {
        try {
            mServer.earnThirdSpecialRewards(mSessionToken, businessPermitTiles);
        } catch (IOException e) {
            handleBaseActionException(e);
        }
    }

    /**
     * Handle every exception that can be thrown while doing a main / fast action.
     * @param e exception to handle.
     * @throws NetworkException if exception is an instance of RemoteException.
     */
    private void handleBaseActionException(IOException e) throws NetworkException {
        if (e instanceof RemoteException) {
            throw new NetworkException(e);
        } else if (e instanceof NotYourTurnException) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            getController().onActionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } else if (e instanceof PoliticCardNotYetDrawn) {
            Debug.debug(DEBUG_POLITIC_CARD_NOT_YET_DRAWN, e);
            getController().onActionNotValid(ErrorCodes.ERROR_POLITIC_CARD_NOT_YET_DRAWN);
        } else if (e instanceof MainActionNotAvailable) {
            Debug.debug(DEBUG_MAIN_ACTION_NOT_AVAILABLE, e);
            getController().onActionNotValid(ErrorCodes.ERROR_MAIN_ACTION_NOT_AVAILABLE);
        } else if (e instanceof FastActionNotAvailable) {
            Debug.debug(DEBUG_FAST_ACTION_NOT_AVAILABLE, e);
            getController().onActionNotValid(ErrorCodes.ERROR_FAST_ACTION_NOT_AVAILABLE);
        } else if (e instanceof LogicException) {
            handleActionLogicException((LogicException) e);
        } else {
            Debug.debug(DEBUG_UNCAUGHT, e);
            getController().onActionNotValid(ErrorCodes.ERROR_GENERIC_SERVER_ERROR);
        }
    }

    /**
     * Handle every possible logic exception that can be thrown while doing a main / fast action.
     * @param e logic exception to handle.
     */
    private void handleActionLogicException(LogicException e) {
        if (e instanceof BalconyUnsatisfiable) {
            Debug.debug(DEBUG_BALCONY_UNSATISFIABLE, e);
            getController().onActionNotValid(ErrorCodes.ERROR_NO_POLITIC_CARD_CAN_SATISFY_REGION_BALCONY);
        } else if (e instanceof NotEnoughCoins) {
            Debug.debug(DEBUG_NO_ENOUGH_COINS, e);
            getController().onActionNotValid(ErrorCodes.ERROR_NOT_ENOUGH_COINS);
        } else if (e instanceof NotEnoughAssistants) {
            Debug.debug(DEBUG_NO_ENOUGH_ASSISTANTS, e);
            getController().onActionNotValid(ErrorCodes.ERROR_NOT_ENOUGH_ASSISTANTS);
        } else if (e instanceof CityNotFound) {
            Debug.debug(DEBUG_CITY_NOT_FOUND, e);
            getController().onActionNotValid(ErrorCodes.ERROR_CITY_NOT_FOUND);
        } else if (e instanceof CityNotValid) {
            Debug.debug(DEBUG_CITY_NOT_VALID, e);
            getController().onActionNotValid(ErrorCodes.ERROR_CITY_NOT_VALID);
        } else if (e instanceof EmporiumAlreadyBuilt) {
            Debug.debug(DEBUG_EMPORIUM_ALREADY_BUILT, e);
            getController().onActionNotValid(ErrorCodes.ERROR_EMPORIUM_ALREADY_BUILT);
        } else if (e instanceof RouteNotValid) {
            Debug.debug(DEBUG_ROUTE_NOT_VALID, e);
            getController().onActionNotValid(ErrorCodes.ERROR_ROUTE_NOT_VALID);
        }
    }

    /**
     * Sell a politic card on market.
     * @param politicCard to sell on market.
     * @param price for that politic card.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void sellPoliticCard(PoliticCard politicCard, int price) throws NetworkException {
        try {
            mServer.sellPoliticCard(mSessionToken, politicCard, price);
        } catch (IOException e) {
            handleMarketSellException(e);
        }
    }

    /**
     * Sell a business permit tile on market.
     * @param businessPermitTile to sell on market.
     * @param price for that business permit tile.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void sellBusinessPermitTile(BusinessPermitTile businessPermitTile, int price) throws NetworkException {
        try {
            mServer.sellBusinessPermitTile(mSessionToken, businessPermitTile, price);
        } catch (IOException e) {
            handleMarketSellException(e);
        }
    }

    /**
     * Sell an assistant on market.
     * @param price for that assistant.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void sellAssistant(int price) throws NetworkException {
        try {
            mServer.sellAssistant(mSessionToken, price);
        } catch (IOException e) {
            handleMarketSellException(e);
        }
    }

    /**
     * Handle every possible exception thrown while selling a market item.
     * @param e exception to handle.
     * @throws NetworkException if e is an instance of RemoteException.
     */
    private void handleMarketSellException(IOException e) throws NetworkException {
        if (e instanceof RemoteException) {
            throw new NetworkException(e);
        } else if (e instanceof ItemNotFound) {
            Debug.debug(DEBUG_ITEM_NOT_FOUND, e);
            getController().onActionNotValid(ErrorCodes.ERROR_MARKET_ITEM_NOT_FOUND);
        } else if (e instanceof ItemAlreadyOnSale) {
            Debug.debug(DEBUG_ITEM_ALREADY_ON_SALE, e);
            getController().onActionNotValid(ErrorCodes.ERROR_MARKET_ITEM_ALREADY_ON_SALE);
        } else if (e instanceof NotYourTurnException) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            getController().onActionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } else {
            Debug.debug(DEBUG_UNCAUGHT, e);
            getController().onActionNotValid(ErrorCodes.ERROR_GENERIC_SERVER_ERROR);
        }
    }

    /**
     * Buy a market item with the given market id.
     * @param marketId of the object to buy.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void buyItem(String marketId) throws NetworkException {
        try {
            mServer.buyItem(mSessionToken, marketId);
        } catch (RemoteException e) {
            throw new NetworkException(e);
        } catch (ItemNotFound e) {
            Debug.debug(DEBUG_ITEM_NOT_FOUND, e);
            getController().onActionNotValid(ErrorCodes.ERROR_MARKET_ITEM_NOT_FOUND);
        } catch (ItemAlreadySold e) {
            Debug.debug(DEBUG_ITEM_ALREADY_SOLD, e);
            getController().onActionNotValid(ErrorCodes.ERROR_MARKET_ITEM_ALREADY_SOLD);
        } catch (NotEnoughCoins e) {
            Debug.debug(DEBUG_NO_ENOUGH_COINS, e);
            getController().onActionNotValid(ErrorCodes.ERROR_NOT_ENOUGH_COINS);
        } catch (NotYourTurnException e) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            getController().onActionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } catch (IOException e) {
            Debug.debug(DEBUG_UNCAUGHT, e);
            getController().onActionNotValid(ErrorCodes.ERROR_GENERIC_SERVER_ERROR);
        }
    }

    /**
     * Send a request for ending the current turn.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void endTurn() throws NetworkException {
        try {
            mServer.endTurn(mSessionToken);
        } catch (RemoteException e) {
            throw new NetworkException(e);
        } catch (NotYourTurnException e) {
            Debug.debug(DEBUG_NOT_YOUR_TURN, e);
            getController().onActionNotValid(ErrorCodes.ERROR_NOT_PLAYER_TURN);
        } catch (IOException e) {
            Debug.debug(DEBUG_UNCAUGHT, e);
            getController().onActionNotValid(ErrorCodes.ERROR_GENERIC_SERVER_ERROR);
        }
    }

    /**
     * Send a chat message to other players or a specified player.
     * @param receiver nickname of the specific player if a private message, null if should be delivered to all room players.
     * @param message to deliver.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    @Override
    public void sendChatMessage(String receiver, String message) throws NetworkException {
        try {
            mServer.sendChatMessage(mSessionToken, receiver, message);
        } catch (RemoteException e) {
            throw new NetworkException(e);
        } catch (PlayerNotFound e) {
            Debug.debug(DEBUG_PLAYER_NOT_FOUND, e);
            getController().onActionNotValid(ErrorCodes.ERROR_CHAT_PLAYER_NOT_FOUND);
        } catch (IOException e) {
            Debug.debug(DEBUG_UNCAUGHT, e);
            getController().onActionNotValid(ErrorCodes.ERROR_GENERIC_SERVER_ERROR);
        }
    }

    /**
     * Dispatch game session to the remote player.
     * @param gameSession to dispatch the player.
     * @throws RemoteException if player is not reachable from the server
     */
    @Override
    public void setGameSession(BaseGame gameSession) throws RemoteException {
        getController().onGameStarted(gameSession);
    }

    /**
     * Notify player that a new game turn is started.
     * @param nickname of the player that is starting the turn.
     * @param seconds that the player has to make the actions.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyGameTurnStarted(String nickname, int seconds) throws RemoteException {
        getController().onTurnStarted(nickname, seconds);
    }

    /**
     * Notify player of remaining seconds to make the turn actions.
     * @param remainingSeconds remaining time in seconds to make the actions.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyTurnCountdownUpdated(int remainingSeconds) throws RemoteException {
        getController().onTurnUpdateCountdown(remainingSeconds);
    }

    /**
     * Notify player that a politic card has been drawn.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyPoliticCardDrawn(UpdateState updateState) throws RemoteException {
        getController().onDrawnPoliticCard(updateState);
    }

    /**
     * Notify player that requested action list is available.
     * @param actionList list of all available actions for the player.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyActionListReady(ActionList actionList) throws RemoteException {
        getController().onActionList(actionList);
    }

    /**
     * Notify player that someone has elected a councillor.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyCouncillorElected(UpdateState updateState) throws RemoteException {
        getController().onActionElectCouncillor(updateState);
    }

    /**
     * Notify player that someone has acquired a business permit tile.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyBusinessPermitTileAcquired(UpdateState updateState) throws RemoteException {
        getController().onActionAcquireBusinessPermitTile(updateState);
    }

    /**
     * Notify player that someone has built an emporium using a business permit tile.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyEmporiumBuiltWithBusinessPermitTile(UpdateState updateState) throws RemoteException {
        getController().onActionBuildEmporiumWithBusinessPermitTile(updateState);
    }

    /**
     * Notify player that someone has built an emporium with king helps.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyEmporiumBuiltWithKingHelp(UpdateState updateState) throws RemoteException {
        getController().onActionBuildEmporiumWithKingHelp(updateState);
    }

    /**
     * Notify player that someone has engaged an assistant.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyAssistantEngaged(UpdateState updateState) throws RemoteException {
        getController().onActionEngageAssistant(updateState);
    }

    /**
     * Notify player that someone has changed the business permit tiles of a region.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyBusinessPermitTilesChanged(UpdateState updateState) throws RemoteException {
        getController().onActionChangeBusinessPermitTiles(updateState);
    }

    /**
     * Notify player that someone has sent an assistant to elect a councillor.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyAssistantSentToElectCouncillor(UpdateState updateState) throws RemoteException {
        getController().onActionSendAssistantToElectCouncillor(updateState);
    }

    /**
     * Notify player that someone has obtained an additional main action.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyAdditionalMainActionGranted(UpdateState updateState) throws RemoteException {
        getController().onActionPerformAdditionalMainAction(updateState);
    }

    /**
     * Notify player that someone has earned one or more first special rewards.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyFirstSpecialRewardsEarned(UpdateState updateState) throws RemoteException {
        getController().onFirstSpecialRewardsEarned(updateState);
    }

    /**
     * Notify player that someone has earned one or more second special rewards.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifySecondSpecialRewardsEarned(UpdateState updateState) throws RemoteException {
        getController().onSecondSpecialRewardsEarned(updateState);
    }

    /**
     * Notify player that someone has earned one or more third special rewards.
     * @param updateState bundle with updated contents.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyThirdSpecialRewardsEarned(UpdateState updateState) throws RemoteException {
        getController().onThirdSpecialRewardsEarned(updateState);
    }

    /**
     * Notify player that market session is started.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyMarketSessionStarted() throws RemoteException {
        getController().onMarketSessionStarted();
    }

    /**
     * Notify player that a new market turn is started.
     * @param nickname of the player that is starting the turn.
     * @param seconds that the player has to make the actions.
     * @param mode of the market turn.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyMarketTurnStarted(String nickname, int seconds, MarketTurn.Mode mode) throws RemoteException {
        switch (mode) {
            case SELL:
                getController().onMarketSellTurnStarted(nickname, seconds);
                break;
            case BUY:
                getController().onMarketBuyTurnStarted(nickname, seconds);
                break;
            default:
                Debug.debug("[rmi client] unknown market turn mode");
                break;
        }
    }

    /**
     * Notify player that a new item has been added to the market.
     * @param item that has been added to the market.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyMarketItemAddedOnSale(Item item) throws RemoteException {
        getController().onMarketItemAdded(item);
    }

    /**
     * Notify player that a market item has been bought.
     * @param nickname of the player that bought the item.
     * @param marketId id of the market item that has been bought.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyMarketItemBought(String nickname, String marketId) throws RemoteException {
        getController().onMarketItemBought(marketId, nickname);
    }

    /**
     * Notify player that market session is finished.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyMarketSessionFinished() throws RemoteException {
        getController().onMarketSessionFinished();
    }

    /**
     * Notify player that a new chat message has been received.
     * @param author nickname of the player that sent the message.
     * @param message that the author has sent.
     * @param privateMessage if message is private, false if public.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyNewChatMessage(String author, String message, boolean privateMessage) throws RemoteException {
        getController().onChatMessage(privateMessage, author, message);
    }

    /**
     * Notify player that another player has disconnected.
     * @param nickname of the player that has disconnected.
     * @throws RemoteException if player is not reachable from the server.
     */
    @Override
    public void notifyPlayerDisconnected(String nickname) throws RemoteException {
        getController().onPlayerDisconnected(nickname);
    }

    /**
     * Notify player that a player has built all his emporiums and the last turn is starting.
     * @param nickname of the player has built all his emporiums
     * @throws RemoteException if client is not reachable.
     */
    @Override
    public void notifyLastTurnStarted(String nickname) throws RemoteException {
        getController().onLastTurnStarted(nickname);
    }

    /**
     * Notify player that the game is finished and dispatch all last update of all players.
     * @param updateStates of all players (bonus and end-game rewards).
     * @param ranking list of players nickname sorted by winner to loser.
     * @throws RemoteException if client is not reachable.
     */
    @Override
    public void notifyGameEnded(UpdateState[] updateStates, List<String> ranking) throws RemoteException {
        getController().onGameEnded(updateStates, ranking);
    }
}
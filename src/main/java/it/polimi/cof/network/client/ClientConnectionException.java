package it.polimi.cof.network.client;

import java.io.IOException;

/**
 * This exception is thrown when an error occur while connecting to the server.
 */
public class ClientConnectionException extends IOException {

    /**
     * Base constructor.
     * @param cause of the exception.
     */
    public ClientConnectionException(Throwable cause) {
        super(cause);
    }
}
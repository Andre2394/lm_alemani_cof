package it.polimi.cof.network;

/**
 * This enum represent all supported Network types.
 */
public enum NetworkType {

    /**
     * Socket
     */
    SOCKET,

    /**
     * Remote Method Invocation (RMI)
     */
    RMI
}
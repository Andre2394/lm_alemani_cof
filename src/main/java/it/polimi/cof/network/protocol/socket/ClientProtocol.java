package it.polimi.cof.network.protocol.socket;

import it.polimi.cof.model.*;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.network.NetworkException;
import it.polimi.cof.network.client.IClient;
import it.polimi.cof.network.exceptions.CreateRoomException;
import it.polimi.cof.network.exceptions.InvalidConfigurationException;
import it.polimi.cof.network.exceptions.JoinRoomException;
import it.polimi.cof.network.exceptions.LoginException;
import it.polimi.cof.network.server.game.MarketTurn;
import it.polimi.cof.util.Debug;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * This class is used to define the Socket protocol for communicating with server trough Sockets.
 * The methods here must communicate with {@link ServerProtocol}.
 */
public class ClientProtocol {

    /**
     * Generic debug tag.
     */
    private static final String DEBUG_PROTOCOL_EXCEPTION = "Exception while handling server message";

    /**
     * Input stream used to read server responses.
     */
    private final ObjectInputStream mInput;

    /**
     * Output stream used to send messages to server.
     */
    private final ObjectOutputStream mOutput;

    /**
     * Interface used as callback to communicate with the client.
     */
    private final IClient mCallback;

    /**
     * Object used as mutex to ensure that two threads never send a message over output stream concurrently.
     * (Only one thread can write on output stream).
     */
    private static final Object OUTPUT_MUTEX = new Object();

    /**
     * Map of all defined server responses headers.
     */
    private final HashMap<Object, ResponseHandler> mResponseMap;

    /**
     * Base constructor.
     * @param input stream from the socket.
     * @param output stream from the socket.
     * @param callback used to communicate with the client.
     */
    public ClientProtocol(ObjectInputStream input, ObjectOutputStream output, IClient callback) {
        mInput = input;
        mOutput = output;
        mCallback = callback;
        mResponseMap = new HashMap<>();
        loadResponses();
    }

    /**
     * Load all possible responses and associate an handler.
     */
    private void loadResponses() {
        mResponseMap.put(ProtocolConstants.ACTION_NOT_VALID, this::actionNotValid);
        mResponseMap.put(ProtocolConstants.GAME_STARTED, this::gameStarted);
        mResponseMap.put(ProtocolConstants.GAME_TURN_STARTED, this::gameTurnStarted);
        mResponseMap.put(ProtocolConstants.COUNTDOWN_UPDATED, this::countdownUpdated);
        mResponseMap.put(ProtocolConstants.POLITIC_CARD_DRAWN, this::politicCardDrawn);
        mResponseMap.put(ProtocolConstants.ACTION_LIST, this::actionList);
        mResponseMap.put(ProtocolConstants.COUNCILLOR_ELECTED, this::councillorElected);
        mResponseMap.put(ProtocolConstants.BUSINESS_PERMIT_TILE_ACQUIRED, this::businessPermitTileAcquired);
        mResponseMap.put(ProtocolConstants.EMPORIUM_BUILT_WITH_BUSINESS_PERMIT_TILE, this::emporiumBuiltWithBusinessPermitTile);
        mResponseMap.put(ProtocolConstants.EMPORIUM_BUILT_WITH_KING_HELP, this::emporiumBuiltWithKingHelp);
        mResponseMap.put(ProtocolConstants.ASSISTANT_ENGAGED, this::assistantEngaged);
        mResponseMap.put(ProtocolConstants.BUSINESS_PERMIT_TILES_CHANGED, this::businessPermitTilesChanged);
        mResponseMap.put(ProtocolConstants.ASSISTANT_SENT_TO_ELECT_COUNCILLOR, this::assistantSentToElectCouncillor);
        mResponseMap.put(ProtocolConstants.ADDITIONAL_MAIN_ACTION_GRANTED, this::additionalMainActionGranted);
        mResponseMap.put(ProtocolConstants.FIRST_SPECIAL_REWARD_EARNED, this::firstSpecialRewardsEarned);
        mResponseMap.put(ProtocolConstants.SECOND_SPECIAL_REWARD_EARNED, this::secondSpecialRewardsEarned);
        mResponseMap.put(ProtocolConstants.THIRD_SPECIAL_REWARD_EARNED, this::thirdSpecialRewardsEarned);
        mResponseMap.put(ProtocolConstants.MARKET_SESSION_STARTED, this::marketSessionStarted);
        mResponseMap.put(ProtocolConstants.MARKET_TURN_STARTED, this::marketTurnStarted);
        mResponseMap.put(ProtocolConstants.MARKET_ITEM_ADDED_ON_SALE, this::marketItemAddedOnSale);
        mResponseMap.put(ProtocolConstants.MARKET_ITEM_BOUGHT, this::marketItemBought);
        mResponseMap.put(ProtocolConstants.MARKET_SESSION_FINISHED, this::marketSessionFinished);
        mResponseMap.put(ProtocolConstants.CHAT_MESSAGE, this::onChatMessage);
        mResponseMap.put(ProtocolConstants.PLAYER_DISCONNECTED, this::onPlayerDisconnected);
        mResponseMap.put(ProtocolConstants.LAST_TURN_STARTED, this::onLastTurnStarted);
        mResponseMap.put(ProtocolConstants.GAME_ENDED, this::onGameEnded);
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void actionNotValid() {
        try {
            int errorCode = (int) mInput.readObject();
            mCallback.onActionNotValid(errorCode);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void gameStarted() {
        try {
            BaseGame baseGame = (BaseGame) mInput.readObject();
            mCallback.onGameStarted(baseGame);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void gameTurnStarted() {
        try {
            String nickname = (String) mInput.readObject();
            int seconds = (int) mInput.readObject();
            mCallback.onTurnStarted(nickname, seconds);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void countdownUpdated() {
        try {
            int remainingSeconds = (int) mInput.readObject();
            mCallback.onTurnUpdateCountdown(remainingSeconds);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void politicCardDrawn() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onDrawnPoliticCard(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void actionList() {
        try {
            ActionList actionList = (ActionList) mInput.readObject();
            mCallback.onActionList(actionList);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void councillorElected() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onActionElectCouncillor(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void businessPermitTileAcquired() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onActionAcquireBusinessPermitTile(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void emporiumBuiltWithBusinessPermitTile() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onActionBuildEmporiumWithBusinessPermitTile(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void emporiumBuiltWithKingHelp() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onActionBuildEmporiumWithKingHelp(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void assistantEngaged() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onActionEngageAssistant(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void businessPermitTilesChanged() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onActionChangeBusinessPermitTiles(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void assistantSentToElectCouncillor() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onActionSendAssistantToElectCouncillor(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void additionalMainActionGranted() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onActionPerformAdditionalMainAction(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void firstSpecialRewardsEarned() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onFirstSpecialRewardsEarned(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void secondSpecialRewardsEarned() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onSecondSpecialRewardsEarned(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void thirdSpecialRewardsEarned() {
        try {
            UpdateState updateState = (UpdateState) mInput.readObject();
            mCallback.onThirdSpecialRewardsEarned(updateState);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void marketSessionStarted() {
        mCallback.onMarketSessionStarted();
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void marketTurnStarted() {
        try {
            String nickname = (String) mInput.readObject();
            int seconds = (int) mInput.readObject();
            MarketTurn.Mode mode = (MarketTurn.Mode) mInput.readObject();
            switch (mode) {
                case SELL:
                    mCallback.onMarketSellTurnStarted(nickname, seconds);
                    break;
                case BUY:
                    mCallback.onMarketBuyTurnStarted(nickname, seconds);
                    break;
                default:
                    throw new IOException("Unknown market mode");
            }
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void marketItemAddedOnSale() {
        try {
            Item item = (Item) mInput.readObject();
            mCallback.onMarketItemAdded(item);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void marketItemBought() {
        try {
            String nickname = (String) mInput.readObject();
            String marketId = (String) mInput.readObject();
            mCallback.onMarketItemBought(marketId, nickname);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void marketSessionFinished() {
        mCallback.onMarketSessionFinished();
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void onChatMessage() {
        try {
            String author = (String) mInput.readObject();
            String message = (String) mInput.readObject();
            boolean privateMessage = (boolean) mInput.readObject();
            mCallback.onChatMessage(privateMessage, author, message);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void onPlayerDisconnected() {
        try {
            String nickname = (String) mInput.readObject();
            mCallback.onPlayerDisconnected(nickname);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void onLastTurnStarted() {
        try {
            String nickname = (String) mInput.readObject();
            mCallback.onLastTurnStarted(nickname);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void onGameEnded() {
        try {
            UpdateState[] updateStates = (UpdateState[]) mInput.readObject();
            String[] ranking = (String[]) mInput.readObject();
            mCallback.onGameEnded(updateStates, Arrays.asList(ranking));
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    /**
     * Blocking request to server for user login.
     * @param nickname to use for login.
     * @throws LoginException if provided nickname is already in use.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void loginPlayer(String nickname) throws NetworkException {
        int responseCode;
        try {
            mOutput.writeObject(ProtocolConstants.LOGIN_REQUEST);
            mOutput.writeObject(nickname);
            mOutput.flush();
            responseCode = (int) mInput.readObject();
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            throw new NetworkException(e);
        }
        if (responseCode == ProtocolConstants.RESPONSE_PLAYER_ALREADY_EXISTS) {
            throw new LoginException();
        }
    }

    /**
     * Blocking request to server for join user in the first available room.
     * @throws JoinRoomException if no available room has been found where join the player.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void joinFirstAvailableRoom() throws NetworkException {
        int responseCode;
        try {
            mOutput.writeObject(ProtocolConstants.JOIN_ROOM_REQUEST);
            mOutput.flush();
            responseCode = (int) mInput.readObject();
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            throw new NetworkException(e);
        }
        if (responseCode == ProtocolConstants.RESPONSE_NO_ROOM_AVAILABLE) {
            throw new JoinRoomException();
        }
    }

    /**
     * Blocking request to server for creating a new room.
     * @param maxPlayers that should be accepted in this new room.
     * @throws CreateRoomException if a new room has been created in the meanwhile and the player has been added.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public Configuration createNewRoom(int maxPlayers) throws NetworkException {
        Object response;
        try {
            mOutput.writeObject(ProtocolConstants.CREATE_ROOM_REQUEST);
            mOutput.writeObject(maxPlayers);
            mOutput.flush();
            response = mInput.readObject();
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            throw new NetworkException(e);
        }
        if (response instanceof Integer && (int) response == ProtocolConstants.RESPONSE_PLAYER_FORCE_JOINED) {
            throw new CreateRoomException();
        } else if (response instanceof Configuration){
            return (Configuration) response;
        }
        throw new NetworkException("Unknown response");
    }

    /**
     * Try to apply game configuration to the player room.
     * @param configuration that should be applied to the room.
     * @throws InvalidConfigurationException if configuration is not valid.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void applyConfiguration(Configuration configuration) throws NetworkException {
        int responseCode;
        String errorMessage;
        try {
            mOutput.writeObject(ProtocolConstants.APPLY_CONFIGURATION_REQUEST);
            mOutput.writeObject(configuration);
            mOutput.flush();
            responseCode = (int) mInput.readObject();
            errorMessage = (String) mInput.readObject();
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            throw new NetworkException(e);
        }
        if (responseCode == ProtocolConstants.RESPONSE_CONFIGURATION_NOT_VALID) {
            throw new InvalidConfigurationException(errorMessage);
        }
    }

    /**
     * Retrieve a list that contains all possible actions.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void getActionList() throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.GET_ACTION_LIST);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Draw a politic card.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void drawPoliticCard() throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.DRAW_POLITIC_CARD);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * [MAIN ACTION 1]: Elect a councillor.
     * @param councilor to add to the balcony.
     * @param region where the balcony where insert the councillor is placed.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void electCouncillor(Councilor councilor, String region) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.ELECT_COUNCILLOR);
                mOutput.writeObject(councilor);
                mOutput.writeObject(region);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * [MAIN ACTION 2]: Acquire a business permit tile.
     * @param politicCards list of politic cards to satisfy the balcony.
     * @param region where the balcony to satisfy is placed.
     * @param permitTileIndex first or second permit tile index.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void acquireBusinessPermitTile(List<PoliticCard> politicCards, String region, int permitTileIndex) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.ACQUIRE_BUSINESS_PERMIT_TILE);
                mOutput.writeObject(politicCards.toArray(new PoliticCard[politicCards.size()]));
                mOutput.writeObject(region);
                mOutput.writeObject(permitTileIndex);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * [MAIN ACTION 3]: Build an emporium with a business permit tile.
     * @param businessPermitTile to use to build the emporium.
     * @param city where the emporium should be built.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void buildEmporiumWithBusinessPermitTile(BusinessPermitTile businessPermitTile, String city) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.BUILD_EMPORIUM_WITH_BUSINESS_PERMIT_TILE);
                mOutput.writeObject(businessPermitTile);
                mOutput.writeObject(city);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * [MAIN ACTION 4]: Build an emporium with the king helps.
     * @param politicCards to use to satisfy the king's balcony.
     * @param cities which represent the track that the king should do for moving.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void buildEmporiumWithKingHelp(List<PoliticCard> politicCards, List<String> cities) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.BUILD_EMPORIUM_WITH_KING_HELP);
                mOutput.writeObject(politicCards.toArray(new PoliticCard[politicCards.size()]));
                mOutput.writeObject(cities.toArray(new String[cities.size()]));
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * [FAST ACTION 1]: Engage an assistant.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void engageAssistant() throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.ENGAGE_ASSISTANT);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * [FAST ACTION 2]: Change business permit tile of the given region.
     * @param region where business permit tiles should be changed.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void changeBusinessPermitTiles(String region) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.CHANGE_BUSINESS_PERMIT_TILES);
                mOutput.writeObject(region);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * [FAST ACTION 3]: Send an assistant to elect a councillor in a given region's balcony.
     * @param councilor to add to the balcony.
     * @param region where the balcony where insert the councillor is placed.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void sendAssistantElectCouncillor(Councilor councilor, String region) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.SEND_ASSISTANT_TO_ELECT_COUNCILLOR);
                mOutput.writeObject(councilor);
                mOutput.writeObject(region);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * [FAST ACTION 4]: Perform an additional main action.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void performAdditionalMainAction() throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.PERFORM_ADDITIONAL_MAIN_ACTION);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Earn first special reward.
     * @param cities where the player has built an emporium and want to retrieve the reward.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void earnFirstSpecialRewards(List<String> cities) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.EARN_FIRST_SPECIAL_REWARDS);
                mOutput.writeObject(cities.toArray(new String[cities.size()]));
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Earn second special reward.
     * @param regions list of region where the permit tiles are placed.
     * @param indices list of indices that represent the position of the visible permit tiles.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void earnSecondSpecialRewards(List<String> regions, List<Integer> indices) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.EARN_SECOND_SPECIAL_REWARDS);
                mOutput.writeObject(regions.toArray(new String[regions.size()]));
                mOutput.writeObject(indices.toArray(new Integer[indices.size()]));
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Earn third special reward.
     * @param businessPermitTiles list of player's business permit tiles.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void earnThirdSpecialRewards(List<BusinessPermitTile> businessPermitTiles) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.EARN_THIRD_SPECIAL_REWARDS);
                mOutput.writeObject(businessPermitTiles.toArray(new BusinessPermitTile[businessPermitTiles.size()]));
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Sell a politic card on market.
     * @param politicCard to sell on market.
     * @param price for that politic card.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void sellPoliticCard(PoliticCard politicCard, int price) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.SELL_POLITIC_CARD);
                mOutput.writeObject(politicCard);
                mOutput.writeObject(price);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Sell a business permit tile on market.
     * @param businessPermitTile to sell on market.
     * @param price for that business permit tile.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void sellBusinessPermitTile(BusinessPermitTile businessPermitTile, int price) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.SELL_BUSINESS_PERMIT_TILE);
                mOutput.writeObject(businessPermitTile);
                mOutput.writeObject(price);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Sell an assistant on market.
     * @param price for that assistant.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void sellAssistant(int price) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.SELL_ASSISTANT);
                mOutput.writeObject(price);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Buy a market item with the given market id.
     * @param marketId of the object to buy.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void buyItem(String marketId) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.BUY_ITEM);
                mOutput.writeObject(marketId);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Send a request for ending the current turn.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void endTurn() throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.END_TURN);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Send a chat message to other players or a specified player.
     * @param receiver nickname of the specific player if a private message, null if should be delivered to all room players.
     * @param message to deliver.
     * @throws NetworkException if server is not reachable or something went wrong.
     */
    public void sendChatMessage(String receiver, String message) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.CHAT_MESSAGE);
                mOutput.writeObject(receiver);
                mOutput.writeObject(message);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Handle the server response and execute the defined method.
     * @param object response header from server.
     */
    public void handleResponse(Object object) {
        ResponseHandler handler = mResponseMap.get(object);
        if (handler != null) {
            handler.handle();
        }
    }

    /**
     * This interface is used like {@link Runnable} interface.
     */
    @FunctionalInterface
    private interface ResponseHandler {

        /**
         * Handle the server response.
         */
        void handle();
    }
}
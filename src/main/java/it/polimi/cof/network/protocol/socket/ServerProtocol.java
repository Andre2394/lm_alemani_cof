package it.polimi.cof.network.protocol.socket;

import it.polimi.cof.model.*;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.network.NetworkException;
import it.polimi.cof.network.exceptions.CreateRoomException;
import it.polimi.cof.network.exceptions.InvalidConfigurationException;
import it.polimi.cof.network.exceptions.JoinRoomException;
import it.polimi.cof.network.exceptions.LoginException;
import it.polimi.cof.network.protocol.ErrorCodes;
import it.polimi.cof.network.server.game.MarketTurn;
import it.polimi.cof.util.Debug;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * This class is used to define the Socket protocol for communicating with clients trough Sockets.
 * The methods here must communicate with {@link ClientProtocol}.
 */
public class ServerProtocol {

    /**
     * Generic debug tag.
     */
    private static final String DEBUG_PROTOCOL_EXCEPTION = "Exception while handling client request";

    /**
     * Input stream used to read client requests.
     */
    private final ObjectInputStream mInput;

    /**
     * Output stream used to send messages to client.
     */
    private final ObjectOutputStream mOutput;

    /**
     * Interface used as callback to communicate with the server player.
     */
    private final ServerSocketProtocolInt mCallback;

    /**
     * Object used as mutex to ensure that two threads never send a message over output stream concurrently.
     * (Only one thread can write on output stream).
     */
    private static final Object OUTPUT_MUTEX = new Object();

    /**
     * Map of all defined client requests headers.
     */
    private final HashMap<Object, RequestHandler> mRequestMap;

    /**
     * Base constructor.
     * @param input stream from the socket.
     * @param output stream from the socket.
     * @param callback used to communicate with the server.
     */
    public ServerProtocol(ObjectInputStream input, ObjectOutputStream output, ServerSocketProtocolInt callback) {
        mInput = input;
        mOutput = output;
        mCallback = callback;
        mRequestMap = new HashMap<>();
        loadRequests();
    }

    /**
     * Load all possible requests and associate an handler.
     */
    private void loadRequests() {
        mRequestMap.put(ProtocolConstants.LOGIN_REQUEST, this::loginPlayer);
        mRequestMap.put(ProtocolConstants.JOIN_ROOM_REQUEST, this::joinRoom);
        mRequestMap.put(ProtocolConstants.CREATE_ROOM_REQUEST, this::createRoom);
        mRequestMap.put(ProtocolConstants.APPLY_CONFIGURATION_REQUEST, this::applyConfiguration);
        mRequestMap.put(ProtocolConstants.GET_ACTION_LIST, mCallback::sendActionList);
        mRequestMap.put(ProtocolConstants.DRAW_POLITIC_CARD, mCallback::drawPoliticCard);
        mRequestMap.put(ProtocolConstants.ELECT_COUNCILLOR, this::electCouncillor);
        mRequestMap.put(ProtocolConstants.ACQUIRE_BUSINESS_PERMIT_TILE, this::acquireBusinessPermitTile);
        mRequestMap.put(ProtocolConstants.BUILD_EMPORIUM_WITH_BUSINESS_PERMIT_TILE, this::buildEmporiumWithBusinessPermitTile);
        mRequestMap.put(ProtocolConstants.BUILD_EMPORIUM_WITH_KING_HELP, this::buildEmporiumWithKingHelp);
        mRequestMap.put(ProtocolConstants.ENGAGE_ASSISTANT, mCallback::engageAssistant);
        mRequestMap.put(ProtocolConstants.CHANGE_BUSINESS_PERMIT_TILES, this::changeBusinessPermitTiles);
        mRequestMap.put(ProtocolConstants.SEND_ASSISTANT_TO_ELECT_COUNCILLOR, this::sendAssistantElectCouncillor);
        mRequestMap.put(ProtocolConstants.PERFORM_ADDITIONAL_MAIN_ACTION, mCallback::performAdditionalMainAction);
        mRequestMap.put(ProtocolConstants.EARN_FIRST_SPECIAL_REWARDS, this::earnFirstSpecialRewards);
        mRequestMap.put(ProtocolConstants.EARN_SECOND_SPECIAL_REWARDS, this::earnSecondSpecialRewards);
        mRequestMap.put(ProtocolConstants.EARN_THIRD_SPECIAL_REWARDS, this::earnThirdSpecialRewards);
        mRequestMap.put(ProtocolConstants.SELL_POLITIC_CARD, this::sellPoliticCard);
        mRequestMap.put(ProtocolConstants.SELL_BUSINESS_PERMIT_TILE, this::sellBusinessPermitTile);
        mRequestMap.put(ProtocolConstants.SELL_ASSISTANT, this::sellAssistant);
        mRequestMap.put(ProtocolConstants.BUY_ITEM, this::buyItem);
        mRequestMap.put(ProtocolConstants.END_TURN, mCallback::endTurn);
        mRequestMap.put(ProtocolConstants.CHAT_MESSAGE, this::sendChatMessage);
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void loginPlayer() {
        try {
            String nickname = (String) mInput.readObject();
            loginPlayerAndRespond(nickname);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    private void loginPlayerAndRespond(String nickname) throws IOException {
        int responseCode;
        try {
            mCallback.loginPlayer(nickname);
            responseCode = ProtocolConstants.RESPONSE_OK;
        } catch (LoginException e) {
            Debug.debug("[socket protocol] LoginException", e);
            responseCode = ProtocolConstants.RESPONSE_PLAYER_ALREADY_EXISTS;
        }
        mOutput.writeObject(responseCode);
        mOutput.flush();
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void joinRoom() {
        try {
            joinRoomAndRespond();
        } catch (IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    private void joinRoomAndRespond() throws IOException {
        int responseCode;
        try {
            mCallback.joinRoom();
            responseCode = ProtocolConstants.RESPONSE_OK;
        } catch (JoinRoomException e) {
            Debug.debug("[socket protocol] JoinRoomException", e);
            responseCode = ProtocolConstants.RESPONSE_NO_ROOM_AVAILABLE;
        }
        mOutput.writeObject(responseCode);
        mOutput.flush();
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void createRoom() {
        try {
            int maxPlayers = (int) mInput.readObject();
            createRoomAndRespond(maxPlayers);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    private void createRoomAndRespond(int maxPlayers) throws IOException {
        Object response;
        try {
            response = mCallback.createRoom(maxPlayers);
        } catch (CreateRoomException e) {
            Debug.debug("[socket protocol] CreateRoomException", e);
            response = ProtocolConstants.RESPONSE_PLAYER_FORCE_JOINED;
        }
        mOutput.writeObject(response);
        mOutput.flush();
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void applyConfiguration() {
        try {
            Configuration configuration = (Configuration) mInput.readObject();
            applyConfigurationAndRespond(configuration);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    private void applyConfigurationAndRespond(Configuration configuration) throws IOException {
        int responseCode;
        String errorMessage;
        try {
            mCallback.applyConfiguration(configuration);
            responseCode = ProtocolConstants.RESPONSE_OK;
            errorMessage = null;
        } catch (InvalidConfigurationException e) {
            Debug.debug("[socket protocol] CreateRoomException", e);
            responseCode = ProtocolConstants.RESPONSE_CONFIGURATION_NOT_VALID;
            errorMessage = e.getMessage();
        }
        mOutput.writeObject(responseCode);
        mOutput.writeObject(errorMessage);
        mOutput.flush();
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void electCouncillor() {
        try {
            Councilor councilor = (Councilor) mInput.readObject();
            String region = (String) mInput.readObject();
            mCallback.electCouncillor(councilor, region);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void acquireBusinessPermitTile() {
        try {
            PoliticCard[] politicCards = (PoliticCard[]) mInput.readObject();
            String region = (String) mInput.readObject();
            int index = (int) mInput.readObject();
            mCallback.acquireBusinessPermitTile(Arrays.asList(politicCards), region, index);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void buildEmporiumWithBusinessPermitTile() {
        try {
            BusinessPermitTile businessPermitTile = (BusinessPermitTile) mInput.readObject();
            String city = (String) mInput.readObject();
            mCallback.buildEmporiumWithBusinessPermitTile(businessPermitTile, city);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void buildEmporiumWithKingHelp() {
        try {
            PoliticCard[] politicCards = (PoliticCard[]) mInput.readObject();
            String[] cities = (String[]) mInput.readObject();
            mCallback.buildEmporiumWithKingHelp(Arrays.asList(politicCards), Arrays.asList(cities));
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void changeBusinessPermitTiles() {
        try {
            String region = (String) mInput.readObject();
            mCallback.changeBusinessPermitTiles(region);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sendAssistantElectCouncillor() {
        try {
            Councilor councilor = (Councilor) mInput.readObject();
            String region = (String) mInput.readObject();
            mCallback.sendAssistantElectCouncillor(councilor, region);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void earnFirstSpecialRewards() {
        try {
            String[] cities = (String[]) mInput.readObject();
            mCallback.earnFirstSpecialRewards(Arrays.asList(cities));
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void earnSecondSpecialRewards() {
        try {
            String[] regions = (String[]) mInput.readObject();
            Integer[] indices = (Integer[]) mInput.readObject();
            mCallback.earnSecondSpecialRewards(Arrays.asList(regions), Arrays.asList(indices));
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void earnThirdSpecialRewards() {
        try {
            BusinessPermitTile[] businessPermitTiles = (BusinessPermitTile[]) mInput.readObject();
            mCallback.earnThirdSpecialRewards(Arrays.asList(businessPermitTiles));
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sellPoliticCard() {
        try {
            PoliticCard politicCard = (PoliticCard) mInput.readObject();
            int price = (int) mInput.readObject();
            mCallback.sellPoliticCard(politicCard, price);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sellBusinessPermitTile() {
        try {
            BusinessPermitTile businessPermitTile = (BusinessPermitTile) mInput.readObject();
            int price = (int) mInput.readObject();
            mCallback.sellBusinessPermitTile(businessPermitTile, price);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sellAssistant() {
        try {
            int price = (int) mInput.readObject();
            mCallback.sellAssistant(price);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void buyItem() {
        try {
            String marketId = (String) mInput.readObject();
            mCallback.buyItem(marketId);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    @java.lang.SuppressWarnings("squid:UnusedPrivateMethod")
    private void sendChatMessage() {
        try {
            String receiver = (String) mInput.readObject();
            String message = (String) mInput.readObject();
            mCallback.sendChatMessage(receiver, message);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            Debug.debug(DEBUG_PROTOCOL_EXCEPTION, e);
        }
    }

    /**
     * Notify the client that an error as occurred.
     * @param errorCode that identify the error. {@link ErrorCodes} for details.
     */
    public void actionNotValid(int errorCode) {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.ACTION_NOT_VALID);
                mOutput.writeObject(errorCode);
                mOutput.flush();
            } catch (IOException e) {
                Debug.debug("[socket protocol] Player is disconnected", e);
            }
        }
    }

    /**
     * Dispatch base game session to the player at the beginning of the match.
     * @param baseGame to dispatch to the player.
     */
    public void notifyGameStarted(BaseGame baseGame) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.GAME_STARTED);
                mOutput.writeObject(baseGame);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that a new game turn is started.
     * @param nickname of the player that is starting the turn.
     * @param seconds that the player has to make the actions.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyGameTurnStarted(String nickname, int seconds) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.GAME_TURN_STARTED);
                mOutput.writeObject(nickname);
                mOutput.writeObject(seconds);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player of remaining seconds to make the turn actions.
     * @param seconds remaining time in seconds to make the actions.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyTurnCountdownUpdated(int seconds) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.COUNTDOWN_UPDATED);
                mOutput.writeObject(seconds);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that a politic card has been drawn.
     * @param updateState to send to the player.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyPoliticCardDrawn(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.POLITIC_CARD_DRAWN);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Send action list to the player.
     * @param actionList that should be sent.
     * @throws NetworkException if client is not reachable.
     */
    public void sendActionList(ActionList actionList) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.ACTION_LIST);
                mOutput.writeObject(actionList);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has made a main action.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyCouncillorElected(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.COUNCILLOR_ELECTED);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has acquired a business permit tile.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyBusinessPermitTileAcquired(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.BUSINESS_PERMIT_TILE_ACQUIRED);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has built an emporium using a business permit tile.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyEmporiumBuiltWithPermitTile(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.EMPORIUM_BUILT_WITH_BUSINESS_PERMIT_TILE);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has built an emporium with king helps.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyEmporiumBuiltWithKingHelp(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.EMPORIUM_BUILT_WITH_KING_HELP);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has engaged an assistant.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyAssistantEngaged(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.ASSISTANT_ENGAGED);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has changed the business permit tiles of a region.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyBusinessPermitTilesChanged(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.BUSINESS_PERMIT_TILES_CHANGED);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has sent an assistant to elect a councillor.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyAssistantSentToElectCouncillor(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.ASSISTANT_SENT_TO_ELECT_COUNCILLOR);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has obtained an additional main action.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyAdditionalMainActionGranted(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.ADDITIONAL_MAIN_ACTION_GRANTED);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has earned one or more first special rewards.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyFirstSpecialRewardsEarned(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.FIRST_SPECIAL_REWARD_EARNED);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has earned one or more second special rewards.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifySecondSpecialRewardsEarned(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.SECOND_SPECIAL_REWARD_EARNED);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that someone has earned one or more third special rewards.
     * @param updateState that contains all changes to the server game state.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyThirdSpecialRewardsEarned(UpdateState updateState) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.THIRD_SPECIAL_REWARD_EARNED);
                mOutput.writeObject(updateState);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that market session is started.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyMarketSessionStarted() throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.MARKET_SESSION_STARTED);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that a new market turn is started.
     * @param nickname of the player that is starting the turn.
     * @param seconds that the player has to make the actions.
     * @param mode of the market turn.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyMarketTurnStarted(String nickname, int seconds, MarketTurn.Mode mode) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.MARKET_TURN_STARTED);
                mOutput.writeObject(nickname);
                mOutput.writeObject(seconds);
                mOutput.writeObject(mode);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that a new item has been added to the market.
     * @param item that has been added to the market.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyMarketItemAddedOnSale(Item item) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.MARKET_ITEM_ADDED_ON_SALE);
                mOutput.writeObject(item);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that a market item has been bought.
     * @param nickname of the player that bought the item.
     * @param marketId id of the market item that has been bought.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyMarketItemBought(String nickname, String marketId) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.MARKET_ITEM_BOUGHT);
                mOutput.writeObject(nickname);
                mOutput.writeObject(marketId);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that market session is finished.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyMarketSessionFinished() throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.MARKET_SESSION_FINISHED);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Send a chat message to the player.
     * @param author nickname of the player that sent the message.
     * @param message that the author has sent.
     * @param privateMessage if message is private, false if public.
     * @throws NetworkException if client is not reachable.
     */
    public void sendChatMessage(String author, String message, boolean privateMessage) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.CHAT_MESSAGE);
                mOutput.writeObject(author);
                mOutput.writeObject(message);
                mOutput.writeObject(privateMessage);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that another player has disconnected.
     * @param nickname of the disconnected player.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyPlayerDisconnected(String nickname) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.PLAYER_DISCONNECTED);
                mOutput.writeObject(nickname);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that a player has built all his emporiums and the last turn is starting.
     * @param nickname of the player has built all his emporiums
     * @throws NetworkException if client is not reachable.
     */
    public void notifyLastTurnStarted(String nickname) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.LAST_TURN_STARTED);
                mOutput.writeObject(nickname);
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Notify player that the game is finished and dispatch all last update of all players.
     * @param updateStates of all players (bonus and end-game rewards).
     * @param ranking list of players nickname sorted by winner to loser.
     * @throws NetworkException if client is not reachable.
     */
    public void notifyGameEnded(UpdateState[] updateStates, List<String> ranking) throws NetworkException {
        synchronized (OUTPUT_MUTEX) {
            try {
                mOutput.writeObject(ProtocolConstants.GAME_ENDED);
                mOutput.writeObject(updateStates);
                mOutput.writeObject(ranking.toArray(new String[ranking.size()]));
                mOutput.flush();
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        }
    }

    /**
     * Handle the client request and execute the defined method.
     * @param object request header from client.
     */
    public void handleClientRequest(Object object) {
        RequestHandler handler = mRequestMap.get(object);
        if (handler != null) {
            synchronized (OUTPUT_MUTEX) {
                handler.handle();
            }
        }
    }

    /**
     * This interface is used like {@link Runnable} interface.
     */
    @FunctionalInterface
    private interface RequestHandler {

        /**
         * Handle the client request.
         */
        void handle();
    }
}
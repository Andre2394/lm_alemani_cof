package it.polimi.cof.model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by andre on 10/07/2016.
 */
public class ActionListTest {

    @Test
    public void setPoliticCardDrawn() throws Exception {
        ActionList actionList = new ActionList();
        assertFalse(actionList.isPoliticCardDrawn());
        actionList.setPoliticCardDrawn();
        assertTrue(actionList.isPoliticCardDrawn());
    }

    @Test
    public void incrementMainActionCounter() throws Exception {
        ActionList actionList = new ActionList();
        assertTrue(actionList.getMainActionCount() == 1);
        actionList.incrementMainActionCounter();
        assertTrue(actionList.getMainActionCount() == 2);
    }

    @Test
    public void decrementMainActionCounter() throws Exception {
        ActionList actionList = new ActionList();
        assertTrue(actionList.getMainActionCount() == 1);
        actionList.decrementMainActionCounter();
        assertTrue(actionList.getMainActionCount() == 0);
    }

    @Test
    public void decrementFastActionCounter() throws Exception {
        ActionList actionList = new ActionList();
        assertTrue(actionList.getFastActionCount() == 1);
        actionList.decrementFastActionCounter();
        assertTrue(actionList.getFastActionCount() == 0);
    }
}
package it.polimi.cof.model;

import it.polimi.cof.model.exceptions.EmporiumAlreadyBuilt;
import it.polimi.cof.network.server.game.FakeRemotePlayer;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test on City class.
 */
public class CityTest {

    @Test
    public void createCity() throws Exception {
        City city = new City("Milan", "gold", 45, 78, null);
        assertTrue(city.getName().equals("Milan"));
        assertTrue(city.getType().equals("gold"));
        assertTrue(city.getPositionX() == 45);
        assertTrue(city.getPositionY() == 78);
        assertTrue(city.getReward() == null);
    }

    @Test
    public void addReward() throws Exception {
        City city = new City("Milan", "gold", 45, 78, null);
        assertTrue(city.getReward() == null);
        city.addReward(0, 0, 0, 0, 0, 0);
        assertTrue(city.getReward() == null);
        city.addReward(1, 2, 3, 4, 5, 6);
        assertTrue(city.getReward().generateFingerprint().equals(new Reward(1, 2, 3, 4, 5, 6).generateFingerprint()));
        city.addReward(2, 0, 0, 0, 0, 0);
        assertTrue(city.getReward().generateFingerprint().equals(new Reward(3, 2, 3, 4, 5, 6).generateFingerprint()));
    }

    @Test
    public void removeReward() throws Exception {
        City city = new City("Milan", "gold", 45, 78, null);
        assertTrue(city.getReward() == null);
        city.removeReward(2, 3, 4, 5, 1, 2);
        assertTrue(city.getReward() == null);
        city.addReward(1, 2, 3, 4, 5, 6);
        assertTrue(city.getReward().generateFingerprint().equals(new Reward(1, 2, 3, 4, 5, 6).generateFingerprint()));
        city.removeReward(0, 0, 0, 0, 0, 6);
        assertTrue(city.getReward().generateFingerprint().equals(new Reward(1, 2, 3, 4, 5, 0).generateFingerprint()));
        city.removeReward(1, 2, 3, 4, 5, 0);
        assertTrue(city.getReward() == null);
    }

    @Test
    public void buildEmporium() throws Exception {
        FakeRemotePlayer player = new FakeRemotePlayer("Napoleone");
        City city = new City("Milan", "gold", 45, 78, null);
        assertFalse(city.hasPlayerAnEmporium(player));
        city.buildEmporium(player);
        assertTrue(city.hasPlayerAnEmporium(player));
    }

    @Test(expected= EmporiumAlreadyBuilt.class)
    public void buildEmporiumFail() throws Exception {
        FakeRemotePlayer player = new FakeRemotePlayer("Napoleone");
        City city = new City("Milan", "gold", 45, 78, null);
        city.buildEmporiumSilently(player.getNickname());
        city.buildEmporium(player);
    }

    @Test
    public void buildEmporiumSilently() throws Exception {
        City city = new City("Milan", "gold", 45, 78, null);
        city.buildEmporiumSilently("Napoleone");
        assertTrue(city.getEmporiums().size() == 1);
        assertTrue(city.getEmporiums().contains("Napoleone"));
        assertFalse(city.getEmporiums().contains("Carlo Magno"));
        city.buildEmporiumSilently("Carlo Magno");
        assertTrue(city.getEmporiums().size() == 2);
        assertTrue(city.getEmporiums().contains("Napoleone"));
        assertTrue(city.getEmporiums().contains("Carlo Magno"));
        city.buildEmporiumSilently("Napoleone");
        assertTrue(city.getEmporiums().size() == 2);
    }

    @Test
    public void hasInitial() throws Exception {
        City city = new City("Milan", "gold", 45, 78, null);
        assertTrue(city.hasInitial(new char[] {'l', 'm', 'j'}));
        assertTrue(city.hasInitial(new char[] {'L', 'M', 'J'}));
        assertFalse(city.hasInitial(new char[] {'p', 'q'}));
        assertFalse(city.hasInitial(new char[] {'P', 'Q'}));
        assertFalse(city.hasInitial(new char[] {}));
    }
}
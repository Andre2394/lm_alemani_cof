package it.polimi.cof.model;

import it.polimi.cof.model.exceptions.CityNotValid;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * This test class will test business permit tile methods.
 */
public class BusinessPermitTileTest {

    @Test(expected = CityNotValid.class)
    public void validateCity() throws Exception {
        BusinessPermitTile tileA = new BusinessPermitTile(3, 2, 0, 1, 4, 2, 'f', 'g');
        tileA.validateCity(new City("", "gold", 30, 45, null));
    }

    @SuppressWarnings({"EqualsWithItself", "ObjectEqualsNull"})
    @Test
    public void equals() throws Exception {
        // create four tiles
        BusinessPermitTile tileA = new BusinessPermitTile(3, 2, 0, 1, 4, 2, 'f', 'g');
        BusinessPermitTile tileB = new BusinessPermitTile(0, 1, 0, 3, 2, 8, 'a');
        BusinessPermitTile tileC = new BusinessPermitTile(3, 2, 0, 1, 4, 2, 'f', 'g');
        BusinessPermitTile tileD = new BusinessPermitTile(3, 2, 0, 1, 4, 2, 'f', 'g');
        BusinessPermitTile tileE = new BusinessPermitTile(3, 2, 0, 1, 4, 2, 'f');
        BusinessPermitTile tileF = new BusinessPermitTile(3, 2, 0, 1, 4, 5, 'f', 'g');
        // 1) reflexive property
        assertTrue(tileA.equals(tileA));
        // 2) symmetric property
        assertTrue(tileA.equals(tileC) && tileC.equals(tileA));
        // 3) transitive property
        if (tileA.equals(tileC) && tileC.equals(tileD)) {
            assertTrue(tileA.equals(tileD));
        }
        // 4) consistency property
        assertTrue(tileA.equals(tileC) == tileA.equals(tileC));
        // 5) non-null property
        assertFalse(tileA.equals(null));
        // 6) comparing two different tiles
        assertFalse(tileA.equals(tileB));
        assertFalse(tileB.equals(tileA));
        assertFalse(tileD.equals(tileE));
        assertFalse(tileD.equals(tileF));
        // class check
        assertFalse(tileA.equals(new Reward(3, 2, 0, 1, 4, 2)));
    }

    @Test
    public void hashCodeTest() throws Exception {
        // create three business permit tiles
        BusinessPermitTile tileA = new BusinessPermitTile(3, 2, 0, 1, 4, 2, 'f', 'g');
        BusinessPermitTile tileB = new BusinessPermitTile(0, 1, 0, 3, 2, 8, 'a');
        BusinessPermitTile tileC = new BusinessPermitTile(3, 2, 0, 1, 4, 2, 'f', 'g');
        // equal objects should return the same hashcode
        if (tileA.equals(tileC)) {
            assertTrue(tileA.hashCode() == tileC.hashCode());
        }
        if (!tileA.equals(tileB)) {
            assertFalse(tileA.hashCode() == tileB.hashCode());
        }
    }

    @Test
    public void isValid() throws Exception {
        BusinessPermitTile businessPermitTile = new BusinessPermitTile(1, 0, 0, 0, 0, 0, 'a');
        assertTrue(businessPermitTile.isValid());
        businessPermitTile.removeAssistants(1);
        assertFalse(businessPermitTile.isValid());
        BusinessPermitTile businessPermitTile1 = new BusinessPermitTile(1, 2, 3, 4, 5, 6);
        assertFalse(businessPermitTile1.isValid());
        BusinessPermitTile businessPermitTile2 = new BusinessPermitTile(0, 0, 0, 0, 0, 0);
        assertFalse(businessPermitTile2.isValid());
    }
}
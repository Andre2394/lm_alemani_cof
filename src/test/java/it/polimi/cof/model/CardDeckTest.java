package it.polimi.cof.model;

import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * This test class will test card deck methods.
 */
public class CardDeckTest {

    @Test
    public void getLast() throws Exception {
        CardDeck<String> items = createInstance();
        items.add("Item0");
        items.add("Item1");
        items.add("Item2");
        assertTrue(items.getLast().equals("Item2"));
        assertTrue(getSize(items) == 3);
        assertTrue(items.getLast().equals("Item2"));
        assertTrue(getSize(items) == 3);
        assertTrue(items.getLast().equals("Item2"));
        assertTrue(getSize(items) == 3);
    }

    @Test
    public void takeLast() throws Exception {
        CardDeck<String> items = createInstance();
        items.add("Item0");
        items.add("Item1");
        items.add("Item2");
        items.add("Item1");
        items.add("Item2");
        assertTrue(items.takeLast().equals("Item2"));
        assertTrue(getSize(items) == 4);
        assertTrue(items.takeLast().equals("Item1"));
        assertTrue(getSize(items) == 3);
        assertTrue(items.takeLast().equals("Item2"));
        assertTrue(getSize(items) == 2);
        assertTrue(items.takeLast().equals("Item1"));
        assertTrue(getSize(items) == 1);
        assertTrue(items.takeLast().equals("Item0"));
        assertTrue(getSize(items) == 0);
        assertTrue(items.takeLast() == null);
    }

    @Test
    public void add() throws Exception {
        CardDeck<String> items = createInstance();
        items.add("Item0");
        items.add("Item1");
        items.add("Item2");
        assertTrue(getSize(items) == 3);
    }

    @Test
    public void addAtBottom() throws Exception {
        CardDeck<String> items = createInstance();
        items.add("Item1");
        items.add("Item2");
        assertTrue(getSize(items) == 2);
        items.addAtBottom("Item0");
        assertTrue(getSize(items) == 3);
        assertTrue(items.takeLast().equals("Item2"));
        assertTrue(items.takeLast().equals("Item1"));
        assertTrue(items.takeLast().equals("Item0"));
    }

    protected CardDeck<String> createInstance() {
        return new CardDeck<>();
    }

    /*package-local*/ int getSize(CardDeck<?> cardDeck) {
        Iterator<?> iterator = cardDeck.iterator();
        int size = 0;
        while (iterator.hasNext()) {
            size += 1;
            iterator.next();
        }
        return size;
    }
}
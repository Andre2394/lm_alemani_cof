package it.polimi.cof.model;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by andre on 10/07/2016.
 */
public class KingBoardTest {

    @Test
    public void takeKingReward() throws Exception {
        List<Reward> kingRewards = Arrays.asList(new Reward(1, 1, 1, 1, 1, 1), new Reward(2, 2, 2, 2, 2, 2));
        KingBoard kingBoard = new KingBoard(kingRewards, new HashMap<>());
        assertTrue(kingBoard.takeKingReward().generateFingerprint().equals(new Reward(2, 2, 2, 2, 2, 2).generateFingerprint()));
        assertTrue(kingBoard.takeKingReward().generateFingerprint().equals(new Reward(1, 1, 1, 1, 1, 1).generateFingerprint()));
        assertTrue(kingBoard.takeKingReward() == null);
    }

    @Test
    public void getNobilityTrack() throws Exception {
        Map<Integer, NobilityReward> nobilityTrack = new HashMap<>();
        nobilityTrack.put(1, new NobilityReward());
        nobilityTrack.put(44, new NobilityReward());
        KingBoard kingBoard = new KingBoard(new ArrayList<>(), nobilityTrack);
        assertTrue(kingBoard.getNobilityTrack().containsKey(1));
        assertTrue(kingBoard.getNobilityTrack().containsKey(44));
    }

}
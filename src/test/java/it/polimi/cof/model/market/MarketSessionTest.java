package it.polimi.cof.model.market;

import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.BusinessPermitTile;
import it.polimi.cof.model.PoliticCard;
import it.polimi.cof.model.exceptions.ItemAlreadyOnSale;
import it.polimi.cof.model.exceptions.ItemAlreadySold;
import it.polimi.cof.model.exceptions.ItemNotFound;
import it.polimi.cof.model.exceptions.NotEnoughCoins;
import it.polimi.cof.network.server.game.FakeRemotePlayer;
import it.polimi.cof.network.server.game.ServerGameTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * This test class will test all market's logic methods. It uses {@link FakeRemotePlayer} implementation to initialize
 * the game instance.
 */
public class MarketSessionTest {

    private BaseGame mBaseGame;
    private FakeRemotePlayer mPlayer1;
    private FakeRemotePlayer mPlayer2;

    @Before
    public void setUp() throws Exception {
        mPlayer1 = new FakeRemotePlayer("Player1");
        mPlayer1.addAssistants(10);
        mPlayer1.addCoins(20);
        mPlayer1.addPoliticCard(new PoliticCard("red"));
        mPlayer1.addPoliticCard(new PoliticCard());
        mPlayer2 = new FakeRemotePlayer("Player2");
        mPlayer2.addAssistants(15);
        mPlayer2.addCoins(10);
        mPlayer2.addPoliticCard(new PoliticCard("blue"));
        mPlayer2.addBusinessPermitTile(new BusinessPermitTile(2, 5, 0, 3, 0, 0, 'a', 'b'));
        mBaseGame = ServerGameTest.generateGameSession(mPlayer1, mPlayer2);
    }

    @Test
    public void isMarketFinished() throws Exception {
        // add some items to market session
        MarketSession marketSession = new MarketSession();
        Item item1 = marketSession.sellAssistant(mPlayer1, 3);
        Item item2 = marketSession.sellPoliticCard(mPlayer1, new PoliticCard("red"), 8);
        Item item3 = marketSession.sellBusinessPermitTile(mPlayer2, new BusinessPermitTile(2, 5, 0, 3, 0, 0, 'a', 'b'), 15);
        // make test
        assertFalse(marketSession.isMarketFinished());
        // check
        assertTrue(marketSession.getPoliticCards().size() == 1);
        assertTrue(marketSession.getBusinessPermitTiles().size() == 1);
        assertTrue(marketSession.getAssistants().size() == 1);
        assertTrue(marketSession.getItemsOnSale().size() == 3);
        assertTrue(marketSession.getItemsSold().size() == 0);
        assertTrue(marketSession.getAllItems().size() == 3);
        // buy
        marketSession.buyItem(mPlayer2, item1.getMarketId(), mBaseGame);
        marketSession.buyItem(mPlayer2, item2.getMarketId(), mBaseGame);
        marketSession.buyItem(mPlayer1, item3.getMarketId(), mBaseGame);
        // make test
        assertTrue(marketSession.isMarketFinished());
        // check
        assertTrue(marketSession.getPoliticCards().size() == 0);
        assertTrue(marketSession.getBusinessPermitTiles().size() == 0);
        assertTrue(marketSession.getAssistants().size() == 0);
        assertTrue(marketSession.getItemsOnSale().size() == 0);
        assertTrue(marketSession.getItemsSold().size() == 3);
        assertTrue(marketSession.getAllItems().size() == 3);
    }

    @Test(expected = ItemNotFound.class)
    public void itemNotFound() throws Exception {
        MarketSession marketSession = new MarketSession();
        marketSession.getItem("magicId");
    }

    @Test
    public void sellItem() throws Exception {
        MarketSession marketSession = new MarketSession();
        marketSession.sellPoliticCard(mPlayer1, new PoliticCard("red"), 3);
        assertTrue(marketSession.getItemsOnSale().size() == 1);
    }

    @Test(expected = ItemNotFound.class)
    public void sellPoliticCardNotFound() throws Exception {
        MarketSession marketSession = new MarketSession();
        mPlayer1.getPoliticCards().clear();
        marketSession.sellPoliticCard(mPlayer1, new PoliticCard("yellow"), 3);
    }

    @Test(expected = ItemNotFound.class)
    public void sellPermitTileNotFound() throws Exception {
        MarketSession marketSession = new MarketSession();
        mPlayer1.getBusinessPermitTiles().clear();
        marketSession.sellBusinessPermitTile(mPlayer1, new BusinessPermitTile(1, 1, 1, 1, 1, 1, 'd'), 3);
    }

    @Test(expected = ItemNotFound.class)
    public void sellAssistantNotFound() throws Exception {
        MarketSession marketSession = new MarketSession();
        mPlayer1.payAssistants(mPlayer1.getAssistants());
        marketSession.sellAssistant(mPlayer1, 3);
    }

    @Test(expected = ItemAlreadyOnSale.class)
    public void sellItemAlreadyOnSale() throws Exception {
        MarketSession marketSession = new MarketSession();
        marketSession.sellPoliticCard(mPlayer1, new PoliticCard("red"), 3);
        marketSession.sellPoliticCard(mPlayer1, new PoliticCard("red"), 4);
    }

    @Test
    public void buyItem() throws Exception {
        MarketSession marketSession = new MarketSession();
        marketSession.sellPoliticCard(mPlayer1, new PoliticCard("red"), 3);
        assertTrue(marketSession.getItemsOnSale().size() == 1);
        String marketId = marketSession.getItemsOnSale().get(0).getMarketId();
        marketSession.buyItem(mPlayer2, marketId, mBaseGame);
    }

    @Test(expected = ItemAlreadySold.class)
    public void buyItemAlreadySold() throws Exception {
        MarketSession marketSession = new MarketSession();
        marketSession.sellPoliticCard(mPlayer1, new PoliticCard("red"), 3);
        assertTrue(marketSession.getItemsOnSale().size() == 1);
        String marketId = marketSession.getItemsOnSale().get(0).getMarketId();
        marketSession.buyItem(mPlayer2, marketId, mBaseGame);
        marketSession.buyItem(mPlayer2, marketId, mBaseGame);
    }

    @Test(expected = NotEnoughCoins.class)
    public void buyItemWithNoEnoughCoins() throws Exception {
        MarketSession marketSession = new MarketSession();
        marketSession.sellPoliticCard(mPlayer1, new PoliticCard("red"), 300);
        assertTrue(marketSession.getItemsOnSale().size() == 1);
        String marketId = marketSession.getItemsOnSale().get(0).getMarketId();
        marketSession.buyItem(mPlayer2, marketId, mBaseGame);
    }

    @Test
    public void applyUpdate() throws Exception {
        MarketSession marketSession = new MarketSession();
        Item item = marketSession.sellPoliticCard(mPlayer1, new PoliticCard("red"), 300);
        item.setBought("Player2");
        marketSession.applyUpdate(item);
        assertTrue(marketSession.getItem(item.getMarketId()).equals(item));
    }

    @Test
    public void getItem() throws Exception {
        MarketSession marketSession = new MarketSession();
        Item item = marketSession.sellPoliticCard(mPlayer1, new PoliticCard("red"), 300);
        assertTrue(marketSession.getItemsOnSale().size() == 1);
        String marketId = marketSession.getItemsOnSale().get(0).getMarketId();
        assertTrue(marketSession.getItem(marketId).equals(item));
    }
}
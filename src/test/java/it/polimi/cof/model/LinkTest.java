package it.polimi.cof.model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by andre on 10/07/2016.
 */
public class LinkTest {

    @Test
    public void getCityFrom() throws Exception {
        Link link = new Link("Milano", "Genova");
        assertTrue(link.getCityFrom().equals("Milano"));
    }

    @Test
    public void getCityTo() throws Exception {
        Link link = new Link("Milano", "Genova");
        assertTrue(link.getCityTo().equals("Genova"));
    }

}
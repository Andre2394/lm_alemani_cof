package it.polimi.cof.model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by andre on 10/07/2016.
 */
public class ConfigurationTest {

    @Test
    public void genericTest() throws Exception {
        Configuration configuration = new Configuration(180, null, null, null, null, null);
        configuration.getLeftRegionBoards().clear();
        configuration.getCenterRegionBoards().clear();
        configuration.getRightRegionBoards().clear();
        assertTrue(configuration.getWaitingTime() == 180);
        assertTrue(configuration.getLeftRegionBoard() == null);
        assertTrue(configuration.getCenterRegionBoard() == null);
        assertTrue(configuration.getRightRegionBoard() == null);
    }
}
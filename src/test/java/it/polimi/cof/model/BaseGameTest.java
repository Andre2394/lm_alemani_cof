package it.polimi.cof.model;

import it.polimi.cof.model.exceptions.CityNotFound;
import it.polimi.cof.network.server.game.FakeRemotePlayer;
import it.polimi.cof.network.server.game.ServerGameTest;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by Guido on 01/07/2016.
 */
public class BaseGameTest {

    private FakeRemotePlayer mPlayer1;
    private FakeRemotePlayer mPlayer2;
    private BaseGame mBaseGame;

    @Before
    public void setUp() throws Exception {
        mPlayer1 = new FakeRemotePlayer("Napoleone");
        mPlayer2 = new FakeRemotePlayer("CarloMagno");
        mBaseGame = ServerGameTest.generateGameSession(mPlayer1, mPlayer2);
    }

    @Test
    public void getCouncillorsText() throws Exception {
        assertTrue(mBaseGame.mCouncilors == mBaseGame.getCouncilors());
    }

    @Test
    public void getPlayersTest() throws Exception {
        assertTrue(mBaseGame.getPlayers().size() == 2);
        assertTrue(mBaseGame.getPlayer("Napoleone") == mPlayer1);
    }

    @Test(expected = CityNotFound.class)
    public void getCityTest() throws Exception {
        mBaseGame.getCity("BustoArsizio");
    }

    @Test
    public void findCity() throws Exception {
        assertTrue(mBaseGame.findCity("Juvelar") != null);
        assertTrue(mBaseGame.findCity("BustoArsizio") == null);
    }

    @Test
    public void applyUpdate() throws Exception {
        UpdateState updateState = new UpdateState(mPlayer1.getNickname());
        updateState.addAssistants(20);
        updateState.addVictoryPoints(30);
        updateState.addNobilitySteps(4);
        updateState.addCoins(70);
        updateState.addPoliticCard(new PoliticCard("Blue"));
        updateState.addPoliticCard(new PoliticCard());
        updateState.addPoliticCard(new PoliticCard("Green"));
        updateState.addMainAction(1);
        updateState.addEmporium("Arkon");
        updateState.addBusinessPermitTile(new BusinessPermitTile(0, 1, 0, 0, 0, 0, 'f'));
        updateState.addReward(new Reward(1, 3, 0, 0, 0, 0));
        updateState.changeBusinessPermitTileAtIndex(new BusinessPermitTile(2, 0, 0, 5, 0, 3, '$'), "left", 1);
        updateState.changeCouncillor(new Councilor("unknown"), "right");
        updateState.changeKingCity("Esti");
        updateState.addKingReward(new Reward(1, 0, 0, 0, 4, 0));
        Map<String, Reward> bonus = new HashMap<>();
        bonus.put("gold", new Reward());
        bonus.put("iron", new Reward());
        updateState.addBonus(bonus);
        updateState.removeAssistants(10);
        updateState.removeCoins(56);
        updateState.removePoliticCards(Arrays.asList(new PoliticCard("Yellow"), new PoliticCard("Red")));
        updateState.removeBusinessPermitTile(new BusinessPermitTile(0, 2, 3, 0, 0, 0, 'f'));
        mPlayer1.payAssistants(mPlayer1.getAssistants());
        mPlayer1.payCoins(mPlayer1.getCoins());
        mPlayer1.getPoliticCards().clear();
        mPlayer1.addPoliticCard(new PoliticCard("Yellow"));
        mPlayer1.addPoliticCard(new PoliticCard("Green"));
        mPlayer1.addPoliticCard(new PoliticCard("Red"));
        mPlayer1.addBusinessPermitTile(new BusinessPermitTile(0, 2, 3, 0, 0, 0, 'f'));
        assertTrue(mPlayer1.getAssistants() == 0);
        assertTrue(mPlayer1.getVictoryPoints() == 0);
        assertTrue(mPlayer1.getNobilityTrackPosition() == 0);
        assertTrue(mPlayer1.getCoins() == 0);
        assertTrue(mPlayer1.getPoliticCards().size() == 3);
        assertTrue(mPlayer1.getBusinessPermitTiles().size() == 1);
        assertTrue(mPlayer1.getUsedBusinessPermitTiles().size() == 0);
        assertFalse(mBaseGame.findCity("Arkon").hasPlayerAnEmporium(mPlayer1));
        String[] balcony = mBaseGame.getRightBalconyColors();
        mBaseGame.applyUpdate(updateState);
        assertTrue(mPlayer1.getAssistants() == 10);
        assertTrue(mPlayer1.getVictoryPoints() == 30);
        assertTrue(mPlayer1.getNobilityTrackPosition() == 4);
        assertTrue(mPlayer1.getCoins() == 14);
        assertTrue(mPlayer1.getPoliticCards().size() == 4);
        assertTrue(mPlayer1.getBusinessPermitTiles().size() == 1);
        assertTrue(mPlayer1.getBusinessPermitTiles().contains(new BusinessPermitTile(0, 1, 0, 0, 0, 0, 'f')));
        assertTrue(mPlayer1.getUsedBusinessPermitTiles().size() == 1);
        assertTrue(mPlayer1.getUsedBusinessPermitTiles().contains(new BusinessPermitTile(0, 2, 3, 0, 0, 0, 'f')));
        assertTrue(mBaseGame.findCity("Arkon").hasPlayerAnEmporium(mPlayer1));
        assertTrue(mBaseGame.getLeftRegionFirstVisibleTile().equals(new BusinessPermitTile(2, 0, 0, 5, 0, 3, '$')));
        String[] newBalconyColors = new String[] {
                balcony[1],
                balcony[2],
                balcony[3],
                "unknown"
        };
        assertTrue(Arrays.equals(mBaseGame.getRightBalconyColors(), newBalconyColors));
        assertTrue(mBaseGame.getKingCityName().equals("Esti"));
        assertTrue(mPlayer1.getKingRewards().size() == 1);
        assertTrue(mPlayer1.getKingRewards().get(0).generateFingerprint().equals(new Reward(1, 0, 0, 0, 4, 0).generateFingerprint()));
        assertTrue(mPlayer1.getBonusCards().size() == 2);
        assertTrue(mPlayer1.getBonusCards().containsKey("gold"));
        assertTrue(mPlayer1.getBonusCards().containsKey("iron"));
    }

    @Test
    public void applyLittleUpdate() throws Exception {
        int coins = mPlayer2.getCoins();
        UpdateState updateState = new UpdateState(mPlayer2.getNickname());
        updateState.removeCoins(coins);
        mBaseGame.applyUpdate(updateState);
        assertTrue(mPlayer2.getCoins() == 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUnknownRegion() throws Exception {
        mBaseGame.getAbstractBoard("notARegion");
    }

    @Test
    public void addCouncilorTest() throws Exception {
        int initialSize = mBaseGame.getCouncilors().size();
        Councilor councilor = mBaseGame.getCouncilors().get(0);
        Councilor oldCouncilor = new Councilor(mBaseGame.getLeftRegionBoard().getBalconyColors()[0]);
        mBaseGame.addCouncilor(councilor, "left");
        assertTrue(mBaseGame.getCouncilors().get(mBaseGame.getCouncilors().size() - 1).equals(oldCouncilor));
        assertTrue(mBaseGame.getLeftRegionBoard().getBalconyColors()[3].equals(councilor.getColor()));
        assertTrue(mBaseGame.getCouncilors().size() == initialSize);
    }

    @Test(expected = CityNotFound.class)
    public void getNotSpecifiedKingCity() throws Exception {
        City city = mBaseGame.getKingCity();
        city.setKing(false);
        mBaseGame.getKingCity();
    }

    @Test
    public void getNotSpecifiedKingCityName() throws Exception {
        City city = mBaseGame.getKingCity();
        city.setKing(false);
        assertTrue(mBaseGame.getKingCityName().equals("unknown?"));
    }
}
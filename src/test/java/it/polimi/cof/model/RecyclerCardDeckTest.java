package it.polimi.cof.model;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

/**
 * This test class will test recycler card deck methods.
 */
public class RecyclerCardDeckTest extends CardDeckTest {

    @Test
    public void recycle() throws Exception {
        RecyclerCardDeck<String> cardDeck = new RecyclerCardDeck<>();
        cardDeck.add("Item0");
        cardDeck.add("Item1");
        cardDeck.add("Item2");
        assertTrue(getSize(cardDeck) == 3);
        assertTrue(cardDeck.takeLast().equals("Item2"));
        assertTrue(getSize(cardDeck) == 2);
        cardDeck.recycle(Arrays.asList("Item-recycled1", "Item-recycled2"));
        assertTrue(getSize(cardDeck) == 2);
        assertTrue(cardDeck.takeLast().equals("Item1"));
        assertTrue(getSize(cardDeck) == 1);
        assertTrue(cardDeck.takeLast().equals("Item0"));
        assertTrue(getSize(cardDeck) == 0);
        cardDeck.takeLast();
        assertTrue(getSize(cardDeck) == 1);
    }

    @Override
    protected CardDeck<String> createInstance() {
        return new RecyclerCardDeck<>();
    }

}
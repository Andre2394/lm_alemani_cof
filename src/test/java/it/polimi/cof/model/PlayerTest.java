package it.polimi.cof.model;

import it.polimi.cof.network.server.game.FakeRemotePlayer;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by andre on 10/07/2016.
 */
public class PlayerTest {

    @Test
    public void getColorTest() throws Exception {
        FakeRemotePlayer remotePlayer = new FakeRemotePlayer("Napoleone");
        remotePlayer.setColor(Color.RED);
        assertTrue(remotePlayer.getColor().equals(Color.RED));
    }

    @Test
    public void useBusinessPermitTilesTest() throws Exception {
        FakeRemotePlayer remotePlayer = new FakeRemotePlayer("Napoleone");
        assertTrue(remotePlayer.getBusinessPermitTiles().size() == 0);
        assertTrue(remotePlayer.getUsedBusinessPermitTiles().size() == 0);
        remotePlayer.addBusinessPermitTile(new BusinessPermitTile(5, 3, 5, 6, 3, 6, 'd'));
        remotePlayer.addBusinessPermitTile(new BusinessPermitTile(5, 3, 4, 5, 3, 6, 'e'));
        assertTrue(remotePlayer.getBusinessPermitTiles().size() == 2);
        assertTrue(remotePlayer.getUsedBusinessPermitTiles().size() == 0);
        List<BusinessPermitTile> businessPermitTileList = new ArrayList<>();
        businessPermitTileList.add(new BusinessPermitTile(5, 3, 5, 6, 3, 6, 'd'));
        businessPermitTileList.add(new BusinessPermitTile(5, 3, 4, 5, 3, 6, 'e'));
        remotePlayer.useBusinessPermitTiles(businessPermitTileList);
        assertTrue(remotePlayer.getBusinessPermitTiles().size() == 0);
        assertTrue(remotePlayer.getUsedBusinessPermitTiles().size() == 2);
    }

    @Test
    public void addRemovePoliticCard() throws Exception {
        FakeRemotePlayer remotePlayer = new FakeRemotePlayer("Napoleone");
        assertTrue(remotePlayer.getPoliticCards().size() == 0);
        remotePlayer.addPoliticCard(null);
        assertTrue(remotePlayer.getPoliticCards().size() == 0);
        remotePlayer.addPoliticCard(new PoliticCard("Red"));
        assertTrue(remotePlayer.getPoliticCards().size() == 1);
        remotePlayer.removePoliticCard(null);
        assertTrue(remotePlayer.getPoliticCards().size() == 1);
        remotePlayer.removePoliticCard(new PoliticCard("Red"));
        assertTrue(remotePlayer.getPoliticCards().size() == 0);
    }

    @Test
    public void onlineTest() throws Exception {
        FakeRemotePlayer remotePlayer = new FakeRemotePlayer("Napoleone");
        assertTrue(remotePlayer.isOnline());
        remotePlayer.setOnline(false);
        assertFalse(remotePlayer.isOnline());
    }

    @Test
    public void removePermitTile() throws Exception {
        FakeRemotePlayer remotePlayer = new FakeRemotePlayer("Napoleone");
        assertTrue(remotePlayer.getBusinessPermitTiles().size() == 0);
        remotePlayer.addBusinessPermitTile(new BusinessPermitTile(5, 3, 4, 5, 3, 6, 'e'));
        assertTrue(remotePlayer.getBusinessPermitTiles().size() == 1);
        remotePlayer.removeBusinessPermitTile(null);
        assertTrue(remotePlayer.getBusinessPermitTiles().size() == 1);
        remotePlayer.removeBusinessPermitTile(new BusinessPermitTile(5, 3, 4, 5, 3, 6, 'e'));
        assertTrue(remotePlayer.getBusinessPermitTiles().size() == 0);
    }

    @Test
    public void addKingRewards() throws Exception {
        FakeRemotePlayer remotePlayer = new FakeRemotePlayer("Napoleone");
        assertTrue(remotePlayer.getKingRewards().size() == 0);
        List<Reward> kingRewards = new ArrayList<>();
        kingRewards.add(new Reward());
        kingRewards.add(new Reward());
        kingRewards.add(null);
        kingRewards.add(new Reward());
        kingRewards.add(new Reward());
        kingRewards.add(null);
        remotePlayer.addKingRewards(kingRewards);
        assertTrue(remotePlayer.getKingRewards().size() == 4);
        remotePlayer.addKingReward(null);
        assertTrue(remotePlayer.getKingRewards().size() == 4);
        remotePlayer.addKingReward(new Reward());
        assertTrue(remotePlayer.getKingRewards().size() == 5);
    }
}
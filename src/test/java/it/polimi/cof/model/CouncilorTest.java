package it.polimi.cof.model;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * This test class will test councillor methods.
 */
public class CouncilorTest {

    @SuppressWarnings({"EqualsWithItself", "ObjectEqualsNull"})
    @Test
    public void equals() throws Exception {
        // create four councilors
        Councilor councilorA = new Councilor("blue");
        Councilor councilorB = new Councilor("cyan");
        Councilor councilorC = new Councilor("blue");
        Councilor councilorD = new Councilor("blue");
        // 1) reflexive property
        assertTrue(councilorA.equals(councilorA));
        // 2) symmetric property
        assertTrue(councilorA.equals(councilorC) && councilorC.equals(councilorA));
        // 3) transitive property
        if (councilorA.equals(councilorC) && councilorC.equals(councilorD)) {
            assertTrue(councilorA.equals(councilorD));
        }
        // 4) consistency property
        assertTrue(councilorA.equals(councilorC) == councilorA.equals(councilorC));
        // 5) non-null property
        assertFalse(councilorA.equals(null));
        // 6) comparing two different councilors
        assertFalse(councilorA.equals(councilorB));
        assertFalse(councilorB.equals(councilorA));
        // class test
        assertFalse(councilorA.equals(new AwesomeCouncillor(councilorA.getColor())));
    }

    @Test
    public void hashCodeTest() throws Exception {
        // create three business permit tiles
        Councilor councillorA = new Councilor("Red");
        Councilor councillorB = new Councilor("Blue");
        Councilor councillorC = new Councilor("Red");
        // equal objects should return the same hashcode
        if (councillorA.equals(councillorC)) {
            assertTrue(councillorA.hashCode() == councillorC.hashCode());
        }
        if (!councillorA.equals(councillorB)) {
            assertFalse(councillorA.hashCode() == councillorB.hashCode());
        }
    }

    private class AwesomeCouncillor extends Councilor {

        private AwesomeCouncillor(String color) {
            super(color);
        }
    }
}
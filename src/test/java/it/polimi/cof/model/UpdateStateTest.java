package it.polimi.cof.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * This test class will test UpdateState class
 */
public class UpdateStateTest {

    @Test
    public void getNickname() throws Exception {
        //create updatestate
        UpdateState updateState = new UpdateState("Napoleone");
        // make start test
        assertTrue("Napoleone".equals(updateState.getNickname()));
        updateState = new UpdateState("Carlo Magno");
        assertTrue("Carlo Magno".equals(updateState.getNickname()));
    }

    @Test
    public void isPlayer() throws Exception {
        UpdateState updateState = new UpdateState("Napoleone");
        assertFalse(updateState.isPlayer(""));
        assertFalse(updateState.isPlayer("Carlo Magno"));
        UpdateState updateState1 = new UpdateState("Carlo Magno");
        assertFalse(updateState1.isPlayer("Napoleone"));
        assertTrue(updateState1.isPlayer("Carlo Magno"));
        UpdateState updateState2 = new UpdateState(null);
        assertFalse(updateState2.isPlayer("Napoleone"));
    }

    @Test
    public void addReward() throws Exception {
        Reward reward = new Reward();
        Reward reward1 = new Reward();
        UpdateState updateState = new UpdateState("Papa Giovanni");
        assertFalse(updateState.getRewards().contains(reward));
        updateState.addReward(reward);
        assertTrue(updateState.getRewards().contains(reward));
        updateState.addReward(reward1);
        assertTrue(updateState.getRewards().contains(reward1));
        updateState.addReward(null);
        assertTrue(updateState.getRewards().size() == 2);
    }

    @Test
    public void setNobilityReward() throws Exception {
        NobilityReward nobilityReward = new NobilityReward();
        UpdateState updateState = new UpdateState("Re sole");
        updateState.setNobilityReward(nobilityReward);
        assertTrue(updateState.getNobilityReward().equals(nobilityReward));
    }

    @Test
    public void getNobilityReward() throws Exception {
        NobilityReward nobilityReward = new NobilityReward();
        UpdateState updateState = new UpdateState("Re sole");
        updateState.setNobilityReward(nobilityReward);
        assertTrue(updateState.getNobilityReward().equals(nobilityReward));
    }

    @Test
    public void addRewards() throws Exception {
        Reward reward = new Reward();
        Reward reward1 = new Reward();
        Reward reward2 = new Reward();
        List<Reward> rewardList = new ArrayList<>();
        rewardList.add(reward);
        rewardList.add(reward1);
        rewardList.add(reward2);
        UpdateState updateState = new UpdateState("Immanuel Kant");
        assertFalse(updateState.getRewards().containsAll(rewardList));
        updateState.addRewards(rewardList);
        assertTrue(updateState.getRewards().containsAll(rewardList));
    }

    @Test
    public void getRewards() throws Exception {
        Reward reward = new Reward();
        Reward reward1 = new Reward();
        UpdateState updateState = new UpdateState("Putin");
        assertFalse(updateState.getRewards().contains(reward));
        updateState.addReward(reward);
        updateState.addReward(reward1);
        assertTrue(updateState.getRewards().contains(reward));

    }

    @Test
    public void addAssistants() throws Exception {
        UpdateState updateState = new UpdateState("Cristiano Ronaldo");
        assertTrue(updateState.getAddedAssistants() == 0);
        updateState.addAssistants(5);
        assertTrue(updateState.getAddedAssistants() == 5);
    }

    @Test
    public void getAddedAssistants() throws Exception {
        UpdateState updateState = new UpdateState("Mario Draghi");
        updateState.addAssistants(5);
        assertTrue(updateState.getAddedAssistants() == 5);
    }

    @Test
    public void addVictoryPoints() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        assertTrue(updateState.getAddedVictoryPoints() ==0);
        updateState.addVictoryPoints(5);
        assertTrue(updateState.getAddedVictoryPoints() == 5);
    }

    @Test
    public void getAddedVictoryPoints() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        updateState.addVictoryPoints(5);
        assertTrue(updateState.getAddedVictoryPoints()==5);
    }

    @Test
    public void addNobilitySteps() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        assertTrue(updateState.getAddedNobilitySteps() ==0);
        updateState.addNobilitySteps(5);
        assertTrue(updateState.getAddedNobilitySteps() == 5);
    }

    @Test
    public void getAddedNobilitySteps() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        updateState.addNobilitySteps(5);
        assertTrue(updateState.getAddedNobilitySteps()==5);
    }

    @Test
    public void addCoins() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        assertTrue(updateState.getAddedCoins() ==0);
        updateState.addCoins(5);
        assertTrue(updateState.getAddedCoins() == 5);
    }

    @Test
    public void getAddedCoins() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        updateState.addCoins(5);
        assertTrue(updateState.getAddedCoins()==5);
    }

    @Test
    public void addEmporium() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        assertTrue(updateState.getAddedEmporiums().isEmpty());
        updateState.addEmporium("Juvelar");
        assertTrue(updateState.getAddedEmporiums().contains("Juvelar"));
    }

    @Test
    public void getAddedEmporiums() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        updateState.addEmporium("Juvelar");
        assertTrue(updateState.getAddedEmporiums().contains("Juvelar"));
    }

    @Test
    public void addBusinessPermitTile() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        assertTrue(updateState.getAddedBusinessPermitTiles().isEmpty());
        BusinessPermitTile businessPermitTile = new BusinessPermitTile();
        BusinessPermitTile businessPermitTile1 = new BusinessPermitTile();
        updateState.addBusinessPermitTile(businessPermitTile);
        updateState.addBusinessPermitTile(businessPermitTile1);
        assertTrue(updateState.getAddedBusinessPermitTiles().contains(businessPermitTile));
    }

    @Test
    public void getAddedBusinessPermitTiles() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        BusinessPermitTile businessPermitTile = new BusinessPermitTile();
        updateState.addBusinessPermitTile(businessPermitTile);
        assertTrue(updateState.getAddedBusinessPermitTiles().contains(businessPermitTile));
    }

    @Test
    public void addPoliticCard() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        PoliticCard politicCard = new PoliticCard();
        PoliticCard politicCard1 = new PoliticCard();
        assertTrue(updateState.getAddedPoliticCards().isEmpty());
        updateState.addPoliticCard(politicCard);
        updateState.addPoliticCard(politicCard1);
        assertTrue(updateState.getAddedPoliticCards().contains(politicCard));
    }

    @Test
    public void getAddedPoliticCards() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        PoliticCard politicCard = new PoliticCard();
        updateState.addPoliticCard(politicCard);
        assertTrue(updateState.getAddedPoliticCards().contains(politicCard));
    }

    @Test
    public void addKingReward() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        Reward reward = new Reward();
        Reward reward1 = new Reward();
        assertTrue(updateState.getKingRewards().isEmpty());
        updateState.addKingReward(reward);
        updateState.addKingReward(reward1);
        assertTrue(updateState.getKingRewards().contains(reward));
        updateState.addKingReward(null);
        assertTrue(updateState.getKingRewards().size() == 2);
    }

    @Test
    public void getKingRewards() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        Reward reward = new Reward();
        updateState.addKingReward(reward);
        assertTrue(updateState.getKingRewards().contains(reward));
    }

    @Test
    public void addBonus() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        Reward reward = new Reward();
        Reward reward1 = new Reward();
        Map<String,Reward> bonus = new HashMap<>();
        bonus.put("Prova", reward);
        bonus.put("Prova2", reward1);
        updateState.addBonus(bonus);
        assertTrue(updateState.getBonus().equals(bonus));
    }

    @Test
    public void getBonus() throws Exception {
        UpdateState updateState = new UpdateState("Colombo");
       Map <String, Reward> bonus = new HashMap<>();
        bonus.put("foo", new Reward());
        assertTrue(updateState.getBonus().isEmpty());
        updateState.addBonus(bonus);
        assertTrue(updateState.getBonus().equals(bonus));
    }

    @Test
    public void addMainAction() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        assertTrue(updateState.getAddedMainActions() ==0);
        updateState.addMainAction(5);
        assertTrue(updateState.getAddedMainActions() == 5);
    }

    @Test
    public void getAddedMainActions() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        updateState.addMainAction(5);
        assertTrue(updateState.getAddedMainActions()==5);
    }

    @Test
    public void removeAssistants() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        assertTrue(updateState.getRemovedAssistants() ==0);
        updateState.removeAssistants(5);
        assertTrue(updateState.getRemovedAssistants() == 5);
    }

    @Test
    public void getRemovedAssistants() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        updateState.removeAssistants(5);
        assertTrue(updateState.getRemovedAssistants()==5);
    }

    @Test
    public void removeCoins() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        assertTrue(updateState.getRemovedCoins() ==0);
        updateState.removeCoins(5);
        assertTrue(updateState.getRemovedCoins() == 5);
    }

    @Test
    public void getRemovedCoins() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        updateState.removeCoins(5);
        assertTrue(updateState.getRemovedCoins()==5);
    }

    @Test
    public void removeBusinessPermitTile() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        assertTrue(updateState.getRemovedBusinessPermitTiles().isEmpty());
        BusinessPermitTile businessPermitTile = new BusinessPermitTile();
        BusinessPermitTile businessPermitTile1 = new BusinessPermitTile();
        updateState.removeBusinessPermitTile(businessPermitTile);
        updateState.removeBusinessPermitTile(businessPermitTile1);
        assertTrue(updateState.getRemovedBusinessPermitTiles().contains(businessPermitTile));
    }

    @Test
    public void getRemovedBusinessPermitTiles() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        BusinessPermitTile businessPermitTile = new BusinessPermitTile();
        updateState.removeBusinessPermitTile(businessPermitTile);
        assertTrue(updateState.getRemovedBusinessPermitTiles().contains(businessPermitTile));
    }

    @Test
    public void removePoliticCards() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        PoliticCard politicCard = new PoliticCard();
        PoliticCard politicCard1 = new PoliticCard();
        List<PoliticCard> politicCards = new ArrayList<>();
        politicCards.add(politicCard);
        politicCards.add(politicCard1);
        assertTrue(updateState.getRemovedPoliticCards().isEmpty());
        updateState.removePoliticCards(politicCards);
        assertTrue(updateState.getRemovedPoliticCards().contains(politicCard));
    }

    @Test
    public void getRemovedPoliticCards() throws Exception {
        UpdateState updateState = new UpdateState("Andrea");
        PoliticCard politicCard = new PoliticCard();
        List<PoliticCard> politicCards = new ArrayList<>();
        politicCards.add(politicCard);
        updateState.removePoliticCards(politicCards);
        assertTrue(updateState.getRemovedPoliticCards().contains(politicCard));
    }

    @Test
    public void changeBusinessPermitTileAtIndex() throws Exception {
        UpdateState updateState = new UpdateState("Guido");
        BusinessPermitTile businessPermitTile = new BusinessPermitTile();
        int index = 5;
        String region = "mountain";
        updateState.changeBusinessPermitTileAtIndex(businessPermitTile, region, index);
        UpdateState.ChangedBusinessPermitTile changedBusinessPermitTile;
        assertTrue (updateState.getChangedBusinessPermitTiles().size() == 1);
        changedBusinessPermitTile = updateState.getChangedBusinessPermitTiles().get(0);
        assertTrue(changedBusinessPermitTile.getRegion() == region);
        assertTrue(changedBusinessPermitTile.getIndex() == index);
        assertTrue(changedBusinessPermitTile.getBusinessPermitTile() == businessPermitTile);
    }

    @Test
    public void getChangedBusinessPermitTiles() throws Exception {
        UpdateState updateState = new UpdateState("Marcello");
        BusinessPermitTile businessPermitTile = new BusinessPermitTile();
        int index = 100;
        String region = "seaside";
        updateState.changeBusinessPermitTileAtIndex(businessPermitTile, region, index);
        assertTrue(updateState.getChangedBusinessPermitTiles().size() == 1);
        assertTrue(updateState.getChangedBusinessPermitTiles().get(0).getBusinessPermitTile() == businessPermitTile);
        assertTrue(updateState.getChangedBusinessPermitTiles().get(0).getRegion() == region);
        assertTrue(updateState.getChangedBusinessPermitTiles().get(0).getIndex() == index);
    }

    @Test
    public void changeCouncillor() throws Exception {
        UpdateState updateState = new UpdateState("Tizio");
        Councilor councilor = new Councilor("red");
        String region = "seaside";
        UpdateState.ChangedCouncilor changedCouncilor;
        updateState.changeCouncillor(councilor, region);
        assertTrue(updateState.getChangedCouncillors().size() == 1);
        changedCouncilor = updateState.getChangedCouncillors().get(0);
        assertTrue(changedCouncilor.getRegion().equals(region));
        assertTrue(changedCouncilor.getCouncillor().equals(councilor));

    }

    @Test
    public void getChangedCouncillors() throws Exception {
        UpdateState updateState = new UpdateState("Pizza");
        Councilor councilor = new Councilor("yellow");
        String region = "seaside";
        updateState.changeCouncillor(councilor, region);
        assertTrue(updateState.getChangedCouncillors().size() == 1);
        UpdateState.ChangedCouncilor changedCouncilor = updateState.getChangedCouncillors().get(0);
        assertTrue(changedCouncilor.getCouncillor().equals(councilor));
        assertTrue(changedCouncilor.getRegion().equals(region));
    }

    @Test
    public void changeKingCity() throws Exception {
        UpdateState updateState = new UpdateState("Ferrari");
        String city = "Kaliningrad";
        updateState.changeKingCity(city);
        assertTrue(updateState.getNewKingCity().equals(city));
    }

    @Test
    public void isKingCityChanged() throws Exception {
        UpdateState updateState = new UpdateState("Enzo");
        assertFalse(updateState.isKingCityChanged());
        String city = "Kaliningrad";
        updateState.changeKingCity(city);
        assertTrue(updateState.isKingCityChanged());
    }

    @Test
    public void getNewKingCity() throws Exception {
        UpdateState updateState = new UpdateState("Lenin");
        String city = "Ozersk";
        updateState.changeKingCity(city);
        assertTrue(updateState.getNewKingCity().equals(city));
    }
}
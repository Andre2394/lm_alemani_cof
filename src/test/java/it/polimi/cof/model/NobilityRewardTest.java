package it.polimi.cof.model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by andre on 10/07/2016.
 */
public class NobilityRewardTest {

    @Test
    public void addFirstSpecialBonus() throws Exception {
        NobilityReward nobilityReward = new NobilityReward();
        assertTrue(nobilityReward.getFirstSpecialBonusCount() == 0);
        nobilityReward.addFirstSpecialBonus(1);
        assertTrue(nobilityReward.getFirstSpecialBonusCount() == 1);
    }

    @Test
    public void removeFirstSpecialBonus() throws Exception {
        NobilityReward nobilityReward = new NobilityReward();
        assertTrue(nobilityReward.getFirstSpecialBonusCount() == 0);
        nobilityReward.addFirstSpecialBonus(1);
        assertTrue(nobilityReward.getFirstSpecialBonusCount() == 1);
        nobilityReward.removeFirstSpecialBonus(1);
        assertTrue(nobilityReward.getFirstSpecialBonusCount() == 0);
    }

    @Test
    public void addSecondSpecialBonus() throws Exception {
        NobilityReward nobilityReward = new NobilityReward();
        assertTrue(nobilityReward.getSecondSpecialBonusCount() == 0);
        nobilityReward.addSecondSpecialBonus(1);
        assertTrue(nobilityReward.getSecondSpecialBonusCount() == 1);
    }

    @Test
    public void removeSecondSpecialBonus() throws Exception {
        NobilityReward nobilityReward = new NobilityReward();
        assertTrue(nobilityReward.getSecondSpecialBonusCount() == 0);
        nobilityReward.addSecondSpecialBonus(1);
        assertTrue(nobilityReward.getSecondSpecialBonusCount() == 1);
        nobilityReward.removeSecondSpecialBonus(1);
        assertTrue(nobilityReward.getSecondSpecialBonusCount() == 0);
    }

    @Test
    public void addThirdSpecialBonus() throws Exception {
        NobilityReward nobilityReward = new NobilityReward();
        assertTrue(nobilityReward.getThirdSpecialBonusCount() == 0);
        nobilityReward.addThirdSpecialBonus(1);
        assertTrue(nobilityReward.getThirdSpecialBonusCount() == 1);
    }

    @Test
    public void removeThirdSpecialBonus() throws Exception {
        NobilityReward nobilityReward = new NobilityReward();
        assertTrue(nobilityReward.getThirdSpecialBonusCount() == 0);
        nobilityReward.addThirdSpecialBonus(1);
        assertTrue(nobilityReward.getThirdSpecialBonusCount() == 1);
        nobilityReward.removeThirdSpecialBonus(1);
        assertTrue(nobilityReward.getThirdSpecialBonusCount() == 0);
    }

    @Test
    public void isValid() throws Exception {
        NobilityReward nobilityReward = new NobilityReward();
        assertFalse(nobilityReward.isValid());
        nobilityReward.addFirstSpecialBonus(1);
        assertTrue(nobilityReward.isValid());
        nobilityReward.addMainActions(1);
        assertTrue(nobilityReward.isValid());
        nobilityReward.removeFirstSpecialBonus(1);
        assertTrue(nobilityReward.isValid());
    }
}
package it.polimi.cof.model;

import it.polimi.cof.model.exceptions.BalconyUnsatisfiable;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by andre on 10/07/2016.
 */
public class AbstractBoardTest {

    @Test
    public void fillBalcony() throws Exception {
        AbstractBoard abstractBoard = new TestAbstractBoard();
        abstractBoard.fillBalcony(new ArrayList<>());
        for (String color : abstractBoard.getBalconyColors()) {
            assertTrue(color == null);
        }
        List<Councilor> colors = new ArrayList<>();
        colors.add(new Councilor("Red"));
        colors.add(new Councilor("Yellow"));
        colors.add(new Councilor("Blue"));
        colors.add(new Councilor("Green"));
        colors.add(new Councilor("Magenta"));
        abstractBoard.fillBalcony(colors);
        assertTrue(colors.size() == 1);
        String[] balconyColors = abstractBoard.getBalconyColors();
        assertTrue(balconyColors.length == 4);
        assertTrue(balconyColors[0].equals("Red"));
        assertTrue(balconyColors[1].equals("Yellow"));
        assertTrue(balconyColors[2].equals("Blue"));
        assertTrue(balconyColors[3].equals("Green"));
    }

    @Test(expected = BalconyUnsatisfiable.class)
    public void satisfyEmptyBalcony() throws Exception {
        AbstractBoard abstractBoard = new TestAbstractBoard();
        abstractBoard.satisfyBalcony(Arrays.asList(new PoliticCard("Red"), new PoliticCard()));
    }

    private class TestAbstractBoard extends AbstractBoard {

    }
}
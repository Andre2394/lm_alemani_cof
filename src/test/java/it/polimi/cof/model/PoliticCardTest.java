package it.polimi.cof.model;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * This test class will test politic card methods.
 */
public class PoliticCardTest {

    @SuppressWarnings({"EqualsWithItself", "ObjectEqualsNull"})
    @Test
    public void equals() throws Exception {
        // create four cards
        PoliticCard cardA = new PoliticCard("blue");
        PoliticCard cardB = new PoliticCard("cyan");
        PoliticCard cardC = new PoliticCard("blue");
        PoliticCard cardD = new PoliticCard("blue");
        // 1) reflexive property
        assertTrue(cardA.equals(cardA));
        // 2) symmetric property
        assertTrue(cardA.equals(cardC) && cardC.equals(cardA));
        // 3) transitive property
        if (cardA.equals(cardC) && cardC.equals(cardD)) {
            assertTrue(cardA.equals(cardD));
        }
        // 4) consistency property
        assertTrue(cardA.equals(cardC) == cardA.equals(cardC));
        // 5) non-null property
        assertFalse(cardA.equals(null));
        // 6) comparing two different cards
        assertFalse(cardA.equals(cardB));
        assertFalse(cardB.equals(cardA));
        // class test
        assertFalse(cardA.equals(new AwesomePoliticCard(cardA.getColor())));
    }

    @Test
    public void hashCodeTest() throws Exception {
        // create three business permit tiles
        PoliticCard cardA = new PoliticCard("Red");
        PoliticCard cardB = new PoliticCard("Blue");
        PoliticCard cardC = new PoliticCard("Red");
        // equal objects should return the same hashcode
        if (cardA.equals(cardC)) {
            assertTrue(cardA.hashCode() == cardC.hashCode());
        }
        if (!cardA.equals(cardB)) {
            assertFalse(cardA.hashCode() == cardB.hashCode());
        }
    }

    private class AwesomePoliticCard extends PoliticCard {

        private AwesomePoliticCard(String color) {
            super(color);
        }
    }
}
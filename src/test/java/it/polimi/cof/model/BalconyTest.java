package it.polimi.cof.model;

import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * This test class will test balcony methods.
 */
public class BalconyTest {

    @Test
    public void addCouncilor() throws Exception {
        // councilor list
        Councilor councilor1 = new Councilor("cyan");
        Councilor councilor2 = new Councilor("red");
        Councilor councilor3 = new Councilor("green");
        Councilor councilor4 = new Councilor("blue");
        Councilor councilor5 = new Councilor("black");
        // create balcony
        Balcony balcony = new Balcony();
        balcony.addCouncilor(councilor1);
        balcony.addCouncilor(councilor2);
        balcony.addCouncilor(councilor3);
        balcony.addCouncilor(councilor4);
        // expected colors
        List<String> colorList = new ArrayList<>();
        colorList.add("cyan");
        colorList.add("red");
        colorList.add("green");
        colorList.add("blue");
        // make start test
        assertTrue(colorList.equals(balcony.getColors()));
        // now add councilor
        balcony.addCouncilor(councilor5);
        colorList.remove(0);
        colorList.add("black");
        // remake check
        assertTrue(colorList.equals(balcony.getColors()));
    }

    @Test
    public void isFull() throws Exception {
        // create balcony
        Balcony balcony = new Balcony();
        // make assertion
        assertFalse(balcony.isFull());
        // add one councilor
        balcony.addCouncilor(new Councilor("cyan"));
        // make assertion
        assertFalse(balcony.isFull());
        // add one councilor
        balcony.addCouncilor(new Councilor("red"));
        // make assertion
        assertFalse(balcony.isFull());
        // add one councilor
        balcony.addCouncilor(new Councilor("green"));
        // make assertion
        assertFalse(balcony.isFull());
        // add one councilor
        balcony.addCouncilor(new Councilor("blue"));
        // make assertion
        assertTrue(balcony.isFull());
    }

    @Test
    public void getColors() throws Exception {
        Balcony balcony = new Balcony();
        List<String> colorList = new ArrayList<>();
        colorList.add("red");
        colorList.add("yellow");
        balcony.addCouncilor(new Councilor("red"));
        balcony.addCouncilor(new Councilor("yellow"));
        assertTrue(colorList.equals(balcony.getColors()));
        colorList.add("cyan");
        colorList.add("gray");
        colorList.add("blue");
        colorList.remove(0);
        balcony.addCouncilor(new Councilor("cyan"));
        balcony.addCouncilor(new Councilor("gray"));
        balcony.addCouncilor(new Councilor("blue"));
        assertTrue(colorList.equals(balcony.getColors()));
    }

}
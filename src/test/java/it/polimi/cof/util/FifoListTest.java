package it.polimi.cof.util;

import org.junit.Test;

/**
 * Created by andre on 10/07/2016.
 */
public class FifoListTest {

    @Test(expected = IllegalArgumentException.class)
    public void createNotValidFifoList() throws Exception {
        new FifoList<>(-4);
    }
}
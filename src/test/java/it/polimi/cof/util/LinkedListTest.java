package it.polimi.cof.util;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by andre on 10/07/2016.
 */
public class LinkedListTest {

    @Test
    public void areNodesLinked() throws Exception {
        LinkedList<String, String> linkedList = new LinkedList<>();
        linkedList.addNode("key1", "node1");
        assertFalse(linkedList.areNodesLinked("key1", "key2"));
        assertFalse(linkedList.areNodesLinked("key2", "key1"));
        linkedList.addNode("key2", "node2");
        assertFalse(linkedList.areNodesLinked("key1", "key2"));
        linkedList.linkNode("key1", "key2");
        assertTrue(linkedList.areNodesLinked("key1", "key2"));
    }

    @Test
    public void getLinkedKeys() throws Exception {
        LinkedList<String, String> linkedList = new LinkedList<>();
        linkedList.addNode("key1", "node1");
        linkedList.addNode("key2", "node2");
        linkedList.addNode("key3", "node3");
        linkedList.addNode("key4", "node4");
        linkedList.linkNode("key1", "key2");
        linkedList.linkNode("key1", "key3");
        List<String> keys = linkedList.getLinkedKeys("key1");
        assertTrue(keys.contains("key2"));
        assertTrue(keys.contains("key3"));
        assertTrue(keys.size() == 2);
        assertTrue(linkedList.getLinkedKeys("NotExists").size() == 0);
    }

    @Test
    public void getLinkedNodes() throws Exception {
        LinkedList<String, String> linkedList = new LinkedList<>();
        linkedList.addNode("key1", "node1");
        linkedList.addNode("key2", "node2");
        linkedList.addNode("key3", "node3");
        linkedList.addNode("key4", "node4");
        linkedList.linkNode("key1", "key2");
        linkedList.linkNode("key1", "key3");
        List<String> nodes = linkedList.getLinkedNodes("key1");
        assertTrue(nodes.contains("node2"));
        assertTrue(nodes.contains("node3"));
        assertTrue(nodes.size() == 2);
        assertTrue(linkedList.getLinkedNodes("NotExists").size() == 0);
    }

}
package it.polimi.cof.network.server.game;

import it.polimi.cof.model.ActionList;
import it.polimi.cof.model.BaseGame;
import it.polimi.cof.model.UpdateState;
import it.polimi.cof.model.market.Item;
import it.polimi.cof.network.NetworkException;
import it.polimi.cof.network.server.RemotePlayer;
import org.junit.Test;

import java.awt.*;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * This class represent a fake implementation of a remote player in order to test the server room's logic.
 * This player never communicate with clients.
 */
public class FakeRemotePlayer extends RemotePlayer {

    public FakeRemotePlayer(String nickname) {
        setNickname(nickname);
    }

    @Override
    public void dispatchGameSession(BaseGame baseGame) throws NetworkException {

    }

    @Override
    public void onGameTurnStarted(String nickname, int seconds) throws NetworkException {

    }

    @Override
    public void onUpdateTurnCountdown(int remainingSeconds) throws NetworkException {

    }

    @Override
    public void onPoliticCardDrawn(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onActionList(ActionList actionList) throws NetworkException {

    }

    @Override
    public void onCouncillorElected(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onBusinessPermitTileAcquired(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onEmporiumBuiltWithPermitTile(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onEmporiumBuiltWithKingHelp(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onAssistantEngaged(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onBusinessPermitTilesChanged(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onAssistantSentToElectCouncillor(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onAdditionalMainActionGranted(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onFirstSpecialRewardsEarned(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onSecondSpecialRewardsEarned(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onThirdSpecialRewardsEarned(UpdateState updateState) throws NetworkException {

    }

    @Override
    public void onMarketSessionStarted() throws NetworkException {

    }

    @Override
    public void onMarketTurnStarted(String nickname, int seconds, MarketTurn.Mode mode) throws NetworkException {

    }

    @Override
    public void onMarketItemAddedOnSale(Item item) throws NetworkException {

    }

    @Override
    public void onMarketItemBought(String nickname, String marketId) throws NetworkException {

    }

    @Override
    public void onMarketSessionFinished() throws NetworkException {

    }

    @Override
    public void onChatMessage(String author, String message, boolean privateMessage) throws NetworkException {

    }

    @Override
    public void onPlayerDisconnected(String nickname) throws NetworkException {

    }

    @Override
    public void onLastTurn(String nickname) throws NetworkException {

    }

    @Override
    public void onGameEnd(UpdateState[] updateStates, List<String> ranking) throws NetworkException {

    }
}

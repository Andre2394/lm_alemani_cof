package it.polimi.cof.network.server.game;

import it.polimi.cof.model.*;
import it.polimi.cof.model.exceptions.*;
import it.polimi.cof.network.server.RemotePlayer;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;
import java.util.List;

import static org.junit.Assert.*;

/**
 * This test class will test all server's logic methods. It uses {@link FakeRemotePlayer} implementation to initialize
 * the game instance.
 */
public class ServerGameTest {

    private RegionBoard mLeftRegion;
    private RegionBoard mCenterRegion;
    private RegionBoard mRightRegion;

    private List<RemotePlayer> mPlayers;
    private ServerGame mServerGame;

    @Before
    public void setUp() throws Exception {
        loadConfigurationFile();
        loadPlayers();
        Configuration configuration = Configurator.getConfigurationBundle();
        mLeftRegion = configuration.getLeftRegionBoard();
        mCenterRegion = configuration.getCenterRegionBoard();
        mRightRegion = configuration.getRightRegionBoard();
        assertTrue(mLeftRegion.getRegionType().equals("seaside"));
        assertTrue(mCenterRegion.getRegionType().equals("hillside"));
        assertTrue(mRightRegion.getRegionType().equals("mountain"));
        assertTrue(mLeftRegion.getFirstVisibleTile() == null);
        assertTrue(mLeftRegion.getSecondVisibleTile() == null);
        mServerGame = Configurator.getGameInstance(mPlayers, configuration);
        assertTrue(mLeftRegion.getFirstVisibleTile() != null);
        assertTrue(mLeftRegion.getSecondVisibleTile() != null);
    }

    private static void loadConfigurationFile() throws Exception {
        Configurator.loadConfigurationFile("/files/configuration.json");
    }

    private void loadPlayers() {
        mPlayers = new ArrayList<>();
        mPlayers.add(new FakeRemotePlayer("Player1"));
        mPlayers.add(new FakeRemotePlayer("Player2"));
        mPlayers.add(new FakeRemotePlayer("Player3"));
        mPlayers.add(new FakeRemotePlayer("Player4"));
    }

    @Test
    public void drawPoliticCard() throws Exception {
        Player player1 = mPlayers.get(0);
        Player player2 = mPlayers.get(1);
        Player player3 = mPlayers.get(2);
        Player player4 = mPlayers.get(3);
        int politicCardCount1 = player1.getPoliticCards().size();
        int politicCardCount2 = player2.getPoliticCards().size();
        int politicCardCount3 = player3.getPoliticCards().size();
        int politicCardCount4 = player4.getPoliticCards().size();
        UpdateState updateState1 = mServerGame.drawPoliticCard(player1);
        assertTrue(player1.getPoliticCards().size() == politicCardCount1 + 1);
        assertTrue(player2.getPoliticCards().size() == politicCardCount2);
        assertTrue(player3.getPoliticCards().size() == politicCardCount3);
        assertTrue(player4.getPoliticCards().size() == politicCardCount4);
        assertTrue("Player1".equals(updateState1.getNickname()));
        assertTrue(updateState1.getAddedPoliticCards().size() == 1);
        UpdateState updateState3 = mServerGame.drawPoliticCard(player3);
        assertTrue(player1.getPoliticCards().size() == politicCardCount1 + 1);
        assertTrue(player2.getPoliticCards().size() == politicCardCount2);
        assertTrue(player3.getPoliticCards().size() == politicCardCount3 + 1);
        assertTrue(player4.getPoliticCards().size() == politicCardCount4);
        assertTrue("Player3".equals(updateState3.getNickname()));
        assertTrue(updateState3.getAddedPoliticCards().size() == 1);
    }

    @Test
    public void electCouncillor() throws Exception {
        String[] leftColors = mLeftRegion.getBalconyColors();
        String[] centerColors = mCenterRegion.getBalconyColors();
        String[] rightColors = mRightRegion.getBalconyColors();
        UpdateState updateState1 = mServerGame.electCouncillor(mPlayers.get(0), new Councilor("awesomeColor"), AbstractBoard.LEFT_REGION, true);
        assertFalse(Arrays.equals(leftColors, mLeftRegion.getBalconyColors()));
        assertTrue(Arrays.equals(centerColors, mCenterRegion.getBalconyColors()));
        assertTrue(Arrays.equals(rightColors, mRightRegion.getBalconyColors()));
        assertTrue(Arrays.equals(mLeftRegion.getBalconyColors(), new String[] {leftColors[1], leftColors[2], leftColors[3], "awesomeColor"}));
        UpdateState updateState2 = mServerGame.electCouncillor(mPlayers.get(1), new Councilor("badColor"), AbstractBoard.RIGHT_REGION, false);
        assertFalse(Arrays.equals(leftColors, mLeftRegion.getBalconyColors()));
        assertTrue(Arrays.equals(centerColors, mCenterRegion.getBalconyColors()));
        assertFalse(Arrays.equals(rightColors, mRightRegion.getBalconyColors()));
        assertTrue(Arrays.equals(mRightRegion.getBalconyColors(), new String[] {rightColors[1], rightColors[2], rightColors[3], "badColor"}));
        assertTrue("Player1".equals(updateState1.getNickname()));
        assertTrue(updateState1.getChangedCouncillors().size() == 1);
        UpdateState.ChangedCouncilor changedCouncilor1 = updateState1.getChangedCouncillors().get(0);
        assertTrue(changedCouncilor1.getRegion().equals(AbstractBoard.LEFT_REGION));
        assertTrue(changedCouncilor1.getCouncillor().equals(new Councilor("awesomeColor")));
        assertTrue(updateState1.getAddedCoins() > 0);
        assertTrue("Player2".equals(updateState2.getNickname()));
        assertTrue(updateState2.getChangedCouncillors().size() == 1);
        UpdateState.ChangedCouncilor changedCouncilor2 = updateState2.getChangedCouncillors().get(0);
        assertTrue(changedCouncilor2.getRegion().equals(AbstractBoard.RIGHT_REGION));
        assertTrue(changedCouncilor2.getCouncillor().equals(new Councilor("badColor")));
        assertTrue(updateState2.getAddedCoins() == 0);
    }

    @Test
    public void acquireBusinessPermitTile() throws Exception {
        String[] balconyColors = mCenterRegion.getBalconyColors();
        List<PoliticCard> politicCards = new ArrayList<>();
        politicCards.add(new PoliticCard(balconyColors[2]));
        politicCards.add(new PoliticCard());
        politicCards.add(new PoliticCard(balconyColors[0]));
        politicCards.add(new PoliticCard("unknown")); // not exists!
        acquireBusinessPermitTile(10, politicCards);
    }

    @Test(expected = NotEnoughCoins.class)
    public void acquireBusinessPermitTileWithNoEnoughCoins() throws Exception {
        List<PoliticCard> politicCards = new ArrayList<>();
        politicCards.add(new PoliticCard());
        politicCards.add(new PoliticCard());
        politicCards.add(new PoliticCard());
        politicCards.add(new PoliticCard());
        acquireBusinessPermitTile(0, politicCards);
    }

    @Test(expected = BalconyUnsatisfiable.class)
    public void acquireBusinessPermitTileWithUnsatisfiablePoliticCards() throws Exception {
        Set<String> gameColors = new HashSet<>();
        gameColors.add("black");
        gameColors.add("white");
        gameColors.add("brown");
        gameColors.add("orange");
        gameColors.add("pink");
        for (String balconyColor : mCenterRegion.getBalconyColors()) {
            gameColors.remove(balconyColor);
        }
        // At least one color still remain in the set and that is the color that is not found in the balcony!
        // Let's use that color to make the test
        List<PoliticCard> politicCards = new ArrayList<>();
        PoliticCard politicCard = new PoliticCard(gameColors.toArray(new String[gameColors.size()])[0]);
        politicCards.add(politicCard);
        acquireBusinessPermitTile(10, politicCards);
    }

    private void acquireBusinessPermitTile(int coins, List<PoliticCard> politicCards) throws Exception {
        GameTurn gameTurn = new GameTurn(null, null);
        Player player = mPlayers.get(0);
        player.getPoliticCards().clear();
        player.payCoins(player.getCoins());
        player.addCoins(coins);
        player.addPoliticCards(politicCards);
        BusinessPermitTile businessPermitTile = mCenterRegion.getSecondVisibleTile();
        UpdateState updateState = mServerGame.acquireBusinessPermitTile(player, gameTurn, politicCards, AbstractBoard.CENTER_REGION, 2);
        assertTrue(updateState.getRemovedCoins() == 5);
        assertTrue(player.getCoins() == coins - updateState.getRemovedCoins() + updateState.getAddedCoins());
        assertTrue(player.getPoliticCards().size() == 1 + updateState.getAddedPoliticCards().size());
        assertTrue(player.getPoliticCards().contains(politicCards.get(3)));
        assertTrue("Player1".equals(updateState.getNickname()));
        assertTrue(updateState.getRemovedPoliticCards().size() == 3);
        assertTrue(updateState.getAddedBusinessPermitTiles().size() == 1);
        assertTrue(updateState.getAddedBusinessPermitTiles().get(0).equals(businessPermitTile));
        assertTrue(updateState.getChangedBusinessPermitTiles().size() == 1);
        UpdateState.ChangedBusinessPermitTile changedBusinessPermitTile = updateState.getChangedBusinessPermitTiles().get(0);
        assertTrue(changedBusinessPermitTile.getRegion().equals(AbstractBoard.CENTER_REGION));
        assertTrue(changedBusinessPermitTile.getIndex() == 2);
    }

    @Test
    public void buildEmporiumWithBusinessPermitTile() throws Exception {
        Player player = mPlayers.get(0);
        player.payAssistants(player.getAssistants());
        player.addAssistants(10);
        buildEmporium(player, new BusinessPermitTile(0, 0, 0, 0, 0, 0, 'a', 'j'), "Juvelar");
    }

    @Test(expected = CityNotFound.class)
    public void buildEmporiumWithPermitTileUnknownCity() throws Exception {
        buildEmporium(mPlayers.get(0), new BusinessPermitTile(0, 0, 0, 0, 0, 0, 'a', 'j'), "UnknownCity");
    }

    @Test(expected = CityNotValid.class)
    public void buildEmporiumWithPermitTileCityNotValid() throws Exception {
        buildEmporium(mPlayers.get(0), new BusinessPermitTile(0, 0, 0, 0, 0, 0, 'a', 'h'), "Juvelar");
    }

    @Test(expected = NotEnoughAssistants.class)
    public void buildEmporiumWithPermitTileWithNoEnoughAssistants() throws Exception {
        Player player = mPlayers.get(0);
        player.payAssistants(player.getAssistants());
        mServerGame.findCity("Juvelar").addEmporium("Player2");
        buildEmporium(mPlayers.get(0), new BusinessPermitTile(0, 0, 0, 0, 0, 0, 'a', 'j'), "Juvelar");
    }

    @Test(expected = EmporiumAlreadyBuilt.class)
    public void buildEmporiumWithPermitTileWhereAlreadyBuilt() throws Exception {
        Player player = mPlayers.get(0);
        player.payAssistants(player.getAssistants());
        mServerGame.findCity("Juvelar").addEmporium("Player1");
        buildEmporium(mPlayers.get(0), new BusinessPermitTile(0, 0, 0, 0, 0, 0, 'a', 'j'), "Juvelar");
    }

    private void buildEmporium(Player player, BusinessPermitTile permitTile, String city) throws Exception {
        GameTurn gameTurn = new GameTurn(null, null);
        UpdateState updateState = mServerGame.buildEmporium(player, gameTurn, permitTile, city);
        assertTrue(updateState.getRemovedAssistants() == mServerGame.findCity(city).getEmporiums().size() - 1);
        assertTrue(updateState.getAddedEmporiums().contains(city));
    }

    @Test
    public void getCoinsToPayToMoveKingDefault() throws Exception {
        List<String> route = new ArrayList<>();
        route.add("Juvelar");
        route.add("Indur");
        route.add("Framek");
        assertTrue(getCoinsToPayToMoveKing(route) == 4);
    }

    @Test
    public void getCoinsToPayToMoveKingNoMovement() throws Exception {
        List<String> route = new ArrayList<>();
        route.add("Juvelar");
        assertTrue(getCoinsToPayToMoveKing(route) == 0);
    }

    @Test(expected = RouteNotValid.class)
    public void getCoinsToPayToMoveKingUnknownCity() throws Exception {
        List<String> route = new ArrayList<>();
        route.add("Juvelar");
        route.add("UnknownCity");
        route.add("Framek");
        getCoinsToPayToMoveKing(route);
    }

    @Test(expected = RouteNotValid.class)
    public void getCoinsToPayToMoveKingNotLinkedCities() throws Exception {
        List<String> route = new ArrayList<>();
        route.add("Juvelar");
        route.add("Indur");
        route.add("Esti");
        getCoinsToPayToMoveKing(route);
    }

    private int getCoinsToPayToMoveKing(List<String> route) throws Exception {
        Method method = ServerGame.class.getDeclaredMethod("getCoinsToPayToMoveKing", List.class);
        method.setAccessible(true);
        try {
            return (int) method.invoke(mServerGame, route);
        } catch (InvocationTargetException e) {
            throw new RouteNotValid(e);
        }
    }

    @Test
    public void buildEmporiumWithKingHelp() throws Exception {
        String[] balconyColors = mServerGame.getKingBalconyColors();
        List<PoliticCard> politicCards = new ArrayList<>();
        politicCards.add(new PoliticCard(balconyColors[2]));
        politicCards.add(new PoliticCard());
        politicCards.add(new PoliticCard(balconyColors[0]));
        politicCards.add(new PoliticCard("unknown")); // not exists!
        List<String> cities = new ArrayList<>();
        cities.add("Juvelar");
        buildEmporiumWithKingHelp(10, 10, politicCards, cities);
    }

    @Test(expected = BalconyUnsatisfiable.class)
    public void buildEmporiumWithKingHelpWithUnsatisfiablePoliticCards() throws Exception {
        Set<String> gameColors = new HashSet<>();
        gameColors.add("black");
        gameColors.add("white");
        gameColors.add("brown");
        gameColors.add("orange");
        gameColors.add("pink");
        for (String balconyColor : mServerGame.getKingBalconyColors()) {
            gameColors.remove(balconyColor);
        }
        // At least one color still remain in the set and that is the color that is not found in the balcony!
        // Let's use that color to make the test
        List<PoliticCard> politicCards = new ArrayList<>();
        PoliticCard politicCard = new PoliticCard(gameColors.toArray(new String[gameColors.size()])[0]);
        politicCards.add(politicCard);
        List<String> cities = new ArrayList<>();
        cities.add("Juvelar");
        buildEmporiumWithKingHelp(10, 10, politicCards, cities);
    }

    @Test(expected = NotEnoughCoins.class)
    public void buildEmporiumWithKingHelpWithNoEnoughCoins() throws Exception {
        String[] balconyColors = mServerGame.getKingBalconyColors();
        List<PoliticCard> politicCards = new ArrayList<>();
        politicCards.add(new PoliticCard(balconyColors[2]));
        politicCards.add(new PoliticCard());
        politicCards.add(new PoliticCard(balconyColors[0]));
        politicCards.add(new PoliticCard("unknown")); // not exists!
        List<String> cities = new ArrayList<>();
        cities.add("Juvelar");
        buildEmporiumWithKingHelp(0, 10, politicCards, cities);
    }

    @Test(expected = NotEnoughAssistants.class)
    public void buildEmporiumWithKingHelpWithNoEnoughAssistants() throws Exception {
        String[] balconyColors = mServerGame.getKingBalconyColors();
        List<PoliticCard> politicCards = new ArrayList<>();
        politicCards.add(new PoliticCard(balconyColors[2]));
        politicCards.add(new PoliticCard());
        politicCards.add(new PoliticCard(balconyColors[0]));
        politicCards.add(new PoliticCard("unknown")); // not exists!
        mServerGame.findCity("Juvelar").addEmporium("Player2");
        List<String> cities = new ArrayList<>();
        cities.add("Juvelar");
        buildEmporiumWithKingHelp(10, 0, politicCards, cities);
    }

    @Test(expected = RouteNotValid.class)
    public void buildEmporiumWithKingHelpWithRouteNotValid() throws Exception {
        String[] balconyColors = mServerGame.getKingBalconyColors();
        List<PoliticCard> politicCards = new ArrayList<>();
        politicCards.add(new PoliticCard(balconyColors[2]));
        politicCards.add(new PoliticCard());
        politicCards.add(new PoliticCard(balconyColors[0]));
        politicCards.add(new PoliticCard("unknown")); // not exists!
        List<String> cities = new ArrayList<>();
        cities.add("Juvelar");
        cities.add("Esti"); // not linked to Juvelar
        buildEmporiumWithKingHelp(10, 0, politicCards, cities);
    }

    @Test(expected = EmporiumAlreadyBuilt.class)
    public void buildEmporiumWithKingHelpWhereAlreadyBuilt() throws Exception {
        String[] balconyColors = mServerGame.getKingBalconyColors();
        List<PoliticCard> politicCards = new ArrayList<>();
        politicCards.add(new PoliticCard(balconyColors[2]));
        politicCards.add(new PoliticCard());
        politicCards.add(new PoliticCard(balconyColors[0]));
        politicCards.add(new PoliticCard("unknown")); // not exists!
        mServerGame.findCity("Juvelar").addEmporium("Player1");
        List<String> cities = new ArrayList<>();
        cities.add("Juvelar");
        buildEmporiumWithKingHelp(10, 0, politicCards, cities);
    }

    private void buildEmporiumWithKingHelp(int coins, int assistants, List<PoliticCard> politicCards, List<String> cities) throws Exception {
        Player player = mPlayers.get(0);
        player.payCoins(player.getCoins());
        player.addCoins(coins);
        player.payAssistants(player.getAssistants());
        player.addAssistants(assistants);
        player.getPoliticCards().clear();
        player.addPoliticCards(politicCards);
        UpdateState updateState = mServerGame.buildEmporium(player, new GameTurn(null, null), politicCards, cities);
        assertTrue(updateState.getAddedEmporiums().contains(cities.get(cities.size() - 1)));
        assertTrue(player.getCoins() == coins - updateState.getRemovedCoins() + updateState.getAddedCoins());
        assertTrue(player.getAssistants() == assistants - updateState.getRemovedAssistants() + updateState.getAddedAssistants());
        assertTrue(player.getPoliticCards().size() == politicCards.size() - updateState.getRemovedPoliticCards().size() + updateState.getAddedPoliticCards().size());
    }

    @Test
    public void engageAssistant() throws Exception {
        engageAssistant(10);
    }

    @Test(expected = NotEnoughCoins.class)
    public void engageAssistantWithNoEnoughCoins() throws Exception {
        engageAssistant(1);
    }

    private void engageAssistant(int coins) throws Exception {
        Player player = mPlayers.get(0);
        player.payCoins(player.getCoins());
        player.addCoins(coins);
        int assistants = player.getAssistants();
        UpdateState updateState = mServerGame.engageAssistant(player);
        assertTrue(assistants == player.getAssistants() - 1);
        assertTrue(player.getCoins() == coins - 3);
        assertTrue(updateState.getAddedAssistants() == 1);
        assertTrue(updateState.getRemovedCoins() == 3);
    }

    @Test
    public void changeBusinessPermitTiles() throws Exception {
        changeBusinessPermitTile(10);
    }

    @Test(expected = NotEnoughAssistants.class)
    public void changeBusinessPermitTilesWithNoEnoughAssistants() throws Exception {
        changeBusinessPermitTile(0);
    }

    private void changeBusinessPermitTile(int assistants) throws Exception {
        Player player = mPlayers.get(0);
        player.payAssistants(player.getAssistants());
        player.addAssistants(assistants);
        BusinessPermitTile firstPermitTile = mLeftRegion.getFirstVisibleTile();
        BusinessPermitTile secondPermitTile = mLeftRegion.getSecondVisibleTile();
        UpdateState updateState = mServerGame.changeBusinessPermitTiles(player, AbstractBoard.LEFT_REGION);
        assertTrue(player.getAssistants() == assistants - 1);
        assertTrue(updateState.getRemovedAssistants() == 1);
        assertFalse(mLeftRegion.getFirstVisibleTile().equals(firstPermitTile));
        assertFalse(mLeftRegion.getSecondVisibleTile().equals(secondPermitTile));
        assertTrue(updateState.getChangedBusinessPermitTiles().size() == 2);
        UpdateState.ChangedBusinessPermitTile changedPermitTile1 = updateState.getChangedBusinessPermitTiles().get(0);
        UpdateState.ChangedBusinessPermitTile changedPermitTile2 = updateState.getChangedBusinessPermitTiles().get(1);
        assertTrue(changedPermitTile1.getRegion().equals(AbstractBoard.LEFT_REGION));
        if (changedPermitTile1.getIndex() == 1) {
            assertTrue(changedPermitTile1.getBusinessPermitTile().equals(mLeftRegion.getFirstVisibleTile()));
        } else {
            assertTrue(changedPermitTile1.getBusinessPermitTile().equals(mLeftRegion.getSecondVisibleTile()));
        }
        assertTrue(changedPermitTile2.getRegion().equals(AbstractBoard.LEFT_REGION));
        if (changedPermitTile2.getIndex() == 1) {
            assertTrue(changedPermitTile2.getBusinessPermitTile().equals(mLeftRegion.getFirstVisibleTile()));
        } else {
            assertTrue(changedPermitTile2.getBusinessPermitTile().equals(mLeftRegion.getSecondVisibleTile()));
        }
    }

    @Test
    public void sendAssistantToElectCouncillor() throws Exception {
        sendAssistantToElectCouncillor(10);
    }

    @Test(expected = NotEnoughAssistants.class)
    public void sendAssistantToElectCouncillorWithNoEnoughAssistants() throws Exception {
        sendAssistantToElectCouncillor(0);
    }

    private void sendAssistantToElectCouncillor(int assistants) throws Exception {
        Player player = mPlayers.get(0);
        player.payAssistants(player.getAssistants());
        player.addAssistants(assistants);
        String[] balconyColors = mServerGame.getRightBalconyColors();
        UpdateState updateState = mServerGame.sendAssistantToElectCouncillor(player, new Councilor("awesomeColor"), AbstractBoard.RIGHT_REGION);
        assertTrue(player.getAssistants() == assistants - 1);
        assertTrue(updateState.getRemovedAssistants() == 1);
        assertArrayEquals(mRightRegion.getBalconyColors(), new String[] {balconyColors[1], balconyColors[2], balconyColors[3], "awesomeColor"});
    }

    @Test
    public void performAdditionalMainAction() throws Exception {
        performAdditionalMainAction(10);
    }

    @Test(expected = NotEnoughAssistants.class)
    public void performAdditionalMainActionWithNoEnoughAssistants() throws Exception {
        performAdditionalMainAction(0);
    }

    private void performAdditionalMainAction(int assistants) throws Exception {
        Player player = mPlayers.get(0);
        player.payAssistants(player.getAssistants());
        player.addAssistants(assistants);
        UpdateState updateState = mServerGame.performAdditionalMainAction(player);
        assertTrue(player.getAssistants() == assistants - 3);
        assertTrue(updateState.getRemovedAssistants() == 3);
    }

    @Test
    public void applyEndGameBonus() throws Exception {
        // prepare player 1
        Map<String, Reward> bonus1 = new HashMap<>();
        bonus1.put("bonus1", new Reward(0, 15, 0, 0, 0, 0));
        setupPlayer(0, 65, 10, bonus1, 1);
        // prepare player 2
        Map<String, Reward> bonus2 = new HashMap<>();
        bonus2.put("bonus1", new Reward(0, 20, 0, 0, 0 ,0));
        setupPlayer(1, 57, 17, bonus2, 0);
        // prepare player 3
        Map<String, Reward> bonus3 = new HashMap<>();
        bonus3.put("bonus1", new Reward(0, 5, 0, 0, 0 ,0));
        setupPlayer(2, 32, 15, bonus3, 6);
        // prepare player 4
        Map<String, Reward> bonus4 = new HashMap<>();
        bonus4.put("bonus1", new Reward(0, 5, 0, 0, 0 ,0));
        bonus4.put("bonus2", new Reward(0, 22, 0, 0, 0 ,0));
        setupPlayer(3, 48, 15, bonus4, 3);
        // execute test
        mServerGame.applyEndGameBonus();
        // make assertions
        assertTrue(mPlayers.get(0).getVictoryPoints() == 80);
        assertTrue(mPlayers.get(1).getVictoryPoints() == 82);
        assertTrue(mPlayers.get(2).getVictoryPoints() == 42);
        assertTrue(mPlayers.get(3).getVictoryPoints() == 77);
    }

    private void setupPlayer(int index, int victoryPoints,  int nobilityPosition, Map<String, Reward> bonus, int numberOfPermitTiles) {
        Player player = mPlayers.get(index);
        player.addVictoryPoints(victoryPoints);
        player.moveNobilityTrack(nobilityPosition);
        player.addBonus(bonus);
        for (int i = 0; i < numberOfPermitTiles; i++) {
            player.addBusinessPermitTile(new BusinessPermitTile(0, 0, 0, 0, 0, 'g'));
        }
    }

    @Test
    public void applyNobilityTrackPositionRewardsStandard() throws Exception {
        // initial positions:
        applyNobilityTrackPositionReward(10, 11, 12, 13);
        // assert expected results
        assertTrue(mPlayers.get(0).getVictoryPoints() == 0);
        assertTrue(mPlayers.get(1).getVictoryPoints() == 0);
        assertTrue(mPlayers.get(2).getVictoryPoints() == 2);
        assertTrue(mPlayers.get(3).getVictoryPoints() == 5);
    }

    @Test
    public void applyNobilityTrackPositionRewardsMorePlayerFirstPosition() throws Exception {
        // initial positions:
        applyNobilityTrackPositionReward(2, 6, 6, 4);
        // assert expected results
        assertTrue(mPlayers.get(0).getVictoryPoints() == 0);
        assertTrue(mPlayers.get(1).getVictoryPoints() == 5);
        assertTrue(mPlayers.get(2).getVictoryPoints() == 5);
        assertTrue(mPlayers.get(3).getVictoryPoints() == 0);
    }

    @Test
    public void applyNobilityTrackPositionRewardsMorePlayerSecondPosition() throws Exception {
        // initial positions:
        applyNobilityTrackPositionReward(1, 6, 3, 3);
        // assert expected results
        assertTrue(mPlayers.get(0).getVictoryPoints() == 0);
        assertTrue(mPlayers.get(1).getVictoryPoints() == 5);
        assertTrue(mPlayers.get(2).getVictoryPoints() == 2);
        assertTrue(mPlayers.get(3).getVictoryPoints() == 2);
    }

    @Test
    public void applyNobilityTrackPositionRewardsAllPlayerSamePosition() throws Exception {
        // initial positions:
        applyNobilityTrackPositionReward(5, 5, 5, 5);
        // assert expected results
        assertTrue(mPlayers.get(0).getVictoryPoints() == 5);
        assertTrue(mPlayers.get(1).getVictoryPoints() == 5);
        assertTrue(mPlayers.get(2).getVictoryPoints() == 5);
        assertTrue(mPlayers.get(3).getVictoryPoints() == 5);
    }

    private void applyNobilityTrackPositionReward(int steps1, int steps2, int steps3, int steps4) throws Exception {
        // generate 4 update states
        Map<String, UpdateState> updateStates = new HashMap<>();
        updateStates.put("Player1", new UpdateState("Player1"));
        updateStates.put("Player2", new UpdateState("Player2"));
        updateStates.put("Player3", new UpdateState("Player3"));
        updateStates.put("Player4", new UpdateState("Player4"));
        // update players nobility track position
        mPlayers.get(0).moveNobilityTrack(steps1);
        mPlayers.get(1).moveNobilityTrack(steps2);
        mPlayers.get(2).moveNobilityTrack(steps3);
        mPlayers.get(3).moveNobilityTrack(steps4);
        // this method is private, so use java reflection to access it
        Method method = ServerGame.class.getDeclaredMethod("applyNobilityTrackPositionRewards", Map.class);
        method.setAccessible(true);
        // build test 1: every player has a different nobility track position.
        method.invoke(mServerGame, updateStates);
    }

    @Test
    public void generateRanking() throws Exception {
        setupPlayer(0, 73, 2, 10);
        setupPlayer(1, 80, 3, 5);
        setupPlayer(2, 60, 1, 8);
        setupPlayer(3, 73, 4, 7);
        // make test
        List<String> ranking = mServerGame.generateRanking();
        // make assertions
        assertTrue(ranking.size() == 4);
        assertTrue(ranking.get(0).equals("Player2"));
        assertTrue(ranking.get(1).equals("Player1"));
        assertTrue(ranking.get(2).equals("Player4"));
        assertTrue(ranking.get(3).equals("Player3"));
    }

    private void setupPlayer(int index, int victoryPoints, int assistants, int politicCards) throws Exception {
        Player player = mPlayers.get(index);
        player.addVictoryPoints(victoryPoints);
        player.payAssistants(player.getAssistants());
        player.addAssistants(assistants);
        for (int i = 0; i < politicCards; i++) {
            player.addPoliticCard(new PoliticCard());
        }
    }

    @Test
    public void earnFirstSpecialRewards() throws Exception {
        City city1 = mServerGame.findCity("Hellar");
        city1.addEmporium("Player1");
        city1.setReward(new Reward(2, 0, 0, 1, 0, 0));
        City city2 = mServerGame.findCity("Indur");
        city2.addEmporium("Player1");
        city2.setReward(new Reward(0, 2, 0, 4, 0, 0));
        List<String> cities = new ArrayList<>();
        cities.add("Hellar");
        cities.add("Indur");
        earnFirstSpecialRewards(mPlayers.get(0), cities);
    }

    @Test(expected = CityNotValid.class)
    public void earnFirstSpecialRewardsWithNoEmporium() throws Exception {
        City city1 = mServerGame.findCity("Hellar");
        city1.addEmporium("Player0");
        city1.setReward(new Reward(2, 0, 0, 1, 0, 0));
        City city2 = mServerGame.findCity("Indur");
        city2.setReward(new Reward(0, 2, 0, 4, 0, 0));
        List<String> cities = new ArrayList<>();
        cities.add("Hellar");
        cities.add("Indur");
        earnFirstSpecialRewards(mPlayers.get(0), cities);
    }

    private void earnFirstSpecialRewards(Player player, List<String> cities) throws Exception {
        UpdateState updateState = mServerGame.earnFirstSpecialRewards(player, new GameTurn(null, null), cities);
        assertTrue(updateState.getAddedAssistants() == 2);
        assertTrue(updateState.getAddedVictoryPoints() == 2);
        assertTrue(updateState.getAddedCoins() == 5);
    }

    @Test
    public void earnSecondSpecialRewards() throws Exception {
        Player player = mPlayers.get(0);
        BusinessPermitTile secondLeftTile = mLeftRegion.getSecondVisibleTile();
        BusinessPermitTile firstRightTile = mRightRegion.getFirstVisibleTile();
        List<String> regions = new ArrayList<>();
        List<Integer> indices = new ArrayList<>();
        regions.add(AbstractBoard.LEFT_REGION);
        indices.add(2);
        regions.add(AbstractBoard.RIGHT_REGION);
        indices.add(1);
        UpdateState updateState = mServerGame.earnSecondSpecialRewards(player, new GameTurn(null, null), regions, indices);
        assertTrue(updateState.getAddedBusinessPermitTiles().size() == 2);
        assertTrue(updateState.getAddedBusinessPermitTiles().get(0).equals(secondLeftTile));
        assertTrue(updateState.getAddedBusinessPermitTiles().get(1).equals(firstRightTile));
    }

    @Test
    public void earnThirdSpecialRewards() throws Exception {
        Player player = mPlayers.get(0);
        List<BusinessPermitTile> businessPermitTiles = new ArrayList<>();
        businessPermitTiles.add(new BusinessPermitTile(0, 3, 0, 4, 0, 0, 'a'));
        businessPermitTiles.add(new BusinessPermitTile(0, 5, 0, 1, 0, 0, 'c'));
        player.addBusinessPermitTiles(businessPermitTiles);
        player.useBusinessPermitTile(businessPermitTiles.get(1));
        UpdateState updateState = mServerGame.earnThirdSpecialRewards(player, new GameTurn(null, null), businessPermitTiles);
        assertTrue(updateState.getAddedVictoryPoints() == 8);
        assertTrue(updateState.getAddedCoins() == 5);
    }

    public static BaseGame generateGameSession(FakeRemotePlayer... players) throws Exception {
        loadConfigurationFile();
        return Configurator.getGameInstance(Arrays.asList(players), Configurator.getConfigurationBundle());
    }
}
Project: Council Of Four

Team members:
- Andrea Alemani 808263
- Guido Borrelli 791160

We have implemented:
- RMI + Socket + GUI + CLI

Advanced features:
- Editable map
- Chat

The project can be compiled using maven plugin, the main class are respectively:
- Client side -> it.polimi.cof.Launcher (The choice between CLI and GUI can be done directly when program starts, like the connection type).
- Server side -> it.polimi.cof.network.server.Server

We also provide compiled .jar files directly from BitBucket download page: https://bitbucket.org/Andre2394/lm_alemani_cof/downloads
- Client side -> lm_43-0.0.1-SNAPSHOT-CLIENT.jar
- Server side -> lm_43-0.0.1-SNAPSHOT-SERVER.jar